#include <fstream>

#include <QtMath>
#include <QList>
#include <QFileDialog>
#include <QMessageBox>

#include "FloorplanDesigner.h"
#include "ui_FloorPlanDesigner.h"

#include "utilities/utilities.h"
#include "utilities/PolygonDetection.h"

#include "data/FloorDesignDocument.h"

#include "FloorDesignItems/WallSegmentItem.h"
#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "ControlPointGraphicsItem.h"

#include "Command/CommandManager.h"
#include "Command/FileBasedStateCommand.h"

//#include "Forms/CreateLoadProjectForm.h"
//#include "Forms/ScannedFloorDesignFileForm.h"
#include "Forms/ScannedFloorDesignSelectorForm.h"
#include "Forms/DrawnPlanAsBaseSelectorForm.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

FloorPlanDesigner::FloorPlanDesigner(Project * iProject, std::string inFileToOpen, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::FloorPlanDesigner)
{
	ui->setupUi(this);
	QDialog::showFullScreen();

	this->m_project = iProject;
	this->fileToOpen = inFileToOpen;

	bool isXercesInitialized = initializeXerces();

	scannedFloorDesignSelectorForm = nullptr;

	m_document = new FloorDesignDocument();
	scene = new FloorplanGraphicsScene(this);
	ui->graphicsView->setScene(scene);
	scene->setWallPropertiesWidget(ui->wallProperties);
	ui->wallProperties->setVisible(false);
	ui->sceneProperties->setScene(scene);

	this->commandManager = new CommandManager(this->m_document);
	CommandManager::setCurrentCommandManager(this->commandManager);

	//bool doesWallsHaveWidth = false;
	bool doesWallsHaveWidth = true;
	scene->setDoesWallsHaveWidth(doesWallsHaveWidth);
	ui->graphicsView->setDoesWallsHaveWidth(doesWallsHaveWidth);

	Initialize();
	InitializeSlots();
}

FloorPlanDesigner::FloorPlanDesigner(QWidget* parent) : FloorPlanDesigner(nullptr, "", parent)
{
}

void FloorPlanDesigner::Initialize()
{
	//ui->menubar->setVisible(false);

	//scene->resetScene();
	//QString fileName("C:\\Users\\selimnar\\Documents\\fd3.fdd");
	//QString fileName("saves\\fd3.fdd");
	//QString fileName("empty.fdd");
	QString fileName(this->fileToOpen.c_str());
	openFloorDesignDocument(fileName);

	ui->sceneProperties->updateUIWidgetsDueToWallSegmentItem();

	//ui->saveButton->setIcon(QIcon("images/wall.svg"));
	//const char * iconPath = "C:\\Users\\selimnar\\Documents\\QTDrawing\\images\\brick.png";

	//QIcon icon;
	//const char * iconPath = "images/wall.svg";
	//icon.addFile(QString(iconPath), QSize(), QIcon::Normal, QIcon::Off);
	////actionExport_Svg->setIcon(icon);
	//ui->saveButton->setIcon(icon);
}

void FloorPlanDesigner::InitializeSlots()
{
	connect(scene, SIGNAL(wallAdded(WallData *, bool)),
		this, SLOT(on_wallAdded(WallData *, bool)));

	connect(scene, SIGNAL(wallBended(WallData *)),
		this, SLOT(on_WallBended(WallData *)));

	connect(scene, SIGNAL(wallWidthModified(WallData *)),
		this, SLOT(on_WallWidthModified(WallData *)));

	connect(scene, SIGNAL(wallSplitted(WallData *, WallData *)),
		this, SLOT(on_WallSplitted(WallData *, WallData *)));

	connect(scene, SIGNAL(mergeControlPointToControlPoint(ControllerData *, ControllerData *)),
		this, SLOT(on_MergeControlPointToControlPoint(ControllerData *, ControllerData *)));

	connect(scene, SIGNAL(attachedToWall(ControllerData *, WallData *)),
		this, SLOT(on_AttachedToWall(ControllerData *, WallData *)));

	connect(scene, SIGNAL(wallSideControlDragEnded(ControllerData *)),
		this, SLOT(on_WallSideControlDragEnded(ControllerData *)));

	connect(scene, SIGNAL(wallSegmentItemDragEnded(WallData *)),
		this, SLOT(on_WallSegmentItemDragEnded(WallData *)));

	connect(ui->wallProperties, SIGNAL(wallSegmentItemPropertiesModifiedViaUIPanel(WallData *)),
		this, SLOT(on_wallSegmentItemPropertiesModifiedViaUIPanel(WallData *)));

	connect(ui->sceneProperties, SIGNAL(scenePropertiesModifiedViaUIPanel()),
		this, SLOT(on_scenePropertiesModifiedViaUIPanel()));

	connect(scene, SIGNAL(wallsRemoved(std::vector<WallData*> *)),
		this, SLOT(on_wallsRemoved(std::vector<WallData*> *)));

	connect(scene, SIGNAL(designedCentered()), this, SLOT(on_designedCentered()));
	
	//connect(ui->rotationSlider, SIGNAL(valueChanged(int)),
	//	this, SLOT(on_rotationSlider_valueChanged()));

	//connect(ui->saveButton, SIGNAL(pressed()),
	//	this, SLOT(on_saveButton_pressed()));

	//connect(ui->createWallButton, SIGNAL(pressed()),
	//	this, SLOT(on_createWallButton_pressed()));

	//connect(ui->createRoomButton, SIGNAL(pressed()),
	//	this, SLOT(on_createRoomButton_pressed()));
}

FloorPlanDesigner::~FloorPlanDesigner()
{
	delete m_document;
	delete ui;
}

void FloorPlanDesigner::keyPressEvent(QKeyEvent * event)
{
	if (event->key() == Qt::Key_Escape)
	{
		this->scene->setInteractionMode(InteractionMode::View);
		event->ignore();
		return;
	}
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Z)
	{
		undo();
	}
	if (event->modifiers() == Qt::CTRL && event->key() == Qt::Key_Y)
	{
		redo();
	}
	QDialog::keyPressEvent(event);
}

void FloorPlanDesigner::closeEvent(QCloseEvent *event)
{
	//event->ignore();
	QDialog::closeEvent(event);
}

void FloorPlanDesigner::on_rotationSlider_valueChanged(int val)
{
	//ui->graphicsView->rotate(ui->rotationSlider->value());
	//ui->graphicsView->rotate(qDegreesToRadians((qreal)val));
	//qreal sign = (val == 0) ? 0 : qreal(val / qAbs(val));
	//ui->graphicsView->rotate(sign);
	//ui->graphicsView->scale((qreal)val, (qreal)val);
	QTransform scaleMat;
	scaleMat.reset();
	scaleMat.scale((qreal)val, (qreal)val);
	ui->graphicsView->setTransform(scaleMat);
}

//void FloorPlanDesigner::on_saveButton_pressed()
//{
//	//DEBUG_OUT("fileName = %s", fileName.toStdString().c_str());
//	std::ofstream ofs(fileToOpen.c_str(), std::ios::binary | std::ios::out);
//	if (ofs.is_open())
//	{
//		//DEBUG_OUT("!!!! saved fileName = %s", fileName.toStdString().c_str());
//		//QMessageBox::information(this, tr("Selected File"), fileName);
//		this->m_document->serialize(ofs);
//		ofs.close();
//	}
//
//	DEBUG_OUT("FloorPlanDesigner::on_saveButton_pressed");
//}

void FloorPlanDesigner::on_saveButton_pressed( )
{
	//QList<QUrl> urls;
	//urls << QUrl::fromLocalFile("/Users/foo/Code/qt5")
	//	<< QUrl::fromLocalFile(QStandardPaths::standardLocations(QStandardPaths::MusicLocation).first());

	QString fileName = QFileDialog::getSaveFileName(this,
		tr("Save Floor Design"), "",
		tr("Floor Design Document (*.fdd);;All Files (*)"));

	if (!fileName.isEmpty())
	{
		//DEBUG_OUT("fileName = %s", fileName.toStdString().c_str());
		std::ofstream ofs(fileName.toStdString().c_str(), std::ios::binary | std::ios::out);
		if (ofs.is_open())
		{
			//DEBUG_OUT("!!!! saved fileName = %s", fileName.toStdString().c_str());
			//QMessageBox::information(this, tr("Selected File"), fileName);
			this->m_document->serialize(ofs);
			ofs.close();
		}
	}

	DEBUG_OUT("FloorPlanDesigner::on_saveButton_pressed");
}

void FloorPlanDesigner::on_createWallButton_pressed()
{
	DEBUG_OUT("FloorPlanDesigner::on_createWallButton_pressed");
	this->scene->setInteractionMode(InteractionMode::WallSegmentAdd);
}

void FloorPlanDesigner::on_addDoorPushButton_pressed()
{
	ui->q3DWidget->clearScene();
	ui->q3DWidget->addDocument(m_document);
	
	DEBUG_OUT("FloorPlanDesigner::on_addDoorPushButton_pressed");
}

void FloorPlanDesigner::on_loadButton_pressed()
{
	//DrawnPlanAsBaseSelectorForm drawnPlanAsBaseSelectorForm(m_project, this);
	//drawnPlanAsBaseSelectorForm.exec();
	//bool result = drawnPlanAsBaseSelectorForm.result();
	//if (result)
	//{
	//	QString drawnFilePath = drawnPlanAsBaseSelectorForm.drawnFilePath();
	//	openFloorDesignDocument(drawnFilePath);
	//}


	QString fileName = QFileDialog::getOpenFileName(this,
		tr("Open Floor Design"), "",
		tr("Floor Design Document (*.fdd);;All Files (*)"));

	openFloorDesignDocument(fileName);



	//PolygonDetector polygonDetector;
	//polygonDetector.detectAndMergePolygons(this->m_document, this->scene);

	DEBUG_OUT("FloorPlanDesigner::on_createRoomButton_pressed");
}

void FloorPlanDesigner::on_undoButton_pressed()
{
	undo();
	DEBUG_OUT("FloorPlanDesigner::on_undoButton_pressed");
}

void FloorPlanDesigner::on_redoButton_pressed()
{
	redo();
	DEBUG_OUT("FloorPlanDesigner::on_redoButton_pressed");
}

void FloorPlanDesigner::on_selectFromScannedDesignButton_pressed()
{
	scannedFloorDesignSelectorForm = new ScannedFloorDesignSelectorForm(m_project);
	scannedFloorDesignSelectorForm->exec();
	bool result = scannedFloorDesignSelectorForm->result();
	if (result)
	{
		scene->loadImage(scannedFloorDesignSelectorForm->pixmap());
	}
	delete scannedFloorDesignSelectorForm;
	scannedFloorDesignSelectorForm = nullptr;

	//DEBUG_OUT("FloorPlanDesigner::on_selectFromScannedDesignButton_pressed: result = %d", result);
}

void FloorPlanDesigner::on_exitPushButton_pressed()
{
	this->close();
	//qApp->exit();
}

void FloorPlanDesigner::on_lightAngleHorizontalSlider_valueChanged(int value)
{
	ui->q3DWidget->setLightAngle((qreal)value);
}

void FloorPlanDesigner::openFloorDesignDocument(QString fileName)
{
	FloorDesignDocument * newDocument = nullptr;
	if (!fileName.isEmpty())
	{
		DEBUG_OUT("fileName = %s", fileName.toStdString().c_str());
		std::ifstream ifs(fileName.toStdString().c_str(), std::ios::binary | std::ios::in);
		if (ifs.is_open())
		{
			DEBUG_OUT("!!!! opened fileName = %s", fileName.toStdString().c_str());
			newDocument = new FloorDesignDocument();
			newDocument->deserialize(ifs);
			ifs.close();
		}
	}

	if (newDocument != nullptr)
	{
		int wallCount = newDocument->wallCount();
		for (int i = 0; i < wallCount; i++)
		{
			WallData * wallData = newDocument->wallAt(i);
			connectWallRemoveSocket(wallData);
		}

		scene->resetScene(true);
		scene->createSceneGraphicsItemsFromDocumentData(newDocument);
		m_document = newDocument;

		if (this->commandManager != nullptr)
		{
			delete this->commandManager;
		}
		this->commandManager = new CommandManager(this->m_document);
		CommandManager::setCurrentCommandManager(this->commandManager);

		// save current command as open document
		FileBasedStateCommand * fileBasedStateCommand = new FileBasedStateCommand(commandManager, commandManager->undoCommandCount());
		this->commandManager->ExecuteCommand((Command *)fileBasedStateCommand);
	}
}

void FloorPlanDesigner::on_wallAdded(WallData * wallData, bool isToAddedToCommandHistory)
{
	if (isToAddedToCommandHistory)
	{
		// UNDO / REDO / COMMAND MECHANISM
		FileBasedStateCommand::staticExecute();
	}

	DEBUG_OUT("FloorPlanDesigner::on_wallcreated : wallData.uuid = %s", wallData->uuid().c_str());
}

void FloorPlanDesigner::on_wallRemoved(WallData * wallData)
{
	DEBUG_OUT("FloorPlanDesigner::on_wallRemoved : wallData.uuid = %s", wallData->uuid().c_str());

	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();
}

void FloorPlanDesigner::on_wallsRemoved(std::vector<WallData*> * wallDataList)
{
	DEBUG_OUT("FloorPlanDesigner::on_wallRemoved : MULTI DELETE !!!!");

	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();
}

void FloorPlanDesigner::on_designedCentered()
{
	DEBUG_OUT("FloorPlanDesigner::on_designedCentered !!!!");

	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();
}

void FloorPlanDesigner::on_WallBended(WallData * wallData)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_WallBended : wallData.uuid = %s", wallData->uuid().c_str());
}

void FloorPlanDesigner::on_WallWidthModified(WallData * wallData)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_WallWidthModified : wallData.uuid = %s", wallData->uuid().c_str());
}

void FloorPlanDesigner::on_WallSplitted(WallData * firstWD, WallData * secondWD)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_WallSplitted : firstWD.uuid = %s, secondWD.uuid = %s",
		firstWD->uuid().c_str(), secondWD->uuid().c_str());
}

void FloorPlanDesigner::on_MergeControlPointToControlPoint(ControllerData * food, ControllerData * swallower)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_MergeControlPointToControlPoint : food.uuid = %s, swallower.uuid = %s",
		food->uuid().c_str(), swallower->uuid().c_str());
}

void FloorPlanDesigner::on_AttachedToWall(ControllerData * attachedController, WallData * wallPriorToAttachment)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_AttachedToWall : attachedController.uuid = %s, wallPriorToAttachment.uuid = %s",
		attachedController->uuid().c_str(), wallPriorToAttachment->uuid().c_str());
}

void FloorPlanDesigner::on_WallSideControlDragEnded(ControllerData * draggedControllerData)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	if (draggedControllerData != nullptr)
	{
		DEBUG_OUT("FloorPlanDesigner::on_WallSideControlDragEnded : draggedControllerData.uuid = %s", draggedControllerData->uuid().c_str());
	}
}

void FloorPlanDesigner::on_WallSegmentItemDragEnded(WallData * draggedWallData)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_WallSegmentItemDragEnded : draggedWallData.uuid = %s", draggedWallData->uuid().c_str());
}

void FloorPlanDesigner::on_wallSegmentItemPropertiesModifiedViaUIPanel(WallData * uiModifiedWallData)
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_wallSegmentItemPropertiesModifiedViaUIPanel : uiModifiedWallData.uuid = %s", uiModifiedWallData->uuid().c_str());
}

void FloorPlanDesigner::on_scenePropertiesModifiedViaUIPanel()
{
	// UNDO / REDO / COMMAND MECHANISM
	FileBasedStateCommand::staticExecute();

	DEBUG_OUT("FloorPlanDesigner::on_wallSegmentItemPropertiesModifiedViaUIPanel");
}

void FloorPlanDesigner::connectWallRemoveSocket(WallData * wallData)
{
	connect(wallData, SIGNAL(wallRemoved(WallData *)),
		this, SLOT(on_wallRemoved(WallData *)));
}

void FloorPlanDesigner::disconnectWallRemoveSocket(WallData * wallData)
{
	disconnect(wallData, SIGNAL(wallRemoved(WallData *)),
		this, SLOT(on_wallRemoved(WallData *)));
}

FloorplanGraphicsScene * FloorPlanDesigner::floorplanGraphicsScene()
{
	return scene;
}

FloorDesignDocument * FloorPlanDesigner::document()
{
	return m_document;
}

FloorplanGraphicsView * FloorPlanDesigner::view()
{
	return ui->graphicsView;
}

void FloorPlanDesigner::undo()
{
	ui->wallProperties->setSelectedWallSegmentItem(nullptr, false);
	scene->deselectAllWalls();
	CommandManager::currentCommandManager()->undo();
}

void FloorPlanDesigner::redo()
{
	ui->wallProperties->setSelectedWallSegmentItem(nullptr, false);
	scene->deselectAllWalls();
	CommandManager::currentCommandManager()->redo();
}
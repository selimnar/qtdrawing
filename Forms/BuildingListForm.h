#ifndef BUILDING_LIST_FORM_H
#define BUILDING_LIST_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class Project;
class BuildingInstance;

namespace Ui {
	class BuildingListForm;
}

class BuildingListForm : public QDialog
{
	Q_OBJECT
public:
	explicit BuildingListForm(Project * iProject, QWidget *parent = nullptr);
	~BuildingListForm();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::BuildingListForm *ui;

	QStringList tableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	BuildingInstance * m_buildingInstance;

	QPixmap m_pixmap;

	QFileInfo qfileInfo;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * buildingInstancesTableViewModel;

	void PopulateBuildingInstanceListView();
	void InitializeForNewBuildingInstance();

private slots:
	void on_editBuildingPushButton_pressed();
	void on_addBuildingPushButton_pressed();
	void on_removeBuildingPushButton_pressed();
	void on_showIn3DViewButton_pressed();

	void on_definitionLineEdit_textChanged();

	void on_definition_Changed(const char * value);

};


#endif // BUILDING_LIST_FORM_H
#include <fstream>

#include <string>

#include <QtMath>
#include <QList>
#include <QFileDialog>
#include <QMessageBox>

#include "ScannedFloorDesignSelectorForm.h"
#include "ui_ScannedFloorDesignSelectorForm.h"

#include "CustomWidgets/ScannedFloorplanView.h"
#include "CustomQObjects/ScannedFloorplanGraphicsScene.h"

#include "utilities/QImageDownloader.h"

#include "data\Project.h"

#include "utilities\utilities.h"
#include "Utilities\FileUtilities.h"
#include "Utilities\ProjectUtilities.h"

using namespace std;

ScannedFloorDesignSelectorForm::ScannedFloorDesignSelectorForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ScannedFloorDesignSelectorForm)
{
	ui->setupUi(this);

	m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	Initialize();
}

ScannedFloorDesignSelectorForm::~ScannedFloorDesignSelectorForm()
{
	delete ui;
}

void ScannedFloorDesignSelectorForm::Initialize()
{
	scene = new ScannedFloorplanGraphicsScene(this);
	ui->graphicsView->setScene(scene);

	tableHeaderLabels << "Id" << "Tanim" << "Dosya Adi";
	PopulateArchitecturalPlanListViewBase(m_project, ui->planTableView, plansTableViewModel, tableHeaderLabels, this);
}

void ScannedFloorDesignSelectorForm::on_selectScannedPlanPushButton_pressed()
{
	bool isListVisible = ui->planTableView->isVisible();
	if (!isListVisible)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QModelIndex selectedPlanItemIndex = ui->planTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
	bool ok;
	unsigned int id = selectedRowIdField->text().toUInt(&ok);

	std::map<unsigned int, ArchitecturalPlan*> scannedImageFiles = m_project->scannedImageFiles();
	QString fileName = scannedImageFiles[id]->fileName();



	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Taranmis mimari proje resim dosyasini yuklemek istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QImage qImage;
		string projectFolder = m_project->GetProjectFolderPath(projectsWorkspaceFolder);
		std::string scannedImageFile = projectFolder + "\\scannedPlans\\" + fileName.toStdString();

		QFile file(scannedImageFile.c_str());
		if (file.exists())
		{
			scene->resetScene();
			bool success = qImage.load(scannedImageFile.c_str());
			if (success)
			{
				scene->loadImage(&qImage);
			}
		}
	}
}

void ScannedFloorDesignSelectorForm::on_getSelectionPushButton_pressed()
{
	bool success = scene->retrieveSelection(m_pixmap);
	done(success);
	DEBUG_OUT("ArchitecturalDesigner::on_getSelectionPushButton_pressed");
}

void ScannedFloorDesignSelectorForm::on_showHideListPushButton_pressed()
{
	bool isListVisible = ui->planTableView->isVisible();
	ui->planTableView->setVisible(!isListVisible);
}

QPixmap ScannedFloorDesignSelectorForm::pixmap()
{
	return m_pixmap;
}
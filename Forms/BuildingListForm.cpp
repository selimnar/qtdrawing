#include "BuildingListForm.h"
#include "ui_BuildingListForm.h"

#include <fstream>

#include <QtMath>
#include <QList>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include "FloorPlanDesigner.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

#include "utilities\utilities.h"
#include "utilities\ProjectUtilities.h"
#include "Utilities\FileUtilities.h"

#include "BuildingInstaceFloorAssignerForm.h"
#include "Full3DBuildingMeshVisualizerForm.h"

using namespace std;


BuildingListForm::BuildingListForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::BuildingListForm)
{
	ui->setupUi(this);

	this->m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	tableHeaderLabels << "Id" << "Tanim";
	PopulateBuildingInstanceListView();

	this->m_buildingInstance = nullptr;
	InitializeForNewBuildingInstance();
}

BuildingListForm::~BuildingListForm()
{
	if (this->m_buildingInstance != nullptr)
	{
		delete this->m_buildingInstance;
	}
	delete ui;
}

void BuildingListForm::PopulateBuildingInstanceListView()
{
	PopulateBuildingInstanceListViewBase(m_project, ui->buildingsTableView, buildingInstancesTableViewModel, tableHeaderLabels, this);
}

void BuildingListForm::InitializeForNewBuildingInstance()
{
	//if (this->m_drawnFloorPlan != nullptr)
	//{
	//	delete this->m_drawnFloorPlan;
	//}
	this->m_buildingInstance = new BuildingInstance();

	//ui->parselLineEdit->setValidator(new QIntValidator(0, INT_MAX, ui->tasinmazIdLineEdit));

	unsigned int maxId = m_project->retrieveMaxBuildingInstanceId() + 1;
	this->m_buildingInstance->setId(maxId);
	ui->idLabel->setText(QString::number(maxId));
	m_buildingInstance->setDefinition("");
	ui->definitionLineEdit->setText("");

	connect(m_buildingInstance, SIGNAL(definitionChanged(const char * value)), this, SLOT(on_definition_Changed(const char * value)));
	//connect(ui->drawnPlansTableView, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));
}

void BuildingListForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void BuildingListForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void BuildingListForm::on_editBuildingPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->buildingsTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Bina ornegini seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden bina ornegini acmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = buildingInstancesTableViewModel->item(selectedPlanItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, BuildingInstance*> buildingInstances = m_project->buildingInstances();
		BuildingInstance * selectedBuildingInstance = buildingInstances[id];

		BuildingInstaceFloorAssignerForm buildingInstaceFloorAssignerForm(m_project, selectedBuildingInstance, this);
		buildingInstaceFloorAssignerForm.exec();

		//DEBUG_OUT("!!!! ---- **** ArchitecturalDesigner::on_actionFloorPlans_triggered: result = %d", selectedBuildingInstance->id());
	}
}

void BuildingListForm::on_addBuildingPushButton_pressed()
{
	QString definition = QString(this->m_buildingInstance->definition());
	if (definition.isEmpty())
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim bos olamaz");
		Msgbox.exec();
		return;
	}

	bool isDefinitionUnique = true;
	std::map<unsigned int, BuildingInstance*> buildingInstances = m_project->buildingInstances();
	for (auto kvp : buildingInstances)
	{
		BuildingInstance * buildingInstance = kvp.second;
		if (definition == QString(buildingInstance->definition()))
		{
			isDefinitionUnique = false;
			break;
		}
	}

	if (!isDefinitionUnique)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim benzersiz olmali");
		Msgbox.exec();
		return;
	}

	m_project->addBuildingInstance(this->m_buildingInstance);
	InitializeForNewBuildingInstance();
	m_project->saveProject(projectsWorkspaceFolder);

	QTimer::singleShot(20, [=] {
		PopulateBuildingInstanceListView();
	});
	//DEBUG_OUT("ArchitecturalDesigner::on_addPlanPushButton_pressed %s", this->m_architecturalPlan->definition());
}

void BuildingListForm::on_removeBuildingPushButton_pressed()
{
	QModelIndex selectedItemIndex = ui->buildingsTableView->currentIndex();
	if (selectedItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Bina ornegini seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden bina ornegini kaldirmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = buildingInstancesTableViewModel->item(selectedItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		//std::map<unsigned int, BuildingInstance*> buildingInstances = m_project->buildingInstances();
		//BuildingInstance * buildingInstance = buildingInstances[id];
		m_project->removeBuildingInstance(id);
		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			InitializeForNewBuildingInstance();
			PopulateBuildingInstanceListView();
		});
	}
}

void BuildingListForm::on_definitionLineEdit_textChanged()
{
	bool success;
	QString definitionQStr = ui->definitionLineEdit->text();
	if (success)
	{
		const bool wasBlocked = m_buildingInstance->blockSignals(true);
		// no signals here
		std::string str = definitionQStr.toStdString();
		const char* cstr = str.c_str();
		m_buildingInstance->setDefinition(cstr);
		m_buildingInstance->blockSignals(wasBlocked);
	}
}

void BuildingListForm::on_showIn3DViewButton_pressed()
{
	QModelIndex selectedItemIndex = ui->buildingsTableView->currentIndex();
	if (selectedItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Bina ornegini seciniz");
		Msgbox.exec();
		return;
	}

	QStandardItem * selectedRowIdField = buildingInstancesTableViewModel->item(selectedItemIndex.row());
	bool ok;
	unsigned int id = selectedRowIdField->text().toUInt(&ok);

	std::map<unsigned int, BuildingInstance*> buildingInstances = m_project->buildingInstances();
	BuildingInstance * buildingInstance = buildingInstances[id];

	Full3DBuildingMeshVisualizerForm full3DBuildingMeshVisualizerForm(m_project, buildingInstance, this);
	full3DBuildingMeshVisualizerForm.exec();
}

void BuildingListForm::on_definition_Changed(const char * value)
{
	ui->definitionLineEdit->setText(QString(value));
}
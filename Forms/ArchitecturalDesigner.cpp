#include <fstream>

#include <QtMath>
#include <QList>
#include <QFileDialog>
#include <QMessageBox>

#include "ArchitecturalDesigner.h"
#include "ui_ArchitecturalDesigner.h"

#include "utilities/utilities.h"
#include "data/FloorDesignDocument.h"

#include "FloorDesignItems/WallSegmentItem.h"
#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "ControlPointGraphicsItem.h"

#include "Command/CommandManager.h"
#include "Command/FileBasedStateCommand.h"

#include "Forms/CreateLoadProjectForm.h"
#include "Forms/ScannedFloorDesignFileForm.h"
#include "Forms/DrawnPlanListForm.h"
#include "Forms/BuildingListForm.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

#include "../FloorplanDesigner.h"

#include "Utilities\FileUtilities.h"

using namespace std;

ArchitecturalDesigner::ArchitecturalDesigner(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::ArchitecturalDesigner)
{
	ui->setupUi(this);
	//QMainWindow::showFullScreen();

	this->m_project = nullptr;

	bool isXercesInitialized = initializeXerces();

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	//this->m_project = new Project();
	//this->m_project->deserialize("F:\\TAPU_MIMARI_PROJELER\\21341\\project.xml");
	//this->m_project->deserialize("F:\\TAPU_MIMARI_PROJELER\\trial\\project.xml");
	//this->m_project->serialize("F:\\TAPU_MIMARI_PROJELER\\trial\\project2.xml");
}

ArchitecturalDesigner::~ArchitecturalDesigner()
{
	if (m_project != nullptr)
	{
		delete m_project;
	}
	delete ui;
}

void ArchitecturalDesigner::openFloorDesignerWindow()
{
	DrawnPlanListForm drawnPlanListForm(this->m_project, this);
	drawnPlanListForm.exec();
}

void ArchitecturalDesigner::keyPressEvent(QKeyEvent * event)
{
	QMainWindow::keyPressEvent(event);
}

void ArchitecturalDesigner::closeEvent(QCloseEvent *event)
{
	finalizeXerces();
	QMainWindow::closeEvent(event);
}

void ArchitecturalDesigner::showEvent(QShowEvent *event)
{
	//QTimer::singleShot(20, [=] {
	//	on_actionCreateLoadProject_triggered();
	//});

	//QTimer::singleShot(20, [=] {
	//	on_actionArchitecturalPlans_triggered();
	//});

	//QTimer::singleShot(20, [=] {
	//	on_actionFloorPlans_triggered();
	//});

	//QTimer::singleShot(20, [=] {
	//	on_actionEditBuildings_triggered();
	//});

	//QTimer::singleShot(20, [=] {
	//	//std::string drawnFilePath = "F:\\TAPU_MIMARI_PROJELER\\21341\\drawnPlans\\2.fdd";
	//	std::string drawnFilePath = "F:\\TAPU_MIMARI_PROJELER\\21341\\drawnPlans\\4.fdd";
	//	FloorPlanDesigner floorPlanDesigner(m_project, drawnFilePath, this);
	//	floorPlanDesigner.exec();
	//	bool result = floorPlanDesigner.result();
	//	if (result)
	//	{
	//		//scene->loadImage(createLoadProjectForm->pixmap());
	//	}
	//});

	//openFloorDesignerWindow();
}

void ArchitecturalDesigner::on_actionCreateLoadProject_triggered()
{
	CreateLoadProjectForm createLoadProjectForm(this->m_project);
	createLoadProjectForm.exec();
	bool result = createLoadProjectForm.result();
	if (result)
	{
		m_project = createLoadProjectForm.project();
		QString projectString = m_project->projectString();
		this->statusBar()->showMessage(projectString);
		//scene->loadImage(createLoadProjectForm->pixmap());
	}

	DEBUG_OUT("ArchitecturalDesigner::on_actionCreateLoadProject_triggered: result = %d", result);
}

void ArchitecturalDesigner::on_actionFloorPlans_triggered()
{
	if (this->m_project == nullptr)
	{
		showMessage("Aktif Proje Yok, \"Dosya->Proje Yukle / Olustur\" i kullaniniz");
		return;
	}
	openFloorDesignerWindow();
}

void ArchitecturalDesigner::on_actionApartmentPlans_triggered()
{

}

void ArchitecturalDesigner::on_actionArchitecturalPlans_triggered()
{
	if (this->m_project == nullptr)
	{
		showMessage("Aktif Proje Yok, \"Dosya->Proje Yukle / Olustur\" i kullaniniz");
		return;
	}
	ScannedFloorDesignFileForm scannedFloorDesignFileForm(this->m_project, this);
	scannedFloorDesignFileForm.exec();
}

void ArchitecturalDesigner::on_actionSave_triggered()
{
	if (this->m_project == nullptr)
	{
		showMessage("Aktif Proje Yok, \"Dosya->Proje Yukle / Olustur\" i kullaniniz");
		return;
	}
	this->m_project->saveProject(projectsWorkspaceFolder);
}

void ArchitecturalDesigner::on_actionEditBuildings_triggered()
{
	if (this->m_project == nullptr)
	{
		showMessage("Aktif Proje Yok, \"Dosya->Proje Yukle / Olustur\" i kullaniniz");
		return;
	}
	BuildingListForm buildingListForm(this->m_project, this);
	buildingListForm.exec();
	DEBUG_OUT("ArchitecturalDesigner::on_actionEditBuildings_triggered");
}

void ArchitecturalDesigner::showMessage(QString message)
{
	QMessageBox Msgbox;
	Msgbox.setText(message);
	Msgbox.exec();
}
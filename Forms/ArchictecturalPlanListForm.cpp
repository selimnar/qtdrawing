#include <fstream>

#include <QtMath>
#include <QList>
#include <QFileDialog>
#include <QMessageBox>

#include "ArchictecturalPlanListForm.h"
#include "ui_ArchictecturalPlanListForm.h"

#include "data\Project.h"

#include "utilities\utilities.h"
#include "utilities\ProjectUtilities.h"
#include "Utilities\FileUtilities.h"

using namespace std;

ArchictecturalPlanListForm::ArchictecturalPlanListForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ArchictecturalPlanListForm)
{
	ui->setupUi(this);

	m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	tableHeaderLabels << "Id" << "Tanim" << "Dosya Adi";

	PopulatePlanListView();
}

ArchictecturalPlanListForm::~ArchictecturalPlanListForm()
{
	delete ui;
}

void ArchictecturalPlanListForm::PopulatePlanListView()
{
	PopulateArchitecturalPlanListViewBase(m_project, ui->planTableView, plansTableViewModel, tableHeaderLabels, this);
}

void ArchictecturalPlanListForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void ArchictecturalPlanListForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void ArchictecturalPlanListForm::on_deletePlanPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->planTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
	bool ok;
	unsigned int id = selectedRowIdField->text().toUInt(&ok);

	std::map<unsigned int, ArchitecturalPlan*> scannedImageFiles = m_project->scannedImageFiles();
	QString fileName = scannedImageFiles[id]->fileName();
	//scannedImageFiles[]

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden taranmis mimari plan resim dosyasini kaldirmak istiyor musunuz?"));
	//msgBox.setText(selectedProjectItemText);
	//msgBox.setInformativeText(selectedProjecItemText);
	//msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		std::string folderPath = m_project->GetProjectFolderPath(projectsWorkspaceFolder);
		std::string projectFile = folderPath + "\\scannedPlans\\" + fileName.toStdString();
		QFile fileToDelete(projectFile.c_str());
		fileToDelete.remove();
		m_project->removeArchitecturalPlan(id);
		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			PopulatePlanListView();
		});

		//openProjectFile(projectFile.c_str());
		DEBUG_OUT("++++++++++++++++++ %s", projectFile.c_str());
	}
	else
	{
		DEBUG_OUT("------------------");
	}
	DEBUG_OUT("CreateLoadProjectForm::on_openPojectPushButton_pressed");
}
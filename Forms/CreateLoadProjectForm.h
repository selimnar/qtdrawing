#ifndef SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H
#define SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H

#include <QtCore>
#include <QtGui>
#include <QDialog>

class Project;
class QStringListModel;
class QListViewItem;

namespace Ui {
	class CreateLoadProjectForm;
}

class CreateLoadProjectForm : public QDialog
{
	Q_OBJECT
public:
	explicit CreateLoadProjectForm(Project * iProject, QWidget *parent = nullptr);
	~CreateLoadProjectForm();

	Project * project();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void showEvent(QShowEvent * event) override;

private:
	Ui::CreateLoadProjectForm *ui;

	QStringList tableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	Project * m_initialProject;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * projectsTableViewModel;

	std::vector<Project*> projectsToLoadList;
	std::vector<Project*> filteredToLoadList;

	std::map<unsigned int, unsigned int> comboIndexCityMap;
	std::map<unsigned int, unsigned int> comboIndexCityInverseMap;
	std::map<unsigned int, unsigned int> comboIndexTownMap;
	std::map<unsigned int, unsigned int> comboIndexTownInverseMap;
	std::map<unsigned int, unsigned int> comboIndexNeighborhoodMap;
	std::map<unsigned int, unsigned int> comboIndexNeighborhoodInverseMap;

	unsigned int selectedCityIndex;
	unsigned int selectedTownIndex;
	unsigned int selectedNeighborhoodIndex;

	bool initialized = false;

	void Initialize();
	void InitializeCities();
	void PopulateProjectsData();
	void PopulateProjectsListView(std::vector<Project*> *projectListToPopulateView);
	bool checkProjectInformationValidity();

	void cityComboBoxCurrentIndexChanged(int index);
	void townComboBoxCurrentIndexChanged(int index);

	void openProjectFile(const char * fileName);
	void saveProjectFile(const char * folderPath, const char * fileName);

	void disableProjectUIWidgetSynch();

	void FilterProjects();

private slots:
	void on_filterProjectsPushButton_pressed();
	void on_clearProjectFiltersPushButton_pressed();
	
	void on_openPojectPushButton_pressed();
	void on_saveProjectPushButton_pressed();
	void on_clonePushButton_pressed();
	void on_confirmPushButton_pressed();
	void on_rejectPushButton_pressed();
	

	void on_cityComboBox_currentIndexChanged(int index);
	void on_townComboBox_currentIndexChanged(int index);
	void on_neighborhoodComboBox_currentIndexChanged(int index);
	void on_tasinmazIdLineEdit_textChanged();
	void on_adaLineEdit_textChanged();
	void on_parselLineEdit_textChanged();

	void on_project_tasinmazId_Changed(int value);
	void on_project_cityId_Changed(unsigned int);
	void on_project_townId_Changed(unsigned int);
	void on_project_neighborhoodId_Changed(unsigned int);
	void on_project_ada_Changed(unsigned int);
	void on_project_parsel_Changed(unsigned int);
};

#endif // SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H
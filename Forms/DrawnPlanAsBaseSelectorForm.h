#ifndef DRAWN_AS_BASE_SELECTOR_FORM_H
#define DRAWN_AS_BASE_SELECTOR_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class Project;
class DrawnFloorPlan;

namespace Ui {
	class DrawnPlanAsBaseSelectorForm;
}

class DrawnPlanAsBaseSelectorForm : public QDialog
{
	Q_OBJECT
public:
	explicit DrawnPlanAsBaseSelectorForm(Project * iProject, QWidget *parent = nullptr);
	~DrawnPlanAsBaseSelectorForm();

	QString drawnFilePath();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::DrawnPlanAsBaseSelectorForm *ui;

	QStringList tableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;

	QString m_drawnFilePath;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * plansTableViewModel;

	void PopulatePlanListView();

private slots:
	void on_openPlanPushButton_pressed();

};


#endif // DRAWN_AS_BASE_SELECTOR_FORM_H
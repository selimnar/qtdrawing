#ifndef SCANNED_FLOOR_DESIGN_FILE_FORM_H
#define SCANNED_FLOOR_DESIGN_FILE_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class ScannedFloorplanView;
class ScannedFloorplanGraphicsScene;
class Project;
class ArchitecturalPlan;
class ArchictecturalPlanListForm;

namespace Ui {
	class ScannedFloorDesignFileForm;
}

class ScannedFloorDesignFileForm : public QDialog
{
	Q_OBJECT
public:
	explicit ScannedFloorDesignFileForm(Project * iProject, QWidget *parent = nullptr);
	~ScannedFloorDesignFileForm();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	static const std::string ScannedPlanFolder;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	ArchitecturalPlan * m_architecturalPlan;

	Ui::ScannedFloorDesignFileForm *ui;

	ScannedFloorplanGraphicsScene *scene;

	QPixmap m_pixmap;

	QFileInfo qfileInfo;

	void Initialize();

	void InitializeForNewArchitecturalPlan();

private slots:
	void on_openPlanImagePushButton_pressed();
	void on_addPlanPushButton_pressed();
	void on_showArchitecturalPlanListViewPushButton_pressed();
	void on_definitionLineEdit_textChanged();

	void on_definition_Changed(const char * value);

private:
	void openScannedFloorDesignImage(const QString &fileName);
};

#endif // SCANNED_FLOOR_DESIGN_FILE_FORM_H
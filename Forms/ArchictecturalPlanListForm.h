#ifndef ARCHITECTURAL_PLAN_LIST_WIDGETWINDOW_H
#define ARCHITECTURAL_PLAN_LIST_WIDGETWINDOW_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QGraphicsItem>

class Project;

namespace Ui {
	class ArchictecturalPlanListForm;
}

class ArchictecturalPlanListForm : public QDialog
{
	Q_OBJECT
public:
	explicit ArchictecturalPlanListForm(Project * iProject, QWidget *parent = nullptr);
	~ArchictecturalPlanListForm();

protected:
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::ArchictecturalPlanListForm *ui;

	QStringList tableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * plansTableViewModel;

	void PopulatePlanListView();

private slots:
	void on_deletePlanPushButton_pressed();
	
};

#endif // ARCHITECTURAL_PLAN_LIST_WIDGETWINDOW_H
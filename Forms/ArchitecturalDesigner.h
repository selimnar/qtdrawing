#ifndef ARCHITECTURAL_DESIGNER_MAINWINDOW_H
#define ARCHITECTURAL_DESIGNER_MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QGraphicsItem>

class Project;
class ScannedFloorDesignFileForm;
class ArchictecturalPlanListForm;
class DrawnPlanListForm;

namespace Ui {
	class ArchitecturalDesigner;
}

class ArchitecturalDesigner : public QMainWindow
{
	Q_OBJECT
public:
	explicit ArchitecturalDesigner(QWidget *parent = nullptr);
	~ArchitecturalDesigner();

protected:
	void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	std::string projectsWorkspaceFolder;

	Ui::ArchitecturalDesigner *ui;

	Project * m_project;

	//CreateLoadProjectForm * createLoadProjectForm;
	//ScannedFloorDesignFileForm * scannedFloorDesignFileForm;
	//DrawnPlanListForm * drawnPlanListForm;
	//ScannedFloorDesignSelectorForm * scannedFloorDesignSelectorForm;

	void openFloorDesignerWindow();

private slots:
	void on_actionCreateLoadProject_triggered();
	void on_actionFloorPlans_triggered();
	void on_actionApartmentPlans_triggered();
	void on_actionArchitecturalPlans_triggered();
	void on_actionSave_triggered();
	void on_actionEditBuildings_triggered();

private:
	void showMessage(QString message);
	
};

#endif // ARCHITECTURAL_DESIGNER_MAINWINDOW_H
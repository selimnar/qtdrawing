#include <fstream>

#include <QtMath>
#include <QList>
#include <QStringList>
#include <QFileDialog>
#include <QMessageBox>

#include "CreateLoadProjectForm.h"
#include "ui_CreateLoadProjectForm.h"

#include "utilities\utilities.h"
#include "Utilities\FileUtilities.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

using namespace std;

CreateLoadProjectForm::CreateLoadProjectForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::CreateLoadProjectForm)
{
	initialized = false;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");
	DEBUG_OUT("!!!!!!!!!!!----------- MIMARI_PROJELER_KLASORU = %s", projectsWorkspaceFolder.c_str());

	m_initialProject = iProject;
	m_project = nullptr;
	ui->setupUi(this);

	Qt::WindowFlags flags = windowFlags();
	Qt::WindowFlags closeFlag = Qt::WindowCloseButtonHint;
	flags = flags & (~closeFlag);
	setWindowFlags(flags);

	tableHeaderLabels << "TasinmazId" << "il" << "ilce" << "mahalle" << "ada" << "parsel";

	PopulateProjectsData();
	PopulateProjectsListView(&projectsToLoadList);

	InitializeCities();
}

void CreateLoadProjectForm::showEvent(QShowEvent * event)
{
	Initialize();
}

CreateLoadProjectForm::~CreateLoadProjectForm()
{
	delete ui;
}

Project * CreateLoadProjectForm::project()
{
	return m_project;
}

void CreateLoadProjectForm::Initialize()
{
	bool isProjectNotNull = m_initialProject != nullptr;
	this->m_project = (isProjectNotNull) ? new Project(m_initialProject) : (new Project());

	ui->tasinmazIdLineEdit->setValidator(new QIntValidator(-1, INT_MAX, ui->tasinmazIdLineEdit));
	ui->adaLineEdit->setValidator(new QIntValidator(0, INT_MAX, ui->tasinmazIdLineEdit));
	ui->parselLineEdit->setValidator(new QIntValidator(0, INT_MAX, ui->tasinmazIdLineEdit));

	if (m_initialProject != nullptr)
	{
		on_project_tasinmazId_Changed(m_initialProject->tasinmazId());
		on_project_cityId_Changed(m_initialProject->cityId());
		on_project_townId_Changed(m_initialProject->townId());
		on_project_neighborhoodId_Changed(m_initialProject->neighborhoodId());
		on_project_ada_Changed(m_initialProject->ada());
		on_project_parsel_Changed(m_initialProject->parsel());
	}

	connect(m_project, SIGNAL(tasinmazIdChanged(int)), this, SLOT(on_project_tasinmazId_Changed(int)));
	connect(m_project, SIGNAL(cityIdChanged(unsigned int)), this, SLOT(on_project_cityId_Changed(unsigned int)));
	connect(m_project, SIGNAL(townIdChanged(unsigned int)), this, SLOT(on_project_townId_Changed(unsigned int)));
	connect(m_project, SIGNAL(neighborhoodIdChanged(unsigned int)), this, SLOT(on_project_neighborhoodId_Changed(unsigned int)));
	connect(m_project, SIGNAL(adaChanged(unsigned int)), this, SLOT(on_project_ada_Changed(unsigned int)));
	connect(m_project, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));

	initialized = true;

	//if (isProjectNotNull)
	//{
	//	this->m_project->emitDataSignals();
	//}
}

void CreateLoadProjectForm::InitializeCities()
{
	CityTownNeighborhoodData &cityTownNeighborhoodData = *(CityTownNeighborhoodData::Instance());

	std::vector<CityRecord> cities = cityTownNeighborhoodData.cities();
	for (const auto &city : cities)
	{
		unsigned int count = ui->cityComboBox->count();
		comboIndexCityMap.insert(pair<unsigned int, unsigned int>(count, city.id));
		comboIndexCityInverseMap.insert(pair<unsigned int, unsigned int>(city.id, count));
		ui->cityComboBox->addItem(city.name);
	}
}

void CreateLoadProjectForm::PopulateProjectsData()
{
	projectsToLoadList.clear();

	QStringList metaFileNameFilters;
	metaFileNameFilters << "*.meta" << "*.META";

	QDir directory(projectsWorkspaceFolder.c_str());
	QStringList nameFilters;
	//nameFilters << "*.jpg" << "*.JPG";
	QStringList folderList = directory.entryList(nameFilters, QDir::Dirs);
	folderList.removeAt(folderList.indexOf("."));
	folderList.removeAt(folderList.indexOf(".."));
	int projectCounter = 0;
	for (QString projectFolderName : folderList)
	{
		QString fullProjectFolder = QString(projectsWorkspaceFolder.c_str()) + "\\" + projectFolderName;
		QDir projectDirectory(fullProjectFolder);
		QStringList metaFiles = projectDirectory.entryList(metaFileNameFilters, QDir::Files);
		if (metaFiles.count() > 0)
		{
			QString metaFile = metaFiles[0];
			metaFile.replace(".meta", "");

			QStringList projectParams = metaFile.split("_");
			if (projectParams.count() == 6)
			{
				int tasinmazId = projectParams[0].toInt();
				unsigned int cityId = projectParams[1].toUInt();
				unsigned int townId = projectParams[2].toUInt();
				unsigned int neighId = projectParams[3].toUInt();
				unsigned int ada = projectParams[4].toUInt();
				unsigned int parsel = projectParams[5].toUInt();

				Project * project = new Project();
				project->setTasinmazId(tasinmazId);
				project->setCityId(cityId);
				project->setTownId(townId);
				project->setNeighborhoodId(neighId);
				project->setAda(ada);
				project->setParsel(parsel);

				projectsToLoadList.push_back(project);
			}
		}
	}
}

void CreateLoadProjectForm::PopulateProjectsListView(std::vector<Project*> *projectListToPopulateView)
{
	CityTownNeighborhoodData &cityTownNeighborhoodData = *(CityTownNeighborhoodData::Instance());
	std::map<int, CityRecord> &cityMap = cityTownNeighborhoodData.cityIdMapping();
	std::map<int, TownRecord> &townMap = cityTownNeighborhoodData.townIdMapping();
	std::map<int, NeighborhoodRecord> &neighborhoodMap = cityTownNeighborhoodData.neighborhoodIdMapping();

	filteredToLoadList.clear();
	filteredToLoadList.insert(filteredToLoadList.begin(), projectListToPopulateView->begin(), projectListToPopulateView->end());

	projectsTableViewModel = new QStandardItemModel(0, 0, this);
	projectsTableViewModel->setHorizontalHeaderLabels(tableHeaderLabels);
	for (Project * project : filteredToLoadList)
	{
		int tasinmazId = project->tasinmazId();
		unsigned int cityId = project->cityId();
		unsigned int townId = project->townId();
		unsigned int neighId = project->neighborhoodId();
		unsigned int ada = project->ada();
		unsigned int parsel = project->parsel();

		QString cityName = cityMap[cityId].name;
		QString townName = townMap[townId].name;
		QString neighborhoodName = neighborhoodMap[neighId].name;

		//QString projectInfo = "TasinmazId: " + QString::number(tasinmazId) + "; il: " + cityName + "; ilce: " + townName + "; mahalle: " +
		//	neighborhoodName + "; ada: " + QString::number(ada) + "; parsel: " + QString::number(parsel);

		//DEBUG_OUT("!!!!!!!!-****** %s", projectInfo.toStdString().c_str());

		QList<QStandardItem *> rowItems;
		//rowItems.append(new QStandardItem("defef fe"));
		rowItems.append(new QStandardItem(QString::number(tasinmazId)));
		rowItems.append(new QStandardItem(cityName));
		rowItems.append(new QStandardItem(townName));
		rowItems.append(new QStandardItem(neighborhoodName));
		rowItems.append(new QStandardItem(QString::number(ada)));
		rowItems.append(new QStandardItem(QString::number(parsel)));
		projectsTableViewModel->appendRow(rowItems);
	}

	QItemSelectionModel *previousTableViewModel = ui->projectsTableView->selectionModel();
	ui->projectsTableView->setModel(projectsTableViewModel);
	if (previousTableViewModel != nullptr)
	{
		delete previousTableViewModel;
	}
	//ui->projectsTableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

bool CreateLoadProjectForm::checkProjectInformationValidity()
{
	if (this->m_project == nullptr)
	{
		return false;
	}
	return true;
	//this->project->
}

void CreateLoadProjectForm::on_filterProjectsPushButton_pressed()
{
	FilterProjects();
	DEBUG_OUT("on_filterProjectsPushButton_pressed");
}

void CreateLoadProjectForm::on_clearProjectFiltersPushButton_pressed()
{
	PopulateProjectsListView(&projectsToLoadList);
}

void CreateLoadProjectForm::on_openPojectPushButton_pressed()
{
	QModelIndex selectedProjectItemIndex = ui->projectsTableView->currentIndex();
	//this->ui->projectsListView = nullptr;
	//QModelIndexList modelIndexList =  this->ui->projectsListView->selectionModel()->selectedIndexes();
	//QModelIndex selectedProjectItemIndex = ui->projectsListView->currentIndex();
	if (selectedProjectItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Proje seciniz");
		Msgbox.exec();
		return;
	}
	//QString selectedProjectItemText = selectedProjectItemIndex.data(Qt::DisplayRole).toString() + ":" + QString::number(selectedProjectItemIndex.row());
	//	selectedIndexes()

	Project * project = filteredToLoadList[selectedProjectItemIndex.row()];
	int tasinmazId = project->tasinmazId();
	unsigned int cityId = project->cityId();
	unsigned int townId = project->townId();
	unsigned int neighId = project->neighborhoodId();
	unsigned int ada = project->ada();
	unsigned int parsel = project->parsel();

	CityTownNeighborhoodData &cityTownNeighborhoodData = *(CityTownNeighborhoodData::Instance());
	std::map<int, CityRecord> &cityMap = cityTownNeighborhoodData.cityIdMapping();
	std::map<int, TownRecord> &townMap = cityTownNeighborhoodData.townIdMapping();
	std::map<int, NeighborhoodRecord> &neighborhoodMap = cityTownNeighborhoodData.neighborhoodIdMapping();
	QString cityName = cityMap[cityId].name;
	QString townName = townMap[townId].name;
	QString neighborhoodName = neighborhoodMap[neighId].name;
				
	QString selectedProjectItemText = "TasinmazId: " + QString::number(tasinmazId) + "; il: " + cityName + "; ilce: " + townName + "; mahalle: " +
		neighborhoodName + "; ada: " + QString::number(ada) + "; parsel: " + QString::number(parsel);

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Proje Yukle"));
	msgBox.setText(selectedProjectItemText);
	//msgBox.setInformativeText(selectedProjecItemText);
	//msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	//msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)//(ret == QMessageBox::Yes)
	{
		std::string folderPath = project->GetProjectFolderPath(projectsWorkspaceFolder);
		std::string projectFile = folderPath + "\\project.xml";
		openProjectFile(projectFile.c_str());
		m_project->setProjectString(selectedProjectItemText);
		DEBUG_OUT("++++++++++++++++++");
	}
	else
	{
		DEBUG_OUT("------------------");
	}
	//DEBUG_OUT("CreateLoadProjectForm::on_openPojectPushButton_pressed");
}

void CreateLoadProjectForm::on_saveProjectPushButton_pressed()
{
	if (this->m_project != nullptr)
	{
		this->m_project->saveProject(projectsWorkspaceFolder);
	}

	DEBUG_OUT("CreateLoadProjectForm::on_saveProjectPushButton_pressed");
}

void CreateLoadProjectForm::on_clonePushButton_pressed()
{
	DEBUG_OUT("CreateLoadProjectForm::on_clonePushButton_pressed");
}

void CreateLoadProjectForm::on_confirmPushButton_pressed()
{
	disableProjectUIWidgetSynch();

	this->close();
	this->setResult(QDialog::Accepted);

	//DEBUG_OUT("CreateLoadProjectForm::on_confirmPushButton_pressed");
}

void CreateLoadProjectForm::on_rejectPushButton_pressed()
{
	disableProjectUIWidgetSynch();
	if (m_project != nullptr && m_initialProject != m_project)
	{
		delete m_project;
	}

	this->close();
	this->setResult(QDialog::Rejected);

	//DEBUG_OUT("CreateLoadProjectForm::on_rejectPushButton_pressed");
}

void CreateLoadProjectForm::on_tasinmazIdLineEdit_textChanged()
{
	bool success;
	int value = ui->tasinmazIdLineEdit->text().toInt(&success);
	if (success)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		m_project->setTasinmazId(value);
		m_project->blockSignals(wasBlocked);
	}
}

void CreateLoadProjectForm::on_adaLineEdit_textChanged()
{
	bool success;
	int value = ui->adaLineEdit->text().toInt(&success);
	if (success)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		m_project->setAda(value);
		m_project->blockSignals(wasBlocked);
	}
}

void CreateLoadProjectForm::on_parselLineEdit_textChanged()
{
	bool success;
	int value = ui->parselLineEdit->text().toInt(&success);
	if (success)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		m_project->setParsel(value);
		m_project->blockSignals(wasBlocked);
	}
}

void CreateLoadProjectForm::on_cityComboBox_currentIndexChanged(int index)
{
	if (m_project == nullptr)
	{
		return;
	}

	if (initialized)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		int cityId = comboIndexCityMap[index];
		m_project->setCityId(cityId);
		m_project->blockSignals(wasBlocked);
	}

	cityComboBoxCurrentIndexChanged(index);
}

void CreateLoadProjectForm::on_townComboBox_currentIndexChanged(int index)
{
	if (m_project == nullptr)
	{
		return;
	}

	if (initialized)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		int townId = comboIndexTownMap[index];
		m_project->setTownId(townId);
		m_project->blockSignals(wasBlocked);
	}

	townComboBoxCurrentIndexChanged(index);
}

void CreateLoadProjectForm::on_neighborhoodComboBox_currentIndexChanged(int index)
{
	if (m_project == nullptr)
	{
		return;
	}

	//DEBUG_OUT("CreateLoadProjectForm::on_neighborhoodComboBox_currentIndexChanged : %d / %d - %d", index,
	//	ui->neighborhoodComboBox->count(), comboIndexNeighborhoodMap[index]);

	if (initialized)
	{
		const bool wasBlocked = m_project->blockSignals(true);
		// no signals here
		selectedNeighborhoodIndex = comboIndexNeighborhoodMap[index];
		m_project->setNeighborhoodId(selectedNeighborhoodIndex);
		m_project->blockSignals(wasBlocked);
	}
}

void CreateLoadProjectForm::cityComboBoxCurrentIndexChanged(int index)
{
	//DEBUG_OUT("CreateLoadProjectForm::on_cityComboBox_currentIndexChanged : %d / %d - %d", index,
	//	ui->cityComboBox->count(), comboIndexCityMap[index]);

	CityTownNeighborhoodData &cityTownNeighborhoodData = *CityTownNeighborhoodData::Instance();

	selectedCityIndex = comboIndexCityMap[index];
	std::map<int, std::vector<TownRecord>> townMap = cityTownNeighborhoodData.towns();
	std::vector<TownRecord> townsOfSelectedCity = townMap[selectedCityIndex];

	comboIndexTownMap.clear();
	comboIndexTownInverseMap.clear();
	ui->townComboBox->clear();
	for (const auto &town : townsOfSelectedCity)
	{
		unsigned int count = ui->townComboBox->count();
		comboIndexTownMap.insert(pair<unsigned int, unsigned int>(count, town.id));
		comboIndexTownInverseMap.insert(pair<unsigned int, unsigned int>(town.id, count));
		ui->townComboBox->addItem(town.name);
	}
}

void CreateLoadProjectForm::townComboBoxCurrentIndexChanged(int index)
{
	//DEBUG_OUT("CreateLoadProjectForm::on_townComboBox_currentIndexChanged : %d / %d - %d", index,
	//	ui->townComboBox->count(), comboIndexTownMap[index]);

	CityTownNeighborhoodData &cityTownNeighborhoodData = *CityTownNeighborhoodData::Instance();

	selectedTownIndex = comboIndexTownMap[index];
	std::map<int, std::vector<NeighborhoodRecord>> neighborhoodMap = cityTownNeighborhoodData.neighborhoods();
	std::vector<NeighborhoodRecord> neighborhoodsOfSelectedTowns = neighborhoodMap[selectedTownIndex];

	comboIndexNeighborhoodMap.clear();
	comboIndexNeighborhoodInverseMap.clear();
	ui->neighborhoodComboBox->clear();
	for (const auto &neighborhood : neighborhoodsOfSelectedTowns)
	{
		unsigned int count = ui->neighborhoodComboBox->count();
		comboIndexNeighborhoodMap.insert(pair<unsigned int, unsigned int>(count, neighborhood.id));
		comboIndexNeighborhoodInverseMap.insert(pair<unsigned int, unsigned int>(neighborhood.id, count));
		ui->neighborhoodComboBox->addItem(neighborhood.name);
	}
}

void CreateLoadProjectForm::openProjectFile(const char * fileName)
{
	disableProjectUIWidgetSynch();
	if (this->m_project != nullptr && m_initialProject != m_project)
	{
		delete this->m_project;
	}

	this->m_project = new Project();

	connect(m_project, SIGNAL(tasinmazIdChanged(int)), this, SLOT(on_project_tasinmazId_Changed(int)));
	connect(m_project, SIGNAL(cityIdChanged(unsigned int)), this, SLOT(on_project_cityId_Changed(unsigned int)));
	connect(m_project, SIGNAL(townIdChanged(unsigned int)), this, SLOT(on_project_townId_Changed(unsigned int)));
	connect(m_project, SIGNAL(neighborhoodIdChanged(unsigned int)), this, SLOT(on_project_neighborhoodId_Changed(unsigned int)));
	connect(m_project, SIGNAL(adaChanged(unsigned int)), this, SLOT(on_project_ada_Changed(unsigned int)));
	connect(m_project, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));

	this->m_project->deserialize(fileName);
}

void CreateLoadProjectForm::saveProjectFile(const char * folderPath, const char * fileName)
{
	this->m_project->serialize(fileName);

	QString projectFolderPath(folderPath);
	projectFolderPath += "\\";

	Project &pro = *m_project;
	QString metaFilePath = projectFolderPath + QString::number(pro.tasinmazId()) + "_" + QString::number(pro.cityId()) + "_" + QString::number(pro.townId()) + "_" +
		QString::number(pro.neighborhoodId()) + "_" + QString::number(pro.ada()) + "_" + QString::number(pro.parsel()) + ".meta";
	
	if (!QFile::exists(metaFilePath))
	{
		QFile file(metaFilePath);
		file.open(QIODevice::WriteOnly);
		file.close();
	}
}

void CreateLoadProjectForm::disableProjectUIWidgetSynch()
{
	if (this->m_project != nullptr)
	{
		disconnect(m_project, SIGNAL(tasinmazIdChanged(int)), this, SLOT(on_project_tasinmazId_Changed(int)));
		disconnect(m_project, SIGNAL(cityIdChanged(unsigned int)), this, SLOT(on_project_cityId_Changed(unsigned int)));
		disconnect(m_project, SIGNAL(townIdChanged(unsigned int)), this, SLOT(on_project_townId_Changed(unsigned int)));
		disconnect(m_project, SIGNAL(neighborhoodIdChanged(unsigned int)), this, SLOT(on_project_neighborhoodId_Changed(unsigned int)));
		disconnect(m_project, SIGNAL(adaChanged(unsigned int)), this, SLOT(on_project_ada_Changed(unsigned int)));
		disconnect(m_project, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));
	}
}

void CreateLoadProjectForm::FilterProjects()
{
	bool filterByCityId = ui->filterByCityCheckBox->checkState();
	bool filterByTownId = ui->filterByTownIdCheckBox->checkState();
	bool filterByNeighborhoodId = ui->filterByNeighborhoodIdCheckBox->checkState();

	QString adaStr = ui->adaLineEdit->text();
	bool filterByAda = !adaStr.isEmpty() && adaStr.toUInt() > 0;

	QString parselStr = ui->parselLineEdit->text();
	bool filterByParsel = !parselStr.isEmpty() && parselStr.toUInt() > 0;

	QString tasinmazIdStr = ui->tasinmazIdLineEdit->text();
	bool filterByTasinmazId = !tasinmazIdStr.isEmpty() && tasinmazIdStr.toInt() > 0;

	Project &pro = *m_project;
	QString projectInfo = QString::number(pro.tasinmazId()) + "_" + QString::number(pro.cityId()) + "_" + QString::number(pro.townId()) + "_" +
		QString::number(pro.neighborhoodId()) + "_" + QString::number(pro.ada()) + "_" + QString::number(pro.parsel()) + ".meta";

	DEBUG_OUT("-------------------------- %s", projectInfo.toStdString().c_str());

	std::vector<Project*> filteredProjects;
	for (Project * project : projectsToLoadList)
	{
		if ( filterByCityId && (project->cityId() != m_project->cityId()) )
		{
			continue;
		}

		if (filterByTownId && (project->townId() != m_project->townId()))
		{
			continue;
		}

		if (filterByNeighborhoodId && (project->neighborhoodId() != m_project->neighborhoodId()))
		{
			continue;
		}

		if (filterByAda && (project->ada() != m_project->ada()))
		{
			continue;
		}

		if (filterByParsel && (project->parsel() != m_project->parsel()))
		{
			continue;
		}

		if (filterByTasinmazId && (project->tasinmazId() != m_project->tasinmazId()))
		{
			continue;
		}

		filteredProjects.push_back(project);
		//DEBUG_OUT("--------**********--------- %s", std::to_string(project->cityId()).c_str());
	}
	
	PopulateProjectsListView(&filteredProjects);

}

void CreateLoadProjectForm::on_project_tasinmazId_Changed(int value)
{
	ui->tasinmazIdLineEdit->setText(QString::number(value, 'i', 0));
}

void CreateLoadProjectForm::on_project_cityId_Changed(unsigned int value)
{
	int selectionIndex = this->comboIndexCityInverseMap[value];
	ui->cityComboBox->setCurrentIndex(selectionIndex);
	//cityComboBoxCurrentIndexChanged(selectionIndex);
}

void CreateLoadProjectForm::on_project_townId_Changed(unsigned int value)
{
	int selectionIndex = this->comboIndexTownInverseMap[value];
	ui->townComboBox->setCurrentIndex(selectionIndex);
	//townComboBoxCurrentIndexChanged(selectionIndex);
}

void CreateLoadProjectForm::on_project_neighborhoodId_Changed(unsigned int value)
{
	int selectionIndex = this->comboIndexNeighborhoodInverseMap[value];
	ui->neighborhoodComboBox->setCurrentIndex(selectionIndex);
}

void CreateLoadProjectForm::on_project_ada_Changed(unsigned int value)
{
	ui->adaLineEdit->setText(QString::number(value, 'i', 0));
}

void CreateLoadProjectForm::on_project_parsel_Changed(unsigned int value)
{
	ui->parselLineEdit->setText(QString::number(value, 'i', 0));
}
#ifndef DRAWN_FLOOR_LIST_FORM_H
#define DRAWN_FLOOR_LIST_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class Project;
class DrawnFloorPlan;

namespace Ui {
	class DrawnPlanListForm;
}

class DrawnPlanListForm : public QDialog
{
	Q_OBJECT
public:
	explicit DrawnPlanListForm(Project * iProject, QWidget *parent = nullptr);
	~DrawnPlanListForm();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::DrawnPlanListForm *ui;

	QStringList tableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	DrawnFloorPlan * m_drawnFloorPlan;

	QPixmap m_pixmap;

	QFileInfo qfileInfo;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * plansTableViewModel;

	void PopulatePlanListView();
	void InitializeForNewDrawnPlan();

private slots:
	void on_openPlanPushButton_pressed();
	void on_addPlanPushButton_pressed();
	void on_removeDrawnPlanPushButton_pressed();
	void on_definitionLineEdit_textChanged();

	void on_definition_Changed(const char * value);

};


#endif // DRAWN_FLOOR_LIST_FORM_H
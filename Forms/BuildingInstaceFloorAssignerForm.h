#ifndef DRAWN_FLOOR_LIST_FORM_H
#define DRAWN_FLOOR_LIST_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class Project;
class DrawnFloorPlan;
class BuildingInstance;

namespace Ui {
	class BuildingInstaceFloorAssignerForm;
}

class BuildingInstaceFloorAssignerForm : public QDialog
{
	Q_OBJECT
public:
	explicit BuildingInstaceFloorAssignerForm(Project * iProject, BuildingInstance * inBuildingInstance, QWidget *parent = nullptr);
	~BuildingInstaceFloorAssignerForm();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::BuildingInstaceFloorAssignerForm *ui;

	QStringList drawnPlanTableHeaderLabels;
	QStringList buildingDrawnPlanInstancesTableHeaderLabels;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	BuildingInstance * m_buildingInstance;

	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * plansTableViewModel;
	QStandardItemModel * buildingDrawnPlanInstancesTableViewModel;

	void PopulatePlanListView();
	void PopulateBuildingDrawnPlanInstanceListView();

private slots:
	void on_addFloorToBuildingPushButton_pressed();
	void on_removeAssignedFloorPushButton_pressed();
	void on_assignFloorCountPushButton_pressed();
	void on_showIn3DViewButton_pressed();
};


#endif // DRAWN_FLOOR_LIST_FORM_H
#include "DrawnPlanAsBaseSelectorForm.h"
#include "ui_DrawnPlanAsBaseSelectorForm.h"

#include <fstream>

#include <QtMath>
#include <QList>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include "FloorPlanDesigner.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

#include "utilities\utilities.h"
#include "utilities\ProjectUtilities.h"
#include "Utilities\FileUtilities.h"

using namespace std;


DrawnPlanAsBaseSelectorForm::DrawnPlanAsBaseSelectorForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DrawnPlanAsBaseSelectorForm)
{
	ui->setupUi(this);

	this->m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	tableHeaderLabels << "Id" << "Tanim";
	PopulatePlanListView();
}

DrawnPlanAsBaseSelectorForm::~DrawnPlanAsBaseSelectorForm()
{
	delete ui;
}

QString DrawnPlanAsBaseSelectorForm::drawnFilePath()
{
	return m_drawnFilePath;
}

void DrawnPlanAsBaseSelectorForm::PopulatePlanListView()
{
	PopulateDrawnPlanListViewBase(m_project, ui->drawnPlansTableView, plansTableViewModel, tableHeaderLabels, this);
}

void DrawnPlanAsBaseSelectorForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void DrawnPlanAsBaseSelectorForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void DrawnPlanAsBaseSelectorForm::on_openPlanPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->drawnPlansTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden taranmis mimari plan resim dosyasini acmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();
		DrawnFloorPlan * selectedPlan = drawnPlans[id];
		this->m_drawnFilePath = QString(selectedPlan->getFilePath(m_project, projectsWorkspaceFolder).c_str());
		done(true);
	}
}
#include <fstream>

#include <QtMath>
#include <QList>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include "ScannedFloorDesignFileForm.h"
#include "ui_ScannedFloorDesignFileForm.h"

#include "Forms/ArchictecturalPlanListForm.h"

#include "CustomWidgets/ScannedFloorplanView.h"
#include "CustomQObjects/ScannedFloorplanGraphicsScene.h"

#include "utilities/utilities.h"
#include "utilities/FileUtilities.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

using namespace std;

const string ScannedFloorDesignFileForm::ScannedPlanFolder("\\scannedPlans\\");

ScannedFloorDesignFileForm::ScannedFloorDesignFileForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::ScannedFloorDesignFileForm)
{
	ui->setupUi(this);

	Initialize();

	this->m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	InitializeForNewArchitecturalPlan();
}

ScannedFloorDesignFileForm::~ScannedFloorDesignFileForm()
{
	delete ui;
}

void ScannedFloorDesignFileForm::Initialize()
{
	scene = new ScannedFloorplanGraphicsScene(this);
	ui->graphicsView->setScene(scene);
}

void ScannedFloorDesignFileForm::InitializeForNewArchitecturalPlan()
{
	this->m_architecturalPlan = new ArchitecturalPlan();

	//ui->parselLineEdit->setValidator(new QIntValidator(0, INT_MAX, ui->tasinmazIdLineEdit));

	unsigned int maxId = m_project->retrieveMaxArchitecturalPlanId() + 1;
	this->m_architecturalPlan->setId(maxId);
	ui->idLabel->setText(QString::number(maxId));
	m_architecturalPlan->setDefinition("");
	ui->filePathQLabel->setText("");

	connect(m_architecturalPlan, SIGNAL(definitionChanged(const char * value)), this, SLOT(on_definition_Changed(const char * value)));
	//connect(m_project, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));
}

void ScannedFloorDesignFileForm::closeEvent(QCloseEvent *event)
{
	scene->resetScene();
	QDialog::closeEvent(event);
}

void ScannedFloorDesignFileForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void ScannedFloorDesignFileForm::on_openPlanImagePushButton_pressed()
{
	QString filePath = QFileDialog::getOpenFileName(this,
		tr("Open Scanned Floor Plan Image"), "",
		tr("Scanned Floor Plan Image (*.tif);;All Files (*)"));

	QFile file(filePath);
	if (file.exists())
	{
		openScannedFloorDesignImage(filePath);
		qfileInfo.setFile(file);
		QString fileName(qfileInfo.fileName());
		ui->filePathQLabel->setText(fileName);
		this->m_architecturalPlan->setFileName(fileName.toStdString().c_str());
	}

	DEBUG_OUT("ArchitecturalDesigner::on_openPlanImagePushButton_pressed");
}

void ScannedFloorDesignFileForm::on_addPlanPushButton_pressed()
{
	if (QString(this->m_architecturalPlan->fileName()).isEmpty())
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan resmi seciniz");
		Msgbox.exec();
		return;
	}

	QString definition = QString(this->m_architecturalPlan->definition());
	if (definition.isEmpty())
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim bos olamaz");
		Msgbox.exec();
		return;
	}

	bool isDefinitionUnique = true;
	std::map<unsigned int, ArchitecturalPlan*> scannedImageFiles = m_project->scannedImageFiles();
	for (auto kvp : scannedImageFiles)
	{
		ArchitecturalPlan * architecturalPlan = kvp.second;
		if (definition == QString(architecturalPlan->definition()))
		{
			isDefinitionUnique = false;
			break;
		}
	}

	if (!isDefinitionUnique)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim benzersiz olmali");
		Msgbox.exec();
		return;
	}

	string folderPath = this->m_project->GetProjectFolderPath(projectsWorkspaceFolder);
	string scannedPlansFolder = folderPath + ScannedPlanFolder;

	bool folderExists = FileUtilities::PathExists(scannedPlansFolder.c_str());
	if (!folderExists)
	{
		FileUtilities::CreateFolder(scannedPlansFolder.c_str());
	}

	QFile::copy(qfileInfo.filePath(), QString::fromStdString(scannedPlansFolder) + this->m_architecturalPlan->fileName());
	m_project->addArchitecturalPlan(this->m_architecturalPlan);
	InitializeForNewArchitecturalPlan();
	scene->resetScene();
	m_project->saveProject(projectsWorkspaceFolder);
	DEBUG_OUT("ArchitecturalDesigner::on_addPlanPushButton_pressed %s", this->m_architecturalPlan->definition());
}

void ScannedFloorDesignFileForm::on_showArchitecturalPlanListViewPushButton_pressed()
{
	ArchictecturalPlanListForm archictecturalPlanListForm(this->m_project, this);
	archictecturalPlanListForm.exec();
	
	DEBUG_OUT("ArchitecturalDesigner::on_showArchitecturalPlanListViewPushButton_pressed");
}

void ScannedFloorDesignFileForm::on_definitionLineEdit_textChanged()
{
	bool success;
	QString definitionQStr = ui->definitionLineEdit->text();
	if (success)
	{
		const bool wasBlocked = m_architecturalPlan->blockSignals(true);
		// no signals here
		std::string str = definitionQStr.toStdString();
		const char* cstr = str.c_str();
		m_architecturalPlan->setDefinition(cstr);
		m_architecturalPlan->blockSignals(wasBlocked);
	}
}

void ScannedFloorDesignFileForm::on_definition_Changed(const char * value)
{
	ui->definitionLineEdit->setText(QString(value));
}

void ScannedFloorDesignFileForm::openScannedFloorDesignImage(const QString &fileName)
{
	scene->resetScene();
	QImage qImage;
	bool success = qImage.load(fileName);
	if (success)
	{
		scene->loadImage(&qImage);
	}
}

//void ScannedFloorDesignFileForm::on_getSelectionPushButton_pressed()
//{
//	bool success = scene->retrieveSelection(m_pixmap);
//	done(success);
//	DEBUG_OUT("ArchitecturalDesigner::on_getSelectionPushButton_pressed");
//}
#include "BuildingInstaceFloorAssignerForm.h"
#include "ui_BuildingInstaceFloorAssignerForm.h"

#include <fstream>

#include <QtMath>
#include <QList>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include "FloorPlanDesigner.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

#include "utilities\utilities.h"
#include "utilities\ProjectUtilities.h"
#include "Utilities\FileUtilities.h"

#include "Full3DBuildingMeshVisualizerForm.h"

using namespace std;


BuildingInstaceFloorAssignerForm::BuildingInstaceFloorAssignerForm(Project * iProject,
	BuildingInstance * inBuildingInstance, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::BuildingInstaceFloorAssignerForm)
{
	ui->setupUi(this);

	this->m_project = iProject;
	this->m_buildingInstance = inBuildingInstance;

	ui->definitionLabel->setText(inBuildingInstance->definition());
	ui->idLabel->setText(QString::number(inBuildingInstance->id()));

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	drawnPlanTableHeaderLabels << "Id" << "Tanim";
	PopulatePlanListView();

	buildingDrawnPlanInstancesTableHeaderLabels << "Id" << "Tanim" << "Kat Sayisi";
	PopulateBuildingDrawnPlanInstanceListView();
}

BuildingInstaceFloorAssignerForm::~BuildingInstaceFloorAssignerForm()
{
	delete ui;
}

void BuildingInstaceFloorAssignerForm::PopulatePlanListView()
{
	PopulateDrawnPlanListViewBase(m_project, ui->drawnPlansTableView, plansTableViewModel, drawnPlanTableHeaderLabels, this);
}

void BuildingInstaceFloorAssignerForm::PopulateBuildingDrawnPlanInstanceListView()
{
	PopulateBuildingDrawnPlanInstanceListViewBase(m_project, m_buildingInstance, ui->buildingDrawnPlanInstancesTableView,
		buildingDrawnPlanInstancesTableViewModel, buildingDrawnPlanInstancesTableHeaderLabels, this);
}

void BuildingInstaceFloorAssignerForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void BuildingInstaceFloorAssignerForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void BuildingInstaceFloorAssignerForm::on_addFloorToBuildingPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->drawnPlansTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Binaya Kat Plani atamak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();
		DrawnFloorPlan * selectedPlan = drawnPlans[id];
		string drawnFilePath = selectedPlan->getFilePath(m_project, projectsWorkspaceFolder);

		AssignedFloorInstance * assignedFloorInstance = new AssignedFloorInstance();
		assignedFloorInstance->setId(m_buildingInstance->retrieveMaxAssignedFloorInstanceId() + 1);
		assignedFloorInstance->setFloorId(selectedPlan->id());
		assignedFloorInstance->setCount(ui->floorCountSpinBox->value());
		m_buildingInstance->addAssignedFloorInstance(assignedFloorInstance);

		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			PopulateBuildingDrawnPlanInstanceListView();
		});
	}
}

void BuildingInstaceFloorAssignerForm::on_removeAssignedFloorPushButton_pressed()
{
	QModelIndex selectedItemIndex = ui->buildingDrawnPlanInstancesTableView->currentIndex();
	if (selectedItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Atanmis Kat Plani seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Binadan Atanmis Kat plani kaldirmak acmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = buildingDrawnPlanInstancesTableViewModel->item(selectedItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, AssignedFloorInstance*> drawnPlans = m_buildingInstance->assignedFloorPlans();
		//AssignedFloorInstance * assignedFloorInstance = drawnPlans[id];
		m_buildingInstance->removeAssignedFloorInstance(id);

		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			PopulateBuildingDrawnPlanInstanceListView();
		});
	}
}

void BuildingInstaceFloorAssignerForm::on_assignFloorCountPushButton_pressed()
{
	QModelIndex selectedItemIndex = ui->buildingDrawnPlanInstancesTableView->currentIndex();
	if (selectedItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Atanmis Kat Plani seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Atanmis Kat Planinin kullanim sayisini degistirmek istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = buildingDrawnPlanInstancesTableViewModel->item(selectedItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, AssignedFloorInstance*> drawnPlans = m_buildingInstance->assignedFloorPlans();
		AssignedFloorInstance * assignedFloorInstance = drawnPlans[id];
		assignedFloorInstance->setCount(ui->floorCountSpinBox->value());

		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			PopulateBuildingDrawnPlanInstanceListView();
		});
	}
}

void BuildingInstaceFloorAssignerForm::on_showIn3DViewButton_pressed()
{
	Full3DBuildingMeshVisualizerForm full3DBuildingMeshVisualizerForm(m_project, m_buildingInstance, this);
	full3DBuildingMeshVisualizerForm.exec();
}
#include "DrawnPlanListForm.h"
#include "ui_DrawnPlanListForm.h"

#include <fstream>

#include <QtMath>
#include <QList>
#include <QString>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>

#include "FloorPlanDesigner.h"

#include "data\TasinmazData.h"
#include "data\Project.h"

#include "utilities\utilities.h"
#include "utilities\ProjectUtilities.h"
#include "Utilities\FileUtilities.h"

using namespace std;


DrawnPlanListForm::DrawnPlanListForm(Project * iProject, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::DrawnPlanListForm)
{
	ui->setupUi(this);

	this->m_project = iProject;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	tableHeaderLabels << "Id" << "Tanim";
	PopulatePlanListView();

	this->m_drawnFloorPlan = nullptr;
	InitializeForNewDrawnPlan();
}

DrawnPlanListForm::~DrawnPlanListForm()
{
	if (this->m_drawnFloorPlan != nullptr)
	{
		delete this->m_drawnFloorPlan;
	}
	delete ui;
}

void DrawnPlanListForm::PopulatePlanListView()
{
	PopulateDrawnPlanListViewBase(m_project, ui->drawnPlansTableView, plansTableViewModel, tableHeaderLabels, this);
}

void DrawnPlanListForm::InitializeForNewDrawnPlan()
{
	//if (this->m_drawnFloorPlan != nullptr)
	//{
	//	delete this->m_drawnFloorPlan;
	//}
	this->m_drawnFloorPlan = new DrawnFloorPlan();

	//ui->parselLineEdit->setValidator(new QIntValidator(0, INT_MAX, ui->tasinmazIdLineEdit));

	unsigned int maxId = m_project->retrieveMaxDrawnFloorPlanId() + 1;
	this->m_drawnFloorPlan->setId(maxId);
	ui->idLabel->setText(QString::number(maxId));
	m_drawnFloorPlan->setDefinition("");
	ui->definitionLineEdit->setText("");

	connect(m_drawnFloorPlan, SIGNAL(definitionChanged(const char * value)), this, SLOT(on_definition_Changed(const char * value)));
	//connect(ui->drawnPlansTableView, SIGNAL(parselChanged(unsigned int)), this, SLOT(on_project_parsel_Changed(unsigned int)));
}

void DrawnPlanListForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void DrawnPlanListForm::showEvent(QShowEvent *event)
{
	QDialog::showEvent(event);
}

void DrawnPlanListForm::on_openPlanPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->drawnPlansTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden taranmis mimari plan resim dosyasini acmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();
		DrawnFloorPlan * selectedPlan = drawnPlans[id];
		string drawnFilePath = selectedPlan->getFilePath(m_project, projectsWorkspaceFolder);

		DEBUG_OUT("!!!! ---- **** ArchitecturalDesigner::on_actionFloorPlans_triggered: result = %d", 12345);
		FloorPlanDesigner floorPlanDesigner(m_project, drawnFilePath, this);
		floorPlanDesigner.exec();
		bool result = floorPlanDesigner.result();
		if (result)
		{
			//scene->loadImage(createLoadProjectForm->pixmap());
		}
	}
}

void DrawnPlanListForm::on_addPlanPushButton_pressed()
{
	QString definition = QString(this->m_drawnFloorPlan->definition());
	if (definition.isEmpty())
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim bos olamaz");
		Msgbox.exec();
		return;
	}

	bool isDefinitionUnique = true;
	std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();
	for (auto kvp : drawnPlans)
	{
		DrawnFloorPlan * drawnFloorPlan = kvp.second;
		if (definition == QString(drawnFloorPlan->definition()))
		{
			isDefinitionUnique = false;
			break;
		}
	}

	if (!isDefinitionUnique)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Tanim benzersiz olmali");
		Msgbox.exec();
		return;
	}

	this->m_drawnFloorPlan->CreateNewDrawnPlanScene(this->m_project, projectsWorkspaceFolder);
	m_project->addDrawnPlan(this->m_drawnFloorPlan);
	InitializeForNewDrawnPlan();
	m_project->saveProject(projectsWorkspaceFolder);

	QTimer::singleShot(20, [=] {
		PopulatePlanListView();
	});
	//DEBUG_OUT("ArchitecturalDesigner::on_addPlanPushButton_pressed %s", this->m_architecturalPlan->definition());
}

void DrawnPlanListForm::on_removeDrawnPlanPushButton_pressed()
{
	QModelIndex selectedPlanItemIndex = ui->drawnPlansTableView->currentIndex();
	if (selectedPlanItemIndex.row() < 0)
	{
		QMessageBox Msgbox;
		Msgbox.setText("Mimari plan seciniz");
		Msgbox.exec();
		return;
	}

	QMessageBox msgBox;
	msgBox.setWindowTitle(QString("Projeden taranmis mimari plan resim dosyasini kaldirmak istiyor musunuz?"));
	QAbstractButton *myYesButton = msgBox.addButton(trUtf8("Onayla"), QMessageBox::YesRole);
	QAbstractButton *myNoButton = msgBox.addButton(trUtf8("Reddet"), QMessageBox::NoRole);
	msgBox.setIcon(QMessageBox::Question);
	msgBox.setDefaultButton(QMessageBox::Yes);
	msgBox.setDefaultButton(QMessageBox::Ok);
	int ret = msgBox.exec();
	if (msgBox.clickedButton() == myYesButton)
	{
		QStandardItem * selectedRowIdField = plansTableViewModel->item(selectedPlanItemIndex.row());
		bool ok;
		unsigned int id = selectedRowIdField->text().toUInt(&ok);

		std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();
		DrawnFloorPlan * selectedPlan = drawnPlans[id];
		string drawnFilePath = selectedPlan->getFilePath(m_project, projectsWorkspaceFolder);

		QFile fileToDelete(drawnFilePath.c_str());
		fileToDelete.remove();
		m_project->removeDrawnPlan(id);
		m_project->saveProject(projectsWorkspaceFolder);

		QTimer::singleShot(20, [=] {
			InitializeForNewDrawnPlan();
			PopulatePlanListView();
		});
	}
}

void DrawnPlanListForm::on_definitionLineEdit_textChanged()
{
	bool success;
	QString definitionQStr = ui->definitionLineEdit->text();
	if (success)
	{
		const bool wasBlocked = m_drawnFloorPlan->blockSignals(true);
		// no signals here
		std::string str = definitionQStr.toStdString();
		const char* cstr = str.c_str();
		m_drawnFloorPlan->setDefinition(cstr);
		m_drawnFloorPlan->blockSignals(wasBlocked);
	}
}

void DrawnPlanListForm::on_definition_Changed(const char * value)
{
	ui->definitionLineEdit->setText(QString(value));
}
#include "Full3DBuildingMeshVisualizerForm.h"
#include "ui_Full3DBuildingMeshVisualizerForm.h"

#include "data\Project.h"

#include "Utilities\FileUtilities.h"

Full3DBuildingMeshVisualizerForm::Full3DBuildingMeshVisualizerForm(Project * iProject,
	BuildingInstance * iBuildingInstance, QWidget *parent) :
	QDialog(parent),
	ui(new Ui::Full3DBuildingMeshVisualizerForm)
{
	ui->setupUi(this);

	this->m_project = iProject;
	this->m_buildingInstance = iBuildingInstance;

	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");
}

Full3DBuildingMeshVisualizerForm::~Full3DBuildingMeshVisualizerForm()
{
	delete ui;
}

void Full3DBuildingMeshVisualizerForm::closeEvent(QCloseEvent *event)
{
	QDialog::closeEvent(event);
}

void Full3DBuildingMeshVisualizerForm::showEvent(QShowEvent *event)
{
	ui->q3DWidget->clearScene();
	//ui->q3DWidget->addDocument(m_document);
	ui->q3DWidget->addBuilding(m_project, m_buildingInstance);

	QDialog::showEvent(event);
}
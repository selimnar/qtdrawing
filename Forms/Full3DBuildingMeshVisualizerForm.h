#ifndef BUILDING_3D_MESH_VISUALIZER_FORM_H
#define BUILDING_3D_MESH_VISUALIZER_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class Project;
class BuildingInstance;

namespace Ui {
	class Full3DBuildingMeshVisualizerForm;
}

class Full3DBuildingMeshVisualizerForm : public QDialog
{
	Q_OBJECT
public:
	explicit Full3DBuildingMeshVisualizerForm(Project * iProject, BuildingInstance * iBuildingInstance,
		QWidget *parent = nullptr);
	~Full3DBuildingMeshVisualizerForm();

protected:
	//void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;
	void showEvent(QShowEvent *event) override;

private:
	Ui::Full3DBuildingMeshVisualizerForm *ui;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	BuildingInstance * m_buildingInstance;
};


#endif // BUILDING_3D_MESH_VISUALIZER_FORM_H
#ifndef SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H
#define SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsItem>

class ScannedFloorplanView;
class ScannedFloorplanGraphicsScene;
class Project;
class ArchitecturalPlan;

namespace Ui {
	class ScannedFloorDesignSelectorForm;
}

class ScannedFloorDesignSelectorForm : public QDialog
{
	Q_OBJECT
public:
	explicit ScannedFloorDesignSelectorForm(Project * iProject, QWidget *parent = nullptr);
	~ScannedFloorDesignSelectorForm();

	QPixmap pixmap();

protected:
	//void keyPressEvent(QKeyEvent * event) override;

private:
	Ui::ScannedFloorDesignSelectorForm *ui;

	ScannedFloorplanGraphicsScene *scene;

	QStringList tableHeaderLabels;
	// QStandardItemModel provides a classic 
	// item-based approach to working with the model.
	QStandardItemModel * plansTableViewModel;

	std::string projectsWorkspaceFolder;

	Project * m_project;
	ArchitecturalPlan * m_architecturalPlan;

	QPixmap m_pixmap;

	void Initialize();

private slots:
	void on_selectScannedPlanPushButton_pressed();
	void on_getSelectionPushButton_pressed();
	void on_showHideListPushButton_pressed();
};

#endif // SCANNED_FLOOR_DESIGN_SELECTOR_FORM_H
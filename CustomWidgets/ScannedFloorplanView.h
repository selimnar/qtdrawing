#ifndef SCANNED_FLOOR_PLAN_GRAPHICS_VIEW_H
#define SCANNED_FLOOR_PLAN_GRAPHICS_VIEW_H

#include <QApplication>
#include <QGraphicsView>
#include <QtGui>

class QMenu;

class ScannedFloorplanView : public QGraphicsView
{
	Q_OBJECT

public:
	ScannedFloorplanView(QWidget *parent = nullptr);
	~ScannedFloorplanView();

	qreal calculatedScale();

	void setScale(qreal scale);

protected:
	void wheelEvent(QWheelEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *mouseEvent) override;

private:
	const static qreal MinScale;
	const static qreal MaxScale;

	const static qreal ScaleAdder;

	qreal currentViewScale;
	bool m_doesWallsHaveWidth;

	void scaleView();

	QMenu *menu;
};

#endif // SCANNED_FLOOR_PLAN_GRAPHICS_VIEW_H


#ifndef ANIMATED_PUSH_BUTTON_H
#define ANIMATED_PUSH_BUTTON_H

#include <QPushButton>

#include "utilities/StaticConstructor.h"

class QPropertyAnimation;

class QAnimatedPushButton : public QPushButton
{
	Q_OBJECT
	Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)

public:
	QAnimatedPushButton(QWidget *parent = nullptr);
	~QAnimatedPushButton();

	void setBackgroundColor(QColor color);
	QColor backgroundColor();

	// Static Constructor:
	// (Should be called by INVOKE_STATIC_CONSTRUCTOR macro in the CPP file)
	STATIC_CONSTRUCTOR();

	// Static Destructor:
	// (Should be called by INVOKE_STATIC_CONSTRUCTOR macro in the CPP file)
	STATIC_DESTRUCTOR();

protected:

	void showEvent(QShowEvent *event) override;

	void mousePressEvent(QMouseEvent *e) override;
	bool event(QEvent *event) override;

	void hoverEnter(QHoverEvent *event);
	void hoverLeave(QHoverEvent *event);
	void hoverMove(QHoverEvent *event);

private:
	const static QColor normalBackcolor;
	const static QColor hoverBackcolor;
	const static QColor pressedBackcolor;
	const static QString cssTemplate;

	QColor currentBackgroundColor;
	QPropertyAnimation *animation;

	void colorTransitionAnimation(QColor startColor, QColor endColor, int msecs);
	void colorTransitionAnimation(QColor startColor, QColor midColor,
		QColor endColor, int msecs, qreal midKeyTime);
};

#endif // ANIMATED_PUSH_BUTTON_H


//#include <qmath.h>
#include <algorithm>

#include <QMouseEvent>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QMenu>


#include "FloorDesignItems/AbstractFloorDesignItem.h"

#include "FloorplanGraphicsView.h"

#include "utilities/utilities.h"

#include "CustomWidgets/RadialPopupMenu.h"
#include "CustomQObjects/FloorplanGraphicsScene.h"

#include "data/ActionData.h"

const qreal FloorplanGraphicsView::MinScale = -5.0;
const qreal FloorplanGraphicsView::MaxScale = 10;
const qreal FloorplanGraphicsView::ScaleAdder = 0.25;

FloorplanGraphicsView::FloorplanGraphicsView(QWidget *parent)
	: QGraphicsView(parent)
{
	m_doesWallsHaveWidth = true;

	menu = new QMenu(this);

	this->currentViewScale = -2;
	scaleView();

	setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);

	setMouseTracking(true);
	viewport()->setMouseTracking(true);

	setDragMode(QGraphicsView::ScrollHandDrag);
	viewport()->setCursor(Qt::ArrowCursor);
}

FloorplanGraphicsView::~FloorplanGraphicsView()
{
}

qreal FloorplanGraphicsView::calculatedScale()
{
	qreal qScale = (this->currentViewScale > 0) ? (qreal)this->currentViewScale : (1.0 / (qreal)-currentViewScale);
	return qScale;
}

qreal FloorplanGraphicsView::calculatedScaleForControlPoints()
{
	qreal qScale = 1;
	if (this->currentViewScale < -1.0)
	{
		qScale *= (this->currentViewScale < -1.0f) ? qAbs(this->currentViewScale) : 1;
	}
	else
	{
		//DEBUG_OUT("this->currentViewScale = %f", this->currentViewScale);
		qScale = (this->currentViewScale > 0) ? (qreal)this->currentViewScale : (1.0 / (qreal)-currentViewScale);
		//qScale = (qScale > 4) ? (1 / sqrt(qScale - 3)) : qScale;
		qScale = (qScale > 4) ? (1 / sqrt(qScale - 3)) : 1;
	}

	//qreal qScale = (this->currentViewScale > 0) ? (qreal)this->currentViewScale : (1.0 / (qreal)-currentViewScale);
	//qScale *= (this->currentViewScale < -1.0f) ? qAbs(this->currentViewScale) : 1;
	//qScale = (qScale > 4 && this->currentViewScale > 0) ? (1 / sqrt(qScale - 3)) : qScale;
	//DEBUG_OUT("this->currentViewScale = %f", this->currentViewScale);
	return qScale;
}

bool FloorplanGraphicsView::doesWallsHaveWidth()
{
	return m_doesWallsHaveWidth;
}

void FloorplanGraphicsView::setDoesWallsHaveWidth(bool val)
{
	m_doesWallsHaveWidth = val;
}

void FloorplanGraphicsView::setScale(qreal scale)
{
	this->currentViewScale = (scale > -1.25 && scale < 1) ? 1 : scale;
	this->currentViewScale = std::clamp(this->currentViewScale, MinScale, MaxScale);
}

void FloorplanGraphicsView::scaleView()
{
	//QTransform scaleMat;
	//scaleMat.reset();
	//scaleMat.scale((qreal)scale, (qreal)scale);
	//this->setTransform(scaleMat);
	QMatrix matrix;
	this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	qreal qScale = calculatedScale();
	matrix.scale(qScale, qScale);
	this->setMatrix(matrix);
	this->scale(1, 1);

	if (this->scene() != nullptr)
	{
		qreal sideControlScale = calculatedScaleForControlPoints();
		FloorplanGraphicsScene * scene = dynamic_cast<FloorplanGraphicsScene*>(this->scene());
		scene->resizeControlPointGraphicItems(sideControlScale);
	}

	//if (!doesWallsHaveWidth())
	//{
	//	FloorplanGraphicsScene * scene = dynamic_cast<FloorplanGraphicsScene*>(this->scene());
	//	if (scene != nullptr)
	//	{
	//		scene->scaleWallWidthDueToScale(1 / qScale);
	//	}
	//}
}

void FloorplanGraphicsView::wheelEvent(QWheelEvent *event)
{
	int delta = event->delta() / 120;

	if (delta < 0)
	{
		this->currentViewScale = (this->currentViewScale > -1.25 && this->currentViewScale <= 1) ?
			-1.25 : (this->currentViewScale + (delta * ScaleAdder));
	}
	else if (delta > 0)
	{
		this->currentViewScale = (this->currentViewScale >= -1.25 && this->currentViewScale < 1) ?
			1 : (this->currentViewScale + (delta * ScaleAdder));
	}
	this->currentViewScale = std::clamp(this->currentViewScale, MinScale, MaxScale);
	DEBUG_OUT("---this->currentViewScale = %f", this->currentViewScale);
	scaleView();
}

void FloorplanGraphicsView::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
	QGraphicsView::mouseReleaseEvent(mouseEvent);
	viewport()->setCursor(Qt::ArrowCursor);
}

void FloorplanGraphicsView::contextMenuEvent(QContextMenuEvent *event)
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());

	QPoint globalMousePos = event->globalPos(); // QCursor::pos()

	const QPoint origin = mapFromGlobal(globalMousePos);
	const QPointF relativeOrigin = mapToScene(origin);

	AbstractFloorDesignItem * afdi = scene->retrieveAbstractFloorItemAt(relativeOrigin);

	// clear previous action menu items
	QList<QAction*> previousActions = menu->actions();
	if (previousActions.size() > 0)
	{
		QAction* previousAction = previousActions[0];
		QVariant variantActionData = previousAction->data();
		ActionData* previousActionData = (ActionData*)variantActionData.value<void *>();
		delete previousActionData;
	}
	menu->clear();

	QList<QAction*> actions;
	if (afdi != nullptr)
	{
		actions = afdi->getContextMenuActions();
	}
	else
	{
		actions = this->getContextMenuActions();
	}

	if (actions.size() > 0)
	{
		QAction * action = actions[0];
		QVariant variantActionData = action->data();
		ActionData* actionData = (ActionData*)variantActionData.value<void *>();
		actionData->setScenePointerPos(relativeOrigin);
		actionData->setGlobalPointerPos(globalMousePos);
		actionData->setView(this);

		menu->addActions(actions);
		menu->popup(globalMousePos);
	}

	//RadialPopupMenu *popupMenu = new RadialPopupMenu(this);
	//popupMenu->popup(mousePos);
}


QList<QAction*> FloorplanGraphicsView::getContextMenuActions()
{
	QList<QAction*> actions;
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	if (scene->interactionMode() != InteractionMode::View)
	{
		return actions;
	}

	ActionData * actionData = nullptr;

	actionData = new ActionData();
	actionData->setItem(nullptr);
	QVariant variantActionData = qVariantFromValue((void *)actionData);
	if (scene->selectedWallCount() > 0)
	{
		QAction * removeSelectedWallsAction = new QAction("Sil");
		removeSelectedWallsAction->setData(variantActionData);
		this->connect(removeSelectedWallsAction, &QAction::triggered, this, &FloorplanGraphicsView::removeSelectedWallItems);

		actions.append(removeSelectedWallsAction);
	}

	QAction * registerPlanAction = new QAction("Plan ve Cizim Eslestir");
	registerPlanAction->setData(variantActionData);
	this->connect(registerPlanAction, &QAction::triggered, this, &FloorplanGraphicsView::planRegisterLineDraw);

	actions.append(registerPlanAction);

	QAction * centerDesignAction = new QAction("Plan Merkeze Al");
	centerDesignAction->setData(variantActionData);
	this->connect(centerDesignAction, &QAction::triggered, this, &FloorplanGraphicsView::centerPlan);

	actions.append(centerDesignAction);

	int selectedWallCount = scene->selectedWallCount();
	if (scene->doesPlanRegisterLineExists() && scene->doesBackgroundImageExists() && selectedWallCount == 1)
	{
		QAction * transformPlanAction = new QAction("Plan Yerlestir");
		transformPlanAction->setData(variantActionData);
		this->connect(transformPlanAction, &QAction::triggered, this, &FloorplanGraphicsView::affineTransformPlan);

		actions.append(transformPlanAction);
	}

	if (scene->doesBackgroundImageExists())
	{
		QAction * resetTransformPlanAction = new QAction("Plan Resetle");
		resetTransformPlanAction->setData(variantActionData);
		this->connect(resetTransformPlanAction, &QAction::triggered, this, &FloorplanGraphicsView::resetTransformPlan);

		actions.append(resetTransformPlanAction);
	}

	return actions;
}

void FloorplanGraphicsView::removeSelectedWallItems()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->deleteSelectedWalls();
}

void FloorplanGraphicsView::planRegisterLineDraw()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->setInteractionMode(InteractionMode::PlanRegisterLineSegmentAdd);
}

void FloorplanGraphicsView::centerPlan()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->centerDesign();
}

void FloorplanGraphicsView::affineTransformPlan()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->affineTransformPlan();
}

void FloorplanGraphicsView::resetTransformPlan()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->resetTransformPlan();
}
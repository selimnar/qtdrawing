#include "Q3DWidget.h"
#include <Qt3DExtras/qt3dwindow.h>

#include <QHBoxLayout>

#include <QtGui/QScreen>

#include <Qt3DInput/QInputAspect>

#include <Qt3DRender/qcamera.h>
#include <Qt3DRender/qcameralens.h>
#include <QtGui/QScreen>

#include <Qt3DExtras/qtorusmesh.h>
#include <Qt3DRender/qmesh.h>
#include <Qt3DRender/qtechnique.h>
#include <Qt3DRender/qparameter.h>
#include <Qt3DRender/qeffect.h>
#include <Qt3DRender/qfilterkey.h>
#include <Qt3DRender/qshaderprogrambuilder.h>
#include <Qt3DRender/qgraphicsapifilter.h>
#include <Qt3DRender/qtexture.h>
#include <Qt3DRender/qrenderpass.h>
#include <Qt3DRender/qsceneloader.h>
//#include <Qt3DRender/qpointlight.h>
#include <QPointLight>
#include <QDirectionalLight>

#include <QDepthTest>

#include <QLayerFilter>
#include <QLayer>
#include <QRenderSettings>
#include <QRenderSurfaceSelector>
#include <QViewport>
#include <QCameraSelector>
#include <QClearBuffers>

#include <Qt3DCore/qtransform.h>
#include <Qt3DCore/qaspectengine.h>

#include <Qt3DRender/qrenderaspect.h>
#include <Qt3DExtras/qforwardrenderer.h>

//#include <Qt3DExtras/qt3dwindow.h>
#include <Qt3DExtras/qfirstpersoncameracontroller.h>

#include <QMessageBox>

#include "../3DScene/scenemodifier.h"

#include "../utilities/utilities.h"
#include "../utilities/FileUtilities.h"

#include "../Data/WallData.h"

#include "../Data/FloorDesignDocument.h"

#include "../Data/Project.h"


Q3DWidget::Q3DWidget(QWidget *parent = nullptr) : QWidget(parent)
{
	projectsWorkspaceFolder = CONFIG("MIMARI_PROJELER_KLASORU");

	view = new Qt3DExtras::Qt3DWindow();
	view->defaultFrameGraph()->setClearColor(QColor(QRgb(0xF0F9FD)));
	QWidget *container = QWidget::createWindowContainer(view);
	QSize screenSize = view->screen()->size();
	container->setMinimumSize(QSize(200, 100));
	container->setMaximumSize(screenSize);

	QHBoxLayout *hLayout = new QHBoxLayout(this);
	this->setLayout(hLayout);
	QVBoxLayout *vLayout = new QVBoxLayout();
	vLayout->setAlignment(Qt::AlignTop);
	hLayout->addWidget(container, 1);
	hLayout->addLayout(vLayout);

	Qt3DInput::QInputAspect *input = new Qt3DInput::QInputAspect;
	view->registerAspect(input);

	// Root entity
	rootEntity = new Qt3DCore::QEntity();
	// Set root object of the scene
	view->setRootEntity(rootEntity);

	// Camera
	//Qt3DRender::QCamera *mainCamera = view->camera();
	Qt3DRender::QCamera *mainCamera = new Qt3DRender::QCamera(rootEntity);
	mainCamera->lens()->setPerspectiveProjection(45.0f, 16.0f / 9.0f, 0.1f, 10000.0f);
	mainCamera->setPosition(QVector3D(0, 20.0f, 40.0f) * 30);
	mainCamera->setUpVector(QVector3D(0, 1, 0));
	QQuaternion rot = QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0), 45);
	mainCamera->rotate(rot);
	mainCamera->setViewCenter(QVector3D(0, 0, 0));

	Qt3DRender::QCamera *backgroundCamera = new Qt3DRender::QCamera(rootEntity);
	backgroundCamera->lens()->setPerspectiveProjection(45.0f, 16.0f / 9.0f, 0.1f, 10000.0f);
	backgroundCamera->setPosition(QVector3D(0, 20.0f, 40.0f) * 30);
	backgroundCamera->setUpVector(QVector3D(0, 1, 0));
	backgroundCamera->rotate(rot);
	backgroundCamera->setViewCenter(QVector3D(0, 0, 0));

	// For camera controls
	Qt3DExtras::QFirstPersonCameraController *firstPersonCameraController = new Qt3DExtras::QFirstPersonCameraController(rootEntity);
	firstPersonCameraController->setAcceleration(10);
	firstPersonCameraController->setLinearSpeed(20 * 30);
	firstPersonCameraController->setCamera(mainCamera);


	Qt3DRender::QRenderSettings * renderSettings = view->renderSettings();
	Qt3DRender::QLayer* backgroundLayer;
	Qt3DRender::QLayer* sceneLayer;
	Qt3DRender::QTechniqueFilter * techniqueFilter = createWallSceneForwardRenderer(renderSettings, mainCamera, backgroundCamera, view,
		backgroundLayer, this->m_wallObjectsLayer);

	//Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(rootEntity);
	//Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
	//light->setColor("white");
	//light->setIntensity(1);
	//lightEntity->addComponent(light);
	//Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform(lightEntity);
	//lightTransform->setTranslation(mainCamera->position());
	//lightEntity->addComponent(lightTransform);
	Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(rootEntity);
	directionalLight = new Qt3DRender::QDirectionalLight(lightEntity);
	directionalLight->setWorldDirection(QVector3D(1, 0, 0));
	directionalLight->setColor("white");
	directionalLight->setIntensity(0.2);
	lightEntity->addComponent(directionalLight);
	lightEntity->addComponent(this->m_wallObjectsLayer);


	Qt3DCore::QEntity *sphereEntity = new Qt3DCore::QEntity(rootEntity);
	Qt3DExtras::QSphereMesh *sphereMesh = new Qt3DExtras::QSphereMesh;
	sphereMesh->setRadius(30);
	Qt3DCore::QTransform *sphereTransform = new Qt3DCore::QTransform;
	//sphereTransform->setTranslation(QVector3D(0, 20.0f, 40.0f));
	sphereTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));
	Qt3DRender::QMaterial *sphereMaterial = new Qt3DExtras::QPhongMaterial(rootEntity);
	sphereEntity->addComponent(sphereMesh);
	sphereEntity->addComponent(sphereTransform);
	sphereEntity->addComponent(sphereMaterial);
	sphereEntity->addComponent(this->m_wallObjectsLayer);


	wallObjectsSceneEntity = new Qt3DCore::QEntity(rootEntity);

	createBackgroundGradientPlane(backgroundLayer);
}

Qt3DRender::QTechniqueFilter * Q3DWidget::createWallSceneForwardRenderer(Qt3DRender::QRenderSettings * renderSettings,
	Qt3DRender::QCamera * mainCamera, Qt3DRender::QCamera * backgroundCamera, QObject * surface,
	Qt3DRender::QLayer* &backgroundLayer, Qt3DRender::QLayer* &wallObjectsLayer)
{
	Qt3DRender::QTechniqueFilter * techniqueFilter = new Qt3DRender::QTechniqueFilter(renderSettings);
	renderSettings->setActiveFrameGraph(techniqueFilter);

	////////////////////// SETUP LAYERS: BEGIN //////////////////////
	backgroundLayer = new Qt3DRender::QLayer(techniqueFilter);
	backgroundLayer->setRecursive(true);
	wallObjectsLayer = new Qt3DRender::QLayer(techniqueFilter);
	wallObjectsLayer->setRecursive(true);
	////////////////////// SETUP LAYERS: END //////////////////////

	Qt3DRender::QFilterKey * filterKey = new Qt3DRender::QFilterKey(techniqueFilter);
	filterKey->setName(QStringLiteral("renderingStyle"));
	filterKey->setValue(QStringLiteral("forward"));
	techniqueFilter->addMatch(filterKey);

	Qt3DRender::QRenderSurfaceSelector * surfaceSelector = new Qt3DRender::QRenderSurfaceSelector(techniqueFilter);
	Qt3DRender::QViewport * viewport = new Qt3DRender::QViewport(surfaceSelector);
	viewport->setNormalizedRect(QRect(0.0, 0.0, 1.0, 1.0));
	surfaceSelector->setSurface(surface);



	Qt3DRender::QClearBuffers * clearBuffers = new Qt3DRender::QClearBuffers(viewport);
	clearBuffers->setBuffers(Qt3DRender::QClearBuffers::BufferType::ColorDepthBuffer);

	Qt3DRender::QCameraSelector * backgroundCameraSelector = new Qt3DRender::QCameraSelector(viewport);
	backgroundCameraSelector->setCamera(backgroundCamera);

	Qt3DRender::QLayerFilter *backgroundLayerFilter = new Qt3DRender::QLayerFilter(backgroundCameraSelector);
	backgroundLayerFilter->addLayer(backgroundLayer);

	clearBuffers = new Qt3DRender::QClearBuffers(backgroundCameraSelector);
	clearBuffers->setBuffers(Qt3DRender::QClearBuffers::BufferType::DepthBuffer);




	Qt3DRender::QCameraSelector * cameraSelector = new Qt3DRender::QCameraSelector(clearBuffers);
	cameraSelector->setCamera(mainCamera);

	Qt3DRender::QLayerFilter *wallsLayerFilter = new Qt3DRender::QLayerFilter(cameraSelector);
	wallsLayerFilter->addLayer(wallObjectsLayer);

	return techniqueFilter;
}

void Q3DWidget::createBackgroundGradientPlane(Qt3DRender::QLayer* backgroundLayer)
{
	/////////////////////////////////////////////////////
	// Plane
	Qt3DCore::QEntity * backgroundPlaneEntity = new Qt3DCore::QEntity(rootEntity);

	// Plane shape data
	Qt3DExtras::QPlaneMesh *planeMesh = new Qt3DExtras::QPlaneMesh();
	planeMesh->setWidth(2);
	planeMesh->setHeight(2);
	planeMesh->setMeshResolution(QSize(2, 2));

	// Plane mesh transform
	Qt3DCore::QTransform *planeTransform = new Qt3DCore::QTransform();
	planeTransform->setScale(1.0f);
	planeTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 90.0f));
	planeTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	//Qt3DRender::QMaterial *planeMaterial = createGL3PhongMaterial();
	Qt3DRender::QMaterial *planeMaterial = createGL3GradientBackgroundMaterial(backgroundPlaneEntity);

	backgroundPlaneEntity->addComponent(planeMesh);
	backgroundPlaneEntity->addComponent(planeMaterial);
	backgroundPlaneEntity->addComponent(planeTransform);
	backgroundPlaneEntity->addComponent(backgroundLayer);
	/////////////////////////////////////////////////////
}

Qt3DRender::QMaterial * Q3DWidget::createGL3PhongMaterial()
{
	Qt3DRender::QMaterial *material = new Qt3DRender::QMaterial();

	Qt3DRender::QFilterKey * filterKey = new Qt3DRender::QFilterKey();

	Qt3DRender::QParameter * ambientParameter = new Qt3DRender::QParameter(QStringLiteral("ka"), QColor::fromRgbF(0.05f, 0.05f, 0.05f, 1.0f));
	Qt3DRender::QParameter * diffuseParameter = new Qt3DRender::QParameter(QStringLiteral("kd"), QColor::fromRgbF(0.7f, 0.07f, 0.07f, 1.0f));
	Qt3DRender::QParameter * specularParameter = new Qt3DRender::QParameter(QStringLiteral("ks"), QColor::fromRgbF(0.01f, 0.01f, 0.01f, 1.0f));
	Qt3DRender::QParameter * shininessParameter = new Qt3DRender::QParameter(QStringLiteral("shininess"), 150.0f);


	Qt3DRender::QEffect *effect = new Qt3DRender::QEffect();
	// Create technique, render pass and shader
	Qt3DRender::QTechnique *gl3Technique = new Qt3DRender::QTechnique();
	Qt3DRender::QShaderProgram *glShader = new Qt3DRender::QShaderProgram();
	Qt3DRender::QRenderPass *gl3Pass = new Qt3DRender::QRenderPass();


	//glShader->setFragmentShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shader/shaders/gradient_background.frag"))));
	//QByteArray fragmentShader = Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shader/shaders/gl3/background.frag")));
	//int cnt = fragmentShader.count();
	//glShader->setFragmentShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shader/shaders/gl3/background.frag"))));
	//glShader->setVertexShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shader/shaders/gl3/background.vert"))));

	Qt3DRender::QShaderProgramBuilder * phongGL3ShaderBuilder = new Qt3DRender::QShaderProgramBuilder();
	glShader->setVertexShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shaders/gl3/default.vert"))));
	glShader->setFragmentShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shaders/gl3/phong.frag"))));
	phongGL3ShaderBuilder->setParent(material);
	phongGL3ShaderBuilder->setShaderProgram(glShader);
	phongGL3ShaderBuilder->setFragmentShaderGraph(QUrl(QStringLiteral("qrc:/shaders/graphs/phong.frag.json")));
	phongGL3ShaderBuilder->setEnabledLayers({ QStringLiteral("diffuse"),
		QStringLiteral("specular"),
		QStringLiteral("normal") });


	// Set the shader on the render pass
	gl3Pass->setShaderProgram(glShader);

	// Add the pass to the technique
	gl3Technique->addRenderPass(gl3Pass);

	// Set the targeted GL version for the technique
	gl3Technique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
	gl3Technique->graphicsApiFilter()->setMajorVersion(3);
	gl3Technique->graphicsApiFilter()->setMinorVersion(1);
	gl3Technique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);

	filterKey->setParent(material);
	filterKey->setName(QStringLiteral("renderingStyle"));
	filterKey->setValue(QStringLiteral("forward"));
	gl3Technique->addFilterKey(filterKey);

	// Add the technique to the effect
	effect->addTechnique(gl3Technique);

	effect->addParameter(ambientParameter);
	effect->addParameter(diffuseParameter);
	effect->addParameter(specularParameter);
	effect->addParameter(shininessParameter);

	material->setEffect(effect);

	return material;
}

Qt3DRender::QMaterial * Q3DWidget::createGL3GradientBackgroundMaterial(Qt3DCore::QEntity * backgroundPlaneEntity)
{
	Qt3DRender::QMaterial *material = new Qt3DRender::QMaterial(backgroundPlaneEntity);

	Qt3DRender::QEffect *effect = new Qt3DRender::QEffect(material);

	Qt3DRender::QFilterKey * filterKey = new Qt3DRender::QFilterKey(effect);

	// Create technique, render pass and shader
	Qt3DRender::QTechnique *gl3Technique = new Qt3DRender::QTechnique();
	Qt3DRender::QShaderProgram *glShader = new Qt3DRender::QShaderProgram();
	Qt3DRender::QRenderPass *gl3Pass = new Qt3DRender::QRenderPass();


	glShader->setFragmentShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shaders/gl3/background.frag"))));
	glShader->setVertexShaderCode(Qt3DRender::QShaderProgram::loadSource(QUrl(QStringLiteral("qrc:/shaders/gl3/background.vert"))));

	Qt3DRender::QShaderProgramBuilder * phongGL3ShaderBuilder = new Qt3DRender::QShaderProgramBuilder();
	phongGL3ShaderBuilder->setParent(material);
	phongGL3ShaderBuilder->setShaderProgram(glShader);

	// Set the shader on the render pass
	gl3Pass->setShaderProgram(glShader);

	Qt3DRender::QDepthTest * depthTest = new Qt3DRender::QDepthTest(material);
	depthTest->setDepthFunction(Qt3DRender::QDepthTest::DepthFunction::Always);
	gl3Pass->addRenderState(depthTest);

	// Add the pass to the technique
	gl3Technique->addRenderPass(gl3Pass);

	// Set the targeted GL version for the technique
	gl3Technique->graphicsApiFilter()->setApi(Qt3DRender::QGraphicsApiFilter::OpenGL);
	gl3Technique->graphicsApiFilter()->setMajorVersion(3);
	gl3Technique->graphicsApiFilter()->setMinorVersion(1);
	gl3Technique->graphicsApiFilter()->setProfile(Qt3DRender::QGraphicsApiFilter::CoreProfile);

	filterKey->setParent(material);
	filterKey->setName(QStringLiteral("renderingStyle"));
	filterKey->setValue(QStringLiteral("forward"));
	gl3Technique->addFilterKey(filterKey);

	// Add the technique to the effect
	effect->addTechnique(gl3Technique);

	//qreal hue = 0.0;
	//Qt3DRender::QParameter * color1Parameter = new Qt3DRender::QParameter(QStringLiteral("color1"), QColor::fromHslF(fmod(hue + 0.59, 1.0), 0.53, 0.59));
	//Qt3DRender::QParameter * color2Parameter = new Qt3DRender::QParameter(QStringLiteral("color2"), QColor::fromHslF(fmod(hue + 0.59, 1.0), 1.0, 0.15));
	Qt3DRender::QParameter * color1Parameter = new Qt3DRender::QParameter(QStringLiteral("color1"), QColor::fromRgb(0xC8C6FF));
	Qt3DRender::QParameter * color2Parameter = new Qt3DRender::QParameter(QStringLiteral("color2"), QColor::fromRgb(0xFFF4E9));
	effect->addParameter(color1Parameter);
	effect->addParameter(color2Parameter);

	material->setEffect(effect);

	return material;
}

Q3DWidget::~Q3DWidget()
{

}

void Q3DWidget::clearScene()
{
	if (this->wallObjectsSceneEntity != nullptr)
	{
		//DEBUG_OUT("!!!!!!!!!! sceneEntity = %d", this->sceneEntity->id());
		delete(this->wallObjectsSceneEntity);
		this->wallObjectsSceneEntity == nullptr;

		wallObjectsSceneEntity = new Qt3DCore::QEntity(rootEntity);
	}
}

void Q3DWidget::addDocument(FloorDesignDocument * document)
{
	if (this->wallObjectsSceneEntity == nullptr)
	{
		return;
	}

	SceneModifier *modifier = new SceneModifier(wallObjectsSceneEntity);
	int wallCount = document->wallCount();
	for (int i = 0; i < wallCount; i++)
	{
		WallData * wallData = document->wallAt(i);
		Qt3DCore::QEntity * wallEntity = modifier->generate3DWall(wallData, wallObjectsSceneEntity);
		wallEntity->addComponent(m_wallObjectsLayer);
	}

	Qt3DCore::QEntity * roofEntity = modifier->generate3DRoof(document, wallObjectsSceneEntity);
	//roofEntity->addComponent(m_wallObjectsLayer);
}

void Q3DWidget::addBuilding(Project * project, BuildingInstance * buildingInstance)
{
	if (this->wallObjectsSceneEntity == nullptr)
	{
		return;
	}

	SceneModifier *modifier = new SceneModifier(wallObjectsSceneEntity);

	std::map<unsigned int, DrawnFloorPlan*> drawnPlans = project->drawnPlans();
	const std::map<unsigned int, AssignedFloorInstance*> assignedFloorPlans = buildingInstance->assignedFloorPlans();
	float heightOffset = 0;
	for (auto pair : assignedFloorPlans)
	{
		AssignedFloorInstance * assignedFloorInstance = pair.second;
		unsigned int floorId = assignedFloorInstance->floorId();
		std::map<unsigned int, DrawnFloorPlan*>::iterator mapEntryPosition = drawnPlans.find(floorId);
		if (mapEntryPosition == drawnPlans.end())
		{
			QMessageBox Msgbox;
			Msgbox.setText("Bu kat plani mevcut degil");
			Msgbox.exec();
			continue;
		}
		DrawnFloorPlan* drawnFloorPlan = drawnPlans.at(floorId);
		std::string drawnPlanFilePath = drawnFloorPlan->getFilePath(project, projectsWorkspaceFolder);

		FloorDesignDocument * document = nullptr;
		std::ifstream ifs(drawnPlanFilePath.c_str(), std::ios::binary | std::ios::in);
		if (ifs.is_open())
		{
			DEBUG_OUT("!!!! opened fileName = %s", drawnPlanFilePath.c_str());
			document = new FloorDesignDocument();
			document->deserialize(ifs);
			ifs.close();
		}
		if (document != nullptr)
		{
			unsigned int floorCount = assignedFloorInstance->count();
			Qt3DCore::QEntity * buildingBlockEntity = modifier->generate3DBlock(document, wallObjectsSceneEntity, heightOffset, floorCount);
			buildingBlockEntity->addComponent(m_wallObjectsLayer);
		}
	}
}

void Q3DWidget::setLightAngle(qreal angle)
{
	QVector3D rightVector(1, 0, 0);
	QQuaternion rotationAroundUpQuaternion = QQuaternion::fromAxisAndAngle(QVector3D(0, 1, 0),  angle);
	QVector3D worldLightDirection = rotationAroundUpQuaternion * rightVector;
	//DEBUG_OUT("!!!! angle: %f ; light direction(%f, %f, %f)", angle, worldLightDirection.x(), worldLightDirection.y(), worldLightDirection.z());
	directionalLight->setWorldDirection(worldLightDirection);
}
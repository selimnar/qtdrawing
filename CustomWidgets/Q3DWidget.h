#ifndef Q3D_WIDGET_H
#define Q3D_WIDGET_H

#include <QWidget>

class Project;
class BuildingInstance;
class WallData;
class FloorDesignDocument;

namespace Qt3DRender
{
	class QLayer;
	class QTechniqueFilter;
	class QMaterial;
	class QRenderSettings;
	class QCamera;
	class QDirectionalLight;
}

namespace Qt3DExtras
{
	class Qt3DWindow;
}

namespace Qt3DCore
{
	class QEntity;
	class QTransform;
}

class Q3DWidget : public QWidget
{
	Q_OBJECT

public:
	Q3DWidget(QWidget *parent);
	~Q3DWidget();

	void clearScene();

	void addDocument(FloorDesignDocument * document);
	void addBuilding(Project * project, BuildingInstance * buildingInstance);
	void setLightAngle(qreal angle);

protected:

private:
	std::string projectsWorkspaceFolder;

	Qt3DExtras::Qt3DWindow *view;
	Qt3DCore::QEntity *wallObjectsSceneEntity;
	Qt3DCore::QEntity *rootEntity;
	Qt3DRender::QDirectionalLight *directionalLight;

	Qt3DRender::QLayer * m_wallObjectsLayer;

	static Qt3DRender::QMaterial * createGL3PhongMaterial();
	static Qt3DRender::QMaterial * createGL3GradientBackgroundMaterial(Qt3DCore::QEntity * backgroundPlaneEntity);

	Qt3DRender::QTechniqueFilter * createWallSceneForwardRenderer(Qt3DRender::QRenderSettings * renderSettings, Qt3DRender::QCamera * mainCamera,
		Qt3DRender::QCamera * backgroundCamera, QObject * surface, Qt3DRender::QLayer* &backgroundLayer, Qt3DRender::QLayer* &wallObjectsLayer);

	void createBackgroundGradientPlane(Qt3DRender::QLayer* backgroundLayer);
};

#endif // Q3D_WIDGET_H


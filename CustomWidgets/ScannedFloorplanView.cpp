//#include <qmath.h>
#include <algorithm>

#include <QMouseEvent>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QPointF>
#include <QMenu>


#include "FloorDesignItems/AbstractFloorDesignItem.h"

#include "ScannedFloorplanView.h"

#include "utilities/utilities.h"

#include "CustomWidgets/RadialPopupMenu.h"
#include "CustomQObjects/FloorplanGraphicsScene.h"

#include "data/ActionData.h"

const qreal ScannedFloorplanView::MinScale = -15.0;
const qreal ScannedFloorplanView::MaxScale = 10;
const qreal ScannedFloorplanView::ScaleAdder = 0.25;

ScannedFloorplanView::ScannedFloorplanView(QWidget *parent)
	: QGraphicsView(parent)
{
	m_doesWallsHaveWidth = true;

	menu = new QMenu(this);

	this->currentViewScale = -6;
	scaleView();

	setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);

	setMouseTracking(true);
	viewport()->setMouseTracking(true);

	//setDragMode(QGraphicsView::RubberBandDrag);
	setDragMode(QGraphicsView::ScrollHandDrag);
	viewport()->setCursor(Qt::ArrowCursor);
	setBackgroundBrush(QBrush(Qt::lightGray));
}

ScannedFloorplanView::~ScannedFloorplanView()
{
}

qreal ScannedFloorplanView::calculatedScale()
{
	qreal qScale = (this->currentViewScale > 0) ? (qreal)this->currentViewScale : (1.0 / (qreal)-currentViewScale);
	return qScale;
}

void ScannedFloorplanView::setScale(qreal scale)
{
	this->currentViewScale = (scale > -1.25 && scale < 1) ? 1 : scale;
	this->currentViewScale = std::clamp(this->currentViewScale, MinScale, MaxScale);
}

void ScannedFloorplanView::scaleView()
{
	QMatrix matrix;
	this->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	qreal qScale = calculatedScale();
	matrix.scale(qScale, qScale);
	this->setMatrix(matrix);
	this->scale(1, 1);
}

void ScannedFloorplanView::wheelEvent(QWheelEvent *event)
{
	if (event->modifiers().testFlag(Qt::ControlModifier))
	{
		int delta = event->delta() / 120;

		if (delta < 0)
		{
			this->currentViewScale = (this->currentViewScale > -1.25 && this->currentViewScale <= 1) ?
				-1.25 : (this->currentViewScale + (delta * ScaleAdder));
		}
		else if (delta > 0)
		{
			this->currentViewScale = (this->currentViewScale >= -1.25 && this->currentViewScale < 1) ?
				1 : (this->currentViewScale + (delta * ScaleAdder));
		}
		this->currentViewScale = std::clamp(this->currentViewScale, MinScale, MaxScale);
		scaleView();
	}
	else {
		QGraphicsView::wheelEvent(event);
	}
}

void ScannedFloorplanView::mouseReleaseEvent(QMouseEvent *mouseEvent)
{
	QGraphicsView::mouseReleaseEvent(mouseEvent);
	viewport()->setCursor(Qt::ArrowCursor);
}
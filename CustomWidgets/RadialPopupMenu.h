#ifndef RADIAL_POPUP_MENU_H
#define RADIAL_POPUP_MENU_H

#include <QWidget>
#include <QPoint>

class RadialPopupMenu : public QWidget
{
public:
	RadialPopupMenu(QWidget *parent = nullptr);
	~RadialPopupMenu();
	void popup(const QPoint &pos);

protected:
	bool eventFilter(QObject *obj, QEvent *ev) override;

private:
	void CreateActionButtons();
};

#endif // WALL_ITEM_H
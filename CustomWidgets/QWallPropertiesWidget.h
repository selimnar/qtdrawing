#ifndef Q_WALL_PROPERTIES_WIDGET_H
#define Q_WALL_PROPERTIES_WIDGET_H

#include <QWidget>
#include <vector>

class WallSegmentItem;
class WallData;

namespace Ui {
	class WallPropertiesWidget;
} // namespace Ui

class QWallPropertiesWidget : public QWidget
{
	Q_OBJECT

Q_SIGNALS:
	void wallSegmentItemPropertiesModifiedViaUIPanel(WallData *);

public:
	explicit QWallPropertiesWidget(QWidget *parent);
	~QWallPropertiesWidget();

	WallSegmentItem* selectedWallSegmentItem();
	void setSelectedWallSegmentItems(const std::vector<WallSegmentItem*> &selectedWallSegmentItems);
	void setSelectedWallSegmentItem(WallSegmentItem* selectedWallSegmentItem);
	void setSelectedWallSegmentItem(WallSegmentItem* selectedWallSegmentItem, bool doSmthg);
	void updateUIWidgetsDueToWallSegmentItem();

protected:

private:
	static const int MinWallWidth;
	static const int MaxWallWidth;
	static const int MinWallHeight;
	static const int MaxWallHeight;

	int tempWidth;
	int tempHeight;

	Ui::WallPropertiesWidget *ui;

	std::vector<WallSegmentItem*> m_selectedWallSegmentItems;
	WallSegmentItem* m_selectedWallSegmentItem;

	void setWidthOfTheSelectedWalls(float wallWidth);
	void setHeightOfTheSelectedWalls(float wallHeight);

private slots:
	void on_wallWidthSlider_valueChanged(int value);
	void on_wallWidthLineEdit_textChanged();

	void on_wallHeightSlider_valueChanged(int value);
	void on_wallHeightLineEdit_textChanged();

	void on_applyParametersToWallButton_pressed();

	void emitWallSegmentItemPropertiesModifiedViaUIPanel();
};

#endif // Q_WALL_PROPERTIES_WIDGET_H
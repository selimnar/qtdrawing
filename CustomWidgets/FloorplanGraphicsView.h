#ifndef FLOOR_PLAN_GRAPHICS_VIEW_H
#define FLOOR_PLAN_GRAPHICS_VIEW_H

#include <QApplication>
#include <QGraphicsView>
#include <QtGui>

class QMenu;

class FloorplanGraphicsView : public QGraphicsView
{
	Q_OBJECT

public:
	FloorplanGraphicsView(QWidget *parent = nullptr);
	~FloorplanGraphicsView();

	qreal calculatedScale();
	qreal calculatedScaleForControlPoints();

	bool doesWallsHaveWidth();
	void setDoesWallsHaveWidth(bool);
	void setScale(qreal scale);

protected:
	void wheelEvent(QWheelEvent *event) override;
	void mouseReleaseEvent(QMouseEvent *mouseEvent) override;
	void contextMenuEvent(QContextMenuEvent *event) override;

private:
	const static qreal MinScale;
	const static qreal MaxScale;
	const static qreal ScaleAdder;

	qreal currentViewScale;
	bool m_doesWallsHaveWidth;

	void scaleView();
	QList<QAction*> getContextMenuActions();
	void removeSelectedWallItems();
	void planRegisterLineDraw();
	void centerPlan();
	void affineTransformPlan();
	void resetTransformPlan();

	QMenu *menu;
};

#endif // FLOOR_PLAN_GRAPHICS_VIEW_H


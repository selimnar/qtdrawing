#include "RadialPopupMenu.h"

#include <QPushButton>
#include <QLayout>
#include <QMouseEvent>
#include <QApplication>

#include "utilities/utilities.h"


RadialPopupMenu::RadialPopupMenu(QWidget *parent)
	: QWidget(parent)
{
	Qt::WindowFlags flags = Qt::FramelessWindowHint | Qt::Tool;
	setWindowFlags(flags);
	//setAttribute(Qt::WA_DeleteOnClose, false);
	setAttribute(Qt::WA_TransparentForMouseEvents, false);

	setAttribute(Qt::WA_TranslucentBackground, true);
	
	//installEventFilter(this);
	//qApp->activeWindow()->installEventFilter(this);
	qApp->installEventFilter(this);
}


RadialPopupMenu::~RadialPopupMenu()
{
}

void RadialPopupMenu::CreateActionButtons()
{
	//qWarning("MySpecialPopup::exec: Recursive call detected");
	QPushButton * menuActionButton = new QPushButton(this);
	menuActionButton->setText("Deneme");
	menuActionButton->move(QPoint(200, 100));

	menuActionButton = new QPushButton(this);
	menuActionButton->setText("Deneme 2");
	menuActionButton->move(QPoint(200, 200));
	//layout()->addWidget(menuActionButton);
}

void RadialPopupMenu::popup(const QPoint &pos)
{
	CreateActionButtons();

	//QPoint popupPosition = pos;
	//if (parentWidget() != nullptr)
	//{
	//	popupPosition = parentWidget()->mapFromGlobal(pos);
	//	//popupPosition = parentWidget()->mapToParent(pos);
	//}
	//move(popupPosition);
	move(pos);
	show();
}

int counter = 0;
bool RadialPopupMenu::eventFilter(QObject *obj, QEvent *event)
{
	//if (!_dialogIsOpen)
	//	return QWidget::eventFilter(obj, event);

	//if (event->type() == QEvent::MouseButtonPress && obj != this && !findChildren<QObject *>().contains(obj))
	if (event->type() == QEvent::MouseButtonPress)
	{
		QMouseEvent* mEvent = (QMouseEvent*)event;
		QPoint mousePos = mEvent->pos();
		QPoint globalMousePos = mEvent->globalPos();
		//DEBUG_OUT("RadialPopupMenu::eventFilter : isSelf = %s ; isChild = %s ; pos(%d, %d)", ((obj != this) ? "FALSE" : "TRUE"),
		//	(!findChildren<QObject *>().contains(obj) ? "FALSE" : "TRUE"),
		//	mousePos.x(), mousePos.y());
		//DEBUG_OUT("RadialPopupMenu::eventFilter : isSelf = %s ; isChild = %s ; globalMousePos(%d, %d)", ((obj != this) ? "FALSE" : "TRUE"),
		//	(!findChildren<QObject *>().contains(obj) ? "FALSE" : "TRUE"),
		//	globalMousePos.x(), globalMousePos.y());

		bool doesMouseHitAnyButton = false;
		QList<QWidget*> lstChildren = findChildren<QWidget*>();
		foreach(QWidget* pWidget, lstChildren)
		{
			QRect widgetRect = pWidget->geometry();
			QPoint rectPos(widgetRect.x(), widgetRect.y());
			QPoint globalRectPos = mapToGlobal(rectPos);
			widgetRect = QRect(globalRectPos.x(), globalRectPos.y(), widgetRect.width(), widgetRect.height());
			if (widgetRect.contains(globalMousePos))
			{
				doesMouseHitAnyButton = true;
				break;
			}
			//widgetRect.setX(globalRectPos.x());
			//widgetRect.setY(globalRectPos.y());
			//DEBUG_OUT("RadialPopupMenu::eventFilter : widgetRect(%d, %d, %d, %d)", widgetRect.x(), widgetRect.y(),
			//	widgetRect.width(), widgetRect.height());
			//pWidget->rect().contains()
			//QPushButton *button = dynamic_cast<QPushButton *>(pWidget);
			//if (button != nullptr)
			//{
			//	DEBUG_OUT("RadialPopupMenu::eventFilter (%s)", button->text().toStdString().c_str());
			//}
		}
		//DEBUG_OUT("RadialPopupMenu::eventFilter : doesMouseHitAnyButton = %s", !doesMouseHitAnyButton ? "FALSE" : "TRUE");
		if (!doesMouseHitAnyButton)
		{
			this->close();
			return QWidget::eventFilter(obj, event);
		}
	}

	//return false;
	//event->ignore();
	return QWidget::eventFilter(obj, event);
}

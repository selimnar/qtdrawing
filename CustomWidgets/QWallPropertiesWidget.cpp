#include "QWallPropertiesWidget.h"

#include <QRegExpValidator>
#include <QIntValidator>

#include "ui_WallPropertiesWidget.h"

#include "FloorDesignItems\WallSegmentItem.h"

#include "utilities/utilities.h"

const int QWallPropertiesWidget::MinWallWidth = 5;
const int QWallPropertiesWidget::MaxWallWidth = 200;

const int QWallPropertiesWidget::MinWallHeight = 0;
const int QWallPropertiesWidget::MaxWallHeight = 1000;

QWallPropertiesWidget::QWallPropertiesWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::WallPropertiesWidget)
{
	ui->setupUi(this);

	//ui->wallWidthLineEdit->setValidator(new QRegExpValidator(QRegExp("[0-9]*"), ui->wallWidthLineEdit));
	ui->wallWidthLineEdit->setValidator(new QIntValidator(MinWallWidth, MaxWallWidth, ui->wallWidthLineEdit));
	ui->wallHeightLineEdit->setValidator(new QIntValidator(MinWallHeight, MaxWallHeight, ui->wallHeightLineEdit));
	//ui->wallWidthLineEdit->setValidator(new QRegExpValidator(QRegExp("\s*[0-9]*\s*(cm)\s*")));

	m_selectedWallSegmentItem = nullptr;

	tempWidth = -1;
	tempHeight = -1;

	ui->wallWidthSlider->setMinimum(MinWallWidth);
	ui->wallWidthSlider->setMaximum(MaxWallWidth);
	ui->wallHeightSlider->setMinimum(MinWallHeight);
	ui->wallHeightSlider->setMaximum(MaxWallHeight);

	connect(ui->wallWidthSlider, SIGNAL(valueChanged(int)), this, SLOT(on_wallWidthSlider_valueChanged(int)));
	connect(ui->wallWidthLineEdit, SIGNAL(textChanged()), this, SLOT(on_wallWidthLineEdit_textChanged()));

	connect(ui->wallHeightSlider, SIGNAL(valueChanged(int)), this, SLOT(on_wallHeightSlider_valueChanged(int)));
	connect(ui->wallHeightLineEdit, SIGNAL(textChanged()), this, SLOT(on_wallHeightLineEdit_textChanged()));
}

QWallPropertiesWidget::~QWallPropertiesWidget()
{
	delete ui;
}

WallSegmentItem * QWallPropertiesWidget::selectedWallSegmentItem()
{
	return m_selectedWallSegmentItem;
}

void QWallPropertiesWidget::setSelectedWallSegmentItems(const std::vector<WallSegmentItem*> &selectedWallSegmentItems)
{
	m_selectedWallSegmentItems.clear();
	m_selectedWallSegmentItems.insert(m_selectedWallSegmentItems.end(), selectedWallSegmentItems.begin(), selectedWallSegmentItems.end());
}

void QWallPropertiesWidget::setSelectedWallSegmentItem(WallSegmentItem * selectedWallSegmentItem)
{
	setSelectedWallSegmentItem(selectedWallSegmentItem, true);
}

void QWallPropertiesWidget::setSelectedWallSegmentItem(WallSegmentItem * selectedWallSegmentItem, bool doSmthg)
{
	m_selectedWallSegmentItems.clear();

	if (!doSmthg)
	{
		m_selectedWallSegmentItem = nullptr;
		return;
	}

	if (selectedWallSegmentItem != nullptr)
	{
		m_selectedWallSegmentItems.push_back(selectedWallSegmentItem);
		tempWidth = selectedWallSegmentItem->width();
		tempHeight = selectedWallSegmentItem->height();
	}
	// if selected wall is changed then emit wall parameter modification signal
	if (m_selectedWallSegmentItem != nullptr && m_selectedWallSegmentItem != selectedWallSegmentItem)
	{
		emitWallSegmentItemPropertiesModifiedViaUIPanel();
	}
	m_selectedWallSegmentItem = selectedWallSegmentItem;
}

void QWallPropertiesWidget::on_wallWidthSlider_valueChanged(int value)
{
	//float fv = (float)value;
	//float fv2 = (float)ui->wallWidthSlider->value();
	//DEBUG_OUT("QWallPropertiesWidget::on_wallWidth_valueChanged %f - %f", fv, fv2);

	const bool wasBlocked = ui->wallWidthLineEdit->blockSignals(true);
	// no signals here
	ui->wallWidthLineEdit->setText(QString::number(value, 'i', 0));
	ui->wallWidthLineEdit->blockSignals(wasBlocked);

	setWidthOfTheSelectedWalls(value);
}

void QWallPropertiesWidget::on_wallWidthLineEdit_textChanged()
{
	bool success;
	int value = ui->wallWidthLineEdit->text().split(" ")[0].toInt(&success);
	if (success)
	{
		const bool wasBlocked = ui->wallWidthSlider->blockSignals(true);
		// no signals here
		ui->wallWidthSlider->setValue(value);
		ui->wallWidthSlider->blockSignals(wasBlocked);

		setWidthOfTheSelectedWalls(value);
	}
	//DEBUG_OUT("QWallPropertiesWidget::on_wallWidthTextEdit_valueChanged %s - %d",
	//	ui->wallWidthLineEdit->text().toStdString().c_str(), value);
}

void QWallPropertiesWidget::on_wallHeightSlider_valueChanged(int value)
{
	const bool wasBlocked = ui->wallHeightLineEdit->blockSignals(true);
	// no signals here
	ui->wallHeightLineEdit->setText(QString::number(value, 'i', 0));
	ui->wallHeightLineEdit->blockSignals(wasBlocked);

	setHeightOfTheSelectedWalls(value);
}

void QWallPropertiesWidget::on_wallHeightLineEdit_textChanged()
{
	bool success;
	int value = ui->wallHeightLineEdit->text().split(" ")[0].toInt(&success);
	if (success)
	{
		const bool wasBlocked = ui->wallHeightSlider->blockSignals(true);
		// no signals here
		ui->wallHeightSlider->setValue(value);
		ui->wallHeightSlider->blockSignals(wasBlocked);

		setHeightOfTheSelectedWalls(value);
	}
}

void QWallPropertiesWidget::setWidthOfTheSelectedWalls(float wallWidth)
{
	for (WallSegmentItem * wsi : m_selectedWallSegmentItems)
	{
		wsi->data()->setWidth(wallWidth);
		wsi->data()->UpdateMergedPolygon();
	}
}

void QWallPropertiesWidget::setHeightOfTheSelectedWalls(float wallHeight)
{
	for (WallSegmentItem * wsi : m_selectedWallSegmentItems)
	{
		wsi->data()->setHeight(wallHeight);
	}
}

void QWallPropertiesWidget::updateUIWidgetsDueToWallSegmentItem()
{
	WallData * wd = m_selectedWallSegmentItem->data();

	// no signals here
	// wall width
	bool wasBlocked = ui->wallWidthLineEdit->blockSignals(true);
	ui->wallWidthLineEdit->setText(QString::number(wd->width(), 'i', 0));
	ui->wallWidthLineEdit->blockSignals(wasBlocked);

	wasBlocked = ui->wallWidthSlider->blockSignals(true);
	ui->wallWidthSlider->setValue(wd->width());
	ui->wallWidthSlider->blockSignals(wasBlocked);

	// wall height
	wasBlocked = ui->wallHeightLineEdit->blockSignals(true);
	ui->wallHeightLineEdit->setText(QString::number(wd->height(), 'i', 0));
	ui->wallHeightLineEdit->blockSignals(wasBlocked);

	wasBlocked = ui->wallHeightSlider->blockSignals(true);
	ui->wallHeightSlider->setValue(wd->height());
	ui->wallHeightSlider->blockSignals(wasBlocked);
}

void QWallPropertiesWidget::on_applyParametersToWallButton_pressed()
{
	if (m_selectedWallSegmentItem != nullptr)
	{
		emitWallSegmentItemPropertiesModifiedViaUIPanel();
	}
	//DEBUG_OUT("QWallPropertiesWidget::on_applyParametersToWallButton_pressed");
}

void QWallPropertiesWidget::emitWallSegmentItemPropertiesModifiedViaUIPanel()
{
	WallData * wallData = m_selectedWallSegmentItem->data();
	// if wall is modified fire change event
	if (wallData->width() != tempWidth || wallData->height() != tempHeight)
	{
		tempWidth = wallData->width();
		tempHeight = wallData->height();
		emit wallSegmentItemPropertiesModifiedViaUIPanel(wallData);
	}
}
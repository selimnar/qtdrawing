#include <QPropertyAnimation>
#include <QEvent>
#include <QStyle>

#include "QAnimatedPushButton.h"

#include "utilities/utilities.h"

const char * css_str =
"\nQAnimatedPushButton{\n"
"\tbackground-color: COLOR_VALUE;\n"
"\tborder: 2px solid black;\n"
"\tborder-radius: 10px;\n"
"}";

const QColor QAnimatedPushButton::normalBackcolor = QColor("#B0C4DE");
const QColor QAnimatedPushButton::hoverBackcolor = QColor("#50C4DE");
const QColor QAnimatedPushButton::pressedBackcolor = QColor("#CC9999");
const QString QAnimatedPushButton::cssTemplate = QString(css_str);

// Invoke the StaticConstructor & StaticDestructor of the class:
// Make sure you put this AFTER the initialization of the static data members!
INVOKE_STATIC_CONSTRUCTOR(QAnimatedPushButton);


void QAnimatedPushButton::StaticConstructor()
{
	//DEBUG_OUT("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------ QAnimatedPushButton::StaticConstructor");
}


void QAnimatedPushButton::StaticDestructor()
{
	//DEBUG_OUT("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!------ QAnimatedPushButton::StaticDestructor");
}

QAnimatedPushButton::QAnimatedPushButton(QWidget *parent)
	: QPushButton(parent)
{
	this->animation = new QPropertyAnimation(this, "backgroundColor");
}

QAnimatedPushButton::~QAnimatedPushButton()
{
}

void QAnimatedPushButton::setBackgroundColor(QColor color)
{
	currentBackgroundColor = color;
	QString css = cssTemplate;
	css.replace(QString("COLOR_VALUE"), color.name());

	//DEBUG_OUT("QAnimatedPushButton::mousePressEvent : css = \n%s", css.toStdString().c_str());

	//setStyleSheet(QString("background-color: rgb(%1, %2, %3);").arg(color.red()).arg(color.green()).arg(color.blue()));
	setStyleSheet(css);
	this->setProperty("urgent", true);
	this->style()->unpolish(this);
	this->style()->polish(this);
	this->update();
}

QColor QAnimatedPushButton::backgroundColor()
{
	return Qt::black; // getter is not really needed for now
}

void QAnimatedPushButton::showEvent(QShowEvent *)
{
	this->setBackgroundColor(normalBackcolor);
	//DEBUG_OUT("!!!!!!!!------ QAnimatedPushButton::showEvent");
}

void QAnimatedPushButton::mousePressEvent(QMouseEvent *e)
{
	//colorTransitionAnimation(currentBackgroundColor, pressedBackcolor, 500);
	colorTransitionAnimation(currentBackgroundColor, pressedBackcolor, hoverBackcolor, 500, 0.5);
	QPushButton::mousePressEvent(e);
}

bool QAnimatedPushButton::event(QEvent *event)
{
	switch (event->type())
	{
	case QEvent::Create:
		DEBUG_OUT("------ QAnimatedPushButton::CREATED");
		return true;
		break;
	case QEvent::HoverEnter:
		hoverEnter((QHoverEvent*)(event));
		return true;
		break;
	case QEvent::HoverLeave:
		hoverLeave((QHoverEvent*)(event));
		return true;
		break;
	case QEvent::HoverMove:
		hoverMove((QHoverEvent*)(event));
		return true;
		break;
	default:
		break;
	}
	return QWidget::event(event);
}

void QAnimatedPushButton::hoverEnter(QHoverEvent *)
{
	colorTransitionAnimation(currentBackgroundColor, hoverBackcolor, 500);
	//DEBUG_OUT("QAnimatedPushButton::hoverEnter");
}

void QAnimatedPushButton::hoverLeave(QHoverEvent *)
{
	colorTransitionAnimation(currentBackgroundColor, normalBackcolor, 500);
	//DEBUG_OUT("QAnimatedPushButton::hoverLeave");
}

void QAnimatedPushButton::hoverMove(QHoverEvent *)
{
	//DEBUG_OUT("QAnimatedPushButton::hoverMove");
}

void QAnimatedPushButton::colorTransitionAnimation(QColor startColor, QColor endColor, int msecs)
{
	animation->stop();
	animation = new QPropertyAnimation(this, "backgroundColor");
	//QPropertyAnimation *animation = new QPropertyAnimation(this, "backgroundColor");
	//animation->stop();
	animation->setDuration(msecs);
	animation->setStartValue(startColor);
	animation->setEndValue(endColor);
	animation->start();
}

void QAnimatedPushButton::colorTransitionAnimation(QColor startColor, QColor midColor,
	QColor endColor, int msecs, qreal midKeyTime)
{
	animation->stop();
	animation = new QPropertyAnimation(this, "backgroundColor");
	//QPropertyAnimation *animation = new QPropertyAnimation(this, "backgroundColor");
	//animation->stop();
	animation->setDuration(msecs);
	//animation->setLoopCount(2);
	animation->setStartValue(startColor);
	animation->setEndValue(endColor);
	animation->setKeyValueAt(midKeyTime, midColor);
	animation->start();
}
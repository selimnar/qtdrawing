#include "QScenePropertiesWidget.h"

#include <QRegExpValidator>
#include <QIntValidator>

#include "ui_ScenePropertiesWidget.h"

#include "FloorDesignItems\WallSegmentItem.h"
#include "CustomQObjects\FloorplanGraphicsScene.h"

#include "utilities/utilities.h"

const int QScenePropertiesWidget::MinWallWidth = 5;
const int QScenePropertiesWidget::MaxWallWidth = 200;

const int QScenePropertiesWidget::MinWallHeight = 0;
const int QScenePropertiesWidget::MaxWallHeight = 1000;

QScenePropertiesWidget::QScenePropertiesWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::ScenePropertiesWidget)
{
	ui->setupUi(this);

	//ui->wallWidthLineEdit->setValidator(new QRegExpValidator(QRegExp("[0-9]*"), ui->wallWidthLineEdit));
	ui->wallWidthLineEdit->setValidator(new QIntValidator(MinWallWidth, MaxWallWidth, ui->wallWidthLineEdit));
	ui->wallHeightLineEdit->setValidator(new QIntValidator(MinWallHeight, MaxWallHeight, ui->wallHeightLineEdit));
	//ui->wallWidthLineEdit->setValidator(new QRegExpValidator(QRegExp("\s*[0-9]*\s*(cm)\s*")));

	m_scene = nullptr;

	tempWidth = -1;
	tempHeight = -1;

	ui->wallWidthSlider->setMinimum(MinWallWidth);
	ui->wallWidthSlider->setMaximum(MaxWallWidth);
	ui->wallHeightSlider->setMinimum(MinWallHeight);
	ui->wallHeightSlider->setMaximum(MaxWallHeight);

	connect(ui->wallWidthSlider, SIGNAL(valueChanged(int)), this, SLOT(on_wallWidthSlider_valueChanged(int)));
	connect(ui->wallWidthLineEdit, SIGNAL(textChanged()), this, SLOT(on_wallWidthLineEdit_textChanged()));

	connect(ui->wallHeightSlider, SIGNAL(valueChanged(int)), this, SLOT(on_wallHeightSlider_valueChanged(int)));
	connect(ui->wallHeightLineEdit, SIGNAL(textChanged()), this, SLOT(on_wallHeightLineEdit_textChanged()));
}

QScenePropertiesWidget::~QScenePropertiesWidget()
{
	delete ui;
}

void QScenePropertiesWidget::setScene(FloorplanGraphicsScene * scene)
{
	setScene(scene, true);
}

void QScenePropertiesWidget::setScene(FloorplanGraphicsScene * scene, bool doSmthg)
{
	m_scene = scene;
	//m_selectedWallSegmentItems.clear();

	//if (!doSmthg)
	//{
	//	m_selectedWallSegmentItem = nullptr;
	//	return;
	//}

	//if (selectedWallSegmentItem != nullptr)
	//{
	//	m_selectedWallSegmentItems.push_back(selectedWallSegmentItem);
	//	tempWidth = selectedWallSegmentItem->width();
	//	tempHeight = selectedWallSegmentItem->height();
	//}
	//// if selected wall is changed then emit wall parameter modification signal
	//if (m_selectedWallSegmentItem != nullptr && m_selectedWallSegmentItem != selectedWallSegmentItem)
	//{
	//	emitScenePropertiesModifiedViaUIPanel();
	//}
	//m_selectedWallSegmentItem = selectedWallSegmentItem;
}

void QScenePropertiesWidget::on_wallWidthSlider_valueChanged(int value)
{
	//float fv = (float)value;
	//float fv2 = (float)ui->wallWidthSlider->value();
	//DEBUG_OUT("QScenePropertiesWidget::on_wallWidth_valueChanged %f - %f", fv, fv2);

	const bool wasBlocked = ui->wallWidthLineEdit->blockSignals(true);
	// no signals here
	ui->wallWidthLineEdit->setText(QString::number(value, 'i', 0));
	ui->wallWidthLineEdit->blockSignals(wasBlocked);

	setWidthOfTheSelectedWalls(value);
}

void QScenePropertiesWidget::on_wallWidthLineEdit_textChanged()
{
	bool success;
	int value = ui->wallWidthLineEdit->text().split(" ")[0].toInt(&success);
	if (success)
	{
		const bool wasBlocked = ui->wallWidthSlider->blockSignals(true);
		// no signals here
		ui->wallWidthSlider->setValue(value);
		ui->wallWidthSlider->blockSignals(wasBlocked);

		setWidthOfTheSelectedWalls(value);
	}
	//DEBUG_OUT("QScenePropertiesWidget::on_wallWidthTextEdit_valueChanged %s - %d",
	//	ui->wallWidthLineEdit->text().toStdString().c_str(), value);
}

void QScenePropertiesWidget::on_wallHeightSlider_valueChanged(int value)
{
	const bool wasBlocked = ui->wallHeightLineEdit->blockSignals(true);
	// no signals here
	ui->wallHeightLineEdit->setText(QString::number(value, 'i', 0));
	ui->wallHeightLineEdit->blockSignals(wasBlocked);

	setHeightOfTheSelectedWalls(value);
}

void QScenePropertiesWidget::on_wallHeightLineEdit_textChanged()
{
	bool success;
	int value = ui->wallHeightLineEdit->text().split(" ")[0].toInt(&success);
	if (success)
	{
		const bool wasBlocked = ui->wallHeightSlider->blockSignals(true);
		// no signals here
		ui->wallHeightSlider->setValue(value);
		ui->wallHeightSlider->blockSignals(wasBlocked);

		setHeightOfTheSelectedWalls(value);
	}
}

void QScenePropertiesWidget::setWidthOfTheSelectedWalls(float wallWidth)
{
	std::vector<WallSegmentItem *> m_selectedWallSegmentItems = m_scene->wallSegmentItems();
	for (WallSegmentItem * wsi : m_selectedWallSegmentItems)
	{
		wsi->data()->setWidth(wallWidth);
		wsi->data()->UpdateMergedPolygon();
	}
}

void QScenePropertiesWidget::setHeightOfTheSelectedWalls(float wallHeight)
{
	std::vector<WallSegmentItem *> m_selectedWallSegmentItems = m_scene->wallSegmentItems();
	for (WallSegmentItem * wsi : m_selectedWallSegmentItems)
	{
		wsi->data()->setHeight(wallHeight);
	}
}

void QScenePropertiesWidget::updateUIWidgetsDueToWallSegmentItem()
{
	std::vector<WallSegmentItem *> m_selectedWallSegmentItems = m_scene->wallSegmentItems();
	//const qreal WallSegmentItem::DefaultFloorWallWidth = 15;
	qreal width = WallSegmentItem::DefaultWallWidth;
	qreal height = WallSegmentItem::DefaultWallHeight;
	if (m_selectedWallSegmentItems.size() != 0)
	{
		WallData * wd = m_selectedWallSegmentItems[0]->data();
		width = wd->width();
		height = wd->height();
	}

	// no signals here
	// wall width
	bool wasBlocked = ui->wallWidthLineEdit->blockSignals(true);
	ui->wallWidthLineEdit->setText(QString::number(width, 'i', 0));
	ui->wallWidthLineEdit->blockSignals(wasBlocked);

	wasBlocked = ui->wallWidthSlider->blockSignals(true);
	ui->wallWidthSlider->setValue(width);
	ui->wallWidthSlider->blockSignals(wasBlocked);

	// wall height
	wasBlocked = ui->wallHeightLineEdit->blockSignals(true);
	ui->wallHeightLineEdit->setText(QString::number(height, 'i', 0));
	ui->wallHeightLineEdit->blockSignals(wasBlocked);

	wasBlocked = ui->wallHeightSlider->blockSignals(true);
	ui->wallHeightSlider->setValue(height);
	ui->wallHeightSlider->blockSignals(wasBlocked);
}

void QScenePropertiesWidget::on_applyParametersToWallButton_pressed()
{
	emitScenePropertiesModifiedViaUIPanel();
	//DEBUG_OUT("QScenePropertiesWidget::on_applyParametersToWallButton_pressed");
}

void QScenePropertiesWidget::emitScenePropertiesModifiedViaUIPanel()
{
	//WallData * wallData = m_selectedWallSegmentItem->data();
	//// if wall is modified fire change event
	//if (wallData->width() != tempWidth || wallData->height() != tempHeight)
	//{
	//	tempWidth = wallData->width();
	//	tempHeight = wallData->height();
	//	emit scenePropertiesModifiedViaUIPanel();
	//}
	emit scenePropertiesModifiedViaUIPanel();
}
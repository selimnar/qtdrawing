#ifndef Q_SCENE_PROPERTIES_WIDGET_H
#define Q_SCENE_PROPERTIES_WIDGET_H

#include <QWidget>
#include <vector>

class FloorplanGraphicsScene;

namespace Ui {
	class ScenePropertiesWidget;
} // namespace Ui

class QScenePropertiesWidget : public QWidget
{
	Q_OBJECT

Q_SIGNALS:
	void scenePropertiesModifiedViaUIPanel();

public:
	explicit QScenePropertiesWidget(QWidget *parent);
	~QScenePropertiesWidget();

	void setScene(FloorplanGraphicsScene * scene);
	void setScene(FloorplanGraphicsScene * scene, bool doSmthg);
	void updateUIWidgetsDueToWallSegmentItem();

protected:

private:
	static const int MinWallWidth;
	static const int MaxWallWidth;
	static const int MinWallHeight;
	static const int MaxWallHeight;

	int tempWidth;
	int tempHeight;

	Ui::ScenePropertiesWidget *ui;

	FloorplanGraphicsScene * m_scene;

	void setWidthOfTheSelectedWalls(float wallWidth);
	void setHeightOfTheSelectedWalls(float wallHeight);

private slots:
	void on_wallWidthSlider_valueChanged(int value);
	void on_wallWidthLineEdit_textChanged();

	void on_wallHeightSlider_valueChanged(int value);
	void on_wallHeightLineEdit_textChanged();

	void on_applyParametersToWallButton_pressed();

	void emitScenePropertiesModifiedViaUIPanel();
};

#endif // Q_SCENE_PROPERTIES_WIDGET_H
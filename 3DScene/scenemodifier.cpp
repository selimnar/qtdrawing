/****************************************************************************
**
** Copyright (C) 2014 Klaralvdalens Datakonsult AB (KDAB).
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt3D module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "scenemodifier.h"

#include <Qt3DRender/QTextureImage>
#include <Qt3DRender/QTexture>
//#include <Qt3DExtras/QNormalDiffuseSpecularMapMaterial>
//#include <Qt3DExtras/QDiffuseMapMaterial>
#include <Qt3DExtras/QDiffuseSpecularMaterial>


#include <QtCore/QDebug>

#include "../utilities/utilities.h"
#include "utilities\PolygonDetection.h"
//#include "utilities\PathUtilities.h"
//#include "clipper/clipper_utilities.h"

#include "../MeshGeneration/WallMeshGenerator.h"
#include "../MeshGeneration/RoofMesh.h"

#include "../Data/FloorDesignDocument.h"
#include "../Data/WallData.h"

#include "../FloorDesignItems/WallSegmentItem.h"

const qreal SceneModifier::SceneScale = 1.0f;

SceneModifier::SceneModifier(Qt3DCore::QEntity *rootEntity)
    : m_rootEntity(rootEntity)
{
}

SceneModifier::~SceneModifier()
{
}

void SceneModifier::generate3DWallMeshPart(MeshPart * wallMeshPart, Qt3DCore::QEntity *wallEntity,
	QColor diffuseColor, QColor sideWallAmbientColor)
{
	Qt3DCore::QEntity * wallPartEntity = new Qt3DCore::QEntity(wallEntity);

	Qt3DRender::QGeometry * wallPartGeom = wallMeshPart->geometry();

	Qt3DRender::QMesh * wallPartMesh = new Qt3DRender::QMesh();
	wallPartMesh->setGeometry(wallPartGeom);

	//// Plane mesh transform
	//Qt3DCore::QTransform *wallTransform = new Qt3DCore::QTransform();
	//wallTransform->setScale(0.03f);
	////wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	//wallTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	Qt3DExtras::QDiffuseSpecularMaterial *material = new Qt3DExtras::QDiffuseSpecularMaterial();
	material->setDiffuse(diffuseColor);
	material->setAmbient(sideWallAmbientColor);
	material->setShininess(0);
	material->setSpecular(QColor::fromRgb(0, 0, 0));

	wallPartEntity->addComponent(wallPartMesh);
	wallPartEntity->addComponent(material);
	//m_wallEntity->addComponent(wallTransform);
}

Qt3DCore::QEntity * SceneModifier::generate3DWall(WallData * wallData, Qt3DCore::QEntity *rootEntity)
{
	////////////////////////////////////////////////
	Qt3DCore::QEntity * wallEntity = new Qt3DCore::QEntity(rootEntity);

	WallMeshGenerator * wallMeshGenerator = new WallMeshGenerator();
	wallMeshGenerator->generateWallData(wallData);
	//QColor sideWallColor = QColor::fromRgb(0xC6C9C4);
	QColor sideWallDiffuseColor = QColor::fromRgb(0xCCDFE8);
	QColor sideWallAmbientColor = QColor::fromRgb(0xCCCCCC); //0xAAAAAA ; 0xBBBBBB ; 0xCCCCCC ; 0xDDDDDD ; 0xFFFFFF; 
	generate3DWallMeshPart(wallMeshGenerator->firstWallSideMeshPart(), wallEntity, sideWallDiffuseColor, sideWallAmbientColor);
	generate3DWallMeshPart(wallMeshGenerator->secondWallSideMeshPart(), wallEntity, sideWallDiffuseColor, sideWallAmbientColor);
	generate3DWallMeshPart(wallMeshGenerator->topWallMeshPart(), wallEntity, Qt::black, Qt::black);
	delete wallMeshGenerator;

	// Plane mesh transform
	Qt3DCore::QTransform *wallTransform = new Qt3DCore::QTransform();
	wallTransform->setScale(SceneScale);
	//wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	WallSegmentItem * wsi = wallData->relatedGraphicsItem();
	QPointF relativeDragPosition = wsi->relativeDragPosition() * SceneScale;
	wallTransform->setTranslation(QVector3D(relativeDragPosition.x(), 0.0f, relativeDragPosition.y()));

	wallEntity->addComponent(wallTransform);

	return wallEntity;
}

Qt3DCore::QEntity * SceneModifier::generate3DRoof(FloorDesignDocument * document, Qt3DCore::QEntity *rootEntity)
{
	Qt3DCore::QEntity * roofEntity = new Qt3DCore::QEntity(rootEntity);

	float wallHeight = 0;

	PolygonDetector polygonDetector;
	std::vector<QPointF> * roofPoints = polygonDetector.detectAndMergePolygons(document);
	if (roofPoints == nullptr)
	{
		return nullptr;
	}

	QList<QVariant> arguments;
	QVariant varWallHeight(0);
	arguments.append(varWallHeight);
	QVariant varHeightOffset(0);
	arguments.append(varHeightOffset);
	QVariant varIsUp(true);
	arguments.append(varIsUp);

	RoofMesh * roofMeshGenerator = new RoofMesh();
	roofMeshGenerator->prepareMesh(*roofPoints, arguments);
	delete roofPoints;
	//QColor sideWallColor = QColor::fromRgb(0xC6C9C4);
	QColor diffuseColor = QColor::fromRgb(0xFFFF00);
	QColor ambientColor = QColor::fromRgb(0xCCCCCC); //0xAAAAAA ; 0xBBBBBB ; 0xCCCCCC ; 0xDDDDDD ; 0xFFFFFF; 
	generate3DWallMeshPart(roofMeshGenerator, roofEntity, diffuseColor, ambientColor);

	// Plane mesh transform
	Qt3DCore::QTransform *transform = new Qt3DCore::QTransform();
	transform->setScale(SceneScale);
	//wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	transform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	roofEntity->addComponent(transform);

	return roofEntity;
}

Qt3DCore::QEntity * SceneModifier::generate3DBlock(FloorDesignDocument * document, Qt3DCore::QEntity *rootEntity,
	float &heightOffset, int floorCount)
{
	int wallCount = document->wallCount();
	if (wallCount == 0)
	{
		return nullptr;
	}

	WallData * wallData = document->wallAt(0);
	float wallHeight = (float)wallData->height();
	wallHeight *= floorCount;

	Qt3DCore::QEntity * roofsBlockEntity = new Qt3DCore::QEntity(rootEntity);

	PolygonDetector polygonDetector;
	std::vector<QPointF> * roofPoints = polygonDetector.detectAndMergePolygons(document);
	if (roofPoints == nullptr)
	{
		return nullptr;
	}

	QColor ambientColor = QColor::fromRgb(0xCCCCCC); //0xAAAAAA ; 0xBBBBBB ; 0xCCCCCC ; 0xDDDDDD ; 0xFFFFFF; 


	Qt3DCore::QEntity * ceiling = new Qt3DCore::QEntity(roofsBlockEntity);
	QList<QVariant> ceilingArguments;
	ceilingArguments.append(QVariant(wallHeight));
	ceilingArguments.append(QVariant(heightOffset));
	ceilingArguments.append(QVariant(true));

	RoofMesh * roofMeshGenerator = new RoofMesh();
	roofMeshGenerator->prepareMesh(*roofPoints, ceilingArguments);
	//QColor sideWallColor = QColor::fromRgb(0xC6C9C4);
	QColor ceilingDiffuseColor = QColor::fromRgb(0xFFFF00);
	generate3DWallMeshPart(roofMeshGenerator, ceiling, ceilingDiffuseColor, ambientColor);

	// Plane mesh transform
	Qt3DCore::QTransform *ceilingTransform = new Qt3DCore::QTransform();
	ceilingTransform->setScale(SceneScale);
	//wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	ceilingTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	ceiling->addComponent(ceilingTransform);



	//////////////////////////////////////////////////


	Qt3DCore::QEntity * floor = new Qt3DCore::QEntity(roofsBlockEntity);
	QList<QVariant> floorArguments;
	floorArguments.append(QVariant(0));
	floorArguments.append(QVariant(heightOffset));
	floorArguments.append(QVariant(false));

	RoofMesh * ceilingMeshGenerator = new RoofMesh();
	ceilingMeshGenerator->prepareMesh(*roofPoints, floorArguments);
	//QColor sideWallColor = QColor::fromRgb(0xC6C9C4);
	QColor floorDiffuseColor = QColor::fromRgb(0xFF0000);
	generate3DWallMeshPart(ceilingMeshGenerator, floor, floorDiffuseColor, ambientColor);

	// Plane mesh transform
	Qt3DCore::QTransform *floorTransform = new Qt3DCore::QTransform();
	floorTransform->setScale(SceneScale);
	//wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	floorTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	floor->addComponent(floorTransform);


	//////////////////////////////////////////////////



	Qt3DCore::QEntity * sideWalls = new Qt3DCore::QEntity(roofsBlockEntity);
	QList<QVariant> sideWallsArguments;
	sideWallsArguments.append(QVariant(wallHeight));
	sideWallsArguments.append(QVariant(heightOffset));

	BlockSideWallsMesh * sideWallMeshGenerator = new BlockSideWallsMesh();
	sideWallMeshGenerator->prepareMesh(*roofPoints, sideWallsArguments);
	//QColor sideWallColor = QColor::fromRgb(0xC6C9C4);
	QColor sideWallDiffuseColor = QColor::fromRgb(0xCCDFE8);
	generate3DWallMeshPart(sideWallMeshGenerator, sideWalls, sideWallDiffuseColor, ambientColor);

	// Plane mesh transform
	Qt3DCore::QTransform *sideWallsTransform = new Qt3DCore::QTransform();
	sideWallsTransform->setScale(SceneScale);
	//wallTransform->setRotation(QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), 45.0f));
	sideWallsTransform->setTranslation(QVector3D(0.0f, 0.0f, 0.0f));

	sideWalls->addComponent(sideWallsTransform);


	//////////////////////////////////////////////////




	delete roofPoints;

	heightOffset += wallHeight;
	return roofsBlockEntity;
}
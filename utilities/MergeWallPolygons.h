#ifndef MERGE_POLYHON_WALLS_H
#define MERGE_POLYHON_WALLS_H

#include <vector>

#include <QGraphicsItem>

#include "clipper/clipper.hpp"

enum WallSegmentControlledPart;

class ControllerData;
class WallData;
class FloorplanGraphicsScene;
class QGraphicsItem;
class WallPolygonGenerationData;

enum CurvedWallEndGenerationCase
{
	CNoGenCase = 0,
	CTwoPointGenCase = 1,
	CThreePointGenCase = 2,
};

class WallGenericPolygonGenerationData
{
public:
	WallData * wallData;
	CurvedWallEndGenerationCase startControlGenerationCase;
	QPointF startPoint1;
	QPointF startPoint2;
	QPointF startMidPoint;
	int startIndex1;
	int startIndex2;

	CurvedWallEndGenerationCase endControlGenerationCase;
	QPointF endPoint1;
	QPointF endPoint2;
	QPointF endMidPoint;
	int endIndex1;
	int endIndex2;

	std::vector<QPointF> firstSideVectors;
	std::vector<QPointF> secondSideVectors;
	std::vector<QPointF> polygonVectors;
	bool isDataAssigned;
	bool startControlProcessed;
	bool endControlProcessed;

	QPainterPath referencePath;

	WallGenericPolygonGenerationData();
	void reset();
	void resetASide(WallSegmentControlledPart wscp);
};

class CurvedWallDataWithAngle
{
public:
	WallData * wallData;
	qreal angle;

	std::vector<QPointF> firstSideVectors;
	std::vector<QPointF> secondSideVectors;

	QPointF direction;
	QPointF normal;
	QPointF sidePoint1;
	QPointF sidePoint2;

	static double CalculateAngle(QPainterPath &referenceBezierCurvePath, WallData * wallData, ControllerData * cntrlData,
		QPointF &direction, QPointF &normal, QPointF &sidePoint1, QPointF &sidePoint2, std::vector<QGraphicsItem*> &lines);
};

class MergeWallPolygons
{
public:
	const static qreal QPathToClipperLibPathScaler;
	const static qreal ClipperLibPathToQPathScaler;

	static void PopulateProcessControllerMapRelatedToController(ControllerData * controllerData, std::map<ControllerData *, bool> &processedControllerMap);

	static void ModifyCurvedWallSegmentPolygonsRelatedToController(ControllerData * controllerData,
		FloorplanGraphicsScene * scene);

	static void ModifyCurvedWallSegmentPolygonsForGivenControlDataList(std::vector<ControllerData*> * controllerDataList, bool extend,
		FloorplanGraphicsScene * scene);

	////////////////// this method is the base method to calculate merge data of walls related to a controller ///////////////////////
	static void GenerateCurvedWallSegmentPolygonsCreationDataRelatedToTheController(ControllerData * controllerData, FloorplanGraphicsScene * scene);

	static void ModifyCurvedWallSegmentPolygonsDueToWallMove(WallData * wallData, FloorplanGraphicsScene * scene);

	static void CreateLinePolygon(WallGenericPolygonGenerationData &wallPolygonGenerationData, const QPointF &relativeDragPosition, QPointF startPos, QPointF endPos, qreal wallWidth,
		QPainterPath &bezierCurvePath, QPainterPath &referenceBezierCurvePath, ClipperLib::Path &bezierCurveClipperPath);

	////////////////// this method is to create polygon QGraphicsItem from merge data of walls in the prior steps ///////////////////////
	static void CreateFinalBezierPolygon(WallData * wallData, QPainterPath &bezierCurvePath, QPainterPath &referenceBezierCurvePath,
		QPointF relativeDragPosition, QGraphicsScene * scene);

	static void DrawPoint(QPointF cp, QGraphicsScene * scene, QString label, QColor labelColor);
	static void DrawPoint(QPointF cp, QGraphicsScene * scene, QString label, qreal radius, QColor labelColor);
	static void DrawLine(QPointF p1, QPointF p2, QGraphicsScene * scene, QColor color);
	static void DrawCurvedWallOneSideOutline(std::vector<QPointF> &sideVectors, QGraphicsScene * scene, QColor color);

	static void ConvertPointVectorToClipperPolygonData(std::vector<QPointF> bezierCurveVector, ClipperLib::Path &bezierCurveClipperPath);
	static void ConvertClipperPolygonDataToPointVector(ClipperLib::Path bezierCurveClipperPath, std::vector<QPointF> &bezierCurveVector);

private:
	const static qreal ControlpointRatio;

	static std::vector<QGraphicsItem*> lines;

	static void ConvertClipperPolygonPathToQPainterPath(QPainterPath &bezierCurvePath, ClipperLib::Path &bezierCurveClipperPath);

	static void CreatePriorBezierPolygonData(ControllerData * controllerData, WallData * wallData, QPainterPath &referenceBezierCurvePath,
		std::vector<QPointF> &firstSideVectors, std::vector<QPointF> &secondSideVectors);

	static void CreateWallBezierClipperPolyonData(qreal wallWidth, QPainterPath referenceBezierCurvePath,
		ClipperLib::Path &bezierCurveClipperPath, std::vector<QPointF> &firstSideVectors, std::vector<QPointF> &secondSideVectors);
};

#endif // MERGE_POLYHON_WALLS_H
#ifndef QIMAGE_DOWNLOADER_H
#define QIMAGE_DOWNLOADER_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>
#include <QStringList>

#include <vector>

#include "Interfaces/IDownloadedImageHandler.h"

class QImageDownloader : public QObject
{
public:

	Q_OBJECT
public:
	explicit QImageDownloader(IDownloadedImageHandler * downloadedImageHandler);
	virtual ~QImageDownloader();
	void setFile(QString fileURL);
	QImage * image();

private:
	QNetworkAccessManager *manager;
	QNetworkReply *reply;
	//QFile *file;

	IDownloadedImageHandler * downloadedImageHandler;
	QImage * m_image;
	std::vector<QByteArray> * m_imageBitsList;
	qint64 total;

private slots:
	void onDownloadProgress(qint64, qint64);
	void onFinished(QNetworkReply*);
	void onReadyRead();
	void onReplyFinished();
};

#endif // QIMAGE_DOWNLOADER_H
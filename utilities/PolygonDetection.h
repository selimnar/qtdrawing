#ifndef POLYGON_DETECTION_H
#define POLYGON_DETECTION_H

#include <vector>
#include <map>

#include <QPointF>

class FloorDesignDocument;
class ControllerData;
class QGraphicsScene;
class QPainterPath;
class QString;

class ClosedPath
{
public:
//private:
	std::vector<QPointF> nodes;
};

class PolygonDetector
{
public:
	const static qreal QPathToClipperLibPathScaler;
	const static qreal ClipperLibPathToQPathScaler;

	PolygonDetector();
	~PolygonDetector();

	void detectAndMergePolygons(FloorDesignDocument * document, QGraphicsScene * scene);
	std::vector<QPointF> * detectAndMergePolygons(FloorDesignDocument * document);

	void detectPolygons(ControllerData * controllerData);

private:
	const static QString ImageFolderPath;
	const static std::map<int, QString> ImageFileNameMap;

	std::vector<ClosedPath> polygons;

	ClosedPath * traverseControllerData(ControllerData * startControllerData, ControllerData * controllerData,
		ControllerData * parentControllerData, std::map<ControllerData *, bool> &controllerVisitedMap);

	void detectPolygonNearGivenControlPoint(ControllerData * controllerData,
		std::map<ClosedPath *, QPointF> &closedPathToCenterMap);

	void createPolygonItem(ClosedPath * closedPath, QGraphicsScene * scene, int counter);
	void createPolygonItem(std::vector<QPointF> &nodes, QGraphicsScene * scene, int counter);
};

#endif // POLYGON_DETECTION_H
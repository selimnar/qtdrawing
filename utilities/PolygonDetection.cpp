#include "PolygonDetection.h"

#include <QImage>
#include <QPixmap>
#include <QBrush>
#include <QGraphicsPathItem>
#include <QPainterPath>
#include <QGraphicsScene>

#include "data/ControllerData.h"
#include "data/FloorDesignDocument.h"
#include "ControlPointGraphicsItem.h"

#include "utilities.h"

#include "PathUtilities.h"
#include "clipper/clipper_utilities.h"

const QString PolygonDetector::ImageFolderPath = "C:\\development\\QTDrawing\\images\\";
const std::map<int, QString> PolygonDetector::ImageFileNameMap = {
	{ 0, "wall.jpg" },
	{ 1, "wood.jpg" },
};
const qreal PolygonDetector::QPathToClipperLibPathScaler = 10000;
const qreal PolygonDetector::ClipperLibPathToQPathScaler = 1 / WallSegmentItem::QPathToClipperLibPathScaler;

PolygonDetector::PolygonDetector()
{

}

PolygonDetector::~PolygonDetector()
{

}

void PolygonDetector::detectAndMergePolygons(FloorDesignDocument * document, QGraphicsScene * scene)
{
	std::vector<QPointF> * points = detectAndMergePolygons(document);
	createPolygonItem(*points, scene, 0);
	delete points;

	DEBUG_OUT("END");
}

std::vector<QPointF> * PolygonDetector::detectAndMergePolygons(FloorDesignDocument * document)
{
	std::map<ClosedPath *, QPointF> closedPathToCenterMap;
	int controllerCount = document->controllerCount();
	for (int i = 0; i < controllerCount; i++)
	{
		ControllerData * controllerData = document->controllerAt(i);
		detectPolygonNearGivenControlPoint(controllerData, closedPathToCenterMap);
	}

	if (closedPathToCenterMap.size() == 0)
	{
		return nullptr;
	}

	ClipperLib::Paths paths;
	for (auto pair : closedPathToCenterMap)
	{
		ClosedPath * closedPath = pair.first;
		std::vector<QPointF> &points = closedPath->nodes;
		ClipperLib::Path clipperPath;
		ConvertPointsToClipperPolygonPath(points, clipperPath);
		paths.push_back(clipperPath);
	}

	ClipperLib::Paths combinedPaths;
	//SimplifyPolygons(paths, combinedPaths, PolyFillType. fillType)
	ClipperLib::Clipper c;
	c.StrictlySimple(true);
	c.AddPaths(paths, ClipperLib::PolyType::ptSubject, true);
	c.Execute(ClipperLib::ClipType::ctUnion, combinedPaths, ClipperLib::PolyFillType::pftNonZero, ClipperLib::PolyFillType::pftNonZero);

	if (combinedPaths.size() == 0)
	{
		return nullptr;
	}

	std::vector<QPointF> * points = new std::vector<QPointF>();
	ConvertClipperPolygonPathToPoints(combinedPaths[0], *points);

	return points;
}

void PolygonDetector::detectPolygons(ControllerData * controllerData)
{
	std::map<ClosedPath *, QPointF> closedPathToCenterMap;
	detectPolygonNearGivenControlPoint(controllerData, closedPathToCenterMap);

	ControlPointGraphicsItem * controlPointGraphicsItem = controllerData->relatedGraphicsItem();
	QGraphicsScene * scene = controlPointGraphicsItem->scene();
	int counter = 0;
	for (auto pair : closedPathToCenterMap)
	{
		ClosedPath * closedPath = pair.first;
		createPolygonItem(closedPath, scene, counter++);
	}

	DEBUG_OUT("END");
	//traverseControllerData(controllerData, nullptr, polygons);
}

void PolygonDetector::detectPolygonNearGivenControlPoint(ControllerData * controllerData,
	std::map<ClosedPath *, QPointF> &closedPathToCenterMap)
{
	FloorDesignDocument * document = controllerData->ownerDocument();
	std::map<ControllerData *, bool> controllerVisitedMap;
	size_t controllerCount = document->controllerCount();
	for (int i = 0; i < controllerCount; i++)
	{
		ControllerData * cd = document->controllerAt(i);
		controllerVisitedMap.insert(std::pair<ControllerData *, bool>(cd, false));
	}

	std::vector<ClosedPath*> polygons;
	std::vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		//DEBUG_OUT("** wall uid : %s", cmd->controlledWallItemUuid.c_str());
		ControllerData * neighbourController = cmd->controlledWall->getOtherController(controllerData);
		//DEBUG_OUT("-- other CN uid : %s", neighbourController->uuid().c_str());

		std::map<ControllerData *, bool> clonedControllerVisitedMap(controllerVisitedMap);
		//controllerVisitedMap[controllerData] = true;
		ClosedPath * closedPath = traverseControllerData(controllerData, neighbourController,
			controllerData, clonedControllerVisitedMap);
		if (closedPath != nullptr)
		{
			polygons.push_back(closedPath);
		}
	}

	DEBUG_OUT("----------- closed path count = %d\n\n", polygons.size());
	for (ClosedPath * closedPath : polygons)
	{
		std::vector<QPointF> &nodes = closedPath->nodes;
		DEBUG_OUT("\t----------- closed node count = %d", nodes.size());
		QPointF center;
		for (QPointF point : nodes)
		{
			center += point;

			DEBUG_OUT("\t\t(%f, %f)", point.x(), point.y());
		}
		center *= (1.0f / (float)nodes.size());


		bool doesClosedPathExists = false;
		if (closedPathToCenterMap.size() != 0)
		{
			for (auto pair : closedPathToCenterMap)
			{
				QPointF existingCenter = pair.second;
				double length = QPointFLength(existingCenter - center);
				if (length < DBL_EPSILON)
				{
					doesClosedPathExists = true;
					break;
				}
			}
		}
		if (!doesClosedPathExists)
		{
			closedPathToCenterMap.insert(std::pair<ClosedPath *, QPointF>(closedPath, center));
		}

		DEBUG_OUT("\t CENTER = (%f, %f)", center.x(), center.y());
	}

	DEBUG_OUT("END");
	//traverseControllerData(controllerData, nullptr, polygons);
}

void PolygonDetector::createPolygonItem(ClosedPath * closedPath, QGraphicsScene * scene, int counter)
{
	createPolygonItem(closedPath->nodes, scene, counter);
}

void PolygonDetector::createPolygonItem(std::vector<QPointF> &nodes, QGraphicsScene * scene, int counter)
{
	QString imageFileName = ImageFileNameMap.at(counter % 2);
	QImage wallImage(ImageFolderPath + imageFileName);
	QPixmap texture = QPixmap::fromImage(wallImage);
	QColor fillColor = QColor::fromRgb(255, 0, 0);

	QBrush fillBrush(fillColor, Qt::TexturePattern);
	fillBrush.setTexture(texture);
	fillBrush.setColor(fillColor);

	QPainterPath path;
	path.moveTo(nodes[0]);
	size_t size = nodes.size();
	for (size_t i = 1; i < size; i++)
	{
		path.lineTo(nodes[i]);
	}

	QGraphicsPathItem *item = new QGraphicsPathItem(path);
	item->setBrush(fillBrush);
	item->setPen(fillColor);
	item->setZValue(-100);
	scene->addItem(item);
}

ClosedPath * PolygonDetector::traverseControllerData(ControllerData * startControllerData,
	ControllerData * controllerData, ControllerData * parentControllerData,
	std::map<ControllerData *, bool> &controllerVisitedMap)
{
	if (startControllerData == controllerData)
	{
		ClosedPath * closedPath = new ClosedPath();
		closedPath->nodes.push_back(controllerData->position());
		return closedPath;
	}

	controllerVisitedMap[controllerData] = true;

	ClosedPath * shorthestClosedPath = nullptr;
	std::vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	size_t minNodeCount = 100000000;
	for (ControllerMappingData * cmd : controlledList)
	{
		ControllerData * neighbourController = cmd->controlledWall->getOtherController(controllerData);
		if (controllerVisitedMap[neighbourController] || parentControllerData == neighbourController)
		{
			continue;
		}

		ClosedPath * closedPath = traverseControllerData(startControllerData, neighbourController,
			controllerData, controllerVisitedMap);
		if (closedPath != nullptr)
		{
			size_t nodeCount = closedPath->nodes.size();
			if (minNodeCount > nodeCount)
			{
				shorthestClosedPath = closedPath;
				minNodeCount = nodeCount;
			}
			//return closedPath;
		}
	}
	if (shorthestClosedPath != nullptr)
	{
		shorthestClosedPath->nodes.push_back(controllerData->position());
	}
	return shorthestClosedPath;
}
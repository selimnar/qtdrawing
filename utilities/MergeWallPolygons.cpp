#include "MergeWallPolygons.h"
#include "utilities.h"

#include <QGraphicsItem>

#include "data/ControllerData.h"

#include "CustomQObjects/FloorplanGraphicsScene.h"

#include "FloorDesignItems/WallSegmentItem.h"

#include "clipper/clipper_utilities.h"

//#include "../MeshGeneration/delaunator.hpp"
//#include "../MeshGeneration/polygon.h"

using namespace std;

const qreal MergeWallPolygons::ControlpointRatio = 0.2;
const qreal MergeWallPolygons::QPathToClipperLibPathScaler = 10000;
const qreal MergeWallPolygons::ClipperLibPathToQPathScaler = 1 / WallSegmentItem::QPathToClipperLibPathScaler;

vector<QGraphicsItem*> MergeWallPolygons::lines;

double CurvedWallDataWithAngle::CalculateAngle(QPainterPath &referenceBezierCurvePath, WallData * wallData,
	ControllerData * controllerData, QPointF &directionVec, QPointF &normalVec, QPointF &sidePoint1, QPointF &sidePoint2, vector<QGraphicsItem*> &lines)
{
	QGraphicsScene * scene = wallData->relatedGraphicsItem()->scene();
	qreal wallHalfWidth = wallData->width() * 0.5;
	if (wallData->type() == WallType::Curved)
	{
		bool direction = controllerData == wallData->startControllerData();
		qreal percent = (direction) ? 0.01 : 0.99;
		qreal invert = (direction) ? 0 : 180;

		QPointF bezierSamplePoint = referenceBezierCurvePath.pointAtPercent(percent);

		qreal angleAtPercent = referenceBezierCurvePath.angleAtPercent(percent) + invert + 90;
		angleAtPercent = angleAtPercent - ((angleAtPercent <= 360) ? 0 : 360);
		qreal radianAngleAtPercent = DegreeToRad * angleAtPercent;
		directionVec = QPointF(qSin(radianAngleAtPercent), qCos(radianAngleAtPercent));
		QPointF controlCenterPos = controllerData->position();
		normalVec = PerpendicularVector(directionVec);
		QPointF toSideVector = normalVec * wallHalfWidth;
		sidePoint1 = controlCenterPos + toSideVector;
		sidePoint2 = controlCenterPos - toSideVector;

		//QPointF mirrorPoint = bezierSamplePoint + (normalVec * 15);
		//QLineF lineF(mirrorPoint, bezierSamplePoint);
		//QGraphicsLineItem * lineItem = new QGraphicsLineItem(lineF);
		//lineItem->setPen(QPen(Qt::green));
		//scene->addItem(lineItem);
		//lines.push_back(lineItem);

		return radianAngleAtPercent;
	}
	else
	{
		QPointF centerPos = controllerData->position();
		ControllerData * otherController = wallData->getOtherController(controllerData);
		QPointF otherSidePos = otherController->position();
		qreal wallAngle = atan2(otherSidePos.x() - centerPos.x(), otherSidePos.y() - centerPos.y());
		wallAngle = wallAngle + ((wallAngle >= 0) ? 0 : (360 * DegreeToRad));
		//wallData->updateNormal();

		QPointF controlCenterPos = controllerData->position();
		QPointF endPos = otherController->position();

		directionVec = NormalizeQPointF(endPos - controlCenterPos);
		normalVec = PerpendicularVector(directionVec);
		QPointF toSideVector = normalVec * wallHalfWidth;
		sidePoint1 = controlCenterPos + toSideVector;
		sidePoint2 = controlCenterPos - toSideVector;
		//QPointF sideEndPoint1 = endPos + toSideVector;

		//QPointF wallCenter = (endPos1 + controlCenterPos) * 0.5;
		////QLineF lineF(wallCenter, wallCenter + (lineNormal1 * 15));
		//QLineF lineF(controlCenterPos, controlCenterPos + (normalVec * 15));
		////QLineF lineF(wallCenter, wallCenter + (ln * 15));
		//QGraphicsLineItem * lineItem = new QGraphicsLineItem(lineF);
		//lineItem->setPen(QPen(Qt::green));
		//scene->addItem(lineItem);
		//lines.push_back(lineItem);

		return wallAngle;
	}
}

WallGenericPolygonGenerationData::WallGenericPolygonGenerationData()
{
	wallData = nullptr;
	startIndex1 = -1;
	startIndex2 = -1;
	endIndex1 = -1;
	endIndex2 = -1;
	reset();
}

void WallGenericPolygonGenerationData::reset()
{
	isDataAssigned = false;
	startControlProcessed = false;
	endControlProcessed = false;
}

void WallGenericPolygonGenerationData::resetASide(WallSegmentControlledPart wscp)
{
	if (wscp == WallSegmentControlledPart::Start)
	{
		startControlGenerationCase = CurvedWallEndGenerationCase::CNoGenCase;
		startIndex1 = -1;
		startIndex2 = -1;
	}
	else
	{
		endControlGenerationCase = CurvedWallEndGenerationCase::CNoGenCase;
		endIndex1 = -1;
		endIndex2 = -1;
	}
}

bool walldataAnlgleComparer(CurvedWallDataWithAngle &wdwa1, CurvedWallDataWithAngle &wdwa2);

void generateCurvedWallSideMergablePolygonDataDueToIntersections(ControllerData * centerCD, CurvedWallDataWithAngle &wdwa1, CurvedWallDataWithAngle &wdwa2, int wallIndex1, int wallIndex2,
	QGraphicsScene * scene, std::vector<QGraphicsItem*> &lines);


void MergeWallPolygons::PopulateProcessControllerMapRelatedToController(ControllerData * controllerData, map<ControllerData *, bool> &processedControllerMap)
{
	processedControllerMap.insert(pair<ControllerData *, bool>(controllerData, false));

	vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		ControllerData * otherController = wallData->getOtherController(controllerData);
		bool doesMapNotContainController = processedControllerMap.find(otherController) == processedControllerMap.end();
		if (doesMapNotContainController)
		{
			processedControllerMap.insert(pair<ControllerData *, bool>(otherController, false));
		}
	}
}

void MergeWallPolygons::ModifyCurvedWallSegmentPolygonsRelatedToController(ControllerData * controllerData,
	FloorplanGraphicsScene * scene)
{
	map<ControllerData *, bool> processedControllerMap;

	for (QGraphicsItem * gi : lines)
	{
		scene->removeItem(gi);
	}
	lines.clear();
	//DEBUG_OUT("warning: uuid = %s", controllerData->uuid().c_str());

	PopulateProcessControllerMapRelatedToController(controllerData, processedControllerMap);

	for (auto const& pair : processedControllerMap)
	{
		ControllerData * controllerData = pair.first;
		bool isProcessed = pair.second;
		GenerateCurvedWallSegmentPolygonsCreationDataRelatedToTheController(controllerData, scene);
	}
}

void MergeWallPolygons::ModifyCurvedWallSegmentPolygonsForGivenControlDataList(std::vector<ControllerData*> * controllerDataList, bool extend,
	FloorplanGraphicsScene * scene)
{
	map<ControllerData *, bool> processedControllerMap;

	if (scene != nullptr && lines.size() > 0)
	{
		for (QGraphicsItem * gi : lines)
		{
			scene->removeItem(gi);
		}
		lines.clear();
	}

	//DEBUG_OUT("warning: uuid = %s", controllerData->uuid().c_str());

	if (extend)
	{
		for (ControllerData * controllerData : *controllerDataList)
		{
			PopulateProcessControllerMapRelatedToController(controllerData, processedControllerMap);
		}
	}
	else
	{
		for (ControllerData * controllerData : *controllerDataList)
		{
			bool doesMapNotContainController = processedControllerMap.find(controllerData) == processedControllerMap.end();
			if (doesMapNotContainController)
			{
				processedControllerMap.insert(pair<ControllerData *, bool>(controllerData, false));
			}
		}
	}

	for (auto const& pair : processedControllerMap)
	{
		ControllerData * controllerData = pair.first;
		bool isProcessed = pair.second;
		GenerateCurvedWallSegmentPolygonsCreationDataRelatedToTheController(controllerData, scene);
	}
}

////////////////// this method is the base method to calculate merge data of walls related to a controller ///////////////////////
void MergeWallPolygons::GenerateCurvedWallSegmentPolygonsCreationDataRelatedToTheController(ControllerData * controllerData, FloorplanGraphicsScene * scene)
{
	QPen pen;
	pen.setColor(Qt::red);

	vector<ControllerMappingData*> controlledList = controllerData->controlledList();

	if (controlledList.size() == 1)
	{
		ControllerMappingData * cmd = controlledList.front();
		WallData * wallData = cmd->controlledWall;
		WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
		WallGenericPolygonGenerationData &wallPolygonGenerationData = wallItem->wallGenericPolygonGenerationData();
		wallPolygonGenerationData.resetASide(cmd->controlledPart);

		QPainterPath referenceBezierCurvePath;
		if (wallData->type() == WallType::Curved)
		{
			vector<QPointF> firstSideVectors;
			vector<QPointF> secondSideVectors;
			CreatePriorBezierPolygonData(controllerData, wallData, referenceBezierCurvePath, firstSideVectors, secondSideVectors);
		}
	}
	else if (controlledList.size() > 1)
	{
		CurvedWallEndGenerationCase wallPolygonGenerationCase = (controlledList.size() == 2) ? CurvedWallEndGenerationCase::CTwoPointGenCase :
			CurvedWallEndGenerationCase::CThreePointGenCase;

		QPointF centerPos = controllerData->position();
		list<CurvedWallDataWithAngle> angleSorted;
		for (ControllerMappingData * cmd : controlledList)
		{
			WallData * wallData = cmd->controlledWall;
			WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
			WallGenericPolygonGenerationData &wallPolygonGenerationData = wallItem->wallGenericPolygonGenerationData();
			((cmd->controlledPart == WallSegmentControlledPart::Start) ? wallPolygonGenerationData.startControlGenerationCase :
				wallPolygonGenerationData.endControlGenerationCase) = wallPolygonGenerationCase;

			bool doesWallOrginateFromTheControlPoint = controllerData == wallData->startControllerData();
			CurvedWallDataWithAngle wdwa;
			QPainterPath referenceBezierCurvePath;
			if (wallData->type() == WallType::Curved)
			{
				wdwa.firstSideVectors.clear();
				wdwa.secondSideVectors.clear();
				QPainterPath bezierCurvePath;
				std::vector<QPointF> tmpFirstSideVectors;
				std::vector<QPointF> tmpSecondSideVectors;
				CreatePriorBezierPolygonData(controllerData, wallData, referenceBezierCurvePath, tmpFirstSideVectors, tmpSecondSideVectors);

				if (!wallPolygonGenerationData.isDataAssigned)
				{
					wallPolygonGenerationData.firstSideVectors.assign(tmpSecondSideVectors.begin(), tmpSecondSideVectors.end());
					wallPolygonGenerationData.secondSideVectors.assign(tmpFirstSideVectors.begin(), tmpFirstSideVectors.end());
					std::reverse(std::begin(wallPolygonGenerationData.firstSideVectors), std::end(wallPolygonGenerationData.firstSideVectors));
					wallPolygonGenerationData.referencePath = referenceBezierCurvePath;
					wallPolygonGenerationData.isDataAssigned = true;
				}

				//DEBUG_OUT("doesWallOrginateFromTheControlPoint = %s", (doesWallOrginateFromTheControlPoint) ? "TRUE" : "FALSE");
				wdwa.firstSideVectors.swap(doesWallOrginateFromTheControlPoint ? tmpSecondSideVectors : tmpFirstSideVectors);
				wdwa.secondSideVectors.swap(doesWallOrginateFromTheControlPoint ? tmpFirstSideVectors : tmpSecondSideVectors);
				std::reverse(std::begin(wdwa.firstSideVectors), std::end(wdwa.firstSideVectors));
			}
			else
			{
				QPointF originalLineNormal1 = wallData->normal();

				ControllerData * otherController = wallData->getOtherController(controllerData);
				QPointF controlCenterPos = controllerData->position();
				QPointF endPos = otherController->position();
				qreal wallHalfWidth = wallData->width() * 0.5;
				QPointF lineDir = NormalizeQPointF(endPos - controlCenterPos);
				QPointF lineNormal = PerpendicularVector(lineDir);
				QPointF toSideVector = lineNormal * wallHalfWidth;
				QPointF sideStartPoint = controlCenterPos + toSideVector;
				QPointF sideEndPoint = endPos + toSideVector;

				wdwa.firstSideVectors.push_back(sideStartPoint);
				wdwa.firstSideVectors.push_back(sideEndPoint);

				sideStartPoint = controlCenterPos - toSideVector;
				sideEndPoint = endPos - toSideVector;

				wdwa.secondSideVectors.push_back(sideStartPoint);
				wdwa.secondSideVectors.push_back(sideEndPoint);

				if (!wallPolygonGenerationData.isDataAssigned)
				{
					wallData->updateNormal();
					QPointF toOrginalSideVector = wallData->normal() * wallHalfWidth;
					wallPolygonGenerationData.firstSideVectors.clear();
					wallPolygonGenerationData.firstSideVectors.push_back(wallData->startPosition() + toOrginalSideVector);
					wallPolygonGenerationData.firstSideVectors.push_back(wallData->endPosition() + toOrginalSideVector);
					wallPolygonGenerationData.secondSideVectors.clear();
					wallPolygonGenerationData.secondSideVectors.push_back(wallData->startPosition() - toOrginalSideVector);
					wallPolygonGenerationData.secondSideVectors.push_back(wallData->endPosition() - toOrginalSideVector);

					QPainterPath referenceBezierCurvePath;
					referenceBezierCurvePath.moveTo(wallData->startPosition());
					referenceBezierCurvePath.lineTo(wallData->endPosition());
					wallPolygonGenerationData.referencePath = referenceBezierCurvePath;

					wallPolygonGenerationData.isDataAssigned = true;
				}
			}

			wdwa.wallData = cmd->controlledWall;
			QPointF sidePoint1;
			QPointF sidePoint2;
			wdwa.angle = CurvedWallDataWithAngle::CalculateAngle(referenceBezierCurvePath, wdwa.wallData, controllerData,
				wdwa.direction, wdwa.normal, wdwa.sidePoint1, wdwa.sidePoint2, lines);
			angleSorted.push_back(wdwa);
		}
		angleSorted.sort(walldataAnlgleComparer);
		angleSorted.push_back(angleSorted.front());

		int index = 0;
		int previousIndex = 0;
		CurvedWallDataWithAngle previousWdwa;
		for (CurvedWallDataWithAngle &wdwa : angleSorted)
		{
			WallData * currentWall = wdwa.wallData;
			if (index == 0)
			{
				previousWdwa = wdwa;
				previousIndex = index;
				index++;
				continue;
			}

			generateCurvedWallSideMergablePolygonDataDueToIntersections(controllerData, previousWdwa, wdwa, previousIndex, index, scene, lines);

			//ControllerData * otherController = wdwa.wallData->getOtherController(controllerData);
			//QGraphicsTextItem * textItem = new QGraphicsTextItem(QString::number(index) + ":" + QString::number(wdwa.angle * RadToDegree));
			//QFont font;
			//font.setPixelSize(18);
			//font.setBold(true);
			//font.setFamily("Calibri");
			//textItem->setDefaultTextColor(Qt::red);
			//textItem->setFont(font);
			//scene->addItem(textItem);
			//textItem->setPos(otherController->position());
			//scene->addItem(textItem);
			//lines.push_back(textItem);


			previousWdwa = wdwa;
			previousIndex = index;
			index++;
		}
	}
}

void MergeWallPolygons::ModifyCurvedWallSegmentPolygonsDueToWallMove(WallData * wallData, FloorplanGraphicsScene * scene)
{
	for (QGraphicsItem * gi : lines)
	{
		scene->removeItem(gi);
	}
	lines.clear();

	//ControllerData * startControllerData = wallData->startControllerData();
	//ControllerData * endControllerData = wallData->endControllerData();
	map<ControllerData *, bool> processedControllerMap;

	ControllerData * controllerData = wallData->startControllerData();
	processedControllerMap.insert(pair<ControllerData *, bool>(controllerData, false));

	vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		ControllerData * otherController = wallData->getOtherController(controllerData);
		bool doesMapNotContainController = processedControllerMap.find(otherController) == processedControllerMap.end();
		if (doesMapNotContainController)
		{
			processedControllerMap.insert(pair<ControllerData *, bool>(otherController, false));
		}
	}

	controllerData = wallData->endControllerData();
	processedControllerMap.insert(pair<ControllerData *, bool>(controllerData, false));

	controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		ControllerData * otherController = wallData->getOtherController(controllerData);
		bool doesMapNotContainController = processedControllerMap.find(otherController) == processedControllerMap.end();
		if (doesMapNotContainController)
		{
			processedControllerMap.insert(pair<ControllerData *, bool>(otherController, false));
		}
	}
	for (auto const& pair : processedControllerMap)
	{
		ControllerData * controllerData = pair.first;
		bool isProcessed = pair.second;
		GenerateCurvedWallSegmentPolygonsCreationDataRelatedToTheController(controllerData, scene);
	}
}

void MergeWallPolygons::CreateLinePolygon(WallGenericPolygonGenerationData &wallPolygonGenerationData, const QPointF &relativeDragPosition, QPointF startPos, QPointF endPos, qreal wallWidth,
	QPainterPath &bezierCurvePath, QPainterPath &referencePath, ClipperLib::Path &bezierCurveClipperPath)
{
	bezierCurvePath = QPainterPath();

	QPointF lineDir = NormalizeQPointF(endPos - startPos);
	QPointF lineNormal = PerpendicularVector(lineDir);

	QPointF startPoint1 = startPos + (lineNormal * wallWidth);
	QPointF startPoint2 = startPos - (lineNormal * wallWidth);
	if (wallPolygonGenerationData.startControlGenerationCase != CurvedWallEndGenerationCase::CNoGenCase)
	{
		startPoint1 = wallPolygonGenerationData.startPoint1 - relativeDragPosition;
		startPoint2 = wallPolygonGenerationData.startPoint2 - relativeDragPosition;
	}

	QPointF endPoint1 = endPos + (lineNormal * wallWidth);
	QPointF endPoint2 = endPos - (lineNormal * wallWidth);
	if (wallPolygonGenerationData.endControlGenerationCase != CurvedWallEndGenerationCase::CNoGenCase)
	{
		endPoint1 = wallPolygonGenerationData.endPoint1 - relativeDragPosition;
		endPoint2 = wallPolygonGenerationData.endPoint2 - relativeDragPosition;
	}

	bezierCurvePath.moveTo(startPoint1);
	bezierCurvePath.lineTo(endPoint1);
	if (wallPolygonGenerationData.endControlGenerationCase == CurvedWallEndGenerationCase::CThreePointGenCase)
	{
		QPointF midPoint = wallPolygonGenerationData.endMidPoint - relativeDragPosition;
		bezierCurvePath.lineTo(midPoint);
	}
	bezierCurvePath.lineTo(endPoint2);
	bezierCurvePath.lineTo(startPoint2);
	if (wallPolygonGenerationData.startControlGenerationCase == CurvedWallEndGenerationCase::CThreePointGenCase)
	{
		QPointF midPoint = wallPolygonGenerationData.startMidPoint - relativeDragPosition;
		bezierCurvePath.lineTo(midPoint);
	}
	bezierCurvePath.lineTo(startPoint1);
}

void MergeWallPolygons::ConvertClipperPolygonPathToQPainterPath(QPainterPath &bezierCurvePath, ClipperLib::Path &bezierCurveClipperPath)
{
	ClipperLib::IntPoint startPoint = bezierCurveClipperPath[0];
	qreal x = (qreal)startPoint.X * ClipperLibPathToQPathScaler;
	qreal y = (qreal)startPoint.Y * ClipperLibPathToQPathScaler;
	bezierCurvePath.moveTo(x, y);

	bool isFist = true;
	for (auto &point : bezierCurveClipperPath)
	{
		if (isFist)
		{
			isFist = false;
			continue;
		}
		qreal x = (qreal)point.X * ClipperLibPathToQPathScaler;
		qreal y = (qreal)point.Y * ClipperLibPathToQPathScaler;
		bezierCurvePath.lineTo(x, y);

		//DEBUG_OUT("point(%f, %f)", x, y);
	}
}

void MergeWallPolygons::CreatePriorBezierPolygonData(ControllerData * controllerData, WallData * wallData,
	QPainterPath &referenceBezierCurvePath, vector<QPointF> &firstSideVectors, vector<QPointF> &secondSideVectors)
{
	QPainterPath bezierCurvePath;
	ClipperLib::Path bezierCurveClipperPath;

	QPointF mainControlPoint = wallData->controlPosition();
	QPointF startP = wallData->startPosition();
	QPointF endP = wallData->endPosition();
	qreal wallHalfWidth = wallData->width() * 0.5;

	QPointF startToEnd = endP - startP;
	//DEBUG_OUT("mcptlcNormalized(%f, %f); length = %f", mcptlcNormalized.x(), mcptlcNormalized.y(), QPointFLength(mcptlcNormalized));

	QPointF c1 = mainControlPoint - startToEnd * ControlpointRatio;
	QPointF c2 = mainControlPoint + startToEnd * ControlpointRatio;

	referenceBezierCurvePath.moveTo(startP);
	referenceBezierCurvePath.cubicTo(c1, c2, endP);

	bezierCurveClipperPath.clear();
	CreateWallBezierClipperPolyonData(wallHalfWidth, referenceBezierCurvePath, bezierCurveClipperPath, firstSideVectors, secondSideVectors);
}

void ReshapeSideVector(vector<QPointF> &sv, int &si, int &ei, QPointF &sp, QPointF &ep, QGraphicsScene * scene)
{
	if (si >= 0 && si < sv.size())
	{
		sv.erase(sv.begin(), sv.begin() + (si + 1));
		sv.insert(sv.begin(), sp);
		//MergeWallPolygons::DrawPoint(sp, scene, "--", 1, Qt::blue);
	}
	if (ei >= 0 && ei < sv.size())
	{
		sv.erase(sv.end() - (ei + 1), sv.end());
		sv.push_back(ep);
		//MergeWallPolygons::DrawPoint(ep, scene, "ep", Qt::yellow);
	}
}

////////////////// this method is to create polygon QGraphicsItem from merge data of walls in the prior steps ///////////////////////
void MergeWallPolygons::CreateFinalBezierPolygon(WallData * wallData, QPainterPath &bezierCurvePath,
	QPainterPath &referenceBezierCurvePath, QPointF relativeDragPosition, QGraphicsScene * scene)
{
	//const char * uuidTC = "035e693b-c4a7-4e04-9ced-18ed0492114e";
	//if (wallData->uuid() == std::string(uuidTC))
	//{
	//	WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
	//	WallGenericPolygonGenerationData &wpgd = wallItem->wallGenericPolygonGenerationData();
	//	//DEBUG_OUT("%s: %d, %d, %d, %d", wallData->uuid().c_str(), wpgd.startIndex1, wpgd.startIndex2, wpgd.endIndex1, wpgd.endIndex2);
	//}

	////const char * uuidTC = "30abdff4-deda-45fe-afcb-a0ce636e8ece";
	////const char * uuidTC = "7dcc7905-a089-4d37-8996-65597273c054";
	//if (wallData->uuid() != std::string(uuidTC))
	//{
	//	WallSegmentItem * wallItem2 = wallData->relatedGraphicsItem();
	//	WallGenericPolygonGenerationData &wpgd2 = wallItem2->wallGenericPolygonGenerationData();
	//	wpgd2.isDataAssigned = false;
	//	return;
	//}

	WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
	WallGenericPolygonGenerationData &wpgd = wallItem->wallGenericPolygonGenerationData();
	referenceBezierCurvePath = wpgd.referencePath;

	if (!wpgd.isDataAssigned)
	{
		return;
	}

	//DEBUG_OUT("firstSideVectors.size = %d, secondSideVectors.size = %d)", wpgd.firstSideVectors.size(), wpgd.secondSideVectors.size());
	//DEBUG_OUT("%s: %d, %d, %d, %d", wallData->uuid().c_str(), wpgd.startIndex1, wpgd.startIndex2, wpgd.endIndex1, wpgd.endIndex2);
	
	ReshapeSideVector(wpgd.firstSideVectors, wpgd.startIndex1, wpgd.endIndex1, wpgd.startPoint1, wpgd.endPoint1, scene);
	ReshapeSideVector(wpgd.secondSideVectors, wpgd.startIndex2, wpgd.endIndex2, wpgd.startPoint2, wpgd.endPoint2, scene);
	std::reverse(wpgd.secondSideVectors.begin(), wpgd.secondSideVectors.end());

	wpgd.polygonVectors.clear();
	vector<QPointF> &wallOutlineVector = wpgd.polygonVectors;
	wallOutlineVector.push_back(wallData->startPosition());
	wallOutlineVector.insert(wallOutlineVector.end(), wpgd.firstSideVectors.begin(), wpgd.firstSideVectors.end());
	wallOutlineVector.push_back(wallData->endPosition());
	//wallOutlineVector.insert(wallOutlineVector.end(), wpgd.secondSideVectors.end(), wpgd.secondSideVectors.begin());
	//std::reverse_copy(wpgd.secondSideVectors.begin(), wpgd.secondSideVectors.end(), std::back_inserter(wallOutlineVector));
	wallOutlineVector.insert(wallOutlineVector.end(), wpgd.secondSideVectors.begin(), wpgd.secondSideVectors.end());
	wallOutlineVector.push_back(wallData->startPosition());

	// prepare side vectors for 3d mesh generation
	
	wpgd.firstSideVectors.insert(wpgd.firstSideVectors.begin(), wallData->startPosition());
	wpgd.firstSideVectors.push_back(wallData->endPosition());

	wpgd.secondSideVectors.insert(wpgd.secondSideVectors.begin(), wallData->endPosition());
	wpgd.secondSideVectors.push_back(wallData->startPosition());

	if (wallData->type() == WallType::Curved)
	{
		std::reverse(std::begin(wallOutlineVector), std::end(wallOutlineVector));
		ClipperLib::Path tmpBezierCurveClipperPath;

		ConvertPointVectorToClipperPolygonData(wallOutlineVector, tmpBezierCurveClipperPath);
		ClipperLib::Path tmpSimplifiedBezierCurveClipperPath;
		bool result = SimplifyPolygon(tmpBezierCurveClipperPath, tmpSimplifiedBezierCurveClipperPath);
		ClipperLib::Path &bezierCurveClipperPath = (result) ? tmpSimplifiedBezierCurveClipperPath : tmpBezierCurveClipperPath;
		wallOutlineVector.clear();
		ConvertClipperPolygonDataToPointVector(bezierCurveClipperPath, wallOutlineVector);
		wallOutlineVector.push_back(wallOutlineVector[0]);
	}

	qreal dragDistance = relativeDragPosition.manhattanLength();
	if (dragDistance < 0.0000000001)
	{
		bezierCurvePath.moveTo(wallOutlineVector[0]);
		auto vIter = wallOutlineVector.begin();
		while (++vIter != wallOutlineVector.end())
		{
			bezierCurvePath.lineTo(*vIter);
		}
	}
	else
	{
		bezierCurvePath.moveTo(wallOutlineVector[0] - relativeDragPosition);
		auto vIter = wallOutlineVector.begin();
		while (++vIter != wallOutlineVector.end())
		{
			bezierCurvePath.lineTo(*vIter - relativeDragPosition);
		}
	}
	
	//if (wallData->uuid() == std::string(uuidTC))
	//if (wallData->type() == WallType::Curved)
	{
		//DEBUG_OUT("%s: %d, %d, %d, %d", wallData->uuid().c_str(), wpgd.startIndex1, wpgd.startIndex2, wpgd.endIndex1, wpgd.endIndex2);
		//DrawCurvedWallOneSideOutline(wallOutlineVector, scene, Qt::yellow);
		//DrawCurvedWallOneSideOutline(wpgd.firstSideVectors, scene, Qt::yellow);
		//DrawCurvedWallOneSideOutline(wpgd.secondSideVectors, scene, Qt::green);

		//int QPathToClipperLibPathScaler_ = 100;

		//std::vector<double> coords;
		//std::vector<Point2d> polygonICVector;
		//for (const QPointF &p : wallOutlineVector)
		//{
		//	coords.push_back(p.x());
		//	coords.push_back(p.y());

		//	Point2d point2d((int)(p.x() * QPathToClipperLibPathScaler_), (int)(p.y() * QPathToClipperLibPathScaler_));
		//	polygonICVector.push_back(point2d);
		//}

		////triangulation happens here
		//delaunator::Delaunator d(coords);
		//PolygonIC polygonIC(polygonICVector);
		//float triAverager = 1.0f / 3.0f;
		//for (std::size_t i = 0; i < d.triangles.size(); i += 3) {
		//	vector<QPointF> triVec;
		//	triVec.clear();
		//	QPointF tp1(d.coords[2 * d.triangles[i]], d.coords[2 * d.triangles[i] + 1]);
		//	QPointF tp2(d.coords[2 * d.triangles[i + 1]], d.coords[2 * d.triangles[i + 1] + 1]);
		//	QPointF tp3(d.coords[2 * d.triangles[i + 2]], d.coords[2 * d.triangles[i + 2] + 1]);

		//	QPointF triCenter = (tp1 + tp2 + tp3) * triAverager;

		//	Point2d tc((int)(triCenter.x() * QPathToClipperLibPathScaler_), (int)(triCenter.y() * QPathToClipperLibPathScaler_));

		//	if (!polygonIC.inPolygon(tc))
		//	{
		//		continue;
		//	}

		//	triVec.push_back(tp1);
		//	triVec.push_back(tp2);
		//	triVec.push_back(tp3);

		//	DrawPoint(triCenter, scene, "", Qt::red);
		//	DrawCurvedWallOneSideOutline(triVec, scene, Qt::yellow);
		//}


		//DrawPoint((wallData->startPosition() + wallData->endPosition()) * 0.5, scene, wallData->uuid().c_str(), Qt::red);

		//DrawPoint(wpgd.startPoint1, scene, "s_1", Qt::green);
		//DrawPoint(wpgd.endPoint1, scene, "e_1", Qt::green);
		//DrawPoint(wpgd.startPoint2, scene, "s_2", Qt::green);
		//DrawPoint(wpgd.endPoint2, scene, "e_2", Qt::green);

		//DrawPoint(wallData->startPosition(), scene, "S", Qt::green);
		//DrawPoint(wallData->endPosition(), scene, "E", Qt::red);
		//DrawCurvedWallOneSideOutline(wpgd.firstSideVectors, scene, Qt::green);
		//DrawCurvedWallOneSideOutline(wpgd.secondSideVectors, scene, Qt::red);
	}

	wpgd.reset();
}

void MergeWallPolygons::CreateWallBezierClipperPolyonData(qreal wallWidth, QPainterPath referenceBezierCurvePath,
	ClipperLib::Path &bezierCurveClipperPath, vector<QPointF> &firstSideVectors, vector<QPointF> &secondSideVectors)
{
	qreal stepSize = 1.0 / (qreal)WallSegmentItem::BezierStepCount;
	for (int i = 0; i <= WallSegmentItem::BezierStepCount; i++)
	{
		qreal percent = (qreal)i * stepSize;

		QPointF bezierSamplePoint = referenceBezierCurvePath.pointAtPercent(percent);

		qreal angleAtPercent = referenceBezierCurvePath.angleAtPercent(percent);
		qreal radianAngleAtPercent = DegreeToRad * angleAtPercent;
		QPointF mirrorVector(qSin(radianAngleAtPercent), qCos(radianAngleAtPercent));

		QPointF innerMirrorPoint = bezierSamplePoint + (mirrorVector * wallWidth);
		firstSideVectors.push_back(innerMirrorPoint);
		QPointF outerMirrorPoint = bezierSamplePoint + (mirrorVector * -wallWidth);
		secondSideVectors.insert(secondSideVectors.begin(), outerMirrorPoint);
	}

	for (QPointF &point : firstSideVectors)
	{
		ClipperLib::IntPoint point((int)(point.x() * QPathToClipperLibPathScaler),
			(int)(point.y() * QPathToClipperLibPathScaler));
		bezierCurveClipperPath.push_back(point);
	}

	for (QPointF &point : secondSideVectors)
	{
		ClipperLib::IntPoint point((int)(point.x() * QPathToClipperLibPathScaler),
			(int)(point.y() * QPathToClipperLibPathScaler));
		bezierCurveClipperPath.push_back(point);
	}
}

bool walldataAnlgleComparer(CurvedWallDataWithAngle &wdwa1, CurvedWallDataWithAngle &wdwa2)
{
	return wdwa1.angle < wdwa2.angle;
}

void MergeWallPolygons::DrawPoint(QPointF cp, QGraphicsScene * scene, QString label, QColor color)
{
	qreal radius = 1.5;
	MergeWallPolygons::DrawPoint(cp, scene, label, radius, color);
}

void MergeWallPolygons::DrawPoint(QPointF cp, QGraphicsScene * scene, QString label, qreal radius, QColor color)
{
	if (label.size() > 0)
	{
		QGraphicsTextItem * textItem = new QGraphicsTextItem(label);
		QFont font;
		font.setPixelSize(8);
		font.setBold(true);
		font.setFamily("Calibri");
		textItem->setDefaultTextColor(color);
		textItem->setFont(font);
		scene->addItem(textItem);
		textItem->setPos(cp);
		lines.push_back(textItem);
	}

	QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - radius, 0 - radius, (radius * 2.0f) - .0f, (radius * 2.0f) - .0f, nullptr);
	intersect->setBrush(color);
	intersect->setPen(Qt::NoPen);
	scene->addItem(intersect);
	lines.push_back(intersect);
	intersect->setPos(cp);
}

void MergeWallPolygons::DrawLine(QPointF p1, QPointF p2, QGraphicsScene * scene, QColor color)
{
	QLineF lineF(p1, p2);
	QGraphicsLineItem * lineItem = new QGraphicsLineItem(lineF);
	lineItem->setPen(QPen(color));
	scene->addItem(lineItem);
	lines.push_back(lineItem);
}

void MergeWallPolygons::DrawCurvedWallOneSideOutline(std::vector<QPointF> &sideVectors, QGraphicsScene * scene, QColor color)
{
	int count = sideVectors.size();
	if (count > 0)
	{
		for (int i = 0; i < count - 1; i++)
		{
			QPointF cp = sideVectors[i];
			size_t ni = (i + 1) % count;
			QPointF ncp = sideVectors[ni];

			DrawLine(cp, ncp, scene, color);

			//DrawPoint(cp, scene, "", color);
			DrawPoint(cp, scene, QString::number(i), color);
		}
		QPointF lcp = sideVectors[count - 1];
		DrawPoint(lcp, scene, QString::number(count - 1), color);
		//DrawPoint(lcp, scene, "", color);
	}
}

void MergeWallPolygons::ConvertPointVectorToClipperPolygonData(std::vector<QPointF> bezierCurveVector, ClipperLib::Path &bezierCurveClipperPath)
{
	//bezierCurveVector.pop_back();
	for (const QPointF p : bezierCurveVector)
	{
		ClipperLib::IntPoint point((int)(p.x() * QPathToClipperLibPathScaler), (int)(p.y() * QPathToClipperLibPathScaler));
		bezierCurveClipperPath.push_back(point);
	}
}

void MergeWallPolygons::ConvertClipperPolygonDataToPointVector(ClipperLib::Path bezierCurveClipperPath, std::vector<QPointF> &bezierCurveVector)
{
	for (const auto &point : bezierCurveClipperPath)
	{
		qreal x = (qreal)point.X * ClipperLibPathToQPathScaler;
		qreal y = (qreal)point.Y * ClipperLibPathToQPathScaler;
		bezierCurveVector.push_back(QPointF(x, y));
	}
}

void DrawCurvedWallOutline(CurvedWallDataWithAngle &wdwa, QGraphicsScene * scene, vector<QGraphicsItem*> &lines)
{
	MergeWallPolygons::DrawCurvedWallOneSideOutline(wdwa.firstSideVectors, scene, Qt::green);
	MergeWallPolygons::DrawCurvedWallOneSideOutline(wdwa.secondSideVectors, scene, Qt::red);
}

////////////////// this method is to calculate detailed merge data related only one side of onlt one wall related to the controller ///////////////////////
void generateCurvedWallSideMergablePolygonDataDueToIntersections(ControllerData * centerCD, CurvedWallDataWithAngle &wdwa1, CurvedWallDataWithAngle &wdwa2, int wallIndex1, int wallIndex2,
	QGraphicsScene * scene, vector<QGraphicsItem*> &lines)
{
	//DrawCurvedWallOutline(wdwa1, scene, lines);
	//DrawCurvedWallOutline(wdwa2, scene, lines);

	/////////////// wdwa1 //////////////

	WallData * wallData1 = wdwa1.wallData;
	WallSegmentControlledPart wscp1 = wallData1->getControllerPartType(centerCD);
	WallSegmentItem * wsi1 = wallData1->relatedGraphicsItem();
	WallGenericPolygonGenerationData& wallPolygonGenerationData1 = wsi1->wallGenericPolygonGenerationData();

	ControllerData * otherController1 = wallData1->getOtherController(centerCD);
	QPointF controlCenterPos = centerCD->position();
	QPointF endPos1 = otherController1->position();
	qreal wallHalfWidth1 = wallData1->width() * 0.5;
	//QPointF lineDir1 = NormalizeQPointF(endPos1 - controlCenterPos);
	QPointF &lineDir1 = wdwa1.direction;
	//QPointF lineNormal1 = PerpendicularVector(lineDir1);
	QPointF &lineNormal1 = wdwa1.normal;
	//QPointF sideStartPoint1 = controlCenterPos + (lineNormal1 * wallWidth1);
	QPointF &sideStartPoint1 = wdwa1.sidePoint1;
	QPointF sideEndPoint1 = endPos1 + (lineNormal1 * wallHalfWidth1);

	/////////////// wdwa2 //////////////

	WallData * wallData2 = wdwa2.wallData;
	WallSegmentControlledPart wscp2 = wallData2->getControllerPartType(centerCD);
	WallSegmentItem * wsi2 = wallData2->relatedGraphicsItem();
	WallGenericPolygonGenerationData& wallPolygonGenerationData2 = wsi2->wallGenericPolygonGenerationData();

	ControllerData * otherController2 = wallData2->getOtherController(centerCD);
	QPointF endPos2 = otherController2->position();
	qreal wallHalfWidth2 = wallData2->width() * 0.5;
	//QPointF lineDir2 = NormalizeQPointF(endPos2 - controlCenterPos);
	QPointF &lineDir2 = wdwa2.direction;
	//QPointF lineNormal2 = QPointF(lineDir2.y(), -lineDir2.x());
	QPointF &lineNormal2 = wdwa2.normal;
	//QPointF sideStartPoint2 = controlCenterPos - (lineNormal2 * wallWidth2);
	QPointF &sideStartPoint2 = wdwa2.sidePoint2;
	QPointF sideEndPoint2 = endPos2 - (lineNormal2 * wallHalfWidth2);


	//QPointF wallCenter = (endPos1 + controlCenterPos) * 0.5;
	//QLineF lineF(wallCenter, wallCenter + (lineNormal1 * 15));
	////QLineF lineF(wallCenter, wallCenter + (ln * 15));
	//QGraphicsLineItem * lineItem = new QGraphicsLineItem(lineF);
	//lineItem->setPen(QPen(Qt::green));
	//scene->addItem(lineItem);
	//lines.push_back(lineItem);


	int index1 = -1;
	int index2 = -1;
	//QPointF intersectionPoint;
	//bool isPointOnSideOfAnyWall = IntersectionOfTwoLineSegment(sideStartPoint1, sideEndPoint1, sideStartPoint2, sideEndPoint2, intersectionPoint);

	//QPointF originalLineNormal1 = wallData1->normal();
	//QPointF originalLineNormal2 = wallData2->normal();
	QPointF originalLineNormal1;
	QPointF originalLineNormal2;
	QPointF intersectionPoint;
	bool isPointOnSideOfAnyWall = IntersectionOfTwoLineStips(wdwa1.firstSideVectors, wdwa2.secondSideVectors, index1, index2,
		originalLineNormal1, originalLineNormal2, intersectionPoint);
	originalLineNormal1 = originalLineNormal1 * ((wscp1 == WallSegmentControlledPart::Start) ? 1 : -1);
	originalLineNormal2 = originalLineNormal2 * ((wscp2 == WallSegmentControlledPart::Start) ? 1 : -1);

	//////////////////
	//QPointF pos1 = wscp1 == WallSegmentControlledPart::Start ? wallData1->endPosition() : wallData1->startPosition();
	//QLineF lineF1(pos1, pos1 + (originalLineNormal1 * 15));
	//QGraphicsLineItem * lineItem1 = new QGraphicsLineItem(lineF1);
	//lineItem1->setPen(QPen(Qt::green, 2));
	//scene->addItem(lineItem1);
	//lines.push_back(lineItem1);

	//QPointF pos2 = wscp2 == WallSegmentControlledPart::Start ? wallData2->endPosition() : wallData2->startPosition();
	//QLineF lineF2(pos2, pos2 + (originalLineNormal2 * 15));
	//QGraphicsLineItem * lineItem2 = new QGraphicsLineItem(lineF2);
	//lineItem2->setPen(QPen(Qt::red));
	//scene->addItem(lineItem2);
	//lines.push_back(lineItem2);
	//////////////////

	QPointF cloneIntersectionPointForNextWall = intersectionPoint;
	//if (wallData1->type() == WallType::Curved || wallData2->type() == WallType::Curved)
	//{
	//	DEBUG_OUT("%s: index1 = %d, %s: index2 = %d)", wallData1->uuid().c_str(), index1, wallData2->uuid().c_str(), index2);
	//}

	bool doesIntersectWithEndSide = false;
	if (!isPointOnSideOfAnyWall)
	{
		//bool yepyep = wallData1->uuid() == std::string("30abdff4-deda-45fe-afcb-a0ce636e8ece") || wallData2->uuid() == std::string("30abdff4-deda-45fe-afcb-a0ce636e8ece");
		//const char * uuidTC = "035e693b-c4a7-4e04-9ced-18ed0492114e";
		//bool yepyep = wallData1->uuid() == std::string(uuidTC) || wallData2->uuid() == std::string(uuidTC);
		index1 = 0;
		index2 = 0;

		//MergeWallPolygons::DrawPoint(intersectionPoint, scene, "", Qt::darkBlue);

		////if (yepyep)
		//{
		//	MergeWallPolygons::DrawLine(sideStartPoint2, sideEndPoint2, scene, Qt::red);
		//	//MergeWallPolygons::DrawLine(sideEndPoint1, endPos1, scene, Qt::green);
		//	MergeWallPolygons::DrawPoint(sideEndPoint1, scene, "", Qt::blue);
		//	MergeWallPolygons::DrawPoint(endPos1, scene, "", Qt::yellow);
		//	MergeWallPolygons::DrawLine(sideEndPoint1, sideStartPoint1, scene, Qt::green);
		//}

		//QPointF otherSideEndPoint2 = endPos2 + (lineNormal2 * wallWidth2);
		QPointF outerIntersectionPoint;

		//IntersectionOfTwoLineSegment(sideStartPoint1, sideEndPoint1, sideEndPoint2, endPos2, outerIntersectionPoint);
		IntersectionOfTwoLineSegment(sideStartPoint2, sideEndPoint2, sideEndPoint1, endPos1, outerIntersectionPoint);
		doesIntersectWithEndSide = IsPointInLineSegment(outerIntersectionPoint, sideEndPoint1, endPos1);
		//sideStartPoint2 - startPos1

		if (doesIntersectWithEndSide)
		{
			QPointF innerIntersectionPoint;
			IntersectionOfTwoLineSegment(sideStartPoint2, sideEndPoint2, sideStartPoint1, controlCenterPos, innerIntersectionPoint);
			cloneIntersectionPointForNextWall = innerIntersectionPoint;

			intersectionPoint = sideStartPoint1;

			//MergeWallPolygons::DrawPoint(intersectionPoint, scene, "", Qt::green);
			//MergeWallPolygons::DrawPoint(cloneIntersectionPointForNextWall, scene, "", Qt::yellow);

			//((wscp2 == WallSegmentControlledPart::Start) ? wallPolygonGenerationData2.startControlGenerationCase :
			//	wallPolygonGenerationData2.endControlGenerationCase) = WallEndGenerationCase::TwoPointGenCase;

			//qreal radius = 2.0;
			//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - radius, 0 - radius, (radius * 2.0f) - .0f, (radius * 2.0f) - .0f, nullptr);
			////intersect->setBrush(isPointOnTheSideOfTheSecondWall ? QBrush(Qt::green) : QBrush(Qt::red));
			//intersect->setBrush(Qt::NoBrush);
			//intersect->setPen(QPen(Qt::darkBlue, 0.25));
			//scene->addItem(intersect);
			//lines.push_back(intersect);
			//intersect->setPos(innerIntersectionPoint);
		}
	}

	//MergeWallPolygons::DrawPoint(intersectionPoint, scene, "",
	//	(doesIntersectWithEndSide ? Qt::darkBlue : (isPointOnSideOfAnyWall ? Qt::green : Qt::red)));

	////QString text = QString::number(wallIndex1) + "_" + QString::number(wallIndex2);
	//QString text = doesIntersectWithEndSide ? QString("T") : QString("F");
	//QGraphicsTextItem * textItem = new QGraphicsTextItem(text);
	//QFont font;
	//font.setPixelSize(6);
	//font.setBold(true);
	//font.setFamily("Calibri");
	//textItem->setDefaultTextColor(Qt::green);
	//textItem->setFont(font);
	//scene->addItem(textItem);
	//lines.push_back(textItem);
	//textItem->setPos(intersectionPoint - QPointF(9.5, 8));

	qreal dotProductOfTwoWalls = DotProduct(lineDir1, lineDir2);
	if ((dotProductOfTwoWalls + 1) < _DOT_PRODUCT_EPSILON_)
	{
		intersectionPoint = sideStartPoint1;
		cloneIntersectionPointForNextWall = sideStartPoint2;
	}
	//else if (qAbs(wallWidth1 - wallWidth2) > _DOT_PRODUCT_EPSILON_ && !isPointOnSideOfAnyWall)
	//{
	//	if (wallWidth1 > wallWidth2)
	//	{
	//		//intersectionPoint = startPoint1;
	//		//cloneIntersectionPointForNextWall = startPoint2;
	//	}
	//	//else
	//	//{
	//	//	intersectionPoint = startPoint1;
	//	//	cloneIntersectionPointForNextWall = startPoint2;
	//	//}
	//}
	////else if (!doesIntersected && dotProductOfSecondWallAndCenterTointersection > 0)
	////{
	////	((wscp1 == WallSegmentControlledPart::Start) ? wallPolygonGenerationData1.startControlGenerationCase :
	////		wallPolygonGenerationData1.endControlGenerationCase) = WallEndGenerationCase::NoGenCase;
	////	((wscp2 == WallSegmentControlledPart::Start) ? wallPolygonGenerationData2.startControlGenerationCase :
	////		wallPolygonGenerationData2.endControlGenerationCase) = WallEndGenerationCase::NoGenCase;
	////}



	qreal dotProduct = DotProduct(originalLineNormal1, intersectionPoint - controlCenterPos);
	if (wscp1 == WallSegmentControlledPart::Start && dotProduct > 0)
	{
		wallPolygonGenerationData1.startPoint1 = intersectionPoint;
		wallPolygonGenerationData1.startIndex1 = index1;
	}
	else if (wscp1 == WallSegmentControlledPart::Start && dotProduct < 0)
	{
		wallPolygonGenerationData1.startPoint2 = intersectionPoint;
		wallPolygonGenerationData1.startIndex2 = index1;
	}
	else if (wscp1 == WallSegmentControlledPart::End && dotProduct > 0)
	{
		wallPolygonGenerationData1.endPoint1 = intersectionPoint;
		wallPolygonGenerationData1.endIndex1 = index1;
	}
	else if (wscp1 == WallSegmentControlledPart::End && dotProduct < 0)
	{
		wallPolygonGenerationData1.endPoint2 = intersectionPoint;
		wallPolygonGenerationData1.endIndex2 = index1;
	}
	((wscp1 == WallSegmentControlledPart::Start) ?
		wallPolygonGenerationData1.startMidPoint : wallPolygonGenerationData1.endMidPoint) = controlCenterPos;


	dotProduct = DotProduct(originalLineNormal2, intersectionPoint - controlCenterPos);
	if (wscp2 == WallSegmentControlledPart::Start && dotProduct > 0)
	{
		wallPolygonGenerationData2.startPoint1 = cloneIntersectionPointForNextWall;
		wallPolygonGenerationData2.startIndex1 = index2;
	}
	else if (wscp2 == WallSegmentControlledPart::Start && dotProduct < 0)
	{
		wallPolygonGenerationData2.startPoint2 = cloneIntersectionPointForNextWall;
		wallPolygonGenerationData2.startIndex2 = index2;
	}
	else if (wscp2 == WallSegmentControlledPart::End && dotProduct > 0)
	{
		wallPolygonGenerationData2.endPoint1 = cloneIntersectionPointForNextWall;
		wallPolygonGenerationData2.endIndex1 = index2;
	}
	else if (wscp2 == WallSegmentControlledPart::End && dotProduct < 0)
	{
		wallPolygonGenerationData2.endPoint2 = cloneIntersectionPointForNextWall;
		wallPolygonGenerationData2.endIndex2 = index2;
	}
	((wscp2 == WallSegmentControlledPart::Start) ?
		wallPolygonGenerationData2.startMidPoint : wallPolygonGenerationData2.endMidPoint) = controlCenterPos;
}
#include "FileUtilities.h"

#include <fstream>
#include <sstream>

#include <cctype>

#include <stdlib.h>

#include <filesystem> // or #include <experimental/filesystem>

using namespace std;
namespace fs = std::experimental::filesystem;

namespace FileUtilities
{
	std::string retrieveFileName(const std::string& path)
	{
		vector<string> splitPath = split(path, '\\');
		string fileName = splitPath[splitPath.size() - 1];
		return fileName;
	}

	string retrieveParentPath(const std::string& fname)
	{
		std::string fname_tmp = fname;

		size_t pos = fname_tmp.find_last_of("\\/");
		if (pos == fname_tmp.length() - 1)
		{
			fname_tmp.pop_back();
			pos = fname_tmp.find_last_of("\\/");
		}
		return (std::string::npos == pos)
			? ""
			: fname_tmp.substr(0, pos);
	}

	int writeTextToFile(string content, string path)
	{
		std::ofstream out(path);
		out << content;
		out.close();
		return 0;
	}

	void parseKeyValuePairTextFile(const string &filePath, map<string, string> & keyValueMap)
	{
		vector<string> * lines = readAllLines(filePath);

		// for each (string line in *lines)
		for (const auto& line : *lines)
		{
			vector<string> splitLine = split(line, '=');
			std::pair<string, string> kv(splitLine[0], splitLine[1]);
			keyValueMap.insert(kv);
		}

		delete lines;
	}

	void readfile(const std::string &filepath, std::string &buffer)
	{
		std::ifstream fin(filepath.c_str());
		getline(fin, buffer, char(-1));
		fin.close();
	}

	std::vector<std::string> * readAllLines(const std::string &filepath)
	{
		std::vector<std::string> * lines = new std::vector<std::string>();
		std::ifstream fin(filepath.c_str());
		std::string line;
		while (std::getline(fin, line))
		{
			lines->push_back(line);
		}
		fin.close();

		return lines;
	}

	namespace fs = std::experimental::filesystem;

	bool PathExists(const char* dirName)
	{
		return fs::exists(dirName);
	}

	bool CreateFolder(const char* dirName)
	{
		return fs::create_directory(dirName);
	}

	bool RemoveFolder(const char* path)
	{
		return fs::remove_all(path);
	}

	bool CopyFileFrom(const char* src, const char* dst)
	{
		return fs::copy_file(src, dst);
	}

	bool ConvertToBoolean(char * str)
	{
		std::transform(str, str + std::strlen(str), str, static_cast<int(*)(int)>(std::toupper));
		return (strcmp(str, "TRUE") == 0) ? true : false;
	}

	int ConvertToInt(std::string &str)
	{
		return atoi(str.c_str());
	}

	double ConvertToDouble(std::string &str)
	{
		return atof(str.c_str());
	}

	std::string ToUpperCaseString(std::string orginal)
	{
		std::string upperCase;
		upperCase.resize(orginal.length());
		std::transform(orginal.begin(), orginal.end(), upperCase.begin(), static_cast<int(*)(int)>(std::toupper));
		return upperCase;
	}

	std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
		std::stringstream ss(s);
		std::string item;
		while (getline(ss, item, delim)) {
			if (item.size() > 0)
			{
				elems.push_back(item);
			}
		}
		return elems;
	}

	std::vector<std::string> split(const std::string &s, char delim) {
		std::vector<std::string> elems;
		split(s, delim, elems);
		return elems;
	}

	bool beginsWith(std::string content, std::string prefix)
	{
		return !content.compare(0, prefix.size(), prefix);
	}

	bool endsWith(std::string content, std::string suffix)
	{
		return !content.compare(content.size() - suffix.size(), suffix.size(), suffix);
	}

	ConfigurationKeyValueMap * ConfigurationKeyValueMap::instance = nullptr;

	string ConfigurationKeyValueMap::operator[] (const string& key)
	{
		map<string, string>::iterator mapEntryPosition = keyValueMap.find(key);
		if (mapEntryPosition != keyValueMap.end())
		{
			return keyValueMap[key];
		}
		return string("");
	}

	ConfigurationKeyValueMap * ConfigurationKeyValueMap::ins()
	{
		if (instance == nullptr)
		{
			instance = new ConfigurationKeyValueMap();
		}
		return instance;
	}

	ConfigurationKeyValueMap::ConfigurationKeyValueMap()
	{
		std::string filePath = "configuration.txt";
		FileUtilities::parseKeyValuePairTextFile(filePath, keyValueMap);
	}
}
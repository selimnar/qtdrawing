#include "utilities.h"

#include <iterator>

#include <qtmath>

#include <Windows.h>
#include <iostream>
#include <sstream>
#include <QPointF>

#include <stdio.h>

#define XERCES_STATIC_LIBRARY
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//Mandatory for using any feature of Xerces.
#include <xercesc/util/PlatformUtils.hpp>
//Use the Document Object Model (DOM) API
#include <xercesc/dom/DOM.hpp>
//Required for outputing a Xerces DOMDocument to a standard output stream (Also see: XMLFormatTarget)
#include <xercesc/framework/StdOutFormatTarget.hpp>
//Required for outputing a Xerces DOMDocument to the file system (Also see: XMLFormatTarget)
#include <xercesc/framework/LocalFileFormatTarget.hpp>
XERCES_CPP_NAMESPACE_USE

#define BUFFER_LENGTH 1024 * 16

void DBOut(const char *file, const int line, const char *s)
{
	std::wostringstream os_;
	os_ << file << "(" << line << "): ";
	os_ << s << std::endl;
	OutputDebugString(os_.str().c_str());
}

void DebugOut(const char *file, const int line, const char* format, ...)
{
	va_list argptr;
	va_start(argptr, format);

	char dest[BUFFER_LENGTH];
	vsnprintf(dest, BUFFER_LENGTH, format, argptr);
	DBOut(file, line, dest);

	va_end(argptr);
}

void DBOutW(const char *file, const int line, const wchar_t *s)
{
	std::wostringstream os_;
	os_ << file << "(" << line << "): ";
	os_ << s << std::endl;
	OutputDebugString(os_.str().c_str());
}

void DebugOutW(const char *file, const int line, const wchar_t* format, ...)
{
	va_list argptr;
	va_start(argptr, format);

	wchar_t dest[BUFFER_LENGTH];
	_vsnwprintf(dest, BUFFER_LENGTH, format, argptr);
	DBOutW(file, line, dest);

	va_end(argptr);
}

std::ofstream& operator << (std::ofstream& ofs, QPointF &p)
{
	qreal x = p.x();
	qreal y = p.y();
	ofs.write((const char*)&x, sizeof(qreal));
	ofs.write((const char*)&y, sizeof(qreal));

	return ofs;
}

std::ifstream& operator >> (std::ifstream& ifs, QPointF &p)
{
	qreal x;
	qreal y;
	ifs.read((char*)&x, sizeof(qreal));
	ifs.read((char*)&y, sizeof(qreal));

	p.setX(x);
	p.setY(y);

	return ifs;
}

bool ClosestDistanceToLine(const QPointF &P, const QPointF &P1, const QPointF &P2, QPointF &projectedPoint, double &distanceToProjectedPoint)
{
	qreal m = (P2.x() - P1.x()) / (P2.y() - P1.y());
	qreal m_2 = m * m;
	qreal X = (m_2 * P.x() + m * (P.y() - P1.y()) + P1.x()) / (m_2 + 1);
	qreal Y = m * (P.x() - X) + P.y();
	projectedPoint.setX(X);
	projectedPoint.setY(Y);
	distanceToProjectedPoint = Distance(P, projectedPoint);

	QPointF P1_to_P = P - P1;
	QPointF P1_to_P2 = P2 - P1;
	qreal dotProduct = DotProduct(P1_to_P, P1_to_P2);
	if (dotProduct < 0)
	{
		return false;
	}
	else
	{
		return QPointFLength(P1_to_P2) > QPointFLength(P1_to_P);
	}
	return true;
}

void LineEquationFromTwoPoints(const QPointF &P1, const QPointF &P2, double &m, double &c)
{
	double difX = P2.x() - P1.x();
	double difY = P2.y() - P1.y();
	m = difY / difX;
	c = P1.y() - m * P1.x();
}

bool IntersectionOfTwoLineSegment(const QPointF &L1_P1, const QPointF &L1_P2,
	const QPointF &L2_P1, const QPointF &L2_P2, QPointF &intersectionPoint)
{
	double m1, m2, c1, c2;
	LineEquationFromTwoPoints(L1_P1, L1_P2, m1, c1);
	LineEquationFromTwoPoints(L2_P1, L2_P2, m2, c2);
	double x = (c2 - c1) / (m1 - m2);
	double y = m1 * x + c1;
	//double y = m2 * x + c2;
	intersectionPoint.setX(x);
	intersectionPoint.setY(y);

	QPointF L1_P1_to_Inter = intersectionPoint - L1_P1;
	QPointF L1_P2_to_Inter = intersectionPoint - L1_P2;
	bool isIntersectionPointInLineSegmentOne = DotProduct(L1_P1_to_Inter, L1_P2_to_Inter) < 0;
	QPointF L2_P1_to_Inter = intersectionPoint - L2_P1;
	QPointF L2_P2_to_Inter = intersectionPoint - L2_P2;
	bool isIntersectionPointInLineSegmentTwo = DotProduct(L2_P1_to_Inter, L2_P2_to_Inter) < 0;

	return isIntersectionPointInLineSegmentOne && isIntersectionPointInLineSegmentTwo;
}

bool IntersectionOfTwoLineStips(std::vector<QPointF> strip1, std::vector<QPointF> strip2, int &index1, int &index2,
	QPointF &originalLineNormal1, QPointF &originalLineNormal2, QPointF &intersectionPoint)
{
	int strip1PointCount = strip1.size();
	int strip2PointCount = strip2.size();

	index1 = -1;
	index2 = -1;

	QPointF s1flcp0 = strip1[0];
	QPointF s1flcp1 = strip1[1];

	QPointF s2flcp0 = strip2[0];
	QPointF s2flcp1 = strip2[1];

	originalLineNormal1 = PerpendicularVector(NormalizeQPointF(s1flcp1 - s1flcp0));
	originalLineNormal2 = PerpendicularVector(NormalizeQPointF(s2flcp1 - s2flcp0));
	bool doesIntersect = IntersectionOfTwoLineSegment(s1flcp0, s1flcp1, s2flcp0, s2flcp1, intersectionPoint);
	if (doesIntersect)
	{
		index1 = 0;
		index2 = 0;
		//return true;
	}

	//originalLineNormal1 = PerpendicularVector(NormalizeQPointF(strip1[1] - strip1[0]));
	//originalLineNormal2 = PerpendicularVector(NormalizeQPointF(strip2[1] - strip2[0]));
	
	QPointF tmpIntersectionPoint;
	for (int i = 0; i < strip1PointCount - 1; i++)
	{
		QPointF cp1 = strip1[i];
		QPointF ncp1 = strip1[i + 1];
		//size_t ni = (i + 1) % strip1PointCount;
		//QPointF ncp1 = strip1[ni];

		for (int j = 0; j < strip2PointCount - 1; j++)
		{
			QPointF cp2 = strip2[j];
			QPointF ncp2 = strip2[j + 1];
			//size_t nj = (j + 1) % strip2PointCount;
			//QPointF ncp2 = strip2[nj];

			bool doesIntersect = IntersectionOfTwoLineSegment(cp1, ncp1, cp2, ncp2, tmpIntersectionPoint);
			if (doesIntersect)
			{
				intersectionPoint = tmpIntersectionPoint;
				index1 = i;
				index2 = j;

				originalLineNormal1 = PerpendicularVector(NormalizeQPointF(ncp1 - cp1));
				originalLineNormal2 = PerpendicularVector(NormalizeQPointF(ncp2 - cp2));

				return true;
			}
		}
	}
	return false;
}

bool IsPointInLineSegment(const QPointF &P, const QPointF &P1, const QPointF &P2)
{
	QPointF P1_to_P = P - P1;
	QPointF P2_to_P = P - P2;
	return DotProduct(P1_to_P, P2_to_P) < 0;
}

QPointF NormalizeQPointF(QPointF point)
{
	QPointF normalized = point * (1 / QPointFLength(point));
	return normalized;
}

double QPointFLength(QPointF point)
{
	double x = point.x();
	double y = point.y();
	return qSqrt(x * x + y * y);
}

double Distance(QPointF p1, QPointF p2)
{
	double x = p1.x() - p2.x();
	double y = p1.y() - p2.y();
	return qSqrt(x * x + y * y);
}

double DotProduct(QPointF a, QPointF b)
{
	return (a.x() * b.x()) + (a.y() * b.y());
}

double CrossProduct(QPointF a, QPointF b)
{
	return a.x() * b.y() - a.y() * b.x();
}

double AngleInRadianBetweenTwoUnitVectors(QPointF a, QPointF b)
{
	double cross = a.x() * b.y() - a.y() * b.x();
	double dot = (a.x() * b.x()) + (a.y() * b.y());
	return qAtan2(cross, dot);
}

QPointF ProjectBOnA(QPointF a, QPointF b)
{
	double al = QPointFLength(a);
	return a * (DotProduct(a, b) / (al * al));
}

QPointF PerpendicularVector(QPointF a)
{
	return QPointF(a.y(), -a.x());
}

double Sign(double val)
{
	double absVal = qAbs(val);
	if (absVal < EPSILON)
		return 1;
	return val / absVal;
}

bool initializeXerces()
{
	try
	{
		XMLPlatformUtils::Initialize();  // Initialize Xerces infrastructure
		DEBUG_OUT("OHHH YEAAHH XERCES INITIALIZED!!!!!!!!!");
		return true;
	}
	catch (XMLException& e)
	{
		char* message = XMLString::transcode(e.getMessage());
		DEBUG_OUT("XML toolkit initialization error: %s", message);
		//cerr << "XML toolkit initialization error: " << message << endl;
		XMLString::release(&message);
		// throw exception here to return ERROR_XERCES_INIT
	}
	return false;
}

void finalizeXerces()
{
	XMLPlatformUtils::Terminate();
}

void WriteXMLDocument(XERCES_CPP_NAMESPACE::DOMDocument * pmyDOMDocument, const char * FullFilePath)
{
	DOMImplementation    *pImplement = NULL;
	DOMLSSerializer      *pSerializer = NULL;
	XMLFormatTarget      *pTarget = NULL;

	/*
	Return the first registered implementation that
	has the desired features. In this case, we are after
	a DOM implementation that has the LS feature... or Load/Save.
	*/
	pImplement = DOMImplementationRegistry::getDOMImplementation(u"LS");

	/*
	From the DOMImplementation, create a DOMWriter.
	DOMWriters are used to serialize a DOM tree [back] into an XML document.
	*/
	pSerializer = ((DOMImplementationLS*)pImplement)->createLSSerializer();


	/*
	This line is optional. It just sets a feature
	of the Serializer to make the output
	more human-readable by inserting line-feeds,
	without actually inserting any new elements/nodes
	into the DOM tree. (There are many different features to set.)
	Comment it out and see the difference.
	*/
	// Make the output more human readable by inserting line feeds.
	if (pSerializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
		pSerializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);


	/*
	Choose a location for the serialized output. The 3 options are:
	1) StdOutFormatTarget     (std output stream -  good for debugging)
	2) MemBufFormatTarget     (to Memory)
	3) LocalFileFormatTarget  (save to file)
	(Note: You'll need a different header file for each one)
	Don't forget to escape file-paths with a backslash character, or
	just specify a file to be saved in the exe directory.

	eg. c:\\example\\subfolder\\pfile.xml

	*/
	pTarget = new LocalFileFormatTarget(FullFilePath);
	// Write the serialized output to the target.
	DOMLSOutput* pDomLsOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
	pDomLsOutput->setByteStream(pTarget);

	pSerializer->write(pmyDOMDocument, pDomLsOutput);

	//pSerializer->release();

	// Cleanup.
	pSerializer->release();
	XMLCh * fullFilePathXMLCh = XMLString::transcode(FullFilePath);
	//XMLCh**  buf
	XMLString::release(&fullFilePathXMLCh);
	//XMLString::release(XMLString::transcode(FullFilePath));
	delete pTarget;
	pDomLsOutput->release();
}

XERCES_CPP_NAMESPACE::DOMDocument * CreateDocument(const XMLCh * rootElementName)
{
	// Pointer to our DOMImplementation.
	DOMImplementation * p_DOMImplementation = DOMImplementationRegistry::getDOMImplementation(XMLString::transcode("core"));

	// Pointer to our DOMDocument.
	xercesc::DOMDocument * p_DOMDocument = p_DOMImplementation->createDocument(nullptr, rootElementName, nullptr);

	return p_DOMDocument;
}

std::u16string ConvertWcharToXMLChar(const wchar_t * wstr)
{
	std::wstring source(wstr);
	return ConvertWcharToXMLChar(source);
}

std::u16string ConvertWcharToXMLChar(std::wstring source)
{
	std::u16string result;
	std::copy(source.begin(), source.end(), std::back_inserter(result));
	return result;
}

std::u16string ConvertWcharToXMLChar(const char * wstr)
{
	std::string source(wstr);
	return ConvertWcharToXMLChar(source);
}

std::u16string ConvertWcharToXMLChar(std::string source)
{
	std::u16string result;
	std::copy(source.begin(), source.end(), std::back_inserter(result));
	return result;
}

std::u16string ConvertToUint16String(unsigned int number)
{
	//std::wstring value = std::to_wstring(number);
	std::string value = std::to_string(number);
	return ConvertWcharToXMLChar(value.c_str());
}

std::u16string ConvertToUint16String(int number)
{
	//std::wstring value = std::to_wstring(number);
	std::string value = std::to_string(number);
	return ConvertWcharToXMLChar(value.c_str());
}

//void DoOutput2Stream(xercesc::DOMDocument* pmyDOMDocument)
//{
//	DOMImplementation    *pImplement = NULL;
//	DOMLSSerializer      *pSerializer = NULL;
//	XMLFormatTarget      *pTarget = NULL;
//
//	/*
//	Return the first registered implementation that has
//	the desired features. In this case, we are after
//	a DOM implementation that has the LS feature... or Load/Save.
//	*/
//	pImplement = DOMImplementationRegistry::getDOMImplementation(u"LS");
//
//	/*
//	From the DOMImplementation, create a DOMWriter.
//	DOMWriters are used to serialize a DOM tree [back] into an XML document.
//	*/
//	pSerializer = ((DOMImplementationLS*)pImplement)->createLSSerializer();
//
//	/*
//	This line is optional. It just sets a feature
//	of the Serializer to make the output
//	more human-readable by inserting line-feeds and tabs,
//	without actually inserting any new nodes
//	into the DOM tree. (There are many different features to set.)
//	Comment it out and see the difference.
//	*/
//
//	//The end-of-line sequence of characters to be used in the XML being written out. 
//	pSerializer->setNewLine(XMLString::transcode("\n"));
//
//	DOMConfiguration* pDomConfiguration = pSerializer->getDomConfig();
//	pDomConfiguration->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
//
//
//	/*
//	Choose a location for the serialized output. The 3 options are:
//	1) StdOutFormatTarget     (std output stream -  good for debugging)
//	2) MemBufFormatTarget     (to Memory)
//	3) LocalFileFormatTarget  (save to file)
//	(Note: You'll need a different header file for each one)
//	*/
//	pTarget = new StdOutFormatTarget();
//	DOMLSOutput* pDomLsOutput = ((DOMImplementationLS*)pImplement)->createLSOutput();
//	pDomLsOutput->setByteStream(pTarget);
//
//	// Write the serialized output to the target.
//	pSerializer->write(pmyDOMDocument, pDomLsOutput);
//}
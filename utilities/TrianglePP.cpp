#include "TrianglePP.h"

//#include <iostream>

#include <vector>

#include "FileUtilities.h"

#include "utilities.h"


extern "C" {
#include "triangle.h"
}

using namespace std;

double * readPointCoordinatesFromFile(std::string filepath, int &size);
double * convertPoint2DListToDoubleArray(const std::vector<QPointF> &point2DList, int &size);
//std::vector<QPointF> * readPointsFromFile(std::string filepath);
int pnpoly(int nvert, double *vert, double testx, double testy);


int Triangle::i1() { return m_i1; }
int Triangle::i2() { return m_i2; }
int Triangle::i3() { return m_i3; }

Triangle::Triangle(int i1, int i2, int i3)
{
	m_i1 = i1;
	m_i2 = i2;
	m_i3 = i3;
}

void Triangle::getPoint(double * verts, double &centerX, double &centerY)
{
	centerX = (verts[m_i1 * 2] + verts[m_i2 * 2] + verts[m_i3 * 2]) / 3.0f;
	centerY = (verts[(m_i1 * 2) + 1] + verts[(m_i2 * 2) + 1] + verts[(m_i3 * 2) + 1]) / 3.0f;
}


std::vector<Triangle> TriangulatePolygon(const std::vector<QPointF> &polygonPointList)
{
	int pointCount;
	double *pointCoordinates = convertPoint2DListToDoubleArray(polygonPointList, pointCount);
	//double *pointCoordinates = readPointCoordinatesFromFile(filepath, pointCount);
	//for (int i = 0; i < pointCount; i++)
	//{
	//	cout << pointCoordinates[i] << endl;
	//}

	struct triangulateio in, out;

	in.pointlist = pointCoordinates;
	in.numberofpoints = pointCount / 2;
	in.numberofpointattributes = 0;
	in.pointattributelist = NULL;
	in.pointmarkerlist = NULL;
	in.numberofsegments = 0;
	in.segmentlist = NULL;
	in.numberofholes = 0;
	in.numberofregions = 0;
	in.regionlist = NULL;


	//in.numberofsegments = in.numberofpoints;
	//in.segmentlist = (int *)malloc(pointCount * sizeof(int));
	//for (int i = 0; i < in.numberofsegments; i++)
	//{
	//	int offset = i * 2;
	//	in.segmentlist[offset] = i;
	//	in.segmentlist[offset + 1] = (i + 1) % in.numberofsegments;
	//	//cout << "Segment = " << i << endl;
	//}

	//for (int i = 0; i < in.numberofsegments; i++)
	//{
	//	int offset = i * 2;
	//	cout << "Segment : " << in.segmentlist[offset] << "-" << in.segmentlist[offset + 1] << endl;
	//}

	// outputs
	out.pointlist = NULL;
	out.pointattributelist = NULL;
	out.pointmarkerlist = NULL;
	out.trianglelist = NULL;
	out.triangleattributelist = NULL;
	out.neighborlist = NULL;
	out.segmentlist = NULL;
	out.segmentmarkerlist = NULL;
	out.edgelist = NULL;
	out.edgemarkerlist = NULL;
	out.neighborlist = NULL;


	// do triangulation (z=zero-based, n=neighbors, Q=quiet, B=no boundary markers)
	//char parameters[] = "zQB";
	char parameters[] = "z";
	triangulate2(parameters, &in, &out, NULL);

	//cout << "!!!!!!!!!!!!! out.numberofpoints = " << out.numberoftriangles << endl << endl;


	std::vector<Triangle> convexTriangles;
	for (int i = 0; i < out.numberoftriangles; i++)
	{
		int indexOffset = i * 3;
		int index1 = out.trianglelist[indexOffset];
		int index2 = out.trianglelist[indexOffset + 1];
		int index3 = out.trianglelist[indexOffset + 2];
		convexTriangles.push_back(Triangle(index1, index2, index3));
		//cout << " " << i << " " << index1 << " " << index2 << " " << index3 << endl;
	}

	std::vector<Triangle> concaveTriangles;
	for (Triangle triangle : convexTriangles)
	{
		double centerX;
		double centerY;
		triangle.getPoint(pointCoordinates, centerX, centerY);
		int isInsidePolygon = pnpoly(in.numberofpoints, pointCoordinates, centerX, centerY);
		if (isInsidePolygon)
		{
			concaveTriangles.push_back(triangle);
		}
	}

	delete pointCoordinates;

	return concaveTriangles;
}


int pnpoly(int nvert, double *vertx, double *verty, double testx, double testy)
{
	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		if (((verty[i]>testy) != (verty[j]>testy)) &&
			(testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
			c = !c;
	}
	return c;
}

int pnpoly(int nvert, double *vert, double testx, double testy)
{
	int i, j, c = 0;
	for (i = 0, j = nvert - 1; i < nvert; j = i++) {
		int ii = i * 2;
		int ij = j * 2;
		double ix = vert[ii];
		double iy = vert[ii + 1];
		double jx = vert[ij];
		double jy = vert[ij + 1];
		if (((iy > testy) != (jy > testy)) && (testx < (jx - ix) * (testy - iy) / (jy - iy) + ix))
		{
			c = !c;
		}
	}
	return c;
}


double * readPointCoordinatesFromFile(std::string filepath, int &size)
{
	char delim = ' ';
	//std::string filepath = "d:\\polygon.txt";
	std::vector<std::string> * lines = FileUtilities::readAllLines(filepath);
	std::vector<double> pointCoordsVec;
	for (auto line : *lines)
	{
		//printf("%s\n", line.c_str());
		std::vector<std::string> coords = FileUtilities::split(line, delim);
		double x = std::stod(coords[0]);
		double y = std::stod(coords[1]);
		pointCoordsVec.push_back(x);
		pointCoordsVec.push_back(y);
		//printf("%sx%s\n", coords[0].c_str(), coords[1].c_str());
		//printf("%f-%f\n", x, y);
	}
	size = lines->size() * 2;
	double * pointCoordinates = new double[size];
	std::copy(pointCoordsVec.begin(), pointCoordsVec.end(), pointCoordinates);

	delete lines;

	return pointCoordinates;
}


std::vector<QPointF> * readPointsFromFile(std::string filepath)
{
	char delim = ' ';
	//std::string filepath = "d:\\polygon.txt";
	std::vector<std::string> * lines = FileUtilities::readAllLines(filepath);
	std::vector<QPointF> * points = new vector<QPointF>();
	std::vector<double> pointCoordsVec;
	for (auto line : *lines)
	{
		//printf("%s\n", line.c_str());
		std::vector<std::string> coords = FileUtilities::split(line, delim);
		double x = std::stod(coords[0]);
		double y = std::stod(coords[1]);
		QPointF point(x, y);
		points->push_back(point);
		//printf("%sx%s\n", coords[0].c_str(), coords[1].c_str());
		//printf("%f-%f\n", x, y);
	}

	delete lines;

	return points;
}

double * convertPoint2DListToDoubleArray(const std::vector<QPointF> &point2DList, int &size)
{
	std::vector<double> pointCoordsVec;
	for (QPointF point2D : point2DList)
	{
		double x = point2D.x();
		double y = point2D.y();
		pointCoordsVec.push_back(x);
		pointCoordsVec.push_back(y);
	}
	size = point2DList.size() * 2;
	double * pointCoordinates = new double[size];
	std::copy(pointCoordsVec.begin(), pointCoordsVec.end(), pointCoordinates);

	return pointCoordinates;
}
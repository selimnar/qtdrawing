#ifndef PROJECT_UTILITIES_H
#define PROJECT_UTILITIES_H

class Project;
class BuildingInstance;

class QStandardItemModel;
class QStringList;
class QObject;
class QTableView;

void PopulateArchitecturalPlanListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent);

void PopulateDrawnPlanListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent);

void PopulateBuildingInstanceListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent);

void PopulateBuildingDrawnPlanInstanceListViewBase(Project * project, BuildingInstance * buildingInstance, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent);

#endif // PROJECT_UTILITIES_H
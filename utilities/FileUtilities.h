#ifndef FILE_UTILITIES_H
#define FILE_UTILITIES_H

#include <string>
#include <vector>
#include <map>


namespace FileUtilities
{
	std::string retrieveFileName(const std::string& fname);
	std::string retrieveParentPath(const std::string& fname);
	int writeTextToFile(std::string content, std::string path);
	void parseKeyValuePairTextFile(const std::string &filePath, std::map<std::string, std::string> & keyValueMap);
	void readfile(const std::string &filepath, std::string &buffer);
	std::vector<std::string> * readAllLines(const std::string &filepath);

	bool PathExists(const char* dirName);
	bool CreateFolder(const char* dirName);
	bool RemoveFolder(const char* path);
	bool CopyFileFrom(const char* src, const char* dst);

	bool ConvertToBoolean(char * str);
	int ConvertToInt(std::string &str);
	double ConvertToDouble(std::string &str);

	std::string ToUpperCaseString(std::string orginal);

	std::vector<std::string> split(const std::string &s, char delim);

	bool beginsWith(std::string content, std::string prefix);
	bool endsWith(std::string content, std::string suffix);

	class ConfigurationKeyValueMap
	{
	public:
		std::string operator[] (const std::string& key);

		static ConfigurationKeyValueMap * ins();

	private:
		static ConfigurationKeyValueMap * instance;

		std::map<std::string, std::string> keyValueMap;

		ConfigurationKeyValueMap();
	};
};

typedef FileUtilities::ConfigurationKeyValueMap Configuration;
#define CONFIG(key) (*(FileUtilities::ConfigurationKeyValueMap::ins()))[key]

#endif // UTILITIES_H
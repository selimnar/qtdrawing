#include <QImage>

#include "QImageDownloader.h"

#include "utilities.h"

#include "CustomQObjects/FloorplanGraphicsScene.h"

QImageDownloader::QImageDownloader(IDownloadedImageHandler * downloadedImageHandler_) :
	QObject(nullptr)
{
	downloadedImageHandler = downloadedImageHandler_;
	manager = new QNetworkAccessManager;

	total = 0;
	m_image = nullptr;
	m_imageBitsList = new std::vector<QByteArray>();
}

QImageDownloader::~QImageDownloader()
{
	manager->deleteLater();
}

void QImageDownloader::setFile(QString fileURL)
{
	//QString filePath = fileURL;
	//QString saveFilePath;
	//QStringList filePathList = filePath.split('/');
	//QString fileName = filePathList.at(filePathList.count() - 1);
	//saveFilePath = QString("C:/Images/" + fileName);
	//saveFilePath = QString("C:/Images/yep.tif");
	//file = new QFile;
	//file->setFileName(saveFilePath);
	//file->open(QIODevice::WriteOnly);

	QNetworkRequest request;
	request.setUrl(QUrl(fileURL));
	reply = manager->get(request);

	total = 0;
	m_image = nullptr;
	m_imageBitsList->clear();

	connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(onDownloadProgress(qint64, qint64)));
	connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(onFinished(QNetworkReply*)));
	connect(reply, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
	connect(reply, SIGNAL(finished()), this, SLOT(onReplyFinished()));
}

QImage * QImageDownloader::image()
{
	return m_image;
}

void QImageDownloader::onDownloadProgress(qint64 bytesRead, qint64 bytesTotal)
{
	DEBUG_OUT("information: %s / %s", QString::number(bytesRead).toLatin1().toStdString().c_str(),
		QString::number(bytesTotal).toLatin1().toStdString().c_str());
	//qDebug(QString::number(bytesRead).toLatin1() + " - " + QString::number(bytesTotal).toLatin1());
}

void QImageDownloader::onFinished(QNetworkReply * reply)
{
	switch (reply->error())
	{
	case QNetworkReply::NoError:
	{
		DEBUG_OUT("information: file is downloaded successfully.");
		//qDebug("file is downloaded successfully.");
	}break;
	default: {
		DEBUG_OUT("warning: %s", reply->errorString().toLatin1());
		//qDebug(reply->errorString().toLatin1());
	};
	}

	//if (file->isOpen())
	//{
	//	file->close();
	//	file->deleteLater();
	//}
}

void QImageDownloader::onReadyRead()
{
	//qint64 size = file->write(reply->readAll());
	QByteArray qbyteArray = reply->readAll();
	m_imageBitsList->push_back(qbyteArray);
	int size = qbyteArray.size();
	total += size;
	DEBUG_OUT("warning: QImageDownloader::onReadyRead %d / %d", size, total);

	//m_image = new QImage();
	//m_image->loadFromData(reply->readAll());
}

void QImageDownloader::onReplyFinished()
{
	DEBUG_OUT("warning: QImageDownloader::onReplyFinished %d", total);

	if (total > 0)
	{
		unsigned char * fullImageData = new unsigned char[total];
		unsigned char * dest = fullImageData;
		for (QByteArray qbyteArray : *m_imageBitsList)
		{
			int size = qbyteArray.size();
			char * data = qbyteArray.data();
			memcpy(dest, data, size);
			dest += size;
		}

		m_image = new QImage();
		m_image->loadFromData(fullImageData, total);
		downloadedImageHandler->onImageLoaded(m_image);

		delete fullImageData;
	}
	else
	{
		downloadedImageHandler->onFail();
	}

	m_imageBitsList->clear();

	//if (file->isOpen())
	//{
	//	file->close();
	//	file->deleteLater();
	//}
}

#ifndef UTILITIES_H
#define UTILITIES_H

#include <fstream>

#include <vector>

#include <xercesc/util/XercesDefs.hpp>

#define EPSILON 1e-20

class QPointF;

XERCES_CPP_NAMESPACE_BEGIN
	class DOMDocument;
XERCES_CPP_NAMESPACE_END

static const double DegreeToRad = 0.017453292519943295769;
static const double RadToDegree = 1 / DegreeToRad;

bool ClosestDistanceToLine(const QPointF &P, const QPointF &P1, const QPointF &P2, QPointF &projectedPoint, double &distanceToProjectedPoint);

void LineEquationFromTwoPoints(const QPointF &P1, const QPointF &P2, double &m, double &c);

bool IntersectionOfTwoLineSegment(const QPointF &L1_P1, const QPointF &L1_P2,
	const QPointF &L2_P1, const QPointF &L2_P2, QPointF &intersectionPoint);

bool IntersectionOfTwoLineStips(std::vector<QPointF> strip1, std::vector<QPointF> strip2, int &index1, int &index2,
	QPointF &originalLineNormal1, QPointF &originalLineNormal2, QPointF &intersectionPoint);

bool IsPointInLineSegment(const QPointF &P, const QPointF &P1, const QPointF &P2);

QPointF NormalizeQPointF(QPointF);

double QPointFLength(QPointF);

double Distance(QPointF p1, QPointF p2);

double DotProduct(QPointF, QPointF);

double CrossProduct(QPointF, QPointF);

double AngleInRadianBetweenTwoUnitVectors(QPointF a, QPointF b);

QPointF ProjectBOnA(QPointF, QPointF);

QPointF PerpendicularVector(QPointF);

double Sign(double);

void DBOut(const char *file, const int line, const char *s);

void DebugOut(const char *file, const int line, const char* format, ...);

void DBOutW(const char *file, const int line, const wchar_t *s);

void DebugOutW(const char *file, const int line, const wchar_t* format, ...);

#define DBOUT(s)       DBOut(__FILE__, __LINE__, s)

#define DEBUG_OUT(s, ...)       DebugOut(__FILE__, __LINE__, s, __VA_ARGS__)

#define DEBUG_OUTW(s, ...)       DebugOutW(__FILE__, __LINE__, s, __VA_ARGS__)

std::ofstream& operator << (std::ofstream& ofs, QPointF &p);

std::ifstream& operator >> (std::ifstream& ifs, QPointF &p);

inline float convertToMeter(float val)
{
	float newVal = round(val);
	return newVal / 100.0f;
}

bool initializeXerces();

void finalizeXerces();

void WriteXMLDocument(XERCES_CPP_NAMESPACE::DOMDocument * pmyDOMDocument, const char * FullFilePath);

XERCES_CPP_NAMESPACE::DOMDocument * CreateDocument(const XMLCh * rootElementName);

std::u16string ConvertWcharToXMLChar(const wchar_t * wstr);

std::u16string ConvertWcharToXMLChar(std::wstring source);

std::u16string ConvertWcharToXMLChar(const char * wstr);

std::u16string ConvertWcharToXMLChar(std::string source);

std::u16string ConvertToUint16String(unsigned int number);

std::u16string ConvertToUint16String(int number);

#endif // UTILITIES_H
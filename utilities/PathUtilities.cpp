#include "clipper/clipper_utilities.h"

#include <vector>

#include <QPainterPath>

#include "Utilities.h"
#include "PathUtilities.h"

#include "Data\WallData.h"
#include "utilities\MergeWallPolygons.h"

#include "FloorDesignItems\WallSegmentItem.h"

using namespace std;

const qreal ControlpointRatio = 0.2;
const qreal QPathToClipperLibPathScaler = 10000;
const qreal ClipperLibPathToQPathScaler = 1 / QPathToClipperLibPathScaler;
const int BezierStepCount = 20;

QPointF SampleMirrorPoint(qreal percent, qreal wallWidth, bool isInner, QPainterPath referenceBezierCurvePath)
{
	QPointF bezierSamplePoint = referenceBezierCurvePath.pointAtPercent(percent);

	qreal angleAtPercent = referenceBezierCurvePath.angleAtPercent(percent);
	qreal radianAngleAtPercent = DegreeToRad * angleAtPercent;
	QPointF mirrorVector(qSin(radianAngleAtPercent), qCos(radianAngleAtPercent));
	mirrorVector = mirrorVector * (isInner ? 1 : -1);

	return bezierSamplePoint + (mirrorVector * wallWidth);
}

void ConvertClipperPolygonPathToQPainterPath(const ClipperLib::Path &bezierCurveClipperPath, QPainterPath &bezierCurvePath)
{
	ClipperLib::IntPoint startPoint = bezierCurveClipperPath[0];
	qreal x = (qreal)startPoint.X * ClipperLibPathToQPathScaler;
	qreal y = (qreal)startPoint.Y * ClipperLibPathToQPathScaler;
	bezierCurvePath.moveTo(x, y);

	bool isFist = true;
	for (auto &point : bezierCurveClipperPath)
	{
		if (isFist)
		{
			isFist = false;
			continue;
		}
		qreal x = (qreal)point.X * ClipperLibPathToQPathScaler;
		qreal y = (qreal)point.Y * ClipperLibPathToQPathScaler;
		bezierCurvePath.lineTo(x, y);

		//DEBUG_OUT("point(%f, %f)", x, y);
	}
}

void ConvertClipperPolygonPathToPoints(const ClipperLib::Path &bezierCurveClipperPath, std::vector<QPointF> &points)
{
	for (auto &point : bezierCurveClipperPath)
	{
		qreal x = (qreal)point.X * ClipperLibPathToQPathScaler;
		qreal y = (qreal)point.Y * ClipperLibPathToQPathScaler;
		points.push_back(QPointF(x, y));
	}
}

void ConvertPointsToClipperPolygonPath(const std::vector<QPointF> &points, ClipperLib::Path &clipperPath)
{
	for (const QPointF point : points)
	{
		ClipperLib::IntPoint clipperPoint((int)(point.x() * QPathToClipperLibPathScaler),
			(int)(point.y() * QPathToClipperLibPathScaler));
		clipperPath.push_back(clipperPoint);
	}
}

void CreateLinePolygon(WallData * wallData, QPainterPath &bezierCurvePath, QPainterPath &referencePath,
	ClipperLib::Path &bezierCurveClipperPath)
{
	WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
	WallGenericPolygonGenerationData &wpgd = wallItem->wallGenericPolygonGenerationData();
	//referenceBezierCurvePath = wpgd.referencePath;

	wpgd.firstSideVectors.clear();
	wpgd.secondSideVectors.clear();
	wpgd.polygonVectors.clear();

	QPointF relativeMainControllerPos = wallItem->relativeDragPosition();

	qreal wallHalfWidth = wallData->width() * 0.5;
	QPointF startPos = wallData->startPosition() - relativeMainControllerPos;
	QPointF endPos = wallData->endPosition() - relativeMainControllerPos;

	referencePath.moveTo(startPos);
	referencePath.lineTo(endPos);

	QPointF lineDir = NormalizeQPointF(endPos - startPos);
	QPointF lineNormal = QPointF(lineDir.y(), -lineDir.x());

	QPointF startPoint1 = startPos + (lineNormal * wallHalfWidth);
	QPointF startPoint2 = startPos - (lineNormal * wallHalfWidth);

	QPointF endPoint1 = endPos + (lineNormal * wallHalfWidth);
	QPointF endPoint2 = endPos - (lineNormal * wallHalfWidth);

	wpgd.firstSideVectors.push_back(startPos);
	wpgd.firstSideVectors.push_back(startPoint1);
	wpgd.firstSideVectors.push_back(endPoint1);
	wpgd.firstSideVectors.push_back(endPos);

	wpgd.secondSideVectors.push_back(endPos);
	wpgd.secondSideVectors.push_back(endPoint2);
	wpgd.secondSideVectors.push_back(startPoint2);
	wpgd.secondSideVectors.push_back(startPos);

	wpgd.polygonVectors.push_back(startPoint1);
	wpgd.polygonVectors.push_back(endPoint1);
	wpgd.polygonVectors.push_back(endPoint2);
	wpgd.polygonVectors.push_back(startPoint2);
	wpgd.polygonVectors.push_back(startPoint1);

	bezierCurveClipperPath.clear();
	bezierCurveClipperPath.push_back(ClipperLib::IntPoint((int)(startPoint1.x() * QPathToClipperLibPathScaler),
		(int)(startPoint1.y() * QPathToClipperLibPathScaler)));
	bezierCurveClipperPath.push_back(ClipperLib::IntPoint((int)(endPoint1.x() * QPathToClipperLibPathScaler),
		(int)(endPoint1.y() * QPathToClipperLibPathScaler)));
	bezierCurveClipperPath.push_back(ClipperLib::IntPoint((int)(endPoint2.x() * QPathToClipperLibPathScaler),
		(int)(endPoint2.y() * QPathToClipperLibPathScaler)));
	bezierCurveClipperPath.push_back(ClipperLib::IntPoint((int)(startPoint2.x() * QPathToClipperLibPathScaler),
		(int)(startPoint2.y() * QPathToClipperLibPathScaler)));
	bezierCurveClipperPath.push_back(ClipperLib::IntPoint((int)(startPoint1.x() * QPathToClipperLibPathScaler),
		(int)(startPoint1.y() * QPathToClipperLibPathScaler)));

	ConvertClipperPolygonPathToQPainterPath(bezierCurveClipperPath, bezierCurvePath);
}

void CreateWallBezierClipperPolyonData(qreal wallWidth, vector<QPointF> &firstSideVectors, vector<QPointF> &secondSideVectors,
	ClipperLib::Path &bezierCurveClipperPath, QPainterPath referenceBezierCurvePath)
{
	qreal stepSize = 1.0 / (qreal)BezierStepCount;
	for (int i = 0; i <= BezierStepCount; i++)
	{
		qreal innerPercent = (qreal)i * stepSize;
		QPointF innerMirrorPoint = SampleMirrorPoint(innerPercent, wallWidth, true, referenceBezierCurvePath);
		firstSideVectors.push_back(innerMirrorPoint);
		ClipperLib::IntPoint point((int)(innerMirrorPoint.x() * QPathToClipperLibPathScaler),
			(int)(innerMirrorPoint.y() * QPathToClipperLibPathScaler));
		bezierCurveClipperPath.push_back(point);
	}

	for (int i = 0; i <= BezierStepCount; i++)
	{
		qreal outerPercent = 1.0 - ((qreal)i * stepSize);
		QPointF outerMirrorPoint = SampleMirrorPoint(outerPercent, wallWidth, false, referenceBezierCurvePath);
		secondSideVectors.push_back(outerMirrorPoint);
		ClipperLib::IntPoint point((int)(outerMirrorPoint.x() * QPathToClipperLibPathScaler),
			(int)(outerMirrorPoint.y() * QPathToClipperLibPathScaler));
		bezierCurveClipperPath.push_back(point);
	}
}

void CreateBezierPolygon(WallData * wallData, QPainterPath &bezierCurvePath, QPainterPath &referenceBezierCurvePath,
	ClipperLib::Path &bezierCurveClipperPath)
{
	WallSegmentItem * wallItem = wallData->relatedGraphicsItem();
	WallGenericPolygonGenerationData &wpgd = wallItem->wallGenericPolygonGenerationData();
	//referenceBezierCurvePath = wpgd.referencePath;

	wpgd.firstSideVectors.clear();
	wpgd.secondSideVectors.clear();
	wpgd.polygonVectors.clear();

	QPointF relativeMainControllerPos = wallItem->relativeDragPosition();

	qreal wallHalfWidth = wallData->width() * 0.5;
	QPointF startP = wallData->startPosition() - relativeMainControllerPos;
	QPointF endP = wallData->endPosition() - relativeMainControllerPos;
	QPointF mainControlPoint = wallData->controlPosition() - relativeMainControllerPos;

	QPointF startToEnd = endP - startP;
	QPointF startToEndCenter = (startP + endP) * 0.5f;
	//DEBUG_OUT("mcptlcNormalized(%f, %f); length = %f", mcptlcNormalized.x(), mcptlcNormalized.y(), QPointFLength(mcptlcNormalized));

	QPointF c1 = mainControlPoint - startToEnd * ControlpointRatio;
	QPointF c2 = mainControlPoint + startToEnd * ControlpointRatio;

	referenceBezierCurvePath.moveTo(startP);
	referenceBezierCurvePath.cubicTo(c1, c2, endP);

	ClipperLib::Path tmpBezierCurveClipperPath;
	CreateWallBezierClipperPolyonData(wallHalfWidth, wpgd.firstSideVectors, wpgd.secondSideVectors,
		tmpBezierCurveClipperPath, referenceBezierCurvePath);
	ClipperLib::Path tmpSimplifiedBezierCurveClipperPath;
	bool result = SimplifyPolygon(tmpBezierCurveClipperPath, tmpSimplifiedBezierCurveClipperPath);
	bezierCurveClipperPath.swap(tmpSimplifiedBezierCurveClipperPath);
	ConvertClipperPolygonPathToQPainterPath(bezierCurveClipperPath, bezierCurvePath);

	wpgd.firstSideVectors.insert(wpgd.firstSideVectors.begin(), startP);
	wpgd.firstSideVectors.push_back(endP);
	std::reverse(wpgd.firstSideVectors.begin(), wpgd.firstSideVectors.end());

	wpgd.secondSideVectors.insert(wpgd.secondSideVectors.begin(), endP);
	wpgd.secondSideVectors.push_back(startP);
	std::reverse(wpgd.secondSideVectors.begin(), wpgd.secondSideVectors.end());

	MergeWallPolygons::ConvertClipperPolygonDataToPointVector(bezierCurveClipperPath, wpgd.polygonVectors);
	wpgd.polygonVectors.push_back(wpgd.polygonVectors[0]);
}

//void Foo()
//{
//	void SimplifyPolygons(Paths &polys, PolyFillType fillType)
//	{
//		SimplifyPolygons(polys, polys, fillType);
//	}
//}
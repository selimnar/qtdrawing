#ifndef PATH_UTILITIES_H
#define PATH_UTILITIES_H

#include <vector>
#include "clipper/clipper.hpp"
#include "../Data/WallData.h"

QPointF SampleMirrorPoint(qreal percent, qreal wallWidth, bool isInner, class QPainterPath referenceBezierCurvePath);

void ConvertClipperPolygonPathToQPainterPath(const ClipperLib::Path &bezierCurveClipperPath,
	class QPainterPath &bezierCurvePath);

void ConvertClipperPolygonPathToPoints(const ClipperLib::Path &bezierCurveClipperPath,
	class std::vector<QPointF> &points);

void ConvertPointsToClipperPolygonPath(const std::vector<QPointF> &points, ClipperLib::Path &clipperPath);

void CreateLinePolygon(WallData * wallData, QPainterPath &bezierCurvePath, QPainterPath &referencePath,
	ClipperLib::Path &bezierCurveClipperPath);

void CreateWallBezierClipperPolyonData(qreal wallWidth, std::vector<QPointF> &firstSideVectors,
	class std::vector<QPointF> &secondSideVectors, ClipperLib::Path &bezierCurveClipperPath,
	class QPainterPath referenceBezierCurvePath);

void CreateBezierPolygon(WallData * wallData, QPainterPath &bezierCurvePath,
	class QPainterPath &referenceBezierCurvePath, ClipperLib::Path &bezierCurveClipperPath);

#endif // PATH_UTILITIES_H
#include "ProjectUtilities.h"

#include <QItemSelectionModel>
#include <QStandardItemModel>
#include <QTableView>

#include "Data\Project.h"

void PopulateArchitecturalPlanListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent)
{
	std::map<unsigned int, ArchitecturalPlan*> scannedImageFiles = m_project->scannedImageFiles();

	tableViewModel = new QStandardItemModel(0, 0, parent);
	tableViewModel->setHorizontalHeaderLabels(tableHeaderLabels);

	for (auto kvp : scannedImageFiles)
	{
		ArchitecturalPlan * architecturalPlan = kvp.second;

		QString id = QString::number(architecturalPlan->id());
		QString definition = architecturalPlan->definition();
		QString fileName = architecturalPlan->fileName();

		QList<QStandardItem *> rowItems;
		rowItems.append(new QStandardItem(id));
		rowItems.append(new QStandardItem(definition));
		rowItems.append(new QStandardItem(fileName));
		tableViewModel->appendRow(rowItems);
	}

	QItemSelectionModel *previousTableViewModel = tableView->selectionModel();
	tableView->setModel(tableViewModel);
	if (previousTableViewModel != nullptr)
	{
		delete previousTableViewModel;
	}
}

void PopulateDrawnPlanListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent)
{
	std::map<unsigned int, DrawnFloorPlan*> drawnPlans = m_project->drawnPlans();

	tableViewModel = new QStandardItemModel(0, 0, parent);
	tableViewModel->setHorizontalHeaderLabels(tableHeaderLabels);

	for (auto kvp : drawnPlans)
	{
		DrawnFloorPlan * drawnPlan = kvp.second;

		QString id = QString::number(drawnPlan->id());
		QString definition = drawnPlan->definition();

		QList<QStandardItem *> rowItems;
		rowItems.append(new QStandardItem(id));
		rowItems.append(new QStandardItem(definition));
		tableViewModel->appendRow(rowItems);
	}

	QItemSelectionModel *previousTableViewModel = tableView->selectionModel();
	tableView->setModel(tableViewModel);
	if (previousTableViewModel != nullptr)
	{
		delete previousTableViewModel;
	}
}

void PopulateBuildingInstanceListViewBase(Project * m_project, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent)
{
	std::map<unsigned int, BuildingInstance*> buildingInstances = m_project->buildingInstances();

	tableViewModel = new QStandardItemModel(0, 0, parent);
	tableViewModel->setHorizontalHeaderLabels(tableHeaderLabels);

	for (auto kvp : buildingInstances)
	{
		BuildingInstance * buildingInstance = kvp.second;

		QString id = QString::number(buildingInstance->id());
		QString definition = buildingInstance->definition();

		QList<QStandardItem *> rowItems;
		rowItems.append(new QStandardItem(id));
		rowItems.append(new QStandardItem(definition));
		tableViewModel->appendRow(rowItems);
	}

	QItemSelectionModel *previousTableViewModel = tableView->selectionModel();
	tableView->setModel(tableViewModel);
	if (previousTableViewModel != nullptr)
	{
		delete previousTableViewModel;
	}
}

void PopulateBuildingDrawnPlanInstanceListViewBase(Project * project, BuildingInstance * buildingInstance, QTableView * tableView, QStandardItemModel * &tableViewModel,
	const QStringList &tableHeaderLabels, QObject * parent)
{
	std::map<unsigned int, AssignedFloorInstance*> assignedFloorPlans = buildingInstance->assignedFloorPlans();

	tableViewModel = new QStandardItemModel(0, 0, parent);
	tableViewModel->setHorizontalHeaderLabels(tableHeaderLabels);

	std::map<unsigned int, DrawnFloorPlan*> drawnPlans = project->drawnPlans();
	for (auto kvp : assignedFloorPlans)
	{
		AssignedFloorInstance * assignedFloorInstance = kvp.second;

		QString id = QString::number(assignedFloorInstance->id());
		//QString floorId = QString::number(assignedFloorInstance->floorId());
		QString count = QString::number(assignedFloorInstance->count());

		DrawnFloorPlan * drawnFloorPlan = drawnPlans[assignedFloorInstance->floorId()];

		QList<QStandardItem *> rowItems;
		rowItems.append(new QStandardItem(id));
		//rowItems.append(new QStandardItem(floorId));
		rowItems.append(new QStandardItem(drawnFloorPlan->definition()));
		rowItems.append(new QStandardItem(count));
		tableViewModel->appendRow(rowItems);
	}

	QItemSelectionModel *previousTableViewModel = tableView->selectionModel();
	tableView->setModel(tableViewModel);
	if (previousTableViewModel != nullptr)
	{
		delete previousTableViewModel;
	}
}
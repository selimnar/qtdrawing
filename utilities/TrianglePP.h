#ifndef TRIANGLE_PLUS_PLUS_H
#define TRIANGLE_PLUS_PLUS_H

#include <vector>

#include <QPointF>

class Triangle
{
public:
	int i1();
	int i2();
	int i3();

	Triangle(int i1, int i2, int i3);

	void getPoint(double * verts, double &centerX, double &centerY);

private:
	int m_i1;
	int m_i2;
	int m_i3;
};

std::vector<Triangle> TriangulatePolygon(const std::vector<QPointF> &polygonPointList);

std::vector<QPointF> * readPointsFromFile(std::string filepath);

#endif // TRIANGLE_PLUS_PLUS_H
#ifndef FORWARD_VECTOR_MAP_H
#define FORWARD_VECTOR_MAP_H

namespace std
{
	template<class _Ty>
	class allocator;
	template<class _Ty, class _Alloc = allocator<_Ty>>
	class vector;

	template<class _Ty = void>
	struct less;
	template<class _Ty1,
		class _Ty2>
		struct pair;
	template<class _Kty,
		class _Ty,
		class _Pr = less<_Kty>,
		class _Alloc = allocator<pair<const _Kty, _Ty>>>
		class map;
}

#endif // FORWARD_VECTOR_MAP_H
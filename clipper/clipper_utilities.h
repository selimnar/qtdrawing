#ifndef CLIPPER_UTILITIES_H
#define CLIPPER_UTILITIES_H

#include <string>
#include "clipper.hpp"

//---------------------------------------------------------------------------
// SVGBuilder class
// a very simple class that creates an SVG image file
//---------------------------------------------------------------------------

class SVGBuilder
{
private:
	static std::string ColorToHtml(unsigned clr);
	//------------------------------------------------------------------------------

	static float GetAlphaAsFrac(unsigned clr);
	//------------------------------------------------------------------------------

	class StyleInfo
	{
	public:
		ClipperLib::PolyFillType pft;
		unsigned brushClr;
		unsigned penClr;
		double penWidth;
		bool showCoords;

		StyleInfo();
	};

	class PolyInfo
	{
	public:
		ClipperLib::Paths paths;
		StyleInfo si;

		PolyInfo(ClipperLib::Paths paths, StyleInfo style);
	};

	typedef std::vector<PolyInfo> PolyInfoList;

private:
	PolyInfoList polyInfos;
	static const std::string svg_xml_start[];
	static const std::string poly_end[];

public:
	StyleInfo style;

	void AddPaths(ClipperLib::Paths& poly);
	bool SaveToFile(const std::string& filename, double scale = 1.0, int margin = 10);
}; //SVGBuilder
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Miscellaneous function ...
//------------------------------------------------------------------------------

bool SaveToFile(const std::string& filename, ClipperLib::Paths &ppg, double scale = 1.0, unsigned decimal_places = 0);
//------------------------------------------------------------------------------

bool LoadFromFile(ClipperLib::Paths &ppg, const std::string& filename, double scale);

void MakeRandomPoly(int edgeCount, int width, int height, ClipperLib::Paths & poly);
//------------------------------------------------------------------------------

bool ASCII_icompare(const char* str1, const char* str2);

bool SimplifyPolygon(ClipperLib::Path bezierCurveClipperPath, ClipperLib::Path &simplifiedBezierCurveClipperPath);

#endif // CLIPPER_UTILITIES_H
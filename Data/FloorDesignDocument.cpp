#include <algorithm>
#include <vector>
#include <map>
#include <QString>
#include <qmath.h>

#include <float.h>

#include "FloorDesignDocument.h"

#include "WallData.h"
#include "ControllerData.h"

#include "utilities/utilities.h"

#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "ControlPointGraphicsItem.h"

//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>

//#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_serialize.hpp>

using namespace std;

const qreal FloorDesignDocument::MergeWallCornerDistanceLimit = 5;

FloorDesignDocument::FloorDesignDocument()
{
	initialize();
}

FloorDesignDocument::~FloorDesignDocument()
{
	finalize();
}

void FloorDesignDocument::initialize()
{
	wallList = new std::vector<WallData*>();
	wallMap = new std::map<std::string, WallData*>();

	controllerList = new std::vector<ControllerData *>();
	controllerMap = new std::map<std::string, ControllerData *>();
}

void FloorDesignDocument::finalize()
{
	for (WallData * wd : *(this->wallList))
	{
		delete wd;
	}

	for (ControllerData * cd : *(this->controllerList))
	{
		delete cd;
	}

	this->wallList->clear();
	this->wallMap->clear();

	this->controllerList->clear();
	this->controllerMap->clear();

	delete this->wallList;
	delete this->wallMap;

	delete this->controllerList;
	delete this->controllerMap;
}

void FloorDesignDocument::addWall(WallData *wallData, bool doGenerateUuid)
{
	std::string itemUuidStr;
	if (doGenerateUuid)
	{
		boost::uuids::uuid itemUuid = generator();
		itemUuidStr = boost::uuids::to_string(itemUuid);
		wallData->setUuid(itemUuidStr);
	}
	else
	{
		itemUuidStr = wallData->uuid();
	}

	//QString itemUuidQStr = QString::fromStdString(itemUuidStr);
	//DEBUG_OUT("itemUuid = %d", itemUuidQStr.toStdString().c_str());

	addWall(itemUuidStr, wallData);
}

void FloorDesignDocument::addWall(string &itemUuidStr, WallData *wallItem)
{
	wallItem->setOwnerDocument(this);
	wallList->push_back(wallItem);
	wallMap->insert(pair<std::string, WallData*>(itemUuidStr, wallItem));
}

void FloorDesignDocument::removeWall(WallData *wallItem, bool isToDeleteWallDataInstance)
{
	removeWall(wallItem, true, isToDeleteWallDataInstance);
}

void FloorDesignDocument::removeWall(WallData *wallItem, bool isToRemoveControlDatas, bool isToDeleteWallDataInstance)
{
	vector<WallData *>::iterator vectorEntryPosition = std::find(wallList->begin(), wallList->end(), wallItem);
	if (vectorEntryPosition != wallList->end())
	{
		wallList->erase(vectorEntryPosition); 
	}
	map<string, WallData*>::iterator mapEntryPosition = wallMap->find(wallItem->uuid());
	if (mapEntryPosition != wallMap->end())
	{
		wallMap->erase(mapEntryPosition);
	}
	ControllerData * startControllerData = wallItem->startControllerData();
	startControllerData->removeControlledItem(wallItem);
	if (startControllerData->controledItemCount() == 0 && isToRemoveControlDatas)
	{
		this->removeController(startControllerData);
		DEBUG_OUT("FloorDesignDocument::removeWall removed start controller ");
	}
	ControllerData * endControllerData = wallItem->endControllerData();
	endControllerData->removeControlledItem(wallItem);
	if (endControllerData->controledItemCount() == 0 && isToRemoveControlDatas)
	{
		this->removeController(endControllerData);
		DEBUG_OUT("FloorDesignDocument::removeWall removed end controller ");
	}
	if (isToDeleteWallDataInstance)
	{
		delete wallItem;
	}
}

void FloorDesignDocument::removeWall(string &itemUuidStr)
{
	WallData *wallItem = (*wallMap)[itemUuidStr];
	if (wallItem != nullptr)
	{
		removeWall(wallItem);
	}
}

int FloorDesignDocument::wallCount()
{
	return (int)wallList->size();
}

WallData * FloorDesignDocument::wallAt(int index)
{
	if (wallList == nullptr || index < 0 || index >= wallList->size())
	{
		return nullptr;
	}
	return (*wallList)[index];
}

ControllerData * FloorDesignDocument::retrieveMergableControlPoint(ControllerData * controllerData)
{
	QPointF addedControllerPosition = controllerData->position();

	for (ControllerData* cd : *controllerList)
	{
		if (controllerData == cd)
		{
			continue;
		}
		QPointF existingControllerPosition = cd->position();
		double distance = QPointFLength(existingControllerPosition - addedControllerPosition);
		//DEBUG_OUT("!!!!!!!! ControllerData: distance = %f", distance);
		if (distance < MergeWallCornerDistanceLimit)
		{
			return cd;
		}

		//DEBUG_OUT("ControllerData: itemUuid = %s", cd->uuid().c_str());
	}
	return nullptr;
}

WallData * FloorDesignDocument::retrieveAttachableWall(ControllerData * controllerData)
{
	QPointF point = controllerData->position();
	QPointF projectedPoint;
	qreal distanceToProjectedPoint;

	qreal distanceToClosestWall = DBL_MAX;
	WallData * closestWall = nullptr;

	vector<WallData*> excludedWallList(*wallList);
	vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		vector<WallData *>::iterator vectorEntryPosition = std::find(excludedWallList.begin(), excludedWallList.end(), wallData);
		if (vectorEntryPosition != excludedWallList.end())
		{
			excludedWallList.erase(vectorEntryPosition);
		}
	}

	for (WallData* wallData : excludedWallList)
	{
		if (wallData->type() == WallType::Curved)
		{
			continue;
		}

		bool isOnWallSegment = wallData->checkPointProximity(point, projectedPoint, distanceToProjectedPoint);
		// those two have same values for now ControlPointGraphicsItem::Radius ; FloorDesignDocument::MergeWallCornerDistanceLimit
		if (!isOnWallSegment || distanceToProjectedPoint > FloorDesignDocument::MergeWallCornerDistanceLimit)
		{
			continue;
		}
		if (distanceToProjectedPoint < distanceToClosestWall)
		{
			distanceToClosestWall = distanceToProjectedPoint;
			closestWall = wallData;
		}

		//DEBUG_OUT("ControllerData: itemUuid = %s", cd->uuid().c_str());
	}
	return closestWall;
}

bool FloorDesignDocument::mergeControlPoint(ControllerData * food, ControllerData *swallower)
{
	std::vector<ControllerMappingData*> controlledListOfTheAdded = food->controlledList();

	WallData * wallToDestroy = nullptr;
	bool areControllersOfSameWall = false;
	for (ControllerMappingData* cm : controlledListOfTheAdded)
	{
		WallData * wallData = cm->controlledWall;
		// if merging start and end controll point which would result indestroying the wall itself

		if (cm->controlledWall->length() < MergeWallCornerDistanceLimit)
		{
			areControllersOfSameWall = (wallData->startControllerData() == swallower) && (wallData->endControllerData() == food) ||
				(wallData->startControllerData() == food) && (wallData->endControllerData() == swallower);
			if (areControllersOfSameWall)
			{
				wallToDestroy = cm->controlledWall;
				break;
			}
		}
	}

	if (wallToDestroy != nullptr)
	{
		wallToDestroy->removeSelf(false, false);
	}

	if (food->controledItemCount() == 0 && swallower->controledItemCount() == 0)
	{
		return true;
	}


	controlledListOfTheAdded = food->controlledList();
	swallower->appendControlledItems(controlledListOfTheAdded);

	for (ControllerMappingData* cm : controlledListOfTheAdded)
	{
		WallData * wallData = cm->controlledWall;
		if (cm->controlledPart == Start)
		{
			wallData->setStartControllerData(swallower);
		}
		else if (cm->controlledPart == End)
		{
			wallData->setEndControllerData(swallower);
		}
	}
	// later bind this controllerData to a undo redo command instance instead of destroying it
	food->clearList();
	swallower->tradeGraphicsItem(food);
	swallower->setPosition(swallower->position(), true, (WallData *)nullptr);
	removeController(food);

	return areControllersOfSameWall;
}

void FloorDesignDocument::addController(ControllerData *controllerData, bool doGenerateUuid, bool doMerge)
{
	std::string itemUuidStr;
	if (doGenerateUuid)
	{
		boost::uuids::uuid itemUuid = generator();
		itemUuidStr = boost::uuids::to_string(itemUuid);
		controllerData->setUuid(itemUuidStr);
	}
	else
	{
		itemUuidStr = controllerData->uuid();
	}

	//QString itemUuidQStr = QString::fromStdString(itemUuidStr);
	//DEBUG_OUT("itemUuid = %d", itemUuidQStr.toStdString().c_str());

	//bool mergeControlPoint();
	if (doMerge)
	{
		ControllerData * swallower = retrieveMergableControlPoint(controllerData);
		if (swallower != nullptr)
		{
			mergeControlPoint(controllerData, swallower);
			return;
		}
	}

	addController(itemUuidStr, controllerData);
}

void FloorDesignDocument::serializeControllerList(std::ofstream &ofs)
{
	size_t size = controllerList->size();
	//DEBUG_OUT("------------ size = %d", size);
	ofs.write((const char*)&size, sizeof(size_t));
	for (ControllerData * cd : (*controllerList))
	{
		std::string itemUuidStr = cd->uuid();
		cd->serialize(ofs);
		//DEBUG_OUT("----itemUuid = %s ; position(%f, %f)", itemUuidStr.c_str(), cd->position().x(), cd->position().y());
	}
	//unsigned char * uuid_data = new unsigned char[16];
	//boost::uuids::uuid itemUuid = generator();
	//memcpy(&itemUuid, uuid_data, 16);
	//return uuid_data;
}

void FloorDesignDocument::deserializeControllerList(std::ifstream &ifs)
{
	size_t size;
	ifs.read((char*)&size, sizeof(size_t));
	//DEBUG_OUT("************ size = %d", size);
	for (size_t i = 0; i < size; i++)
	{
		ControllerData * cd = new ControllerData(this);
		cd->deserialize(ifs);
		this->addController(cd, false, false);
		//DEBUG_OUT("*** itemUuid = %s ; position(%f, %f)", cd->uuid().c_str(), cd->position().x(), cd->position().y());
	}
}

void FloorDesignDocument::serializeWallList(std::ofstream &ofs)
{
	size_t size = wallList->size();
	//DEBUG_OUT("------------ size = %d", size);
	ofs.write((const char*)&size, sizeof(size_t));
	for (WallData * wd : (*wallList))
	{
		wd->serialize(ofs);
		//DEBUG_OUT("----itemUuid = %s ; position(%f, %f)", wd->uuid().c_str(), wd->controlPosition().x(), wd->controlPosition().y());
	}
}

void FloorDesignDocument::deserializeWallList(std::ifstream &ifs)
{
	size_t size;
	ifs.read((char*)&size, sizeof(size_t));
	//DEBUG_OUT("************ size = %d", size);
	for (size_t i = 0; i < size; i++)
	{
		WallData * wd = new WallData(this);
		wd->deserialize(ifs);
		addWall(wd, false);
		//DEBUG_OUT("*** itemUuid = %s ; position(%f, %f)", wd->uuid().c_str(), wd->controlPosition().x(), wd->controlPosition().y());

		//DEBUG_OUT("%s", cd->uuid().c_str());
		//DEBUG_OUT("%s", existingCD->uuid().c_str());
		//DEBUG_OUT("%s", (existingCD->uuid() == cd->uuid()) ? "EQUAL" : "DIFFERENT");
	}
}

void FloorDesignDocument::serialize(std::ofstream &ofs)
{
	serializeControllerList(ofs);
	serializeWallList(ofs);
}

void FloorDesignDocument::deserialize(std::ifstream &ifs)
{
	deserializeControllerList(ifs);
	deserializeWallList(ifs);
}

ControllerData * FloorDesignDocument::getControllerDataViaUuid(std::string uuid)
{
	map<string, ControllerData*>::iterator mapEntryPosition = controllerMap->find(uuid);
	if (mapEntryPosition != controllerMap->end())
	{
		return (*controllerMap)[uuid];
	}
	return nullptr;
}

void FloorDesignDocument::addController(std::string &itemUuidStr, ControllerData *controllerData)
{
	controllerData->setOwnerDocument(this);
	controllerList->push_back(controllerData);
	controllerMap->insert(pair<std::string, ControllerData*>(itemUuidStr, controllerData));
}

void FloorDesignDocument::removeController(ControllerData *controllerData)
{
	vector<ControllerData *>::iterator vectorEntryPosition = std::find(controllerList->begin(), controllerList->end(), controllerData);
	if (vectorEntryPosition != controllerList->end())
	{
		controllerList->erase(vectorEntryPosition);
	}
	map<string, ControllerData*>::iterator mapEntryPosition = controllerMap->find(controllerData->uuid());
	if (mapEntryPosition != controllerMap->end())
	{
		controllerMap->erase(mapEntryPosition);
	}
	delete controllerData;
}

void FloorDesignDocument::removeController(std::string &itemUuidStr)
{
	map<string, ControllerData*>::iterator mapEntryPosition = controllerMap->find(itemUuidStr);
	if (mapEntryPosition != controllerMap->end())
	{
		ControllerData* controllerData = (*controllerMap)[itemUuidStr];
		vector<ControllerData *>::iterator vectorEntryPosition = std::find(controllerList->begin(), controllerList->end(), controllerData);
		if (vectorEntryPosition != controllerList->end())
		{
			controllerList->erase(vectorEntryPosition);
		}
		controllerMap->erase(mapEntryPosition);
	}
}

int FloorDesignDocument::controllerCount()
{
	return (int)controllerList->size();
}

ControllerData * FloorDesignDocument::controllerAt(int index)
{
	if (controllerList == nullptr || index < 0 || index >= controllerList->size())
	{
		return nullptr;
	}
	return (*controllerList)[index];
}

bool FloorDesignDocument::createWallJunctionsFromIntersection(FloorplanGraphicsScene * scene, WallData* wallData)
{
	FloorDesignDocument * document = wallData->ownerDocument();

	vector<WallData*> controlledWallList;
	controlledWallList.push_back(wallData);

	vector<ControllerMappingData*> controlledList = wallData->startControllerData()->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		if (cmd->controlledWall != wallData)
		{
			controlledWallList.push_back(cmd->controlledWall);
		}
	}

	controlledList = wallData->endControllerData()->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		if (cmd->controlledWall != wallData)
		{
			controlledWallList.push_back(cmd->controlledWall);
		}
	}

	return document->createWallJunctionsFromIntersection(scene, controlledWallList);
}

bool FloorDesignDocument::createWallJunctionsFromIntersection(FloorplanGraphicsScene * scene, vector<WallData*> &controlledWallList)
{
	FloorDesignDocument * document = this;

	vector<WallData*> wallsForIntersectionCheck;
	size_t wallCount = document->wallCount();
	for (int i = 0; i < wallCount; i++)
	{
		WallData * wd = document->wallAt(i);
		wallsForIntersectionCheck.push_back(wd);
	}

	for (WallData * wd : controlledWallList)
	{
		vector<WallData *>::iterator vectorEntryPosition = std::find(
			wallsForIntersectionCheck.begin(), wallsForIntersectionCheck.end(), wd);
		if (vectorEntryPosition != wallsForIntersectionCheck.end())
		{
			wallsForIntersectionCheck.erase(vectorEntryPosition);
		}
	}


	int intersectionIndex = 0;
	map<WallData *, list<ControllerToIntersectionPair> *> nonControlledWallDataToSortedIntersectionMap;

	map<WallData *, list<ControllerToIntersectionPair>> wallDataToSortedIntersectionMap;
	list<ControllerToIntersectionPair> sortedIntersectionList;
	//QPointF L1_P1 = this->m_controllerData->position();
	for (WallData * controlledWall : controlledWallList)
	{
		QPointF L1_P1 = controlledWall->startPosition();
		QPointF L1_P2 = controlledWall->endPosition();

		//ControllerData * otherEndOfTheWall = (cmd->controlledPart == WallSegmentControlledPart::Start) ?
		//	controlledWall->endControllerData() : controlledWall->startControllerData();
		//QPointF L1_P2 = otherEndOfTheWall->position();

		sortedIntersectionList.clear();
		for (WallData * nonControlledWall : wallsForIntersectionCheck)
		{
			QPointF L2_P1 = nonControlledWall->startPosition();
			QPointF L2_P2 = nonControlledWall->endPosition();

			QPointF intersectionPoint;
			bool doesIntersect = IntersectionOfTwoLineSegment(L1_P1, L1_P2, L2_P1, L2_P2, intersectionPoint);
			//DEBUG_OUT("intersectionPoint(%f, %f) ; doesIntersect = %s", intersectionPoint.x(), intersectionPoint.y(), (doesIntersect ? "YEP" : "NOPE"));

			if (doesIntersect)
			{
				qreal distanceToStart = Distance(intersectionPoint, L1_P1);
				qreal distanceToEnd = Distance(intersectionPoint, L1_P2);

				qreal distance2ToStart = Distance(intersectionPoint, L2_P1);
				qreal distance2ToEnd = Distance(intersectionPoint, L2_P2);

				bool isValidIntersection = (distanceToStart > MergeWallCornerDistanceLimit && distanceToEnd > MergeWallCornerDistanceLimit) &&
				 (distance2ToStart > MergeWallCornerDistanceLimit && distance2ToEnd > MergeWallCornerDistanceLimit);
				if (isValidIntersection)
				{
					ControllerToIntersectionPair ctip;
					ctip.intersectionPoint = intersectionPoint;
					//ctip.originController = this->m_controllerData;
					ctip.originController = controlledWall->startControllerData();
					ctip.intersectionIndex = intersectionIndex;
					sortedIntersectionList.push_back(ctip);


					list<ControllerToIntersectionPair> * sortedIntersectionForNonControlledList = nullptr;
					map<WallData *, list<ControllerToIntersectionPair> *>::iterator mapEntryPosition =
						nonControlledWallDataToSortedIntersectionMap.find(nonControlledWall);
					if (mapEntryPosition != nonControlledWallDataToSortedIntersectionMap.end())
					{
						sortedIntersectionForNonControlledList = nonControlledWallDataToSortedIntersectionMap[nonControlledWall];
					}
					else
					{
						// if a list for non controlled walls do not exist till now create and insert it to the map
						sortedIntersectionForNonControlledList = new list<ControllerToIntersectionPair>();
						nonControlledWallDataToSortedIntersectionMap.insert(
							pair<WallData*, list<ControllerToIntersectionPair>*>(nonControlledWall, sortedIntersectionForNonControlledList));
					}

					ctip.originController = nonControlledWall->startControllerData();
					sortedIntersectionForNonControlledList->push_back(ctip);


					intersectionIndex++;
				}
			}
		}
		sortedIntersectionList.sort(contollerToIntersectionDistanceComparer);
		wallDataToSortedIntersectionMap.insert(pair<WallData*, list<ControllerToIntersectionPair>>(controlledWall, sortedIntersectionList));
	}

	// if no intersection occurs do not continue with the method
	if (intersectionIndex == 0)
	{
		return false;
	}

	WallData * wallToSplit = nullptr;
	vector<WallData*> wallInvalidateList;

	map<int, ControllerData*> intersectionIndexToNewlyCreatedControllerDataMap;
	// wallDataToSortedIntersectionPair : wdtsiPair
	for (pair<WallData*, list<ControllerToIntersectionPair>> wdtsiPair : wallDataToSortedIntersectionMap)
	{
		// ControllerToIntersectionPair : ctiPair
		int order = 0;
		wallToSplit = wdtsiPair.first;
		wallInvalidateList.push_back(wallToSplit);
		for (ControllerToIntersectionPair ctiPair : wdtsiPair.second)
		{
			WallData * splitBornWallData;
			ControllerData * splitBornController = WallData::SplitWallFromPoint(wallToSplit, splitBornWallData, ctiPair.intersectionPoint, false);
			wallToSplit = splitBornWallData;
			wallInvalidateList.push_back(wallToSplit);

			intersectionIndexToNewlyCreatedControllerDataMap.insert(
				pair<int, ControllerData*>(ctiPair.intersectionIndex, splitBornController));

			//break;
			//intersectionIndexToNewlyCreatedControllerDataMap.insert(pair<int, ControllerData*>(ctiPair.intersectionPoint, nullptr));

			//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - ControlPointGraphicsItem::Radius,
			//	0 - ControlPointGraphicsItem::Radius, (ControlPointGraphicsItem::Radius * 2.0f) - 1.0f,
			//	(ControlPointGraphicsItem::Radius * 2.0f) - 1.0f, nullptr);
			//intersect->setBrush(QBrush(Qt::red));
			//scene->addItem(intersect);
			//intersect->setPos(ctiPair.intersectionPoint);

			//QGraphicsTextItem * textItem = new QGraphicsTextItem(QString::number(order) + " " + QString::number(ctiPair.intersectionIndex));
			//QFont font;
			//font.setPixelSize(18);
			//font.setBold(true);
			//font.setFamily("Calibri");
			//textItem->setDefaultTextColor(Qt::red);
			//textItem->setFont(font);
			//scene->addItem(textItem);
			//textItem->setPos(ctiPair.intersectionPoint);

			order++;
		}
	}

	// wallDataToSortedIntersectionPair : wdtsiPair
	vector<ControllerData *> splitBornControllerList;
	for (pair<WallData*, list<ControllerToIntersectionPair>*> ncwdtsiPair : nonControlledWallDataToSortedIntersectionMap)
	{
		wallToSplit = ncwdtsiPair.first;
		wallInvalidateList.push_back(wallToSplit);

		list<ControllerToIntersectionPair> * silfncw = ncwdtsiPair.second;
		silfncw->sort(contollerToIntersectionDistanceComparer);
		// ControllerToIntersectionPair : ctiPair
		int order = 0;
		for (ControllerToIntersectionPair ctiPair : *(ncwdtsiPair.second))
		{
			WallData * splitBornWallData;
			ControllerData * splitBornController = WallData::SplitWallFromPoint(wallToSplit, splitBornWallData, ctiPair.intersectionPoint, false);
			wallToSplit = splitBornWallData;
			wallInvalidateList.push_back(wallToSplit);

			ControllerData * previousSplitBornController = intersectionIndexToNewlyCreatedControllerDataMap[ctiPair.intersectionIndex];
			document->mergeControlPoint(splitBornController, previousSplitBornController);
			splitBornControllerList.push_back(previousSplitBornController);

			//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - Radius, 0 - Radius, (Radius * 2.0f) - 1.0f, (Radius * 2.0f) - 1.0f, nullptr);
			//intersect->setBrush(QBrush(Qt::red));
			//scene->addItem(intersect);
			//intersect->setPos(ctiPair.intersectionPoint);

			//QGraphicsTextItem * textItem = new QGraphicsTextItem(QString::number(order) + " " + QString::number(ctiPair.intersectionIndex));
			//QFont font;
			//font.setPixelSize(18);
			//font.setBold(true);
			//font.setFamily("Calibri");
			//textItem->setDefaultTextColor(Qt::red);
			//textItem->setFont(font);
			//scene->addItem(textItem);
			//textItem->setPos(ctiPair.intersectionPoint);

			order++;
		}
		// delete the list since we are done with it
		delete silfncw;
	}

	//MergeWalls::ModifyWallSegmentPolygonsRelatedToMultipleControllersDueToConjuction(&splitBornControllerList, scene);
	MergeWallPolygons::ModifyCurvedWallSegmentPolygonsForGivenControlDataList(&splitBornControllerList, true, scene);
	for (WallData * wd : wallInvalidateList)
	{
		wd->updateRelatedGraphicsItemPolygon();
	}

	//// nonControlledWallDataToSortedIntersectionPair : wdtsiPair
	//// delete sortedIntersectionForNonControlledList dynamic instances in the nonControlledWallDataToSortedIntersectionMap
	//for (pair<WallData*, list<ControllerToIntersectionPair>*> ncwdtsiPair : nonControlledWallDataToSortedIntersectionMap)
	//{
	//	DEBUG_OUT("warning: wallId = %s : ncwdtsiPair.size = %d", ncwdtsiPair.first->uuid().c_str(), ncwdtsiPair.second->size());
	//	delete ncwdtsiPair.second;
	//}

	return true;
}

void FloorDesignDocument::centerControlls()
{
	qreal minX = DBL_MAX;
	qreal maxX = DBL_MIN;
	qreal minY = DBL_MAX;
	qreal maxY = DBL_MIN;
	QPointF centerPos;
	for (ControllerData * cd : (*controllerList))
	{
		QPointF pos = cd->position();
		minX = qMin(minX, pos.x());
		maxX = qMax(maxX, pos.x());
		minY = qMin(minY, pos.y());
		maxY = qMax(maxY, pos.y());
		//centerPos += pos;
	}
	//centerPos = centerPos * (1.0f / (float)controllerList->size());
	centerPos.setX((minX + maxX) * 0.5);
	centerPos.setY((minY + maxY) * 0.5);

	for (ControllerData * cd : (*controllerList))
	{
		QPointF pos = cd->position();
		pos -= centerPos;
		cd->setPositionOnly(pos);
	}
}
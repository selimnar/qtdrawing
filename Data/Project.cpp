#include "Project.h"

//#define XERCES_STATIC_LIBRARY
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

//Mandatory for using any feature of Xerces.
#include <xercesc/util/PlatformUtils.hpp>
//Use the Document Object Model (DOM) API
#include <xercesc/dom/DOM.hpp>
//Required for outputing a Xerces DOMDocument to a standard output stream (Also see: XMLFormatTarget)
#include <xercesc/framework/StdOutFormatTarget.hpp>
//Required for outputing a Xerces DOMDocument to the file system (Also see: XMLFormatTarget)
#include <xercesc/framework/LocalFileFormatTarget.hpp>
XERCES_CPP_NAMESPACE_USE

#include "utilities/utilities.h"
#include "utilities/FileUtilities.h"

#include <QFile>

using namespace std;

AssignedFloorInstance::AssignedFloorInstance()
{
	this->m_id = 0;
	this->m_floorId = 0;
	this->m_count = 0;
}

AssignedFloorInstance::AssignedFloorInstance(AssignedFloorInstance * inPlanInstance)
{
	this->m_id = inPlanInstance->m_id;
	this->m_floorId = inPlanInstance->m_floorId;
	this->m_count = inPlanInstance->m_count;
}

AssignedFloorInstance::~AssignedFloorInstance()
{

}

void AssignedFloorInstance::deserialize(const XERCES_CPP_NAMESPACE::DOMElement * p_floorInstaceElement)
{
	char * idStr = XMLString::transcode(p_floorInstaceElement->getAttribute(u"id"));
	setId(atoi(idStr));

	char * floorPlanId = XMLString::transcode(p_floorInstaceElement->getAttribute(u"floorPlanId"));
	setFloorId(atoi(floorPlanId));

	char * count = XMLString::transcode(p_floorInstaceElement->getAttribute(u"count"));
	setCount(atoi(count));
}

void AssignedFloorInstance::serialize(XERCES_CPP_NAMESPACE::DOMElement * p_floorInstaceElement, XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument)
{
	DOMElement * p_DataElement = p_DOMDocument->createElement(u"floorInstnc");

	std::u16string idXMLStr = ConvertToUint16String(m_id);
	p_DataElement->setAttribute(u"id", idXMLStr.c_str());

	std::u16string floorIdXMLStr = ConvertToUint16String(m_floorId);
	p_DataElement->setAttribute(u"floorPlanId", floorIdXMLStr.c_str());

	std::u16string countXMLStr = ConvertToUint16String(m_count);
	p_DataElement->setAttribute(u"count", countXMLStr.c_str());

	p_floorInstaceElement->appendChild(p_DataElement);
}

void AssignedFloorInstance::setId(const unsigned int value)
{
	m_id = value;
}

unsigned int AssignedFloorInstance::id() const
{
	return m_id;
}

void AssignedFloorInstance::setFloorId(const unsigned int value)
{
	m_floorId = value;
}

unsigned int AssignedFloorInstance::floorId() const
{
	return m_floorId;
}

void AssignedFloorInstance::setCount(const unsigned int value)
{
	m_count = value;
}

unsigned int AssignedFloorInstance::count() const
{
	return m_count;
}

//////////////////////////////////////////////////////////////

BuildingInstance::BuildingInstance()
{
	m_id = 0;
	m_definition = nullptr;
}

BuildingInstance::BuildingInstance(BuildingInstance * bldng)
{
	this->m_id = bldng->m_id;
	this->m_definition = nullptr;

	if (bldng->m_definition != nullptr)
	{
		this->m_definition = new char[strlen(bldng->m_definition) + 1];
		strcpy(this->m_definition, bldng->m_definition);
	}
}

BuildingInstance::~BuildingInstance()
{
	if (this->m_definition != nullptr)
	{
		delete this->m_definition;
	}
}

void BuildingInstance::deserialize(const DOMElement * p_buildingInstanceElement)
{
	char * planIdStr = XMLString::transcode(p_buildingInstanceElement->getAttribute(u"id"));
	setId(atoi(planIdStr));

	char * definitionIdStr = XMLString::transcode(p_buildingInstanceElement->getAttribute(u"definition"));
	setDefinition(definitionIdStr);

	// read floor instances
	const DOMElement * floorInstancesElement = (const DOMElement*)p_buildingInstanceElement->getElementsByTagName(u"floorInstances")->item(0);
	if (floorInstancesElement != nullptr)
	{
		DOMNodeList * floorInstanceNodeList = (DOMNodeList*)floorInstancesElement->getElementsByTagName(u"floorInstnc");
		for (int i = 0; i < floorInstanceNodeList->getLength(); i++)
		{
			AssignedFloorInstance * floorInstance = new AssignedFloorInstance();
			const DOMElement* p_floorInstanceElement = (const DOMElement*)floorInstanceNodeList->item(i);
			floorInstance->deserialize(p_floorInstanceElement);

			this->addAssignedFloorInstance(floorInstance);
		}
	}
}

void BuildingInstance::serialize(DOMElement * p_drawnPlansElement, DOMDocument * p_DOMDocument)
{
	//Create an Element node, then fill in some attributes, and then append this to the root element.
	DOMElement * p_DataElement = p_DOMDocument->createElement(u"building");

	std::u16string idXMLStr = ConvertToUint16String(m_id);
	p_DataElement->setAttribute(u"id", idXMLStr.c_str());

	std::u16string definitionXMLStr = ConvertWcharToXMLChar(m_definition);
	p_DataElement->setAttribute(u"definition", definitionXMLStr.c_str());

	// add floor instaces to xml
	if (m_assignedFloorPlans.size() > 0)
	{
		DOMElement * p_floorInstancesElement = p_DOMDocument->createElement(u"floorInstances");
		p_drawnPlansElement->appendChild(p_floorInstancesElement);
		for (const auto& kv : m_assignedFloorPlans)
		{
			AssignedFloorInstance * floorInstance = kv.second;
			floorInstance->serialize(p_floorInstancesElement, p_DOMDocument);
		}
		p_DataElement->appendChild(p_floorInstancesElement);
	}

	p_drawnPlansElement->appendChild(p_DataElement);
}

void BuildingInstance::setId(const unsigned int value)
{
	m_id = value;
}

unsigned int BuildingInstance::id() const
{
	return m_id;
}

void BuildingInstance::setDefinition(const char * def)
{
	if (m_definition != nullptr)
	{
		delete m_definition;
	}
	m_definition = nullptr;
	if (def != nullptr)
	{
		this->m_definition = new char[strlen(def) + 1];
		strcpy(this->m_definition, def);
	}
	emit definitionChanged(m_definition);
}

char * BuildingInstance::definition() const
{
	return m_definition;
}

unsigned int BuildingInstance::retrieveMaxAssignedFloorInstanceId()
{
	unsigned int maxId = 0;
	for (const auto& vk : m_assignedFloorPlans)
	{
		maxId = qMax(maxId, vk.first);
	}
	return maxId;
}

void BuildingInstance::addAssignedFloorInstance(AssignedFloorInstance * afpi)
{
	m_assignedFloorPlans.insert(std::pair<unsigned int, AssignedFloorInstance *>(afpi->id(), afpi));
}

void BuildingInstance::removeAssignedFloorInstance(unsigned int afpiId)
{
	map<unsigned int, AssignedFloorInstance *>::iterator mapEntryPosition = m_assignedFloorPlans.find(afpiId);
	if (mapEntryPosition != m_assignedFloorPlans.end())
	{
		m_assignedFloorPlans.erase(mapEntryPosition);
	}
}


//////////////////////////////////////////////////////


const string DrawnFloorPlan::DrawnPlanFolder("\\drawnPlans\\");

DrawnFloorPlan::DrawnFloorPlan()
{
	m_id = 0;
	m_definition = nullptr;
}

DrawnFloorPlan::DrawnFloorPlan(DrawnFloorPlan * plan)
{
	this->m_id = plan->m_id;
	this->m_definition = nullptr;

	if (plan->m_definition != nullptr)
	{
		this->m_definition = new char[strlen(plan->m_definition) + 1];
		strcpy(this->m_definition, plan->m_definition);
		//this->m_definition = archiPlan->m_definition;
	}
}

DrawnFloorPlan::~DrawnFloorPlan()
{
	if (this->m_definition != nullptr)
	{
		delete this->m_definition;
	}
}

void DrawnFloorPlan::deserialize(const DOMElement * p_drawnPlansElement)
{
	char * planIdStr = XMLString::transcode(p_drawnPlansElement->getAttribute(u"id"));
	setId(atoi(planIdStr));

	char * definitionIdStr = XMLString::transcode(p_drawnPlansElement->getAttribute(u"definition"));
	setDefinition(definitionIdStr);
}

void DrawnFloorPlan::serialize(DOMElement * p_drawnPlansElement, DOMDocument * p_DOMDocument)
{
	//Create an Element node, then fill in some attributes, and then append this to the root element.
	DOMElement * p_DataElement = p_DOMDocument->createElement(u"plan");

	std::u16string idXMLStr = ConvertToUint16String(m_id);
	p_DataElement->setAttribute(u"id", idXMLStr.c_str());

	std::u16string definitionXMLStr = ConvertWcharToXMLChar(m_definition);
	p_DataElement->setAttribute(u"definition", definitionXMLStr.c_str());

	p_drawnPlansElement->appendChild(p_DataElement);
}

void DrawnFloorPlan::setId(const unsigned int value)
{
	m_id = value;
}

unsigned int DrawnFloorPlan::id() const
{
	return m_id;
}

void DrawnFloorPlan::setDefinition(const char * def)
{
	if (m_definition != nullptr)
	{
		delete m_definition;
	}
	m_definition = nullptr;
	if (def != nullptr)
	{
		this->m_definition = new char[strlen(def) + 1];
		strcpy(this->m_definition, def);
	}
	emit definitionChanged(m_definition);
}

char * DrawnFloorPlan::definition() const
{
	return m_definition;
}

void DrawnFloorPlan::CreateNewDrawnPlanScene(Project * ownerProject, std::string projectsWorkspaceFolder)
{
	string folderPath = ownerProject->GetProjectFolderPath(projectsWorkspaceFolder);
	string drawnPlansFolder = folderPath + DrawnFloorPlan::DrawnPlanFolder;

	bool folderExists = FileUtilities::PathExists(drawnPlansFolder.c_str());
	if (!folderExists)
	{
		FileUtilities::CreateFolder(drawnPlansFolder.c_str());
	}

	QString emptyDrawnPlanFileName = "empty.fdd";
	QString drawnPlanFileName = QString::fromStdString(drawnPlansFolder) + QString::number(m_id) + ".fdd";
	QFile::copy(emptyDrawnPlanFileName, drawnPlanFileName);
}

std::string DrawnFloorPlan::getFilePath(Project * ownerProject, std::string projectsWorkspaceFolder)
{
	string folderPath = ownerProject->GetProjectFolderPath(projectsWorkspaceFolder);
	string drawnPlansFolder = folderPath + DrawnFloorPlan::DrawnPlanFolder;
	string drawnPlanFileName = drawnPlansFolder + to_string(m_id) + ".fdd";
	return drawnPlanFileName;
}



ArchitecturalPlan::ArchitecturalPlan()
{
	m_id = 0;
	m_definition = nullptr;
	m_fileName = nullptr;
}

ArchitecturalPlan::ArchitecturalPlan(ArchitecturalPlan * archiPlan)
{
	this->m_id = archiPlan->m_id;
	this->m_definition = nullptr;
	this->m_fileName = nullptr;

	if (archiPlan->m_definition != nullptr)
	{
		this->m_definition = new char[strlen(archiPlan->m_definition) + 1];
		strcpy(this->m_definition, archiPlan->m_definition);
		//this->m_definition = archiPlan->m_definition;
	}
	if (archiPlan->m_fileName != nullptr)
	{
		//this->m_fileName = archiPlan->m_fileName;
		this->m_fileName = new char[strlen(archiPlan->m_fileName) + 1];
		strcpy(this->m_fileName, archiPlan->m_fileName);
	}
}

ArchitecturalPlan::~ArchitecturalPlan()
{
	if (this->m_definition != nullptr)
	{
		delete this->m_definition;
	}
	if (this->m_fileName != nullptr)
	{
		delete this->m_fileName;
	}
}

void ArchitecturalPlan::deserialize(const DOMElement * p_archPlansElement)
{
	char * planIdStr = XMLString::transcode(p_archPlansElement->getAttribute(u"id"));
	setId(atoi(planIdStr));

	char * definitionIdStr = XMLString::transcode(p_archPlansElement->getAttribute(u"definition"));
	setDefinition(definitionIdStr);

	char * filenameIdStr = XMLString::transcode(p_archPlansElement->getAttribute(u"filename"));
	setFileName(filenameIdStr);
}

void ArchitecturalPlan::serialize(DOMElement * p_archPlansElement, DOMDocument * p_DOMDocument)
{
	//Create an Element node, then fill in some attributes, and then append this to the root element.
	DOMElement * p_DataElement = p_DOMDocument->createElement(u"plan");

	std::u16string idXMLStr = ConvertToUint16String(m_id);
	p_DataElement->setAttribute(u"id", idXMLStr.c_str());

	std::u16string definitionXMLStr = ConvertWcharToXMLChar(m_definition);
	p_DataElement->setAttribute(u"definition", definitionXMLStr.c_str());

	std::u16string fileNameXMLStr = ConvertWcharToXMLChar(m_fileName);
	p_DataElement->setAttribute(u"filename", fileNameXMLStr.c_str());

	p_archPlansElement->appendChild(p_DataElement);
}

void ArchitecturalPlan::setId(const unsigned int value)
{
	m_id = value;
}

unsigned int ArchitecturalPlan::id() const
{
	return m_id;
}

void ArchitecturalPlan::setDefinition(const char * def)
{
	if (m_definition != nullptr)
	{
		delete m_definition;
	}
	m_definition = nullptr;
	if (def != nullptr)
	{
		this->m_definition = new char[strlen(def) + 1];
		strcpy(this->m_definition, def);
	}
	emit definitionChanged(m_definition);
}

char * ArchitecturalPlan::definition() const
{
	return m_definition;
}

void ArchitecturalPlan::setFileName(const char * name)
{
	if (m_fileName != nullptr)
	{
		delete m_fileName;
	}
	m_fileName = nullptr;
	if (name != nullptr)
	{
		this->m_fileName = new char[strlen(name) + 1];
		strcpy(m_fileName, name);
	}
}

char * ArchitecturalPlan::fileName() const
{
	return m_fileName;
}






Project::Project()
{
	this->m_tasinmazId = -1;
	this->m_cityId = 0;
	this->m_townId = 0;
	this->m_neighborhoodId = 0;
	this->m_ada = 0;
	this->m_parsel = 0;
}

Project::Project(Project * proj)
{
	this->m_tasinmazId = proj->m_tasinmazId;
	this->m_cityId = proj->m_cityId;
	this->m_townId = proj->m_townId;
	this->m_neighborhoodId = proj->m_neighborhoodId;
	this->m_ada = proj->m_ada;
	this->m_parsel = proj->m_parsel;
}

Project::~Project()
{
}

void Project::setTasinmazId(int value)
{
	m_tasinmazId = value;
	emit tasinmazIdChanged(value);
}

int Project::tasinmazId() const
{
	return m_tasinmazId;
}

void Project::setCityId(unsigned int value)
{
	m_cityId = value;
	emit cityIdChanged(value);
}

unsigned int Project::cityId() const
{
	return m_cityId;
}

void Project::setTownId(unsigned int value)
{
	m_townId = value;
	emit townIdChanged(value);
}

unsigned int Project::townId() const
{
	return m_townId;
}

void Project::setNeighborhoodId(unsigned int value)
{
	m_neighborhoodId = value;
	emit neighborhoodIdChanged(value);
}

unsigned int Project::neighborhoodId() const
{
	return m_neighborhoodId;
}

void Project::setAda(unsigned int value)
{
	m_ada = value;
	emit adaChanged(value);
}

unsigned int Project::ada() const
{
	return m_ada;
}

void Project::setParsel(unsigned int value)
{
	m_parsel = value;
	emit parselChanged(value);
}

unsigned int Project::parsel() const
{
	return m_parsel;
}

void Project::addArchitecturalPlan(ArchitecturalPlan * ap)
{
	m_scannedImageFiles.insert(std::pair<unsigned int, ArchitecturalPlan *>(ap->id(), ap));
}

void Project::removeArchitecturalPlan(unsigned int apId)
{
	map<unsigned int, ArchitecturalPlan *>::iterator mapEntryPosition = m_scannedImageFiles.find(apId);
	if (mapEntryPosition != m_scannedImageFiles.end())
	{
		m_scannedImageFiles.erase(mapEntryPosition);
	}
}

void Project::addDrawnPlan(DrawnFloorPlan * dp)
{
	m_drawnPlans.insert(std::pair<unsigned int, DrawnFloorPlan *>(dp->id(), dp));
}

void Project::removeDrawnPlan(unsigned int dpId)
{
	map<unsigned int, DrawnFloorPlan *>::iterator mapEntryPosition = m_drawnPlans.find(dpId);
	if (mapEntryPosition != m_drawnPlans.end())
	{
		m_drawnPlans.erase(mapEntryPosition);
	}
}

void Project::addBuildingInstance(BuildingInstance * bi)
{
	m_buildingInstances.insert(std::pair<unsigned int, BuildingInstance *>(bi->id(), bi));
}

void Project::removeBuildingInstance(unsigned int biId)
{
	map<unsigned int, BuildingInstance *>::iterator mapEntryPosition = m_buildingInstances.find(biId);
	if (mapEntryPosition != m_buildingInstances.end())
	{
		m_buildingInstances.erase(mapEntryPosition);
	}
}

unsigned int Project::retrieveMaxArchitecturalPlanId()
{
	unsigned int maxId = 0;
	for (const auto& vk : m_scannedImageFiles)
	{
		maxId = qMax(maxId, vk.first);
	}
	return maxId;
}

unsigned int Project::retrieveMaxDrawnFloorPlanId()
{
	unsigned int maxId = 0;
	for (const auto& vk : m_drawnPlans)
	{
		maxId = qMax(maxId, vk.first);
	}
	return maxId;
}

unsigned int Project::retrieveMaxBuildingInstanceId()
{
	unsigned int maxId = 0;
	for (const auto& vk : m_buildingInstances)
	{
		maxId = qMax(maxId, vk.first);
	}
	return maxId;
}

std::string Project::GetProjectFolderPath(std::string projectsWorkspaceFolder)
{
	std::string folderPath;
	if (m_tasinmazId >= 0)
	{
		folderPath = projectsWorkspaceFolder + "\\" + std::to_string(m_tasinmazId);
	}
	else
	{
		folderPath = projectsWorkspaceFolder + "\\" + std::to_string(m_cityId) + "_" +
			std::to_string(m_townId) + "_" + std::to_string(m_neighborhoodId) + "_"
			+ std::to_string(m_ada) + "_" + std::to_string(m_parsel);
	}
	return folderPath;
}

void Project::saveProjectFile(const char * folderPath, const char * fileName)
{
	serialize(fileName);

	QString projectFolderPath(folderPath);
	projectFolderPath += "\\";

	Project &pro = *this;
	QString metaFilePath = projectFolderPath + QString::number(pro.tasinmazId()) + "_" + QString::number(pro.cityId()) + "_" + QString::number(pro.townId()) + "_" +
		QString::number(pro.neighborhoodId()) + "_" + QString::number(pro.ada()) + "_" + QString::number(pro.parsel()) + ".meta";

	if (!QFile::exists(metaFilePath))
	{
		QFile file(metaFilePath);
		file.open(QIODevice::WriteOnly);
		file.close();
	}
}

void Project::saveProject(string projectsWorkspaceFolder)
{
	std::string folderPath = this->GetProjectFolderPath(projectsWorkspaceFolder);

	bool folderExists = FileUtilities::PathExists(folderPath.c_str());
	DEBUG_OUT("!!!!!!!!!!!----------- MIMARI_PROJELER_KLASORU = %s", folderPath.c_str());
	DEBUG_OUT("!!!!!!!!!!!----------- FOLDER EXISTS = %s", (folderExists ? "TRUE" : "FALSE"));
	if (!folderExists)
	{
		FileUtilities::CreateFolder(folderPath.c_str());
	}
	string projectFilePath = folderPath + "\\" + "project.xml";
	DEBUG_OUT("!!!!!!!!!!!----------- projectFilePath = %s", projectFilePath.c_str());
	saveProjectFile(folderPath.c_str(), projectFilePath.c_str());
}

void Project::setProjectString(QString inProjectString)
{
	m_projectString = inProjectString;
}

QString Project::projectString()
{
	return m_projectString;
}

void Project::emitDataSignals()
{
	emit tasinmazIdChanged(m_tasinmazId);
	emit cityIdChanged(m_cityId);
	emit townIdChanged(m_townId);
	emit neighborhoodIdChanged(m_neighborhoodId);
	emit adaChanged(m_ada);
	emit parselChanged(m_parsel);
}

void Project::serialize(const char * fileName)
{
	XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument = CreateDocument(u"project");

	DOMElement * p_RootElement = p_DOMDocument->getDocumentElement();

	//Fill in the DOM document - different parts
	//Create a Comment node, and then append this to the root element.
	DOMComment * p_DOMComment = p_DOMDocument->createComment(u"Dates are formatted mm/dd/yy."
		u" Don't forget XML is case-sensitive.");
	p_RootElement->appendChild(p_DOMComment);

	//Create an Element node, then fill in some attributes, and then append this to the root element.
	DOMElement * p_DataElement = p_DOMDocument->createElement(u"data");

	//Copy the current system date to a buffer, then set/create the attribute.
	//wchar_t wcharBuffer[128];
	//_wstrdate_s(wcharBuffer, 9);
	//XMLCh * result = ConvertWcharToXMLChar(wcharBuffer);
	//p_DataElement->setAttribute(u"date", result);

	//Convert an integer to a string, then set/create the attribute.
	//_itow_s(65536, wcharBuffer, 128, 10);
	//result = ConvertWcharToXMLChar(wcharBuffer);
	//p_DataElement->setAttribute(u"integer", result);

	//p_DataElement->getAttributeNode()
	//DOMAttrImpl *dd;
	//p_DataElement->setAttribute(u"threhh", u"regreg");
	//delete result;

	std::u16string tasinmazIdXMLStr = ConvertToUint16String(m_tasinmazId);
	p_DataElement->setAttribute(u"tasinmazId", tasinmazIdXMLStr.c_str());

	std::u16string cityIdXMLStr = ConvertToUint16String(m_cityId);
	p_DataElement->setAttribute(u"cityId", cityIdXMLStr.c_str());

	std::u16string townIdXMLStr = ConvertToUint16String(m_townId);
	p_DataElement->setAttribute(u"townId", townIdXMLStr.c_str());

	std::u16string neighborhoodIdXMLStr = ConvertToUint16String(m_neighborhoodId);
	p_DataElement->setAttribute(u"neighborhoodId", neighborhoodIdXMLStr.c_str());

	std::u16string adaXMLStr = ConvertToUint16String(m_ada);
	p_DataElement->setAttribute(u"ada", adaXMLStr.c_str());

	std::u16string parselXMLStr = ConvertToUint16String(m_parsel);
	p_DataElement->setAttribute(u"parsel", parselXMLStr.c_str());




	p_RootElement->appendChild(p_DataElement);


	// add architectural scanned plans to xml
	if (m_scannedImageFiles.size() > 0)
	{
		DOMElement * p_architecturalPlansElement = p_DOMDocument->createElement(u"architecturalPlans");
		p_RootElement->appendChild(p_architecturalPlansElement);
		for (const auto& kv : m_scannedImageFiles)
		{
			ArchitecturalPlan * architecturalPlan = kv.second;
			architecturalPlan->serialize(p_architecturalPlansElement, p_DOMDocument);
		}
	}

	// add drawn plans to xml
	if (m_drawnPlans.size() > 0)
	{
		DOMElement * p_drawnPlansElement = p_DOMDocument->createElement(u"drawnPlans");
		p_RootElement->appendChild(p_drawnPlansElement);
		for (const auto& kv : m_drawnPlans)
		{
			DrawnFloorPlan * drawnFloorPlan = kv.second;
			drawnFloorPlan->serialize(p_drawnPlansElement, p_DOMDocument);
		}
	}

	// add building instaces to xml
	if (m_buildingInstances.size() > 0)
	{
		DOMElement * p_buildingInstancesElement = p_DOMDocument->createElement(u"buildingInstances");
		p_RootElement->appendChild(p_buildingInstancesElement);
		for (const auto& kv : m_buildingInstances)
		{
			BuildingInstance * buildingInstance = kv.second;
			buildingInstance->serialize(p_buildingInstancesElement, p_DOMDocument);
		}
	}


	//Output on console
	//DoOutput2Stream(p_DOMDocument);

	//Output to a file
	WriteXMLDocument(p_DOMDocument, fileName);

	// Cleanup.
	p_DOMDocument->release();
	//delete p_DOMDocument;
}

void Project::deserialize(const char * fileName)
{
	try
	{
		XercesDOMParser* parser = new XercesDOMParser();
		parser->setValidationScheme(XercesDOMParser::Val_Always);
		parser->setDoNamespaces(true);    // optional

		ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
		parser->setErrorHandler(errHandler);

		try {
			//*out << "!!!!! Trying to Parse File" << endl << xmlFile << endl;
			parser->parse(fileName);
			//*out << "!!!!! File Parsed" << endl;

			xercesc::DOMDocument* doc = parser->getDocument();
			DOMElement* rootElement = doc->getDocumentElement(); // Getting root
																 //*out << "***************" << (rootElement->getNodeType() == DOMNode::ELEMENT_NODE) << endl;
			char *localElementName = XMLString::transcode(rootElement->getNodeName());

			//DOMNodeIterator * walker = doc->createNodeIterator(rootElement, DOMNodeFilter::SHOW_ELEMENT, nullptr, true);

			const DOMElement * dataElement = (const DOMElement*)rootElement->getElementsByTagName(u"data")->item(0);
			//char *townId = XMLString::transcode(dataElement->getNodeName());

			char *tasinmazIdStr = XMLString::transcode(dataElement->getAttribute(u"tasinmazId"));
			this->setTasinmazId(atoi(tasinmazIdStr));
			//DEBUG_OUT("tasinmazId: %d", tasinmazId);

			char *cityIdStr = XMLString::transcode(dataElement->getAttribute(u"cityId"));
			this->setCityId((unsigned int)atoll(cityIdStr));
			//DEBUG_OUT("cityId: %d", cityId);

			char *townIdStr = XMLString::transcode(dataElement->getAttribute(u"townId"));
			this->setTownId((unsigned int)atoll(townIdStr));
			//DEBUG_OUT("townId: %d", townId);

			char *neighborhoodIdStr = XMLString::transcode(dataElement->getAttribute(u"neighborhoodId"));
			this->setNeighborhoodId((unsigned int)atoll(neighborhoodIdStr));
			//DEBUG_OUT("neighborhoodId: %d", neighborhoodId);

			char *parselStr = XMLString::transcode(dataElement->getAttribute(u"parsel"));
			this->setParsel((unsigned int)atoll(parselStr));
			//DEBUG_OUT("parsel: %d", parsel);

			char *adaStr = XMLString::transcode(dataElement->getAttribute(u"ada"));
			this->setAda((unsigned int)atoll(adaStr));
			//DEBUG_OUT("ada: %d", ada);


			// read achitectural plans
			const DOMElement * architecturalPlansElement = (const DOMElement*)rootElement->getElementsByTagName(u"architecturalPlans")->item(0);
			if (architecturalPlansElement != nullptr)
			{
				DOMNodeList * planNodeList = (DOMNodeList*)architecturalPlansElement->getElementsByTagName(u"plan");
				for (int i = 0; i < planNodeList->getLength(); i++)
				{
					ArchitecturalPlan * plan = new ArchitecturalPlan();
					const DOMElement* p_archPlansElement = (const DOMElement*)planNodeList->item(i);
					plan->deserialize(p_archPlansElement);

					this->addArchitecturalPlan(plan);
				}
			}

			// read drawn plans
			const DOMElement * drawnPlansElement = (const DOMElement*)rootElement->getElementsByTagName(u"drawnPlans")->item(0);
			if (drawnPlansElement != nullptr)
			{
				DOMNodeList * planNodeList = (DOMNodeList*)drawnPlansElement->getElementsByTagName(u"plan");
				for (int i = 0; i < planNodeList->getLength(); i++)
				{
					DrawnFloorPlan * plan = new DrawnFloorPlan();
					const DOMElement* p_drawnPlansElement = (const DOMElement*)planNodeList->item(i);
					plan->deserialize(p_drawnPlansElement);

					this->addDrawnPlan(plan);
				}
			}

			// read building instances
			const DOMElement * buildingInstancesElement = (const DOMElement*)rootElement->getElementsByTagName(u"buildingInstances")->item(0);
			if (buildingInstancesElement != nullptr)
			{
				DOMNodeList * buildingInstanceNodeList = (DOMNodeList*)buildingInstancesElement->getElementsByTagName(u"building");
				for (int i = 0; i < buildingInstanceNodeList->getLength(); i++)
				{
					BuildingInstance * buildingInstance = new BuildingInstance();
					const DOMElement* p_buildingInstanceElement = (const DOMElement*)buildingInstanceNodeList->item(i);
					buildingInstance->deserialize(p_buildingInstanceElement);

					this->addBuildingInstance(buildingInstance);
				}
			}

		}
		catch (const XMLException& toCatch) {
			char* message = XMLString::transcode(toCatch.getMessage());
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		catch (const DOMException& toCatch) {
			char* message = XMLString::transcode(toCatch.msg);
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		//catch (std::exception& ex) {
		//	//*out << "!!Unexpected Exception: " << ex.what() << endl;
		//	*out << "!!Unexpected Exception: " << endl;
		//	return;
		//}
		catch (...) {
			DEBUG_OUT("Unexpected Exception");
			return;
		}

		delete parser;
		delete errHandler;
	}
	catch (XMLException& e)
	{
		char* message = XMLString::transcode(e.getMessage());
		DEBUG_OUT("XML toolkit initialization error: %s", message);
		//cerr << "XML toolkit initialization error: " << message << endl;
		XMLString::release(&message);
		// throw exception here to return ERROR_XERCES_INIT
	}
}
#include "TasinmazData.h"

#include "utilities/utilities.h"

#include <QString>

#define XERCES_STATIC_LIBRARY
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

using namespace xercesc;
using namespace std;

CityTownNeighborhoodData * CityTownNeighborhoodData::_singleton = nullptr;

CityTownNeighborhoodData::CityTownNeighborhoodData(string cityXmlFile, string townsXmlFile, string neighborhoodsXmlFile)
{
	ReadCityXml(cityXmlFile, cityVector);
	ReadTownXml(townsXmlFile, townMap);
	ReadNeighborhoodXml(neighborhoodsXmlFile, neighborhoodMap);
}

CityTownNeighborhoodData::~CityTownNeighborhoodData()
{

}

CityTownNeighborhoodData * CityTownNeighborhoodData::Instance()
{
	if (_singleton == nullptr)
	{
		//string cityXmlFile = "C:\\Users\\Selim\\source\\repos\\CreateXMLFromDatabase\\CreateXMLFromDatabase\\bin\\Debug\\cities.xml";
		//string townsXmlFile = "C:\\Users\\Selim\\source\\repos\\CreateXMLFromDatabase\\CreateXMLFromDatabase\\bin\\Debug\\towns.xml";
		//string neighborhoodsXmlFile = "C:\\Users\\Selim\\source\\repos\\CreateXMLFromDatabase\\CreateXMLFromDatabase\\bin\\Debug\\neighborhoods.xml";
		string cityXmlFile = "cities.xml";
		string townsXmlFile = "towns.xml";
		string neighborhoodsXmlFile = "neighborhoods.xml";
		_singleton = new CityTownNeighborhoodData(cityXmlFile, townsXmlFile, neighborhoodsXmlFile);
	}
	return _singleton;
}

std::vector<CityRecord> CityTownNeighborhoodData::cities()
{
	return cityVector;
}

std::map<int, std::vector<TownRecord>> CityTownNeighborhoodData::towns()
{
	return townMap;
}

std::map<int, std::vector<NeighborhoodRecord>> CityTownNeighborhoodData::neighborhoods()
{
	return neighborhoodMap;
}

std::map<int, CityRecord> &CityTownNeighborhoodData::cityIdMapping()
{
	return cityIdMap;
}

std::map<int, TownRecord> &CityTownNeighborhoodData::townIdMapping()
{
	return townIdMap;
}

std::map<int, NeighborhoodRecord> &CityTownNeighborhoodData::neighborhoodIdMapping()
{
	return neighborhoodIdMap;
}

void CityTownNeighborhoodData::ReadCityXml(string xmlFile, vector<CityRecord> &cityVector)
{
	try
	{
		XercesDOMParser* parser = new XercesDOMParser();
		parser->setValidationScheme(XercesDOMParser::Val_Always);
		parser->setDoNamespaces(true);    // optional

		ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
		parser->setErrorHandler(errHandler);

		try {
			//*out << "!!!!! Trying to Parse File" << endl << xmlFile << endl;
			parser->parse(xmlFile.c_str());
			//*out << "!!!!! File Parsed" << endl;

			xercesc::DOMDocument* doc = parser->getDocument();
			DOMElement* rootElement = doc->getDocumentElement(); // Getting root
																 //*out << "***************" << (rootElement->getNodeType() == DOMNode::ELEMENT_NODE) << endl;
			char *localElementName = XMLString::transcode(rootElement->getNodeName());

			//DOMNodeIterator * walker = doc->createNodeIterator(rootElement, DOMNodeFilter::SHOW_ELEMENT, nullptr, true);

			DOMNodeList * recordElementList = rootElement->getElementsByTagName(u"record");
			XMLSize_t recordCount = recordElementList->getLength();

			for (XMLSize_t i = 0; i < recordCount; i++)
			{
				DOMElement* recordElement = (DOMElement*)recordElementList->item(i);
				//char *recordContent = XMLString::transcode(recordElement->getTextContent());
				//DEBUG_OUT("recordContent = %s", recordContent);

				//const XMLCh * cityIdXmlCh = recordElement->getAttribute(u"cityId");
				char *cityId = XMLString::transcode(recordElement->getAttribute(u"cityId"));
				//char *cityName = XMLString::transcode(recordElement->getAttribute(u"cityName"));

				const XMLCh * cityNameUtf16 = recordElement->getAttribute(u"cityName");  // XMLCh = char16_t

				CityRecord cr;
				cr.id = stoi(cityId);
				cr.name = QString::fromUtf16(cityNameUtf16);

				cityVector.push_back(cr);
				cityIdMap.insert(pair<int, CityRecord>(cr.id, cr));

				//DEBUG_OUT("cityId = %d ; %s", cr.id, cr.name.c_str());
			}
		}
		catch (const XMLException& toCatch) {
			char* message = XMLString::transcode(toCatch.getMessage());
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		catch (const DOMException& toCatch) {
			char* message = XMLString::transcode(toCatch.msg);
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		//catch (std::exception& ex) {
		//	//*out << "!!Unexpected Exception: " << ex.what() << endl;
		//	*out << "!!Unexpected Exception: " << endl;
		//	return;
		//}
		catch (...) {
			DEBUG_OUT("Unexpected Exception");
			return;
		}

		delete parser;
		delete errHandler;
	}
	catch (XMLException& e)
	{
		char* message = XMLString::transcode(e.getMessage());
		DEBUG_OUT("XML toolkit initialization error: %s", message);
		//cerr << "XML toolkit initialization error: " << message << endl;
		XMLString::release(&message);
		// throw exception here to return ERROR_XERCES_INIT
	}
}

void CityTownNeighborhoodData::ReadTownXml(string xmlFile, map<int, vector<TownRecord>> &townMap)
{
	try
	{
		XercesDOMParser* parser = new XercesDOMParser();
		parser->setValidationScheme(XercesDOMParser::Val_Always);
		parser->setDoNamespaces(true);    // optional

		ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
		parser->setErrorHandler(errHandler);

		try {
			//*out << "!!!!! Trying to Parse File" << endl << xmlFile << endl;
			parser->parse(xmlFile.c_str());
			//*out << "!!!!! File Parsed" << endl;

			xercesc::DOMDocument* doc = parser->getDocument();
			DOMElement* rootElement = doc->getDocumentElement(); // Getting root
																 //*out << "***************" << (rootElement->getNodeType() == DOMNode::ELEMENT_NODE) << endl;
			char *localElementName = XMLString::transcode(rootElement->getNodeName());

			//DOMNodeIterator * walker = doc->createNodeIterator(rootElement, DOMNodeFilter::SHOW_ELEMENT, nullptr, true);

			DOMNodeList * recordElementList = rootElement->getElementsByTagName(u"record");
			XMLSize_t recordCount = recordElementList->getLength();

			for (XMLSize_t i = 0; i < recordCount; i++)
			{
				DOMElement* recordElement = (DOMElement*)recordElementList->item(i);
				//char *recordContent = XMLString::transcode(recordElement->getTextContent());
				//DEBUG_OUT("recordContent = %s", recordContent);

				//const XMLCh * cityIdXmlCh = recordElement->getAttribute(u"cityId");
				char *cityId = XMLString::transcode(recordElement->getAttribute(u"cityId"));
				char *townId = XMLString::transcode(recordElement->getAttribute(u"townId"));
				//char *townName = XMLString::transcode(recordElement->getAttribute(u"townName"));

				const XMLCh * townNameUtf16 = recordElement->getAttribute(u"townName");  // XMLCh = char16_t

				TownRecord tr;
				tr.cityId = stoi(cityId);
				tr.id = stoi(townId);
				//tr.name = townName;
				tr.name = QString::fromUtf16(townNameUtf16);

				townIdMap.insert(pair<int, TownRecord>(tr.id, tr));

				map<int, vector<TownRecord>>::iterator mapEntryPosition = townMap.find(tr.cityId);
				if (mapEntryPosition != townMap.end())
				{
					townMap[tr.cityId].push_back(tr);
				}
				else
				{
					vector<TownRecord> townsOfACityVector;
					townsOfACityVector.push_back(tr);
					townMap.insert(pair<int, vector<TownRecord>>(tr.cityId, townsOfACityVector));
				}

				//DEBUG_OUT("%d ; %d ; %s", tr.cityId, tr.id, tr.name.c_str());
			}
		}
		catch (const XMLException& toCatch) {
			char* message = XMLString::transcode(toCatch.getMessage());
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		catch (const DOMException& toCatch) {
			char* message = XMLString::transcode(toCatch.msg);
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		//catch (std::exception& ex) {
		//	//*out << "!!Unexpected Exception: " << ex.what() << endl;
		//	*out << "!!Unexpected Exception: " << endl;
		//	return;
		//}
		catch (...) {
			DEBUG_OUT("Unexpected Exception");
			return;
		}

		delete parser;
		delete errHandler;
	}
	catch (XMLException& e)
	{
		char* message = XMLString::transcode(e.getMessage());
		DEBUG_OUT("XML toolkit initialization error: %s", message);
		//cerr << "XML toolkit initialization error: " << message << endl;
		XMLString::release(&message);
		// throw exception here to return ERROR_XERCES_INIT
	}
}

void CityTownNeighborhoodData::ReadNeighborhoodXml(string xmlFile, map<int, vector<NeighborhoodRecord>> &neighborhoodMap)
{
	try
	{
		XercesDOMParser* parser = new XercesDOMParser();
		parser->setValidationScheme(XercesDOMParser::Val_Always);
		parser->setDoNamespaces(true);    // optional

		ErrorHandler* errHandler = (ErrorHandler*) new HandlerBase();
		parser->setErrorHandler(errHandler);

		try {
			//*out << "!!!!! Trying to Parse File" << endl << xmlFile << endl;
			parser->parse(xmlFile.c_str());
			//*out << "!!!!! File Parsed" << endl;

			xercesc::DOMDocument* doc = parser->getDocument();
			DOMElement* rootElement = doc->getDocumentElement(); // Getting root
																 //*out << "***************" << (rootElement->getNodeType() == DOMNode::ELEMENT_NODE) << endl;
			char *localElementName = XMLString::transcode(rootElement->getNodeName());

			//DOMNodeIterator * walker = doc->createNodeIterator(rootElement, DOMNodeFilter::SHOW_ELEMENT, nullptr, true);

			DOMNodeList * recordElementList = rootElement->getElementsByTagName(u"record");
			XMLSize_t recordCount = recordElementList->getLength();

			for (XMLSize_t i = 0; i < recordCount; i++)
			{
				DOMElement* recordElement = (DOMElement*)recordElementList->item(i);
				//char *recordContent = XMLString::transcode(recordElement->getTextContent());
				//DEBUG_OUT("recordContent = %s", recordContent);

				//const XMLCh * cityIdXmlCh = recordElement->getAttribute(u"cityId");
				char *townId = XMLString::transcode(recordElement->getAttribute(u"townId"));
				char *neighborhoodId = XMLString::transcode(recordElement->getAttribute(u"neighborhoodId"));
				//char *neighborhoodName = XMLString::transcode(recordElement->getAttribute(u"neighborhoodName"));

				const XMLCh * neighborhoodNameUtf16 = recordElement->getAttribute(u"neighborhoodName");  // XMLCh = char16_t

				NeighborhoodRecord nr;
				nr.townId = stoi(townId);
				nr.id = stoi(neighborhoodId);
				//nr.name = neighborhoodName;
				nr.name = QString::fromUtf16(neighborhoodNameUtf16);

				neighborhoodIdMap.insert(pair<int, NeighborhoodRecord>(nr.id, nr));

				map<int, vector<NeighborhoodRecord>>::iterator mapEntryPosition = neighborhoodMap.find(nr.townId);
				if (mapEntryPosition != neighborhoodMap.end())
				{
					neighborhoodMap[nr.townId].push_back(nr);
				}
				else
				{
					vector<NeighborhoodRecord> neighborhoodsOfATownVector;
					neighborhoodsOfATownVector.push_back(nr);
					neighborhoodMap.insert(pair<int, vector<NeighborhoodRecord>>(nr.townId, neighborhoodsOfATownVector));
				}

				//DEBUG_OUT("%d ; %d ; %s", tr.cityId, tr.id, tr.name.c_str());
			}
		}
		catch (const XMLException& toCatch) {
			char* message = XMLString::transcode(toCatch.getMessage());
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		catch (const DOMException& toCatch) {
			char* message = XMLString::transcode(toCatch.msg);
			DEBUG_OUT("Exception message is: %s", message);
			XMLString::release(&message);
			return;
		}
		//catch (std::exception& ex) {
		//	//*out << "!!Unexpected Exception: " << ex.what() << endl;
		//	*out << "!!Unexpected Exception: " << endl;
		//	return;
		//}
		catch (...) {
			DEBUG_OUT("Unexpected Exception");
			return;
		}

		delete parser;
		delete errHandler;
	}
	catch (XMLException& e)
	{
		char* message = XMLString::transcode(e.getMessage());
		DEBUG_OUT("XML toolkit initialization error: %s", message);
		//cerr << "XML toolkit initialization error: " << message << endl;
		XMLString::release(&message);
		// throw exception here to return ERROR_XERCES_INIT
	}
}

#include "ItemData.h"

#include "FloorDesignDocument.h"

#include <map>

using namespace std;

ItemData::ItemData(string &uuid) : m_uuid(uuid), m_ownerDocument(nullptr)
{
}

ItemData::ItemData(FloorDesignDocument * ownerDoc) : m_uuid("NONE"), m_ownerDocument(ownerDoc)
{
}

ItemData::ItemData(std::string &uuid, FloorDesignDocument * ownerDoc) : m_uuid(uuid), m_ownerDocument(ownerDoc)
{
}

ItemData::ItemData() : m_uuid("NONE"), m_ownerDocument(nullptr)
{
}

ItemData::~ItemData()
{
}

std::string ItemData::uuid() const
{
	return m_uuid;
}

void ItemData::setUuid(std::string uuid)
{
	m_uuid = uuid;
}

FloorDesignDocument * ItemData::ownerDocument() const
{
	return m_ownerDocument;
}

void ItemData::setOwnerDocument(FloorDesignDocument * ownerDocument)
{
	m_ownerDocument = ownerDocument;
}

void ItemData::serialize(std::ofstream &ofs)
{
	ofs << uuid();
}

void ItemData::deserialize(std::ifstream &ifs)
{
	char * uuidCharPtr = new char[37];
	ifs.read((char*)uuidCharPtr, 36);
	uuidCharPtr[36] = '\0';
	this->m_uuid = uuidCharPtr;
	delete uuidCharPtr;
}

#ifndef FLOOR_DESIGN_DOCUMENT_H
#define FLOOR_DESIGN_DOCUMENT_H

#include <fstream>
#include <qglobal.h>

#include "../utilities/forward_std_vecor_map.h"

class WallData;
class ControllerData;
class WallSegmentItem;
class FloorplanGraphicsScene;

namespace boost::uuids
{
	//class random_generator;

	template <typename UniformRandomNumberGenerator>
	class basic_random_generator;
}

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>

class FloorDesignDocument
{
public:
	FloorDesignDocument();
	~FloorDesignDocument();

	void initialize();
	void finalize();

	void addWall(WallData *, bool doGenerateUuid = true);
	void removeWall(WallData * wallData, bool isToDeleteWallDataInstance = true);
	void removeWall(WallData * wallData, bool isToRemoveControlDatas, bool isToDeleteWallDataInstance);
	void removeWall(std::string &itemUuidStr);
	int wallCount();
	WallData * wallAt(int index);

	ControllerData * retrieveMergableControlPoint(ControllerData *);
	WallData * retrieveAttachableWall(ControllerData *);
	bool mergeControlPoint(ControllerData * food, ControllerData *swallower);

	void addController(ControllerData *, bool doGenerateUuid = true, bool doMerge = true);
	void removeController(ControllerData *);
	void removeController(std::string &itemUuidStr);
	int controllerCount();
	ControllerData * controllerAt(int index);

	void serialize(std::ofstream &ofs);
	void deserialize(std::ifstream &ifs);

	ControllerData * getControllerDataViaUuid(std::string);

	bool createWallJunctionsFromIntersection(FloorplanGraphicsScene * scene, WallData* wallData);
	bool createWallJunctionsFromIntersection(FloorplanGraphicsScene * scene, std::vector<WallData*> &controlledWallList);

	void centerControlls();

protected:

private:
	const static qreal MergeWallCornerDistanceLimit;

	std::vector<WallData*> * wallList;
	std::map<std::string, WallData*> * wallMap;

	std::vector<ControllerData*> * controllerList;
	std::map<std::string, ControllerData*> * controllerMap;

	boost::uuids::random_generator generator;

	void addWall(std::string &itemUuidStr, WallData *);
	void addController(std::string &itemUuidStr, ControllerData *);

	void serializeControllerList(std::ofstream &ofs);
	void deserializeControllerList(std::ifstream &ifs);

	void serializeWallList(std::ofstream &ofs);
	void deserializeWallList(std::ifstream &ifs);
};

#endif // FLOOR_DESIGN_DOCUMENT_H


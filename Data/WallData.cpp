#include <map>

#include <QPointF>
#include <QPixmap>
#include <QAction>
#include <QMenu>
#include <QtMath>
#include <QCursor>
#include <QTimer>

#include "WallData.h"

#include "ControllerData.h"

#include "FloorDesignDocument.h"
#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "CustomWidgets/FloorplanGraphicsView.h"

#include "utilities/utilities.h"

#include "../FloorDesignItems/WallSegmentItem.h"
#include "ControlPointGraphicsItem.h"

#include "FloorPlanDesigner.h"

#include "ActionData.h"

using namespace std;

const double WallData::CurvedToLineLimit = 0.02;

WallData::WallData(string &uuid) : ItemData(uuid)
{
	initialize();
}

WallData::WallData(FloorDesignDocument * ownerDoc) : ItemData(ownerDoc)
{
	initialize();
}

WallData::WallData(string &uuid, FloorDesignDocument * ownerDoc) : ItemData(uuid, ownerDoc)
{
	initialize();
}

WallData::WallData(WallData *srcData)
{
	setUuid(srcData->uuid());
	m_type = srcData->m_type;
	m_width = srcData->m_width;
	m_height = srcData->m_height;
	m_controlPosition = srcData->m_controlPosition;
	m_relatedGraphicsItem = srcData->m_relatedGraphicsItem;
}


WallData::WallData()
{
	initialize();
}

void WallData::initialize()
{
	m_type = WallType::Line;
	m_width = WallSegmentItem::DefaultWallWidth;
	m_height = WallSegmentItem::DefaultWallHeight;
	m_controlPosition = QPointF(0, 0);
	m_relatedGraphicsItem = nullptr;
}

WallData::~WallData()
{
}


WallType WallData::type() const
{
	return m_type;
}

void WallData::setType(WallType wallType_)
{
	m_type = wallType_;
}

qreal WallData::width() const
{
	return m_width;
}

void WallData::setWidth(qreal val)
{
	m_width = val;
}

qreal WallData::height() const
{
	return m_height;
}

void WallData::setHeight(qreal val)
{
	m_height = val;
}

QPointF WallData::startPosition() const
{
	return m_startControllerData->position();
}

void WallData::setStartPosition(QPointF val)
{
	m_startControllerData->setPosition(val, false, nullptr);
}

QPointF WallData::endPosition() const
{
	return m_endControllerData->position();
}

void WallData::setEndPosition(QPointF val)
{
	m_endControllerData->setPosition(val, false, nullptr);
}

QPointF WallData::controlPosition() const
{
	return m_controlPosition;
}

void WallData::setControlPosition(QPointF val)
{
	m_controlPosition = val;
}

void WallData::setData(QPointF startPoint, QPointF endPoint,
	QPointF mainControlPoint, qreal wallWidth, qreal wallHeight, WallType wallType)
{
	m_startControllerData->setPosition(startPoint, false, nullptr);
	m_endControllerData->setPosition(endPoint, false, nullptr);
	m_controlPosition = mainControlPoint;
	m_width = wallWidth;
	m_height = wallHeight;
	m_type = wallType;
}

void WallData::updateRelatedGraphicsItemPolygon() const
{
	m_relatedGraphicsItem->CreatePolygon();
}

WallSegmentItem * WallData::relatedGraphicsItem() const
{
	return m_relatedGraphicsItem;
}

void WallData::setRelatedGraphicsItem(WallSegmentItem * val)
{
	m_relatedGraphicsItem = val;
}

void WallData::retrieveOtherParallelWallAndExcludeFromList(std::vector<ControllerMappingData*> &controlledList, WallData * &parallelWall)
{
	parallelWall = nullptr;
	ControllerMappingData * controllerMappingDataForParallelWall = nullptr;

	this->updateNormal();
	QPointF selfNormal = this->normal();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		if (wallData == this)
		{
			continue;
		}
		//parallelWall
		wallData->updateNormal();
		QPointF otherNormal = wallData->normal();
		qreal dotproduct = DotProduct(selfNormal, otherNormal);
		if (1 - dotproduct < _DOT_PRODUCT_EPSILON_)
		{
			wallData->updateNormal();
			parallelWall = wallData;
			controllerMappingDataForParallelWall = cmd;
			break;
		}
	}
	// it there is a parallel wall then exclude it
	if (controllerMappingDataForParallelWall != nullptr)
	{
		vector<ControllerMappingData *>::iterator vectorEntryPosition = std::find(
			controlledList.begin(), controlledList.end(), controllerMappingDataForParallelWall);
		if (vectorEntryPosition != controlledList.end())
		{
			controlledList.erase(vectorEntryPosition);
		}
	}
}

void WallData::retrieveOtherTwoWall(vector<ControllerMappingData*> &controlledList,
	WallData * &otherWallData1, WallData * &otherWallData2)
{
	otherWallData1 = nullptr;
	otherWallData2 = nullptr;

	int index = 0;
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		if (wallData == this)
		{
			continue;
		}

		//(index % 2 == 0) ? otherWallData1 : otherWallData2 = cmd->controlledWall;
		if (index == 0)
		{
			otherWallData1 = wallData;
		}
		else if (index == 1)
		{
			otherWallData2 = wallData;
		}
		index++;
		// only check first two wal other then self / this
		if (index > 1)
		{
			break;
		}
	}
}

vector<ControllerAndOneWallPair*> WallData::RetrieveStraightWallListAlongOneSide(ControllerData * firstController, WallData * oneSidesFirstWall)
{
	vector<ControllerMappingData*> controlledList = firstController->controlledList();

	vector<ControllerAndOneWallPair*> straightWallList;

	WallData * currentWall = oneSidesFirstWall;
	ControllerData * previousControllerData = firstController;
	ControllerData * nextControllerData = (currentWall->startControllerData() != previousControllerData) ?
		currentWall->startControllerData() : currentWall->endControllerData();
	QPointF initialSideWallVector = NormalizeQPointF(nextControllerData->position() - previousControllerData->position());

	ControllerAndOneWallPair * caowp = new ControllerAndOneWallPair();
	caowp->controllerData = nextControllerData;
	caowp->wallData = currentWall;
	straightWallList.push_back(caowp);

	while (nextControllerData != nullptr)
	{
		previousControllerData = nextControllerData;
		vector<ControllerMappingData*> controlledList = previousControllerData->controlledList();
		nextControllerData = nullptr;
		for (ControllerMappingData * cmd : controlledList)
		{
			WallData * wallData = cmd->controlledWall;
			if (wallData == currentWall)
			{
				continue;
			}
			ControllerData * candidateWallOtherControllerData = (wallData->startControllerData() != previousControllerData) ?
				wallData->startControllerData() : wallData->endControllerData();

			QPointF nextSideWallCandidateVector = NormalizeQPointF(candidateWallOtherControllerData->position() - previousControllerData->position());

			qreal dotproduct = DotProduct(initialSideWallVector, nextSideWallCandidateVector);
			if (1 - dotproduct < _DOT_PRODUCT_EPSILON_)
			{
				nextControllerData = candidateWallOtherControllerData;
				currentWall = wallData;

				caowp = new ControllerAndOneWallPair();
				caowp->controllerData = nextControllerData;
				caowp->wallData = wallData;
				straightWallList.push_back(caowp);
				break;
			}
		}

		if (currentWall != nullptr)
		{

		}
		ControllerData * previousControllerData = firstController;

	}
	return straightWallList;
}

void WallData::UpdateMergedPolygon()
{
	WallSegmentItem * wallItem = this->relatedGraphicsItem();
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene *)(wallItem->scene());
	//MergeWalls::ModifyWallSegmentPolygonsDueToWallMove(this, scene);
	MergeWallPolygons::ModifyCurvedWallSegmentPolygonsDueToWallMove(this, scene);
	updatePolygonsForSelfAndOtherSideControllers();
	//this->relatedGraphicsItem()->updatePolygonsForSelfAndOtherSideControllers();
}

void WallData::updatePolygonsForSelfAndOtherSideControllers()
{
	vector<WallData*> uniqueWallList;

	ControllerData * controllerData = this->m_startControllerData;
	vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), wallData) == uniqueWallList.end();
		if (doesNotContainWall)
		{
			uniqueWallList.push_back(wallData);
		}
		ControllerData * otherController = wallData->getOtherController(controllerData);
		vector<ControllerMappingData*> otherControlledList = otherController->controlledList();
		for (ControllerMappingData * otherCmd : otherControlledList)
		{
			if (wallData != otherCmd->controlledWall)
			{
				WallData * otherWallData = otherCmd->controlledWall;
				bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), otherWallData) == uniqueWallList.end();
				if (doesNotContainWall)
				{
					uniqueWallList.push_back(otherWallData);
				}
			}
		}
	}

	controllerData = this->m_endControllerData;
	controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), wallData) == uniqueWallList.end();
		if (doesNotContainWall)
		{
			uniqueWallList.push_back(wallData);
		}
		ControllerData * otherController = wallData->getOtherController(controllerData);
		vector<ControllerMappingData*> otherControlledList = otherController->controlledList();
		for (ControllerMappingData * otherCmd : otherControlledList)
		{
			if (wallData != otherCmd->controlledWall)
			{
				WallData * otherWallData = otherCmd->controlledWall;
				bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), otherWallData) == uniqueWallList.end();
				if (doesNotContainWall)
				{
					uniqueWallList.push_back(otherWallData);
				}
			}
		}
	}

	for (WallData * wallData : uniqueWallList)
	{
		wallData->updateRelatedGraphicsItemPolygon();
	}

	//this->m_startControllerData->updatePolygonsForSelfAndOtherSideControllers();
	//this->m_endControllerData->updatePolygonsForSelfAndOtherSideControllers();
}

bool WallData::isEligibleForMove()
{
	WallData * startParralelWall1 = nullptr;
	WallData * startParralelWall2 = nullptr;
	WallData * endParralelWall1 = nullptr;
	WallData * endParralelWall2 = nullptr;
	WallData * selfStartParralelWall = nullptr;
	WallData * selfEndParralelWall = nullptr;
	SpecialWallMoveDragCase startSpecialWallMoveDragCase;
	SpecialWallMoveDragCase endSpecialWallMoveDragCase;
	return isEligibleForMove(startParralelWall1, startParralelWall2, endParralelWall1,
		endParralelWall2, selfStartParralelWall, selfEndParralelWall, startSpecialWallMoveDragCase, endSpecialWallMoveDragCase);
}

bool WallData::isEligibleForMove(WallData * &startParralelWall1, WallData * &startParralelWall2, WallData * &endParralelWall1,
	WallData * &endParralelWall2, WallData * &selfStartParralelWall, WallData * &selfEndParralelWall,
	SpecialWallMoveDragCase &startSpecialWallMoveDragCase, SpecialWallMoveDragCase &endSpecialWallMoveDragCase)
{
	startSpecialWallMoveDragCase = SpecialWallMoveDragCase::NoCase;
	endSpecialWallMoveDragCase = SpecialWallMoveDragCase::NoCase;

	vector<ControllerMappingData*> startControlledList = m_startControllerData->controlledList();
	vector<ControllerMappingData*> endControlledList = m_endControllerData->controlledList();

	retrieveOtherParallelWallAndExcludeFromList(startControlledList, selfStartParralelWall);
	retrieveOtherParallelWallAndExcludeFromList(endControlledList, selfEndParralelWall);

	if (startControlledList.size() > 3 || endControlledList.size() > 3)
	{
		return false;
	}

	retrieveOtherTwoWall(startControlledList, startParralelWall1, startParralelWall2);
	if (startControlledList.size() == 3)
	{
		//retrieveOtherTwoWall(startControlledList, otherWallData1, otherWallData2);
		//DEBUG_OUT("END:: otherWallData1.uuid = %s ; otherWallData2.uuid = %s", startParralelWall1->uuid().c_str(), startParralelWall2->uuid().c_str());
		startParralelWall1->updateNormal();
		startParralelWall2->updateNormal();
		qreal absoluteDotProduct = qAbs(DotProduct(startParralelWall1->normal(), startParralelWall2->normal()));
		//if (1 - absoluteDotProduct > 0.2)
		if (1 - absoluteDotProduct > _DOT_PRODUCT_EPSILON_)
		{
			return false;
		}
		startSpecialWallMoveDragCase = (m_startControllerData->controledItemCount() == 4) ?
			SpecialWallMoveDragCase::CaseWith4Links : SpecialWallMoveDragCase::CaseWith3Links;
	}

	retrieveOtherTwoWall(endControlledList, endParralelWall1, endParralelWall2);
	if (endControlledList.size() == 3)
	{
		//retrieveOtherTwoWall(endControlledList, otherWallData1, otherWallData2);
		//DEBUG_OUT("END:: otherWallData1.uuid = %s ; otherWallData2.uuid = %s", endParralelWall1->uuid().c_str(), endParralelWall2->uuid().c_str());
		endParralelWall1->updateNormal();
		endParralelWall2->updateNormal();
		qreal absoluteDotProduct = qAbs(DotProduct(endParralelWall1->normal(), endParralelWall2->normal()));
		//if (1 - absoluteDotProduct > 0.2)
		if (1 - absoluteDotProduct > _DOT_PRODUCT_EPSILON_)
		{
			startSpecialWallMoveDragCase = SpecialWallMoveDragCase::NoCase;
			return false;
		}
		endSpecialWallMoveDragCase = (m_endControllerData->controledItemCount() == 4) ?
			SpecialWallMoveDragCase::CaseWith4Links : SpecialWallMoveDragCase::CaseWith3Links;
	}

	return true;
}

void WallData::move(QPointF vector)
{
	move(vector, true);
}

void WallData::move(QPointF pvector, bool isWalldataToBeUpdated)
{
	QPointF startPos = m_startControllerData->position() + pvector;
	QPointF endPos = m_endControllerData->position() + pvector;
	setControlPosition(m_controlPosition + pvector);


	vector<ControllerMappingData*> startControlledList = m_startControllerData->controlledList();
	vector<ControllerMappingData*> endControlledList = m_endControllerData->controlledList();

	WallData * otherWallData1 = nullptr;
	WallData * otherWallData2 = nullptr;
	WallData * startParallelWall = nullptr;
	WallData * endParallelWall = nullptr;
	retrieveOtherParallelWallAndExcludeFromList(startControlledList, startParallelWall);
	retrieveOtherParallelWallAndExcludeFromList(endControlledList, endParallelWall);


	retrieveOtherTwoWall(startControlledList, otherWallData1, otherWallData2);
	QPointF intersectionPoint;
	if (startControlledList.size() == 1)
	{
		m_startControllerData->setPosition(startPos, isWalldataToBeUpdated, this);
	}
	else if (startControlledList.size() == 2 || startControlledList.size() == 3)
	{
		//retrieveOtherTwoWall(startControlledList, otherWallData1, otherWallData2);
		QPointF otherStartPos = otherWallData1->startPosition();
		QPointF otherEndPos = otherWallData1->endPosition();
		IntersectionOfTwoLineSegment(startPos, endPos, otherStartPos, otherEndPos, intersectionPoint);
		m_startControllerData->setPosition(intersectionPoint, isWalldataToBeUpdated, this);
		this->relatedGraphicsItem()->CreatePolygon();
	}

	retrieveOtherTwoWall(endControlledList, otherWallData1, otherWallData2);
	if (endControlledList.size() == 1)
	{
		m_endControllerData->setPosition(endPos, isWalldataToBeUpdated, this);
	}
	else if (endControlledList.size() == 2 || endControlledList.size() == 3)
	{
		//retrieveOtherTwoWall(endControlledList, otherWallData1, otherWallData2);
		QPointF otherStartPos = otherWallData1->startPosition();
		QPointF otherEndPos = otherWallData1->endPosition();
		IntersectionOfTwoLineSegment(startPos, endPos, otherStartPos, otherEndPos, intersectionPoint);
		m_endControllerData->setPosition(intersectionPoint, isWalldataToBeUpdated, this);
		this->relatedGraphicsItem()->CreatePolygon();
	}

	//m_startControllerData->setPosition(m_startControllerData->position() + vector, isWalldataToBeUpdated, this);
	//m_endControllerData->setPosition(m_endControllerData->position() + vector, isWalldataToBeUpdated, this);
}

ControllerData * WallData::startControllerData()
{
	return m_startControllerData;
}

void WallData::setStartControllerData(ControllerData * val)
{
	m_startControllerData = val;
}

ControllerData * WallData::endControllerData()
{
	return m_endControllerData;
}

void WallData::setEndControllerData(ControllerData * val)
{
	m_endControllerData = val;
}



void WallData::cancelBend()
{
	setControlPosition(m_controlPositionBeforeBendStart);
	setType(m_typeBeforeBendStart);
	relatedGraphicsItem()->CreatePolygon();
}

void WallData::cancelModifyWidth()
{
	setWidth(m_widthBeforeModification);
	this->UpdateMergedPolygon();
}

void WallData::bending(QPointF controlPoint)
{
	setControlPosition(controlPoint);
	this->determineType();
	this->UpdateMergedPolygon();
}

void WallData::modifyingWidth(QPointF mouseScenePos)
{
	qreal calculatedWidth = calculateWidth(mouseScenePos);
	setWidth(calculatedWidth);
	this->UpdateMergedPolygon();
	DEBUG_OUT("warning: ---------------- modifyingWidth = %f", calculatedWidth);
}

bool WallData::checkPointProximity(QPointF point, QPointF &projectedPoint, qreal &distanceToProjectedPoint)
{
	QPointF P1 = this->startPosition();
	QPointF P2 = this->endPosition();
	return ClosestDistanceToLine(point, P1, P2, projectedPoint, distanceToProjectedPoint);
}

qreal WallData::calculateWidth(QPointF mouseScenePos)
{
	//m_axesNormal
	//m_axesCenter
	//m_mouseInteractionInitPos

	QPointF widthDeciderVector = (mouseScenePos - m_mouseInteractionInitPos) * 1;
	QPointF mcptlcNormalized = NormalizeQPointF(widthDeciderVector);
	QPointF projectionOnAxesNormal = ProjectBOnA(m_axesNormal, widthDeciderVector);
	qreal projectionOnAxesNormalLength = QPointFLength(projectionOnAxesNormal);
	qreal sign = Sign(DotProduct(projectionOnAxesNormal, m_axesNormal));

	qreal newWidth = m_widthBeforeModification + (projectionOnAxesNormalLength * sign);
	newWidth = (newWidth < 0) ? 0 : newWidth;
	return newWidth;
}

qreal WallData::length() const
{
	return QPointFLength(this->m_startControllerData->position() - this->m_endControllerData->position());
}

QPointF WallData::calculateAxisNormal(QPointF &center)
{
	QPointF startP = this->startPosition();
	QPointF endP = this->endPosition();

	QPointF startToEnd = endP - startP;
	center = (startP + endP) * 0.5f;
	return NormalizeQPointF(PerpendicularVector(startToEnd));
}

void WallData::determineType()
{
	QPointF startP = this->startPosition();
	QPointF endP = this->endPosition();
	QPointF mainControlPoint = this->controlPosition();

	QPointF startToEnd = endP - startP;
	QPointF bezierAxesNormal = NormalizeQPointF(PerpendicularVector(startToEnd));

	qreal startToEndLength = QPointFLength(startToEnd);
	QPointF startToEndCenter = (startP + endP) * 0.5f;
	QPointF mcptlc = (startToEndCenter - mainControlPoint) * -1;
	QPointF mcptlcNormalized = NormalizeQPointF(mcptlc);
	QPointF projectionOnAxesNormal = ProjectBOnA(bezierAxesNormal, mcptlc);
	qreal projectionOnAxesNormalLength = QPointFLength(projectionOnAxesNormal);
	qreal controlPointDisplacementRatio = qAbs(projectionOnAxesNormalLength / startToEndLength);
	//DEBUG_OUT("(mcptlcLength: %f) / (startToEndLength: %f) = (controlPointDisplacementRatio: %f)", mcptlcLength, startToEndLength,
	//	controlPointDisplacementRatio);

	m_type = (controlPointDisplacementRatio < CurvedToLineLimit) ? Line : Curved;
	m_controlPosition = (m_type == Line) ? startToEndCenter : m_controlPosition;

	//DEBUG_OUT("m_type = %s", (m_type == WallType::Line) ? "LINE" : "CURVED");
}

ControllerData * WallData::splitFromPoint(QPointF point, bool isToAddedToCommandHistory)
{
	WallData * splitBornWallData;
	return SplitWallFromPoint(this, splitBornWallData, point, isToAddedToCommandHistory);
}

ControllerData * WallData::SplitWallFromPoint(WallData * wallToSplit, WallData * &splitBornWallData,
	QPointF point, bool isToAddedToCommandHistory)
{
	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());

	WallSegmentItem * existingWallSegmentItem = wallToSplit->relatedGraphicsItem();
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(existingWallSegmentItem->scene());
	FloorDesignDocument * document = wallToSplit->ownerDocument();

	QPointF projectedPoint;
	qreal distanceToProjectedPoint;
	QPointF P1 = wallToSplit->startPosition();
	QPointF P2 = wallToSplit->endPosition();
	bool isOnWallSegment = ClosestDistanceToLine(point, P1, P2, projectedPoint, distanceToProjectedPoint);
	if (isOnWallSegment)
	{
		// first create new WallData born from split action
		splitBornWallData = new WallData();
		floorplanDesigner->connectWallRemoveSocket(splitBornWallData);
		document->addWall(splitBornWallData);
		splitBornWallData->setType(WallType::Line);
		splitBornWallData->setWidth(wallToSplit->width());
		// existing walls old end controller will be split born wall new end controller
		// so add split born walldata to its ControllerMappingData list
		splitBornWallData->bindSelfToControlledItem(WallSegmentControlledPart::End, wallToSplit->m_endControllerData);
		// old end controller will be switched to splitborn controller data
		// so remove this wall from ControllerMappingData list of old end controller
		wallToSplit->m_endControllerData->removeControlledItem(wallToSplit);

		ControllerData * splitControllerData = new ControllerData();
		wallToSplit->bindSelfToControlledItem(WallSegmentControlledPart::End, splitControllerData);
		splitBornWallData->bindSelfToControlledItem(WallSegmentControlledPart::Start, splitControllerData);
		document->addController(splitControllerData, true, false);

		new WallSegmentItem(scene, true, splitBornWallData);

		// finally update existing and splitborn via assigning split position updating their polygon as well
		splitControllerData->setPosition(projectedPoint, true, nullptr);

		if (isToAddedToCommandHistory)
		{
			scene->onWallSplitted(wallToSplit, splitBornWallData, splitControllerData);
		}
		//scene->onWallSplitted(firstWD, secondWD, isToAddedToCommandHistory);

		return splitControllerData;
	}

	return nullptr;
}

void WallData::attachController(ControllerData * attachedController)
{
	attachController(attachedController, true);
}

void WallData::attachController(ControllerData * attachedController, bool doesAttachToBeSignaled)
{
	DEBUG_OUT("---WallData::attachController 1");
	DEBUG_OUT("---WallData::attachController 2");
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->relatedGraphicsItem()->scene());
	FloorDesignDocument * document = attachedController->ownerDocument();

	QPointF point = attachedController->position();
	ControllerData * mergableControlPoint = splitFromPoint(point, false);
	document->mergeControlPoint(attachedController, mergableControlPoint);

	if (doesAttachToBeSignaled)
	{
		scene->onAttachedToWall(mergableControlPoint, this);
	}
}

void WallData::removeSelf(bool doesRemoveToBeSignaled, bool doesControlsToBeRemoved)
{
	removeSelf(doesRemoveToBeSignaled, doesControlsToBeRemoved, true);
}

void WallData::removeSelf(bool doesRemoveToBeSignaled, bool doesControlsToBeRemoved, bool isGraphicsItemToBeRemoved)
{
	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());

	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene *)this->m_relatedGraphicsItem->scene();
	ControllerData * startController = this->m_startControllerData;
	ControllerData * endController = this->m_endControllerData;
	bool willStartStay = startController->controledItemCount() > 1;
	bool willEndStay = endController->controledItemCount() > 1;

	this->m_relatedGraphicsItem->setSelected(false, false, true);

	//delete this->m_relatedGraphicsItem;
	if (isGraphicsItemToBeRemoved)
	{
		//this->m_relatedGraphicsItem->scene()->removeItem(this->m_relatedGraphicsItem);
		delete this->m_relatedGraphicsItem;
	}

	this->ownerDocument()->removeWall(this, doesControlsToBeRemoved, false);

	// if controller still used in document, update its related walls
	if (willStartStay)
	{
		startController->UpdateMergedPolygon(scene);
	}

	// if controller still used in document, update its related walls
	if (willEndStay)
	{
		endController->UpdateMergedPolygon(scene);
	}

	if (doesRemoveToBeSignaled)
	{
		emit wallRemoved(this);
	}

	floorplanDesigner->disconnectWallRemoveSocket(this);

	delete this;
}

// menu action handlers BEGIN //
void WallData::removeFloorDesignItem()
{
	removeSelf(true, true);
}

void WallData::startBending()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->relatedGraphicsItem()->scene());

	QAction* pAction = qobject_cast<QAction*>(sender());
	QVariant variantActionData = pAction->data();
	ActionData* actionData = (ActionData*)variantActionData.value<void *>();
	//DEBUG_OUT("---FloorplanGraphicsScene:: (floorDesignItem != this) = %s", (floorDesignScene != this) ? "TRUE" : "NONE");
	QPointF point = actionData->scenePointerPos();
	//DEBUG_OUT("---FloorplanGraphicsScene::startBendWall point(%f, %f)", point.x(), point.y());

	FloorplanGraphicsView * view = actionData->view();
	QPoint controllerViewPosition = view->mapFromScene(this->m_controlPosition);

	QRect viewRect = view->geometry();
	QPoint viewRectPos(viewRect.x(), viewRect.y());
	QPoint globalRectPos = view->mapToGlobal(viewRectPos);
	viewRect = QRect(globalRectPos.x(), globalRectPos.y(), viewRect.width(), viewRect.height());
	QPoint controllerGlobalPosition = view->mapToGlobal(controllerViewPosition);
	if (viewRect.contains(controllerGlobalPosition))
	{
		QCursor::setPos(controllerGlobalPosition);
	}

	//QTimer::singleShot(20, [=] {
	//	view->setSceneRect(this->relatedGraphicsItem()->boundingRect());
	//	QTimer::singleShot(20, [=] {
	//		QPoint controllerViewPosition = view->mapFromScene(this->m_controlPosition);
	//		QPoint controllerGlobalPosition = view->mapToGlobal(controllerViewPosition);
	//		QCursor::setPos(controllerGlobalPosition);
	//	});
	//});


	m_controlPositionBeforeBendStart = m_controlPosition;
	m_typeBeforeBendStart = m_type;
	scene->startWallBending(this);
}

void WallData::startWidthModifying()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->relatedGraphicsItem()->scene());

	QAction* pAction = qobject_cast<QAction*>(sender());
	QVariant variantActionData = pAction->data();
	ActionData* actionData = (ActionData*)variantActionData.value<void *>();
	QPoint globalMousePosWhenPopupMenuShown = actionData->globalPointerPos();
	//DEBUG_OUT("---FloorplanGraphicsScene:: (floorDesignItem != this) = %s", (floorDesignScene != this) ? "TRUE" : "NONE");
	QPointF point = actionData->scenePointerPos();
	//DEBUG_OUT("---FloorplanGraphicsScene::startBendWall point(%f, %f)", point.x(), point.y());

	//QPoint * pp = (QPoint *)actionData->getData("Position");
	//if (pp != nullptr)
	//{
	//	DEBUG_OUT("---pp(%d, %d)", pp->x(), pp->y());
	//}
	QCursor::setPos(globalMousePosWhenPopupMenuShown);

	m_axesNormal = calculateAxisNormal(m_axesCenter);
	QPointF centerToInteractionInitPoint = point - m_axesCenter;
	qreal sign = Sign(DotProduct(m_axesNormal, centerToInteractionInitPoint));
	m_axesNormal *= sign;

	m_mouseInteractionInitPos = point;
	m_widthBeforeModification = m_width;
	scene->startWallWidthModifying(this);
}

void WallData::split()
{
	DEBUG_OUT("---WallData::split");

	QAction* pAction = qobject_cast<QAction*>(sender());
	QVariant variantActionData = pAction->data();
	ActionData* actionData = (ActionData*)variantActionData.value<void *>();
	QPointF scenePointerPos = actionData->scenePointerPos();
	QPoint globalMousePosWhenPopupMenuShown = actionData->globalPointerPos();
	QCursor::setPos(globalMousePosWhenPopupMenuShown);

	splitFromPoint(scenePointerPos, true);
}
// menu action handlers END //


void WallData::serialize(std::ofstream &ofs)
{
	ItemData::serialize(ofs);
	ofs << m_controlPosition;
	ofs << m_startControllerData->uuid();
	ofs << m_endControllerData->uuid();

	ofs.write((char*)&m_width, sizeof(qreal));
	ofs.write((char*)&m_height, sizeof(qreal));
	ofs.write((char*)&m_type, sizeof(WallType));
}

void WallData::deserialize(std::ifstream &ifs)
{
	ItemData::deserialize(ifs);
	ifs >> m_controlPosition;
	
	// read start and end control point uuids
	char * uuidCharPtr = new char[37];
	ifs.read((char*)uuidCharPtr, 36);
	uuidCharPtr[36] = '\0';
	string startControlPointUuid = uuidCharPtr;
	ifs.read((char*)uuidCharPtr, 36);
	uuidCharPtr[36] = '\0';
	string endControlPointUuid = uuidCharPtr;

	m_startControllerData = this->ownerDocument()->getControllerDataViaUuid(startControlPointUuid);
	bindSelfToControlledItem(WallSegmentControlledPart::Start, m_startControllerData);
	m_endControllerData = this->ownerDocument()->getControllerDataViaUuid(endControlPointUuid);
	bindSelfToControlledItem(WallSegmentControlledPart::End, m_endControllerData);

	ifs.read((char*)&m_width, sizeof(qreal));
	ifs.read((char*)&m_height, sizeof(qreal));
	ifs.read((char*)&m_type, sizeof(WallType));

	delete uuidCharPtr;
}

ControllerData * WallData::getSideController(WallSegmentControlledPart wscp)
{
	if (WallSegmentControlledPart::None)
	{
		return nullptr;
	}
	return (wscp == WallSegmentControlledPart::Start) ? this->m_startControllerData : this->m_endControllerData;
}

WallSegmentControlledPart WallData::detachOneControlledItemSide(ControllerData * controllerDataToDetach)
{
	ControllerData * newController = new ControllerData();
	this->ownerDocument()->addController(newController, true, false);
	WallSegmentControlledPart wscp = this->detachOneSideFromOneControlAndBindToControlledItem(controllerDataToDetach, newController);
	return wscp;
}

WallSegmentControlledPart WallData::detachOneSideFromOneControlAndBindToControlledItem(ControllerData * controllerDataToDetach, ControllerData * controllerDataToBind)
{
	WallSegmentControlledPart wscp = this->getControllerPartType(controllerDataToDetach);
	controllerDataToDetach->removeControlledItem(this);
	this->bindSelfToControlledItem(wscp, controllerDataToBind);
	this->relatedGraphicsItem()->CreatePolygon();
	return wscp;
}

void WallData::bindSelfToControlledItem(WallSegmentControlledPart wscp, ControllerData * controllerData)
{
	ControllerMappingData *controllerMappingData = new ControllerMappingData();
	controllerMappingData->controlledPart = wscp;
	controllerMappingData->controlledWall = this;
	controllerMappingData->controlledWallItemUuid = this->uuid();
	controllerData->addControlledItem(controllerMappingData);
	if (wscp == WallSegmentControlledPart::Start)
	{
		m_startControllerData = controllerData;
	}
	else if (wscp == WallSegmentControlledPart::End)
	{
		m_endControllerData = controllerData;
	}
}

void WallData::indicateAttachability(bool isMergible, bool doesWallHaveWidth)
{
	QPen mergePen = doesWallHaveWidth ? WallSegmentItem::MergibilityIndicationPenWithWallWidthCase : WallSegmentItem::MergeStatePen;
	QPen pen = isMergible ? mergePen : WallSegmentItem::BlackPen;
	m_relatedGraphicsItem->setReferenceItemPen(pen);
}

QPointF WallData::normal() const
{
	return m_axesNormal;
}

void WallData::updateNormal()
{
	m_axesNormal = calculateAxisNormal(m_axesCenter);
}

ControllerData * WallData::getOtherController(ControllerData * controller)
{
	return (m_startControllerData != controller) ? m_startControllerData : m_endControllerData;
}

WallSegmentControlledPart WallData::getControllerPartType(ControllerData * controller)
{
	return (m_startControllerData == controller) ? WallSegmentControlledPart::Start : WallSegmentControlledPart::End;
}
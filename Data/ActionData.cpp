#include <map>

#include "ActionData.h"

#include "FloorDesignItems/WallSegmentItem.h"

#include "utilities/utilities.h"

using namespace std;

ActionData::ActionData()
{
	m_dataMap = new map<QString, void*>();
	m_dataToBeDeletedMap = new map<QString, bool>();
}

ActionData::~ActionData()
{
	//DEBUG_OUT("------------------ActionData::Destructor");
	if (m_dataMap != nullptr)
	{
		if (m_dataMap->size() > 0)
		{
			for (pair<QString, void*> keyValuePair : *m_dataMap)
			{
				QString key = keyValuePair.first;
				bool isToBeDeleted = (*m_dataToBeDeletedMap)[key];
				if (!isToBeDeleted)
				{
					continue;
				}

				void * data = keyValuePair.second;
				//DEBUG_OUT("---ActionData::Destructor deleting item (%s)", key.toStdString().c_str());

				if (data != nullptr)
				{
					delete data;
				}
			}
			m_dataMap->clear();
			m_dataToBeDeletedMap->clear();
		}
		delete m_dataMap;
		delete m_dataToBeDeletedMap;
	}
}

QPoint ActionData::globalPointerPos()
{
	return m_globalMousePos;
}

void ActionData::setGlobalPointerPos(QPoint val)
{
	m_globalMousePos = val;
}

QPointF ActionData::scenePointerPos()
{
	return m_scenePointerPos;
}

void ActionData::setScenePointerPos(QPointF val)
{
	m_scenePointerPos = val;
}

WallSegmentItem * ActionData::item()
{
	return m_item;
}

void ActionData::setItem(WallSegmentItem *val)
{
	m_item = val;
}

FloorplanGraphicsView * ActionData::view()
{
	return m_view;
}

void ActionData::setView(FloorplanGraphicsView * view)
{
	m_view = view;
}

void ActionData::addData(QString key, void* data)
{
	addData(key, data, false);
}

void ActionData::addData(QString key, void* data, bool isToBeDeleted)
{
	m_dataMap->insert(pair<QString, void*>(key, data));
	m_dataToBeDeletedMap->insert(pair<QString, bool>(key, isToBeDeleted));
}

void* ActionData::getData(QString key)
{
	map<QString, void*>::iterator mapEntryPosition = m_dataMap->find(key);
	if (mapEntryPosition == m_dataMap->end())
	{
		return nullptr;
	}
	return (*m_dataMap)[key];
	
}

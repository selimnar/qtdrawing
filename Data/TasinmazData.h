#ifndef TASINMAZ_DATA_H
#define TASINMAZ_DATA_H

#include <string>
#include <vector>
#include <map>
#include <QString>

class CityRecord
{
public:
	int id;
	QString name;
};

class TownRecord
{
public:
	int cityId;
	int id;
	QString name;
};

class NeighborhoodRecord
{
public:
	int townId;
	int id;
	QString name;
};

class CityTownNeighborhoodData
{
public:
	~CityTownNeighborhoodData();

	static CityTownNeighborhoodData * Instance();

	std::vector<CityRecord> cities();
	std::map<int, std::vector<TownRecord>> towns();
	std::map<int, std::vector<NeighborhoodRecord>> neighborhoods();

	std::map<int, CityRecord> &cityIdMapping();
	std::map<int, TownRecord> &townIdMapping();
	std::map<int, NeighborhoodRecord> &neighborhoodIdMapping();

private:
	CityTownNeighborhoodData(std::string cityXmlFile, std::string townsXmlFile, std::string neighborhoodsXmlFile);

	static CityTownNeighborhoodData * _singleton;

	std::vector<CityRecord> cityVector;
	std::map<int, std::vector<TownRecord>> townMap;
	std::map<int, std::vector<NeighborhoodRecord>> neighborhoodMap;

	std::map<int, CityRecord> cityIdMap;
	std::map<int, TownRecord> townIdMap;
	std::map<int, NeighborhoodRecord> neighborhoodIdMap;

	void ReadCityXml(std::string xmlFile, std::vector<CityRecord> &cityVector);
	void ReadTownXml(std::string xmlFile, std::map<int, std::vector<TownRecord>> &townMap);
	void ReadNeighborhoodXml(std::string xmlFile, std::map<int, std::vector<NeighborhoodRecord>> &neighborhoodMap);
};

#endif // TASINMAZ_DATA_H
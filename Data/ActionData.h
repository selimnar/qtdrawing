#ifndef ACTION_DATA_H
#define ACTION_DATA_H

#include <QPointF>
#include <QString>

namespace std
{
	template<class _Ty>
	class allocator;
	//template<class _Ty, class _Alloc = allocator<_Ty>>
	//class vector;

	template<class _Ty = void>
	struct less;
	template<class _Ty1,
		class _Ty2>
		struct pair;
	template<class _Kty,
		class _Ty,
		class _Pr = less<_Kty>,
		class _Alloc = allocator<pair<const _Kty, _Ty>>>
		class map;
}

class WallSegmentItem;
class FloorplanGraphicsView;

class ActionData
{
public:
	ActionData();
	~ActionData();

	QPoint globalPointerPos();
	void setGlobalPointerPos(QPoint);
	QPointF scenePointerPos();
	void setScenePointerPos(QPointF);
	WallSegmentItem * item();
	void setItem(WallSegmentItem *);
	FloorplanGraphicsView * view();
	void setView(FloorplanGraphicsView *);

	void addData(QString key, void* data, bool isToBeDeleted);
	void addData(QString key, void* data);
	void* getData(QString key);

private:
	QPoint m_globalMousePos;
	QPointF m_scenePointerPos;
	WallSegmentItem * m_item;
	FloorplanGraphicsView * m_view;

	std::map<QString, void*> *m_dataMap;
	std::map<QString, bool> *m_dataToBeDeletedMap;
};

#endif // ACTION_DATA_H
#ifndef PROJECT_DATA_H
#define PROJECT_DATA_H

#include <QObject>
#include <vector>

#include <xercesc/util/XercesDefs.hpp>

#define XERCES_STATIC_LIBRARY
XERCES_CPP_NAMESPACE_BEGIN
class DOMElement;
class DOMDocument;
XERCES_CPP_NAMESPACE_END

class Project;

class AssignedFloorInstance : public QObject
{
	Q_OBJECT

public:
	AssignedFloorInstance();
	AssignedFloorInstance(AssignedFloorInstance * plan);
	~AssignedFloorInstance();

	void deserialize(const XERCES_CPP_NAMESPACE::DOMElement * p_floorInstaceElement);
	void serialize(XERCES_CPP_NAMESPACE::DOMElement * p_floorInstaceElement, XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument);

	void setId(const unsigned int value);
	unsigned int id() const;

	void setFloorId(const unsigned int value);
	unsigned int floorId() const;

	void setCount(const unsigned int value);
	unsigned int count() const;

private:
	unsigned int m_id;
	unsigned int m_floorId;
	unsigned int m_count;
};

class BuildingInstance : public QObject
{
	Q_OBJECT
		//Q_PROPERTY(int tasinmazId MEMBER m_tasinmazId NOTIFY tasinmazIdChanged)

public:
	BuildingInstance();
	BuildingInstance(BuildingInstance * buildingInstance);
	~BuildingInstance();

	void deserialize(const XERCES_CPP_NAMESPACE::DOMElement * p_buildingElement);
	void serialize(XERCES_CPP_NAMESPACE::DOMElement * p_buildingElement, XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument);

	void setId(const unsigned int value);
	unsigned int id() const;

	void setDefinition(const char * def);
	char * definition() const;

	const std::map<unsigned int, AssignedFloorInstance*> assignedFloorPlans() const { return m_assignedFloorPlans; }

	unsigned int retrieveMaxAssignedFloorInstanceId();

	void addAssignedFloorInstance(AssignedFloorInstance * afpi);

	void removeAssignedFloorInstance(unsigned int afpiId);

signals:
	void definitionChanged(const char * definition);

private:
	unsigned int m_id;
	char * m_definition;

	std::map<unsigned int, AssignedFloorInstance*> m_assignedFloorPlans;
};

//////////////////////////////////////////

class DrawnFloorPlan : public QObject
{
	Q_OBJECT
		//Q_PROPERTY(int tasinmazId MEMBER m_tasinmazId NOTIFY tasinmazIdChanged)


public:
	static const std::string DrawnPlanFolder;

	DrawnFloorPlan();
	DrawnFloorPlan(DrawnFloorPlan * archiPlan);

	~DrawnFloorPlan();

	void deserialize(const XERCES_CPP_NAMESPACE::DOMElement * p_drawnPlansElement);
	void serialize(XERCES_CPP_NAMESPACE::DOMElement * p_drawnPlansElement, XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument);

	void setId(const unsigned int value);
	unsigned int id() const;

	void setDefinition(const char * def);
	char * definition() const;

	void CreateNewDrawnPlanScene(Project * ownerProject, std::string projectsWorkspaceFolder);
	std::string getFilePath(Project * ownerProject, std::string projectsWorkspaceFolder);

signals:
	void definitionChanged(const char * definition);

private:
	unsigned int m_id;
	char * m_definition;
};

//////////////////////////////////////////

class ArchitecturalPlan : public QObject
{
	Q_OBJECT
		//Q_PROPERTY(int tasinmazId MEMBER m_tasinmazId NOTIFY tasinmazIdChanged)

public:
	ArchitecturalPlan();
	ArchitecturalPlan(ArchitecturalPlan * archiPlan);

	~ArchitecturalPlan();

	void deserialize(const XERCES_CPP_NAMESPACE::DOMElement * p_archPlansElement);
	void serialize(XERCES_CPP_NAMESPACE::DOMElement * p_archPlansElement, XERCES_CPP_NAMESPACE::DOMDocument * p_DOMDocument);

	void setId(const unsigned int value);
	unsigned int id() const;

	void setDefinition(const char * def);
	char * definition() const;

	void setFileName(const char * name);
	char * fileName() const;

signals:
	void definitionChanged(const char * definition);

private:
	unsigned int m_id;
	char * m_definition;
	char * m_fileName;
};

//////////////////////////////////////////

class Project : public QObject
{
	Q_OBJECT
	Q_PROPERTY(int tasinmazId MEMBER m_tasinmazId NOTIFY tasinmazIdChanged)

public:
	Project();
	Project(Project * proj);

	~Project();

	void deserialize(const char * fileName);
	void serialize(const char * fileName);

	void setTasinmazId(int value);
	int tasinmazId() const;

	void setCityId(unsigned int value);
	unsigned int cityId() const;

	void setTownId(unsigned int value);
	unsigned int townId() const;

	void setNeighborhoodId(unsigned int value);
	unsigned int neighborhoodId() const;

	void setAda(unsigned int value);
	unsigned int ada() const;

	void setParsel(unsigned int value);
	unsigned int parsel() const;

	void addArchitecturalPlan(ArchitecturalPlan *);

	void removeArchitecturalPlan(unsigned int apId);

	void addDrawnPlan(DrawnFloorPlan * dp);

	void removeDrawnPlan(unsigned int dpId);

	void addBuildingInstance(BuildingInstance * bi);

	void removeBuildingInstance(unsigned int biId);

	const std::map<unsigned int, ArchitecturalPlan*> scannedImageFiles() const { return m_scannedImageFiles; }

	std::map<unsigned int, DrawnFloorPlan*> drawnPlans() const { return m_drawnPlans; }

	const std::map<unsigned int, BuildingInstance*> buildingInstances() const { return m_buildingInstances; }

	unsigned int retrieveMaxArchitecturalPlanId();

	unsigned int retrieveMaxDrawnFloorPlanId();

	unsigned int retrieveMaxBuildingInstanceId();

	std::string GetProjectFolderPath(std::string projectsWorkspaceFolder);

	void saveProject(std::string projectsWorkspaceFolder);

	void setProjectString(QString inProjectString);

	QString projectString();

	void emitDataSignals();

signals:
	void tasinmazIdChanged(int value);
	void cityIdChanged(unsigned int value);
	void townIdChanged(unsigned int value);
	void neighborhoodIdChanged(unsigned int value);
	void adaChanged(unsigned int value);
	void parselChanged(unsigned int value);

private:
	int m_tasinmazId;
	unsigned int m_cityId;
	unsigned int m_townId;
	unsigned int m_neighborhoodId;
	unsigned int m_ada;
	unsigned int m_parsel;

	QString m_projectString;

	std::map<unsigned int, ArchitecturalPlan*> m_scannedImageFiles;

	std::map<unsigned int, DrawnFloorPlan*> m_drawnPlans;

	std::map<unsigned int, BuildingInstance*> m_buildingInstances;

	void saveProjectFile(const char * folderPath, const char * fileName);
};

#endif // PROJECT_DATA_H
#ifndef CONTROLLER_DATA_H
#define CONTROLLER_DATA_H

#include <fstream>

#include <QtGlobal>
#include <QPointF>

#include <string>

//#include "ItemData.h"
#include "WallData.h"

class ControlPointGraphicsItem;
class FloorplanGraphicsScene;

enum WallSegmentControlledPart
{
	Start = 1,
	End = 2,
	None = 3
};

class ControllerMappingData
{
public:
	ControllerMappingData();

	ControllerMappingData(WallSegmentControlledPart _controlledPart,
		std::string _controlledWallItemUuid, WallData * _controlledWallItem);

	WallSegmentControlledPart controlledPart;
	std::string controlledWallItemUuid;
	WallData * controlledWall;
};

class ControllerData : public ItemData
{
//	Q_OBJECT

//Q_SIGNALS:
//	void wallRemoved(ControllerData *);

public:
	ControllerData(std::string &uuid);
	ControllerData(FloorDesignDocument * ownerDoc);
	ControllerData(std::string &uuid, FloorDesignDocument * ownerDoc);
	ControllerData(ControllerData *srcData);
	ControllerData();
	~ControllerData();

	ControlPointGraphicsItem * relatedGraphicsItem() const;
	void setRelatedGraphicsItem(ControlPointGraphicsItem*);

	void addControlledItem(ControllerMappingData*);
	void appendControlledItems(std::vector<ControllerMappingData*> &);
	void removeControlledItem(ControllerMappingData*);
	void removeControlledItem(WallData*);

	int controledItemCount() const;

	std::vector<ControllerMappingData*> controlledList() const;
	void clearList();

	void tradeGraphicsItem(ControllerData * previousOwner);

	QPointF position() const;
	void setPosition(QPointF, bool, WallData * sender);
	void setPositionOnly(QPointF pos);

	void indicateMergeability(bool isMergible, bool doesWallHaveWidth);

	void serialize(std::ofstream &ofs) override;
	void deserialize(std::ifstream &ifs) override;

	void UpdateMergedPolygon();
	void UpdateMergedPolygon(FloorplanGraphicsScene * scene);
	void updatePolygonsForSelfAndOtherSideControllers();

private:

	ControlPointGraphicsItem * m_relatedGraphicsItem;

	std::vector<ControllerMappingData*> m_controlledList;
	std::map<WallData *, ControllerMappingData*> m_controlledMap;

	QPointF m_position;

	void initialize();
};


struct ControllerToIntersectionPair
{
	ControllerData * originController;
	QPointF intersectionPoint;
	int intersectionIndex;
};

bool contollerToIntersectionDistanceComparer(ControllerToIntersectionPair &ctip1, ControllerToIntersectionPair &ctip2);

#endif // CONTROLLER_DATA_H
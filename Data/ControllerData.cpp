#include <map>

#include <QGraphicsItem>
#include <QGraphicsScene>
#include <QPointF>
#include <QPixmap>
#include <QAction>
#include <QMenu>

#include "ControllerData.h"

#include "FloorDesignDocument.h"
#include "FloorDesignItems/WallSegmentItem.h"

#include "utilities/utilities.h"
#include "utilities/MergeWallPolygons.h"

#include "../ControlPointGraphicsItem.h"

using namespace std;

ControllerMappingData::ControllerMappingData()
{
	controlledPart = None;
	controlledWall = nullptr;
}

ControllerMappingData::ControllerMappingData(WallSegmentControlledPart _controlledPart,
	std::string _controlledWallItemUuid, WallData * _controlledWallItem)
{
	controlledPart = _controlledPart;
	controlledWallItemUuid = _controlledWallItemUuid;
	controlledWall = _controlledWallItem;
}






ControllerData::ControllerData(string &uuid) : ItemData(uuid)
{
	initialize();
}

ControllerData::ControllerData(FloorDesignDocument * ownerDoc) : ItemData(ownerDoc)
{
	initialize();
}

ControllerData::ControllerData(string &uuid, FloorDesignDocument * ownerDoc) : ItemData(uuid, ownerDoc)
{
	initialize();
}

ControllerData::ControllerData(ControllerData *srcData)
{
	setUuid(srcData->uuid());
	m_relatedGraphicsItem = srcData->m_relatedGraphicsItem;
	m_controlledList.insert(m_controlledList.end(), srcData->m_controlledList.begin(), srcData->m_controlledList.end());
	m_position = srcData->m_position;

	m_relatedGraphicsItem = srcData->m_relatedGraphicsItem;
}

ControllerData::ControllerData()
{
	initialize();
}

ControllerData::~ControllerData()
{
}

void ControllerData::initialize()
{
	m_position = QPointF(0, 0);
	m_relatedGraphicsItem = nullptr;
}

void ControllerData::addControlledItem(ControllerMappingData * controllerMappingData)
{
	m_controlledList.push_back(controllerMappingData);
	m_controlledMap.insert(pair<WallData *, ControllerMappingData*>(controllerMappingData->controlledWall, controllerMappingData));
}

void ControllerData::appendControlledItems(std::vector<ControllerMappingData*> &al)
{
	m_controlledList.insert(m_controlledList.end(), al.begin(), al.end());
	for (ControllerMappingData* mappingData : al)
	{
		m_controlledMap.insert(pair<WallData *, ControllerMappingData*>(mappingData->controlledWall, mappingData));
	}
}

void ControllerData::removeControlledItem(WallData * wallData)
{
	std::map<WallData *, ControllerMappingData*>::iterator mapEntryPosition = m_controlledMap.find(wallData);
	if (mapEntryPosition != m_controlledMap.end())
	{
		ControllerMappingData * cmd = m_controlledMap[wallData];
		removeControlledItem(cmd);
		delete cmd;
	}
}

void ControllerData::removeControlledItem(ControllerMappingData * controllerMappingData)
{
	std::vector<ControllerMappingData*>::iterator listEntryPosition =
		std::find(m_controlledList.begin(), m_controlledList.end(), controllerMappingData);
	if (listEntryPosition != m_controlledList.end())
	{
		m_controlledList.erase(listEntryPosition);
	}
	std::map<WallData *, ControllerMappingData*>::iterator mapEntryPosition =
		m_controlledMap.find(controllerMappingData->controlledWall);
	if (mapEntryPosition != m_controlledMap.end())
	{
		m_controlledMap.erase(mapEntryPosition);
	}
}

int ControllerData::controledItemCount() const
{
	return (int)m_controlledList.size();
}

std::vector<ControllerMappingData*> ControllerData::controlledList() const
{
	return m_controlledList;
}

void ControllerData::clearList()
{
	m_controlledList.clear();
	m_controlledMap.clear();
}

void ControllerData::tradeGraphicsItem(ControllerData * previousOwner)
{
	ControlPointGraphicsItem * foodControlItem = previousOwner->relatedGraphicsItem();
	if (foodControlItem != nullptr)
	{
		this->setRelatedGraphicsItem(foodControlItem);
		foodControlItem->setPosition(this->position(), false);
	}
}

QPointF ControllerData::position() const
{
	return m_position;
}

void ControllerData::setPosition(QPointF pos, bool isWallSegmentPolygonToBeUpdated, WallData * sender)
{
	m_position = pos;
	if (isWallSegmentPolygonToBeUpdated)
	{
		for (ControllerMappingData * controllerMd : this->m_controlledList)
		{
			if (sender != controllerMd->controlledWall)
			{
				controllerMd->controlledWall->relatedGraphicsItem()->CreatePolygon();
			}
		}
	}
}

void ControllerData::setPositionOnly(QPointF pos)
{
	m_position = pos;
}

void ControllerData::indicateMergeability(bool isMergible, bool doesWallHaveWidth)
{
	QPen mergePen = doesWallHaveWidth ? WallSegmentItem::MergibilityIndicationPenWithWallWidthCase : WallSegmentItem::MergeStatePen;
	QPen pen = isMergible ? mergePen : WallSegmentItem::BlackPen;
	for (ControllerMappingData * controllerMd : this->m_controlledList)
	{
		controllerMd->controlledWall->relatedGraphicsItem()->setReferenceItemPen(pen);
	}
}

ControlPointGraphicsItem * ControllerData::relatedGraphicsItem() const
{
	return m_relatedGraphicsItem;
}

void ControllerData::setRelatedGraphicsItem(ControlPointGraphicsItem * val)
{
	m_relatedGraphicsItem = val;
	if (m_relatedGraphicsItem != nullptr)
	{
		m_relatedGraphicsItem->setControllerData(this);
	}
}

void ControllerData::serialize(std::ofstream &ofs)
{
	ItemData::serialize(ofs);
	ofs << m_position;
}

void ControllerData::deserialize(std::ifstream &ifs)
{
	ItemData::deserialize(ifs);
	ifs >> m_position;
}

void ControllerData::UpdateMergedPolygon()
{
	ControlPointGraphicsItem * controlItem = this->relatedGraphicsItem();
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(controlItem->scene());
	UpdateMergedPolygon(scene);
}

void ControllerData::UpdateMergedPolygon(FloorplanGraphicsScene * scene)
{
	//MergeWalls::ModifyWallSegmentPolygonsDueToControllerMove(this, scene);
	MergeWallPolygons::ModifyCurvedWallSegmentPolygonsRelatedToController(this, scene);
	this->updatePolygonsForSelfAndOtherSideControllers();
}

void ControllerData::updatePolygonsForSelfAndOtherSideControllers()
{
	vector<WallData*> uniqueWallList;
	vector<ControllerMappingData*> controlledList = this->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), wallData) == uniqueWallList.end();
		if (doesNotContainWall)
		{
			uniqueWallList.push_back(wallData);
		}
		//uniqueWallList.push_back(wallData);
		ControllerData * otherController = wallData->getOtherController(this);
		vector<ControllerMappingData*> otherControlledList = otherController->controlledList();
		for (ControllerMappingData * otherCmd : otherControlledList)
		{
			if (wallData != otherCmd->controlledWall)
			{
				WallData * otherWallData = otherCmd->controlledWall;
				bool doesNotContainWall = std::find(uniqueWallList.begin(), uniqueWallList.end(), otherWallData) == uniqueWallList.end();
				if (doesNotContainWall)
				{
					uniqueWallList.push_back(otherWallData);
				}
			}
		}
	}

	for (WallData * wallData : uniqueWallList)
	{
		wallData->updateRelatedGraphicsItemPolygon();
		WallSegmentItem * rgi = wallData->relatedGraphicsItem();
		if (rgi != nullptr)
		{
			rgi->showMeasurementLine(true);
		}
	}
}

bool contollerToIntersectionDistanceComparer(ControllerToIntersectionPair &ctip1, ControllerToIntersectionPair &ctip2)
{
	QPointF originPoint = ctip1.originController->position();
	qreal distance1 = QPointFLength(ctip1.intersectionPoint - originPoint);
	qreal distance2 = QPointFLength(ctip2.intersectionPoint - originPoint);
	return distance1 < distance2;
}
#ifndef WALL_DATA_H
#define WALL_DATA_H

#include <fstream>

#include <QtGlobal>
#include <QPointF>

#include <string>

#include "ItemData.h"

#define _DOT_PRODUCT_EPSILON_ 0.0001

//#include <boost/archive/text_oarchive.hpp>
//#include <boost/archive/text_iarchive.hpp>

enum WallSegmentControlledPart;

class WallSegmentItem;
class ControllerData;
class ControllerMappingData;

class ControllerAndOneWallPair;

enum WallType { Line = 0, Curved = 1 };

enum SpecialWallMoveDragCase { NoCase = 0, CaseWith3Links = 1, CaseWith4Links = 2 };

class WallData : public ItemData
{
	Q_OBJECT

Q_SIGNALS:
	void wallRemoved(WallData *);

public:
	WallData(std::string &uuid);
	WallData(FloorDesignDocument * ownerDoc);
	WallData(std::string &uuid, FloorDesignDocument * ownerDoc);
	WallData(WallData *srcData);
	WallData();
	~WallData();

	WallType type() const;
	void setType(WallType);
	qreal width() const;
	void setWidth(qreal);
	qreal height() const;
	void setHeight(qreal val);
	QPointF startPosition() const;
	void setStartPosition(QPointF);
	QPointF endPosition() const;
	void setEndPosition(QPointF);
	QPointF controlPosition() const;
	void setControlPosition(QPointF);
	void setData(QPointF startPoint, QPointF endPoint, QPointF mainControlPoint, qreal wallWidth, qreal wallHeight, WallType wallType);
	void updateRelatedGraphicsItemPolygon() const;
	WallSegmentItem * relatedGraphicsItem() const;
	void setRelatedGraphicsItem(WallSegmentItem *);

	ControllerData * splitFromPoint(QPointF point, bool isToAddedToCommandHistory);

	void retrieveOtherParallelWallAndExcludeFromList(std::vector<ControllerMappingData*> &controlledList, WallData * &parallelWall);
	void retrieveOtherTwoWall(std::vector<ControllerMappingData*> &controlledList,
		WallData * &otherWallData1, WallData * &otherWallData2);
	bool isEligibleForMove();
	bool isEligibleForMove(WallData * &startParralelWall1, WallData * &startParralelWall2, WallData * &endParralelWall1,
		WallData * &endParralelWall2, WallData * &selfStartParralelWall, WallData * &selfEndParralelWall,
		SpecialWallMoveDragCase &startSpecialWallMoveDragCase, SpecialWallMoveDragCase &endSpecialWallMoveDragCase);
	void move(QPointF vector);
	void move(QPointF vector, bool isWalldataToBeUpdated);

	ControllerData * startControllerData();
	void setStartControllerData(ControllerData *);
	ControllerData * endControllerData();
	void setEndControllerData(ControllerData *);

	void cancelBend();
	void cancelModifyWidth();
	void bending(QPointF);
	void modifyingWidth(QPointF);

	bool checkPointProximity(QPointF point, QPointF &projectedPoint, qreal &distanceToProjectedPoint);
	qreal calculateWidth(QPointF);
	qreal length() const;

	void attachController(ControllerData * attachedController);
	void attachController(ControllerData * attachedController, bool doesAttachToBeSignaled);

	// menu action handlers
	void removeFloorDesignItem();
	void removeSelf(bool doesRemoveToBeSignaled, bool doesControlsToBeRemoved);
	void removeSelf(bool doesRemoveToBeSignaled, bool doesControlsToBeRemoved, bool isGraphicsItemToBeRemoved);
	void startBending();
	void startWidthModifying();
	void split();

	void serialize(std::ofstream &ofs) override;
	void deserialize(std::ifstream &ifs) override;

	ControllerData * getSideController(WallSegmentControlledPart wscp);
	WallSegmentControlledPart detachOneControlledItemSide(ControllerData * controllerDataToDetach);
	WallSegmentControlledPart detachOneSideFromOneControlAndBindToControlledItem(ControllerData * controllerDataToDetach, ControllerData * controllerDataToBind);
	void bindSelfToControlledItem(WallSegmentControlledPart wscp, ControllerData * controllerData);
	void indicateAttachability(bool isMergible, bool doesWallHaveWidth);

	QPointF normal() const;
	void updateNormal();

	ControllerData * getOtherController(ControllerData * controller);
	WallSegmentControlledPart getControllerPartType(ControllerData * controller);

	static ControllerData * SplitWallFromPoint(WallData * wallToSplit, WallData * &splitBornWallData,
		QPointF point, bool isToAddedToCommandHistory);

	static std::vector<ControllerAndOneWallPair*> RetrieveStraightWallListAlongOneSide(ControllerData * controller, WallData * oneSidesFirstWall);

	void UpdateMergedPolygon();
	void updatePolygonsForSelfAndOtherSideControllers();

private:
	const static double CurvedToLineLimit;

	WallType m_type;
	qreal m_width;
	qreal m_height;
	QPointF m_controlPosition;

	WallSegmentItem * m_relatedGraphicsItem;

	ControllerData * m_startControllerData;
	ControllerData * m_endControllerData;

	////// BEGIN: members for bending //////
	WallType m_typeBeforeBendStart;
	QPointF m_controlPositionBeforeBendStart;
	////// END //////

	////// BEGIN: members for width modification //////
	qreal m_widthBeforeModification;
	QPointF m_mouseInteractionInitPos;
	QPointF m_axesNormal;
	QPointF m_axesCenter;
	////// END //////

	void initialize();

	QPointF calculateAxisNormal(QPointF &center);
	void determineType();
};

class ControllerAndOneWallPair
{
public:
	ControllerData * controllerData;
	WallData * wallData;
};

class WallDragInitializeData
{
public:
	QPointF dragInitPos;
	QPointF startPoint;
	QPointF endPoint;
	QPointF controlPoint;
	WallData * startParralelWall1;
	WallData * startParralelWall2;
	std::vector<ControllerAndOneWallPair *> startParralelWallList1;
	std::vector<ControllerAndOneWallPair *> startParralelWallList2;
	WallData * selfStartParralelWall;
	WallData * endParralelWall1;
	WallData * endParralelWall2;
	std::vector<ControllerAndOneWallPair *> endParralelWallList1;
	std::vector<ControllerAndOneWallPair *> endParralelWallList2;
	QPointF endParallelWall1Vec;
	WallData * selfEndParralelWall;
	bool isAtOriginalState;
	SpecialWallMoveDragCase startSpecialWallMoveDragCase;
	SpecialWallMoveDragCase endSpecialWallMoveDragCase;
	bool isCutFromSelfStartParalledWall;
	bool isCutFromSelfEndParalledWall;
};

#endif // WALL_DATA_H
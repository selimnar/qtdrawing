#ifndef ITEM_DATA_H
#define ITEM_DATA_H

#include <QApplication>

#include <string>

class FloorDesignDocument;

class ItemData : public QObject
{
	Q_OBJECT

public:
	ItemData(std::string &uuid);
	ItemData(FloorDesignDocument * ownerDoc);
	ItemData(std::string &uuid, FloorDesignDocument * ownerDoc);
	ItemData();
	~ItemData();

	std::string uuid() const;
	void setUuid(std::string uuid);

	FloorDesignDocument * ownerDocument() const;
	void setOwnerDocument(FloorDesignDocument *);

	virtual void serialize(std::ofstream &ofs);
	virtual void deserialize(std::ifstream &ifs);

private:
	std::string m_uuid;

	FloorDesignDocument * m_ownerDocument;
};

#endif // ITEM_DATA_H
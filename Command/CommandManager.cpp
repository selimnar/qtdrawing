#include <vector>

#include "CommandManager.h"
#include "Command.h"

#include "data\FloorDesignDocument.h"

#include "../utilities/utilities.h"

using namespace std;

CommandManager * CommandManager::m_currentCommandManager = nullptr;

CommandManager::CommandManager(FloorDesignDocument * document)
{
	m_document = document;

	undoList = new vector<UndoableCommand*>();
	redoList = new vector<Command*>();
}

CommandManager::~CommandManager()
{
	for (Command * uc : *undoList)
	{
		delete uc;
	}
	undoList->clear();
	delete undoList;

	for (Command * uc : *redoList)
	{
		delete uc;
	}
	redoList->clear();
	delete redoList;
}



void CommandManager::ExecuteCommand(Command * command)
{
	command->execute();
	UndoableCommand * undoableCommand = dynamic_cast<UndoableCommand *>(command);
	if (undoableCommand != nullptr)
	{
		undoList->push_back(undoableCommand);
		redoList->clear();
	}
}

void CommandManager::redo()
{
	if (redoCommandCount() > 0)
	{
		Command * command = redoList->back();
		redoList->pop_back();
		command->redo();

		UndoableCommand * undoableCommand = dynamic_cast<UndoableCommand *>(command);
		undoList->push_back(undoableCommand);
	}
}


void CommandManager::undo()
{
	if (undoCommandCount() > 1)
	{
		UndoableCommand * undoableCommand = undoList->back();
		undoList->pop_back();
		undoableCommand->undo();

		redoList->push_back(undoableCommand);
	}
}

FloorDesignDocument * CommandManager::document()
{
	return m_document;
}

size_t CommandManager::undoCommandCount()
{
	return undoList->size();
}

size_t CommandManager::redoCommandCount()
{
	return redoList->size();
}

CommandManager * CommandManager::currentCommandManager()
{
	return m_currentCommandManager;
}

void CommandManager::setCurrentCommandManager(CommandManager * currentCommandManager)
{
	m_currentCommandManager = currentCommandManager;
}

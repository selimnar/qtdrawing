#ifndef COMMAND_MANAGER_H
#define COMMAND_MANAGER_H

#include "../utilities/forward_std_vecor_map.h"

//#include <QtGlobal>

class Command;
class UndoableCommand;
class FloorDesignDocument;




class CommandManager
{
//	Q_OBJECT
//
//Q_SIGNALS:
//	void wallRemoved(WallData *);

public:
	CommandManager(FloorDesignDocument *);
	~CommandManager();

	void ExecuteCommand(Command * cmd);
	void redo();
	void undo();

	FloorDesignDocument * document();

	size_t undoCommandCount();
	size_t redoCommandCount();

	static CommandManager * currentCommandManager();
	static void setCurrentCommandManager(CommandManager * currentCommandManager);

private:
	FloorDesignDocument * m_document;

	std::vector<UndoableCommand*> * undoList;
	std::vector<Command*> * redoList;

	static CommandManager * m_currentCommandManager;
};

#endif // COMMAND_MANAGER_H
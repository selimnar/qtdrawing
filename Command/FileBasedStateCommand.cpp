#include <vector>

#include <QApplication>
#include <QTimer>

#include "FileBasedStateCommand.h"
#include "CommandManager.h"
#include "data/FloorDesignDocument.h"
#include "../FloorplanDesigner.h"
#include "../CustomQObjects/FloorplanGraphicsScene.h"


#include "../utilities/utilities.h"

using namespace std;

//const string FileBasedStateCommand::UndoRedoFolderPath = "C:\\Users\\selimnar\\Documents\\FloorDesignUndoRedo\\";
const string FileBasedStateCommand::UndoRedoFolderPath = "FloorDesignUndoRedo\\";

FileBasedStateCommand::FileBasedStateCommand(CommandManager * owner, size_t index) :
	UndoableCommand(owner, index)
{
}

FileBasedStateCommand::~FileBasedStateCommand()
{
}

void FileBasedStateCommand::execute()
{
	SaveStateToFile();
}

void FileBasedStateCommand::redo()
{
	LoadStateFromFile(index());
}

void FileBasedStateCommand::undo()
{
	LoadStateFromFile(index() - 1);
}

void FileBasedStateCommand::staticExecute()
{
	CommandManager * cm = CommandManager::currentCommandManager();
	FileBasedStateCommand * fileBasedStateCommand = new FileBasedStateCommand(cm, cm->undoCommandCount());
	cm->ExecuteCommand((Command *)fileBasedStateCommand);
}

void FileBasedStateCommand::SaveStateToFile()
{
	FloorDesignDocument * fdd = this->owner()->document();
	if (fdd != nullptr)
	{
		string fileName = UndoRedoFolderPath + std::to_string(index());
		std::ofstream ofs(fileName.c_str(), std::ios::binary | std::ios::out);
		if (ofs.is_open())
		{
			//DEBUG_OUT("!!!! saved fileName = %s", fileName.c_str());
			fdd->serialize(ofs);
			ofs.close();
		}
	}
}

void FileBasedStateCommand::LoadStateFromFile(size_t fileIndex)
{
	FloorDesignDocument * fdd = this->owner()->document();
	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());
	FloorplanGraphicsScene * scene = floorplanDesigner->floorplanGraphicsScene();
	if (scene != nullptr)
	{
		QTimer::singleShot(20, [=] {
			//DEBUG_OUT("FileBasedStateCommand::LoadStateFromFile");

			string fileName = UndoRedoFolderPath + std::to_string(fileIndex);
			std::ifstream ifs(fileName.c_str(), std::ios::binary | std::ios::in);
			if (ifs.is_open())
			{
				//DEBUG_OUT("!!!! opened fileName = %s", fileName.c_str());
				fdd->finalize();
				fdd->initialize();
				fdd->deserialize(ifs);
				ifs.close();

				int wallCount = fdd->wallCount();
				for (int i = 0; i < wallCount; i++)
				{
					WallData * wallData = fdd->wallAt(i);
					floorplanDesigner->connectWallRemoveSocket(wallData);
				}

				scene->resetScene(false);
				scene->createSceneGraphicsItemsFromDocumentData(fdd);
				scene->invalidate();
			}
		});
	}
}
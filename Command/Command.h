#ifndef COMMAND_H
#define COMMAND_H

class CommandManager;

class Command
{
public:
	Command(CommandManager * owner, size_t index);
	~Command();

	size_t index();
	CommandManager * owner();

	virtual void execute() = 0;
	virtual void redo() = 0;

private:
	size_t m_index;
	CommandManager * m_owner;
};

class UndoableCommand : public Command
{
public:
	UndoableCommand(CommandManager * owner, size_t index);
	~UndoableCommand();

	virtual void undo() = 0;
};

#endif // COMMAND_H
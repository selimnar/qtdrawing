
#include "Command.h"
#include "CommandManager.h"

Command::Command(CommandManager * owner, size_t index) : m_owner(owner), m_index(index)
{
}

Command::~Command()
{
}

size_t Command::index()
{
	return m_index;
}

CommandManager * Command::owner()
{
	return m_owner;
}


UndoableCommand::UndoableCommand(CommandManager * owner, size_t index) : Command(owner, index)
{
}

UndoableCommand::~UndoableCommand()
{
}

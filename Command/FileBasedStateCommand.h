#ifndef FILE_BASED_STATE_COMMAND_H
#define FILE_BASED_STATE_COMMAND_H

#include <string>

#include "Command.h"

class CommandManager;
class FloorplanGraphicsScene;

enum WallSegmentControlledPart;

class FileBasedStateCommand : UndoableCommand
{
public:
	FileBasedStateCommand(CommandManager * owner, size_t index);
	~FileBasedStateCommand();

	void execute() override;
	void redo() override;
	void undo() override;

	static void staticExecute();

private:
	const static std::string UndoRedoFolderPath;

	void SaveStateToFile();
	void LoadStateFromFile(size_t fileIndex);
};

#endif // FILE_BASED_STATE_COMMAND_H
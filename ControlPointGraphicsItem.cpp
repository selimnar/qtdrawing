#include <map>

#include <QGraphicsSceneMouseEvent>
//#include <QTimer>

#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "ControlPointGraphicsItem.h"
#include "utilities/utilities.h"
#include "utilities/MergeWallPolygons.h"
#include "utilities/PolygonDetection.h"

#include "Data/ControllerData.h"
#include "Data/FloorDesignDocument.h"

#include "Command/FileBasedStateCommand.h"

//const qreal ControlPointGraphicsItem::Radius = 2.0f;
const qreal ControlPointGraphicsItem::Radius = 13.0f;
//const QBrush BlueBrush("#047777");
const QBrush ControlPointGraphicsItem::MergeStateBrush("#777777");
const QBrush ControlPointGraphicsItem::StartNormalStateBrush(Qt::green);
const QBrush ControlPointGraphicsItem::EndNormalStateBrush(Qt::blue);

using namespace std;

ControlPointGraphicsItem::ControlPointGraphicsItem(ControllerData * controllerData, FloorplanGraphicsScene *scene,
	bool inIsIndicatedAsStart)
//: QGraphicsEllipseItem(center.x() - Radius, center.y() - Radius, (Radius * 2.0f) - 1.0f, (Radius * 2.0f) - 1.0f, parent)
	: QGraphicsEllipseItem(0 - Radius, 0 - Radius, (Radius * 2.0f) - 1.0f, (Radius * 2.0f) - 1.0f, nullptr)
{
	isDragged = false;
	isDraggedCancelled = false;

	mergableControlPoint = nullptr;
	attachableWall = nullptr;

	isIndicatedAsStart = inIsIndicatedAsStart;

	scene->addItem(this);

	m_controllerData = controllerData;
	position = controllerData->position();
	this->setPos(position);
	controllerData->setRelatedGraphicsItem(this);
	relativePositionToControlledPath = position;

	setBrush(NormalStateBrush());
	setPen(Qt::NoPen);

	//setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges);
	setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges | QGraphicsItem::ItemIsFocusable);

	//setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemIsFocusable | QGraphicsItem::ItemSendsGeometryChanges);
}

ControlPointGraphicsItem::~ControlPointGraphicsItem()
{
}

void ControlPointGraphicsItem::setPosition(QPointF newPos, bool isItemChangeToBeCalled)
{
	this->position = newPos;

	setFlag(QGraphicsItem::ItemSendsGeometryChanges, isItemChangeToBeCalled);
	this->setPos(this->position);
	setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

ControllerData * ControlPointGraphicsItem::controllerData()
{
	return m_controllerData;
}

void ControlPointGraphicsItem::setControllerData(ControllerData * val)
{
	m_controllerData = val;
}

void ControlPointGraphicsItem::cancelDrag()
{
	this->setPos(this->m_positionBeforeDragStarted);
	this->m_controllerData->setPosition(this->m_positionBeforeDragStarted, true, nullptr);
	isDraggedCancelled = true;

	DEBUG_OUT("warning: ControlPointGraphicsItem::cancelDrag");
}

void ControlPointGraphicsItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	//QPointF scenePos = event->scenePos();
	//DEBUG_OUT("ControlPointGraphicsItem::mousePressEvent = scenePos(%f, %f)", scenePos.x(), scenePos.y());
	//setPosition(event->scenePos(), false);
	previousPosition = position;
	setBrush(Qt::NoBrush);
	m_positionBeforeDragStarted = pos();

	//PolygonDetector polygonDetector;
	//polygonDetector.detectPolygons(this->m_controllerData);

	QGraphicsItem::mousePressEvent(event);
}

bool ControlPointGraphicsItem::createWallJunctionsFromIntersection()
{
	FloorDesignDocument * document = m_controllerData->ownerDocument();

	vector<WallData*> controlledWallList;
	vector<ControllerMappingData*> controlledList = this->m_controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		controlledWallList.push_back(cmd->controlledWall);
	}

	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	return document->createWallJunctionsFromIntersection(scene, controlledWallList);
}

void ControlPointGraphicsItem::updateControlledWallNormals()
{
	vector<ControllerMappingData*> controlledList = this->m_controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		cmd->controlledWall->updateNormal();
	}
}

void ControlPointGraphicsItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//DEBUG_OUT("WallSegmentItem::mouseReleaseEvent");
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	if (isDragged && !isDraggedCancelled)
	{
		bool doesIntersect = createWallJunctionsFromIntersection();
		if (doesIntersect)
		{
			QTimer::singleShot(20, [=] {
				scene->clearControlPointGraphicItems();
			});
		}
		bool doRegisterAsDragEvent = true;
		FloorDesignDocument * document = m_controllerData->ownerDocument();
		ControllerData * mergableControlPoint = document->retrieveMergableControlPoint(this->m_controllerData);
		if (mergableControlPoint != nullptr)
		{
			bool someWallIsRemoved = document->mergeControlPoint(this->m_controllerData, mergableControlPoint);
			if (someWallIsRemoved)
			{
				QTimer::singleShot(20, [=] {
					scene->clearControlPointGraphicItems();
				});
			}

			if ( (this->m_controllerData->controledItemCount() + mergableControlPoint->controledItemCount()) == 0)
			{
				scene->notifyControlPointItemDraggedEnded(this, doRegisterAsDragEvent);
				//scene->removeItem(mergableControlPoint->relatedGraphicsItem());
				document->removeController(this->m_controllerData);
				document->removeController(mergableControlPoint);
				//QTimer::singleShot(20, [=] {
				//	scene->removeItem(this);
				//});

				QGraphicsItem::mouseReleaseEvent(event);
				return;
			}
		}
		else
		{
			WallData * attachableWall = document->retrieveAttachableWall(this->m_controllerData);
			if (attachableWall != nullptr)
			{
				attachableWall->indicateAttachability(false, scene->doesWallsHaveWidth());
				attachableWall->attachController(m_controllerData);
				doRegisterAsDragEvent = false;
			}
		}
		this->m_controllerData->UpdateMergedPolygon();
		this->m_controllerData->indicateMergeability(false, scene->doesWallsHaveWidth());
		this->updateControlledWallNormals();

		scene->notifyControlPointItemDraggedEnded(this, doRegisterAsDragEvent);
		DEBUG_OUT("ControlPointGraphicsItem::drag ended");
	}

	isDragged = false;
	isDraggedCancelled = false;
	setBrush(NormalStateBrush());

	QGraphicsItem::mouseReleaseEvent(event);
}

QVariant ControlPointGraphicsItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if (change == QGraphicsItem::ItemPositionHasChanged)
	{
		if (isDraggedCancelled)
		{
			return pos();
		}
		FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
		//QGraphicsItem* item = value.value<QGraphicsItem*>();
		if (!isDragged)
		{
			scene->setControlPointItemThatIsBeingDragged(this);
		}
		isDragged = true;

		// value is the new position.
		//this->m_controllerData->setPosition(position, true, nullptr);
		this->m_controllerData->setPosition(position, false, nullptr);
		position = value.toPointF();
		updateControlledWallNormals();
		this->m_controllerData->UpdateMergedPolygon();

		indicateMergeability();

		//FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
		//AbstractFloorDesignItem * abstractItemAt = scene->retrieveAbstractFloorItemAt(position);
	}
	return QGraphicsEllipseItem::itemChange(change, value);
}

void ControlPointGraphicsItem::indicateMergeability()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	FloorDesignDocument * document = m_controllerData->ownerDocument();

	ControllerData * newMergeableControlPoint = document->retrieveMergableControlPoint(this->m_controllerData);
	bool isToChangeMereabilitySate = (this->mergableControlPoint != newMergeableControlPoint);
	bool isThereNodeMergeability = newMergeableControlPoint != nullptr;
	if (this->mergableControlPoint != nullptr)
	{
		if (isToChangeMereabilitySate)
		{
			this->mergableControlPoint->indicateMergeability(false, scene->doesWallsHaveWidth());
			if (newMergeableControlPoint != nullptr)
			{
				newMergeableControlPoint->indicateMergeability(true, scene->doesWallsHaveWidth());
			}
		}
	}
	if (newMergeableControlPoint != nullptr)
	{
		if (isToChangeMereabilitySate)
		{
			newMergeableControlPoint->indicateMergeability(true, scene->doesWallsHaveWidth());
		}
	}
	this->m_controllerData->indicateMergeability(isThereNodeMergeability, scene->doesWallsHaveWidth());

	this->mergableControlPoint = newMergeableControlPoint;

	// if there is node mergebility do not check wall attach mergibility or wall intersection mergibility
	if (isThereNodeMergeability)
	{
		return;
	}

	WallData * newAttachableWall = document->retrieveAttachableWall(this->m_controllerData);
	bool isToChangeAttachableSate = (this->attachableWall != newAttachableWall);

	if (this->attachableWall != nullptr)
	{
		if (isToChangeAttachableSate)
		{
			this->attachableWall->indicateAttachability(false, scene->doesWallsHaveWidth());
			if (newAttachableWall != nullptr)
			{
				newAttachableWall->indicateAttachability(true, scene->doesWallsHaveWidth());
			}
		}
	}
	if (newAttachableWall != nullptr)
	{
		if (isToChangeAttachableSate)
		{
			newAttachableWall->indicateAttachability(true, scene->doesWallsHaveWidth());
		}
	}
	
	this->attachableWall = newAttachableWall;
}

QBrush ControlPointGraphicsItem::NormalStateBrush()
{
	return (isIndicatedAsStart) ? StartNormalStateBrush : EndNormalStateBrush;
}
#ifndef DOWNLOADED_IMAGE_HANDLER_H
#define DOWNLOADED_IMAGE_HANDLER_H

#include <QImage>

class IDownloadedImageHandler
{
public:

	virtual void onImageLoaded(QImage * loadedImage) = 0;

	virtual void onFail() = 0;

};

#endif // DOWNLOADED_IMAGE_HANDLER_H
//#include <qmath.h>
#include <algorithm>

#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QPointF>
#include <QAction>

#include <qmath.h>

#include "FloorDesignItems/WallSegmentItem.h"
#include "ControlPointGraphicsItem.h"
#include "FloorplanGraphicsScene.h"
#include "../CustomWidgets/FloorplanGraphicsView.h"
#include "Data/FloorDesignDocument.h"
#include "../data/FloorDesignDocument.h"
#include "FloorPlanDesigner.h"

#include "utilities/utilities.h"
#include "utilities/QImageDownloader.h"
#include "data/WallData.h"
#include "data/ControllerData.h"

#include "../CustomWidgets/QWallPropertiesWidget.h"

const QBrush FloorplanGraphicsScene::GhostyBrush("#54AAAAAA");
const qreal FloorplanGraphicsScene::CenterPointSize = 15;

FloorplanGraphicsScene::FloorplanGraphicsScene(QObject *parent)
	: QGraphicsScene(parent)
{
	isDrawingSelection = false;
	this->selectionRect = new QGraphicsRectItem(nullptr);
	this->selectionRect->setBrush(GhostyBrush);
	this->selectionRect->setZValue(1000);
	this->addItem(selectionRect);
	this->selectionRect->setVisible(isDrawingSelection);

	m_doesWallsHaveWidth = true;

	wallSegmentToAdd = nullptr;
	m_wallToModify = nullptr;
	planRegisterLineToAdd = nullptr;
	m_interactionMode = InteractionMode::View;

	m_wallSegmentThatIsBeingDragged = nullptr;
	m_controlPointGraphicsItemThatIsBeingDraged = nullptr;

	m_backgroundPixmapItem = nullptr;
}

FloorplanGraphicsScene::~FloorplanGraphicsScene()
{
}

void FloorplanGraphicsScene::resetScene(bool isBackGroundImageToBeLoaded)
{
	if (isBackGroundImageToBeLoaded)
	{
		clear();

		//QImage scannedFloorDesignImage("C:\\Users\\selimnar\\Documents\\tapu_mimari_cizim_taramalar\\48_6.tif");
		//QPixmap bgPixmap = QPixmap::fromImage(scannedFloorDesignImage);
		//m_backgroundPixmapItem = new QGraphicsPixmapItem(bgPixmap);
		m_backgroundPixmapItem = new QGraphicsPixmapItem();
		//QGraphicsPixmapItem * item = new QGraphicsPixmapItem(scaled);
		QMatrix matrix;
		//matrix.scale(0.3, 0.3);
		matrix.scale(1, 1);
		m_backgroundPixmapItem->setMatrix(matrix);
		m_backgroundPixmapItem->setOpacity(0.15);
		this->addItem(m_backgroundPixmapItem);

		//QImage qImage;
		//bool success = qImage.load("images/room.tif");
		//if (success)
		//{
		//	QPixmap bgPixmap = QPixmap::fromImage(qImage);
		//	this->loadImage(bgPixmap);
		//}
		
		QGraphicsEllipseItem * center =
			new QGraphicsEllipseItem(0 - CenterPointSize, 0 - CenterPointSize,
			(CenterPointSize * 2.0f) - 1.0f, (CenterPointSize * 2.0f) - 1.0f, nullptr);
		center->setBrush(GhostyBrush);
		addItem(center);

		isDrawingSelection = false;
		this->selectionRect = new QGraphicsRectItem(nullptr);
		this->selectionRect->setBrush(GhostyBrush);
		this->selectionRect->setZValue(1000);
		this->addItem(selectionRect);
		this->selectionRect->setVisible(isDrawingSelection);
	}
	else
	{
		clearControlPointGraphicItems();
		clearWallSegmentItems();
	}
}

void FloorplanGraphicsScene::setInteractionMode(InteractionMode im)
{
	// if quiting from wall add mode
	if (m_interactionMode == WallSegmentAdd && im == View)
	{
		// and if an wall segment is created and not added to floor design data structure
		// it's  should be deleted
		if (this->wallSegmentToAdd != nullptr)
		{
			this->removeItem(this->wallSegmentToAdd);
			delete this->wallSegmentToAdd;
			this->wallSegmentToAdd = nullptr;
			//setAllFloorItemsInteractable(true);
		}
	}

	if (m_interactionMode == PlanRegisterLineSegmentAdd && im == View)
	{
		if (this->planRegisterLineToAdd != nullptr)
		{
			this->removeItem(this->planRegisterLineToAdd);
			delete this->planRegisterLineToAdd;
			this->planRegisterLineToAdd = nullptr;
		}
	}

	// if quiting from wall bend mode
	if (m_interactionMode == WallSegmentBend && im == View)
	{
		if (this->m_wallToModify != nullptr)
		{
			this->m_wallToModify->cancelBend();
		}
	}

	// if quiting from wall width modify mode
	if (m_interactionMode == WallWidthModify && im == View)
	{
		if (this->m_wallToModify != nullptr)
		{
			this->m_wallToModify->cancelModifyWidth();
		}
	}

	// if cancelling from wall drag
	if (m_interactionMode == WallBeingDragged && im == View)
	{
		if (this->m_wallSegmentThatIsBeingDragged != nullptr)
		{
			this->m_wallSegmentThatIsBeingDragged->cancelDrag();
			this->m_wallSegmentThatIsBeingDragged = nullptr;
		}
	}

	// if cancelling from wall side controller drag
	if (m_interactionMode == SideControlBeingDragged && im == View)
	{
		if (this->m_controlPointGraphicsItemThatIsBeingDraged != nullptr)
		{
			this->m_controlPointGraphicsItemThatIsBeingDraged->cancelDrag();
			this->m_controlPointGraphicsItemThatIsBeingDraged = nullptr;
		}
	}

	m_interactionMode = im;

	setAllFloorItemsInteractable(m_interactionMode == InteractionMode::View);
	//// if entering wall creation mode
	//if (m_interactionMode != InteractionMode::View)
	//{
	//	setAllFloorItemsInteractable(false);
	//}
}

InteractionMode FloorplanGraphicsScene::interactionMode()
{
	return m_interactionMode;
}

WallData * FloorplanGraphicsScene::wallToBend()
{
	return m_wallToModify;
}

void FloorplanGraphicsScene::startWallBending(WallData * wall)
{
	m_wallToModify = wall;
	this->setInteractionMode(InteractionMode::WallSegmentBend);
}

void FloorplanGraphicsScene::startWallWidthModifying(WallData *wall)
{
	m_wallToModify = wall;
	this->setInteractionMode(InteractionMode::WallWidthModify);
}

void FloorplanGraphicsScene::onWallAdded(WallData *wallData, bool isToAddedToCommandHistory)
{
	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());
	FloorDesignDocument * document = floorplanDesigner->document();

	floorplanDesigner->connectWallRemoveSocket(wallData);
	document->addWall(wallData);
	document->addController(wallData->startControllerData());
	document->addController(wallData->endControllerData());

	bool doesIntersect = document->createWallJunctionsFromIntersection(this, wallData);
	if (doesIntersect)
	{
		QTimer::singleShot(20, [=] {
			this->clearControlPointGraphicItems();
			wallData->relatedGraphicsItem()->CreatePolygon();
		});
	}

	wallData->UpdateMergedPolygon();

	emit wallAdded(wallData, isToAddedToCommandHistory);
}

void FloorplanGraphicsScene::onWallBended(WallData *wallItem)
{
	this->m_wallToModify = nullptr;
	this->m_interactionMode = InteractionMode::View;
	setAllFloorItemsInteractable(true);
	emit wallBended(wallItem);
}

void FloorplanGraphicsScene::onWallWidthModified(WallData *wallItem)
{
	this->m_wallToModify = nullptr;
	this->m_interactionMode = InteractionMode::View;
	setAllFloorItemsInteractable(true);
	emit wallWidthModified(wallItem);
}

void FloorplanGraphicsScene::onWallSplitted(WallData * firstWD, WallData * secondWD, ControllerData * splitBornControllerData)
{
	splitBornControllerData->UpdateMergedPolygon(this);
	emit wallSplitted(firstWD, secondWD);
}

void FloorplanGraphicsScene::onMergeControlPointToControlPoint(ControllerData * food, ControllerData * swallower)
{
	emit mergeControlPointToControlPoint(food, swallower);
}

void FloorplanGraphicsScene::onAttachedToWall(ControllerData * attachedController, WallData * wallPriorToAttachment)
{
	emit attachedToWall(attachedController, wallPriorToAttachment);
}

void FloorplanGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	QPointF mousePos = mouseEvent->scenePos();
	//DEBUG_OUT("---mouseMoveEvent: mousePos(%f, %f)", mousePos.x(), mousePos.y());

	if (this->m_interactionMode == InteractionMode::WallSegmentAdd)
	{
		if (this->wallSegmentToAdd != nullptr)
		{
			this->wallSegmentToAdd->setEndPosition(mousePos);
		}
	}

	if (this->m_interactionMode == InteractionMode::PlanRegisterLineSegmentAdd)
	{
		if (this->planRegisterLineToAdd != nullptr)
		{
			planRegisterLineEndPoint = mousePos;
			QPainterPath path;
			path.moveTo(this->planRegisterLineStartPoint);
			path.lineTo(this->planRegisterLineEndPoint);
			this->planRegisterLineToAdd->setPath(path);

			//DEBUG_OUT("----------- (%f, %f) ; (%f, %f)", planRegisterLineStartPoint.x(), planRegisterLineStartPoint.y(),
			//	planRegisterLineEndPoint.x(), planRegisterLineEndPoint.y());
		}
	}

	if (this->m_interactionMode == InteractionMode::WallSegmentBend)
	{
		if (this->m_wallToModify != nullptr)
		{
			this->m_wallToModify->bending(mousePos);
		}
	}

	if (this->m_interactionMode == InteractionMode::WallWidthModify)
	{
		if (this->m_wallToModify != nullptr)
		{
			this->m_wallToModify->modifyingWidth(mousePos);
		}
	}

	if (isDrawingSelection)
	{
		if (qApp->keyboardModifiers().testFlag(Qt::AltModifier))
		{
			float top = qMin(selectionAnchorPos.y(), mousePos.y());
			float left = qMin(selectionAnchorPos.x(), mousePos.x());
			selectionRect->setPos(left, top);
			selectionRect->setRect(0, 0, qAbs(mousePos.x() - selectionAnchorPos.x()),
				qAbs(mousePos.y() - selectionAnchorPos.y()));

			//DEBUG_OUT("---mouseMoveEvent: mousePos(%f, %f)", mousePos.x(), mousePos.y());
		}
	}

	return QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void FloorplanGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	//FloorplanGraphicsView * fpgv = (FloorplanGraphicsView*)(mouseEvent->widget());
	//if (fpgv == nullptr)
	//{
	//	return QGraphicsScene::mousePressEvent(mouseEvent);
	//}
	QPointF mousePos = mouseEvent->scenePos();
	//DEBUG_OUT("---mousePressEvent: mousePos(%f, %f)", mousePos.x(), mousePos.y());

	if (this->m_interactionMode == InteractionMode::WallSegmentAdd)
	{
		// add last wall to floor design data structure
		if (this->wallSegmentToAdd != nullptr)
		{
			//this->wallSegmentToAdd->setEndPosition(mousePos);
			//this->wallSegmentToAdd->setInteractable(true);
			onWallAdded(this->wallSegmentToAdd->data(), true);
		}

		this->wallSegmentToAdd = new WallSegmentItem(this);
		this->wallSegmentToAdd->setWallData(mousePos, mousePos, mousePos,
			WallSegmentItem::DefaultWallWidth, WallSegmentItem::DefaultWallHeight, WallType::Line);
	}

	if (this->m_interactionMode == InteractionMode::PlanRegisterLineSegmentAdd)
	{
		Qt::MouseButton mouseButton = mouseEvent->button();
		if (mouseButton == Qt::LeftButton)
		{
			// add last wall to floor design data structure
			if (this->planRegisterLineToAdd != nullptr)
			{
				this->removeItem(this->planRegisterLineToAdd);
				delete this->planRegisterLineToAdd;
				this->planRegisterLineToAdd = nullptr;
			}

			this->planRegisterLineToAdd = new QGraphicsPathItem(nullptr);
			this->addItem(this->planRegisterLineToAdd);
			this->planRegisterLineStartPoint = mousePos;
		}
		else if (mouseButton == Qt::MidButton)
		{
			this->m_interactionMode = InteractionMode::View;
			this->setAllFloorItemsInteractable(true);
		}
	}

	if (this->m_interactionMode == InteractionMode::WallSegmentBend)
	{
		// add last wall to floor design data structure
		if (this->m_wallToModify != nullptr)
		{
			onWallBended(this->m_wallToModify);
		}
	}

	if (this->m_interactionMode == InteractionMode::WallWidthModify)
	{
		// add last wall to floor design data structure
		if (this->m_wallToModify != nullptr)
		{
			onWallWidthModified(this->m_wallToModify);
		}
	}

	return QGraphicsScene::mousePressEvent(mouseEvent);
}

void FloorplanGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	bool isCtrlKey = mouseEvent->modifiers().testFlag(Qt::ControlModifier);
	QPointF mousePos = mouseEvent->scenePos();
	if (!isCtrlKey)
	{
		WallSegmentItem * wsi = dynamic_cast<WallSegmentItem*>(retrieveAbstractFloorItemAt(mousePos));
		if (wsi == nullptr)
		{
			this->deselectAllWalls();
		}
		//DEBUG_OUT("---mousePressEvent: mousePos(%f, %f)  %s", mousePos.x(), mousePos.y(), (wsi != nullptr) ? "TRUE" : "FALSE");
	}
	
	bool isAltKey = mouseEvent->modifiers().testFlag(Qt::AltModifier);
	if (isAltKey)
	{
		isDrawingSelection = !isDrawingSelection;
		//isDrawingSelection = true;
		if (isDrawingSelection)
		{
			selectionAnchorPos = mousePos;
			this->selectionRect->setVisible(false);
			//this->selectionRect->setPos(mousePos);

			QRectF rect(mousePos.x(), mousePos.y(), 0, 0);
			this->selectionRect->setRect(rect);
			this->selectionRect->setVisible(true);
		}
		else
		{
			std::vector<WallData*> selectedWalls;
			bool result = retrieveSelection(selectedWalls);
			this->selectionRect->setVisible(false);
		}

		DEBUG_OUT("---mouseMoveEvent: mousePos(%f, %f) ; isDrawingSelection = %d", mousePos.x(), mousePos.y(), isDrawingSelection);
	}

	return QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void FloorplanGraphicsScene::keyReleaseEvent(QKeyEvent *event)
{
	bool isControlKey = event->key() == Qt::Key_Alt;
	if (isControlKey)
	{
		isDrawingSelection = false;
		this->selectionRect->setVisible(false);
	}
	DEBUG_OUT("---FloorplanGraphicsScene:keyReleaseEvent %s", (isControlKey ? "TRUE" : "FALSE"));
}

void FloorplanGraphicsScene::setAllFloorItemsInteractable(bool interactable)
{
	QList<QGraphicsItem *> itemList = this->items();
	for (auto item : itemList)
	{
		//AbstractFloorDesignItem * afdi = (AbstractFloorDesignItem*)item;
		AbstractFloorDesignItem * afdi = dynamic_cast<AbstractFloorDesignItem*>(item);
		if (afdi != nullptr)
		{
			afdi->setInteractable(interactable);
		}
	}
}

bool FloorplanGraphicsScene::doesWallsHaveWidth()
{
	return m_doesWallsHaveWidth;
}

void FloorplanGraphicsScene::setDoesWallsHaveWidth(bool val)
{
	m_doesWallsHaveWidth = val;
}

void FloorplanGraphicsScene::createSceneGraphicsItemsFromDocumentData(FloorDesignDocument * document)
{
	int wallCount = document->wallCount();
	for (int i = 0; i < wallCount; i++)
	{
		WallData * walldata = document->wallAt(i);
		WallSegmentItem * wsi = new WallSegmentItem(this, true, walldata);
		//wsi->CreatePolygon();
	}

	std::vector<ControllerData*> * controllerList = new std::vector<ControllerData*>();
	int controllerCount = document->controllerCount();
	for (int i = 0; i < controllerCount; i++)
	{
		ControllerData * controllerData = document->controllerAt(i);
		controllerList->push_back(controllerData);
	}

	MergeWallPolygons::ModifyCurvedWallSegmentPolygonsForGivenControlDataList(controllerList, false, this);

	for (int i = 0; i < wallCount; i++)
	{
		WallData * walldata = document->wallAt(i);
		//walldata->UpdateMergedPolygon();
		walldata->updateRelatedGraphicsItemPolygon();
	}
}

AbstractFloorDesignItem * FloorplanGraphicsScene::retrieveAbstractFloorItemAt(QPointF position)
{
	QGraphicsItem * itemUnderMouse = itemAt(position, QTransform());

	if (itemUnderMouse == nullptr)
	{
		return nullptr;
	}

	QPainterPath opaqueArea = itemUnderMouse->opaqueArea();
	bool isUnderMouse = opaqueArea.contains(position);

	if (!isUnderMouse)
	{
		return nullptr;
	}

	AbstractFloorDesignItem * afdi = dynamic_cast<AbstractFloorDesignItem*>(itemUnderMouse);
	if (afdi == nullptr)
	{
		afdi = dynamic_cast<AbstractFloorDesignItem*>(itemUnderMouse->parentItem());
	}

	return afdi;
}

void FloorplanGraphicsScene::setControlPointItemThatIsBeingDragged(ControlPointGraphicsItem * cpgi)
{
	m_controlPointGraphicsItemThatIsBeingDraged = cpgi;
	m_interactionMode = SideControlBeingDragged;
}

void FloorplanGraphicsScene::notifyControlPointItemDraggedEnded(ControlPointGraphicsItem * cpgi, bool isToAddedToCommandHistory)
{
	m_controlPointGraphicsItemThatIsBeingDraged = nullptr;
	m_interactionMode = View;
	if (isToAddedToCommandHistory)
	{
		emit wallSideControlDragEnded(cpgi->controllerData());
	}
}

void FloorplanGraphicsScene::setWallSegmentItemThatIsBeingDragged(WallSegmentItem * wsi)
{
	m_wallSegmentThatIsBeingDragged = wsi;
	m_interactionMode = WallBeingDragged;
}

void FloorplanGraphicsScene::notifyWallSegmentItemDraggedEnded(WallSegmentItem * wsi)
{
	m_wallSegmentThatIsBeingDragged = nullptr;
	m_interactionMode = View;
	clearDopplerWallList();
	emit wallSegmentItemDragEnded(wsi->data());
}

void FloorplanGraphicsScene::clearControlPointGraphicItems()
{
	QList<QGraphicsItem*> items = this->items();
	for each (QGraphicsItem * qgItem in items)
	{
		ControlPointGraphicsItem * controlPointGraphicsItem = dynamic_cast<ControlPointGraphicsItem*>(qgItem);
		if (controlPointGraphicsItem != nullptr)
		{
			this->removeItem(controlPointGraphicsItem);
		}
	}
}

void FloorplanGraphicsScene::clearWallSegmentItems()
{
	QList<QGraphicsItem*> items = this->items();
	for each (QGraphicsItem * qgItem in items)
	{
		WallSegmentItem * wallSegmentItem = dynamic_cast<WallSegmentItem*>(qgItem);
		if (wallSegmentItem != nullptr)
		{
			delete wallSegmentItem;
		}
	}
}

void FloorplanGraphicsScene::resizeControlPointGraphicItems(qreal scale)
{
	QMatrix matrix;
	matrix.scale(scale, scale);
	QList<QGraphicsItem*> items = this->items();
	for each (QGraphicsItem * qgItem in items)
	{
		ControlPointGraphicsItem * controlPointGraphicsItem = dynamic_cast<ControlPointGraphicsItem*>(qgItem);
		if (controlPointGraphicsItem != nullptr)
		{
			controlPointGraphicsItem->setMatrix(matrix);
		}
	}
}

void FloorplanGraphicsScene::setPenOfDopplerWallsItems(QPen pen, QBrush brush)
{
	for (WallSegmentItem* wsi : dopplerWallList) 
	{
		wsi->setReferenceItemPen(pen);
		wsi->setBrush(brush);
	}
}

void FloorplanGraphicsScene::createDopplerWallItemListDueToWallDrag(WallData * wallData)
{
	std::vector<WallData*> wallDataList;
	createDopplerWallDataListDueToControllerDrag(wallData->startControllerData(), wallDataList);
	createDopplerWallDataListDueToControllerDrag(wallData->endControllerData(), wallDataList);
	createDopplerWallList(wallDataList);
}

void FloorplanGraphicsScene::createDopplerWallItemListDueToControllerDrag(ControllerData* controllerData)
{
	std::vector<WallData*> wallDataList;
	createDopplerWallDataListDueToControllerDrag(controllerData, wallDataList);
	createDopplerWallList(wallDataList);
}

void FloorplanGraphicsScene::createDopplerWallDataListDueToControllerDrag(ControllerData* controllerData, std::vector<WallData*> &wallDataList)
{
	std::vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData* cmd : controlledList)
	{
		wallDataList.push_back(cmd->controlledWall);
	}
}

bool FloorplanGraphicsScene::retrieveSelection(std::vector<WallData*> &selectedWalls)
{
	QRectF selectRectF = this->selectionRect->rect();
	QPointF selectRectPos = this->selectionRect->pos();
	selectRectF.translate(selectRectPos);
	QRect selectRect = selectRectF.toRect();

	if (selectRect.width() < 10 || selectRect.height() < 10)
	{
		return false;
	}

	QList<QGraphicsItem*> itemsUnderRectangleArea = items(selectRectF);
	itemsUnderRectangleArea.removeOne(this->selectionRect);

	QList<WallSegmentItem*> wallItemsUnderRectangleArea;
	for (QGraphicsItem* item : itemsUnderRectangleArea)
	{
		WallSegmentItem * wallItem = dynamic_cast<WallSegmentItem*>(item);
		if (wallItem != nullptr)
		{
			wallItemsUnderRectangleArea.push_back(wallItem);
		}
	}

	bool isControlModifierOn = qApp->keyboardModifiers().testFlag(Qt::ControlModifier);

	if (!isControlModifierOn)
	{
		deselectAllWalls();
	}

	for (WallSegmentItem* wallItem : wallItemsUnderRectangleArea)
	{
		if (!(wallItem->selected()))
		{
			wallItem->setSelected(true, true);
		}
	}

	DEBUG_OUT("(%f, %f, %f, %f)  itemsUnderRectangleArea.length = %d", selectRectF.x(), selectRectF.y(), selectRectF.width(),
		selectRectF.height(), wallItemsUnderRectangleArea.length());

	return true;
}

void FloorplanGraphicsScene::createDopplerWallList(std::vector<WallData*> &wallDataList)
{
	clearDopplerWallList();
	for (WallData* wd : wallDataList)
	{
		WallSegmentItem * dopplerWallSegmentItem = new WallSegmentItem(this);
		dopplerWallSegmentItem->setInteractable(false);
		dopplerWallSegmentItem->setStartPosition(wd->startPosition());
		dopplerWallSegmentItem->setEndPosition(wd->endPosition());
		dopplerWallSegmentItem->CreatePolygon();
		dopplerWallList.push_back(dopplerWallSegmentItem);
	}
}

void FloorplanGraphicsScene::clearDopplerWallList()
{
	for (WallSegmentItem* wsi : dopplerWallList)
	{
		this->removeItem(wsi);
	}
	dopplerWallList.clear();
}

void FloorplanGraphicsScene::loadImage(const QPixmap &bgPixmap)
{
	m_backgroundPixmapItem->setPixmap(bgPixmap);
}

void FloorplanGraphicsScene::selectWall(WallSegmentItem * sWsi, bool isSelected, bool isCtrlKey, bool isToRemoveWall)
{
	bool isWsiToBeUnselected = false;
	if (isCtrlKey)
	{
		if (isSelected)
		{
			m_selectedWalls.push_back(sWsi);
			m_wallPropertiesWidget->setSelectedWallSegmentItem(sWsi);
			m_wallPropertiesWidget->updateUIWidgetsDueToWallSegmentItem();
			m_wallPropertiesWidget->setVisible(true);
		}
		else
		{
			isWsiToBeUnselected = true;
		}
	}
	else
	{
		if (isSelected)
		{
			bool doesWallExistInSelection = false;
			for (WallSegmentItem * wsi : m_selectedWalls)
			{
				if (wsi != sWsi)
				{
					wsi->setSelected(false, false);
				}
			}
			m_selectedWalls.clear();
			m_selectedWalls.push_back(sWsi);

			m_wallPropertiesWidget->setSelectedWallSegmentItem(sWsi);
			m_wallPropertiesWidget->updateUIWidgetsDueToWallSegmentItem();
			m_wallPropertiesWidget->setVisible(true);
		}
		else
		{
			isWsiToBeUnselected = true;
		}
	}
	if (isToRemoveWall && isWsiToBeUnselected)
	{
		std::vector<WallSegmentItem *>::iterator vectorEntryPosition = std::find(m_selectedWalls.begin(), m_selectedWalls.end(), sWsi);
		if (vectorEntryPosition != m_selectedWalls.end())
		{
			m_selectedWalls.erase(vectorEntryPosition);
		}
		bool isLastSelectedItem = sWsi == m_wallPropertiesWidget->selectedWallSegmentItem();
		if (isLastSelectedItem)
		{
			bool isThereAnySelectedWallInTheListLeft = (m_selectedWalls.size() > 0);
			if (isThereAnySelectedWallInTheListLeft)
			{
				m_wallPropertiesWidget->setVisible(true);
				m_wallPropertiesWidget->setSelectedWallSegmentItem(m_selectedWalls[0]);
				m_wallPropertiesWidget->updateUIWidgetsDueToWallSegmentItem();
			}
			else
			{
				m_wallPropertiesWidget->setVisible(false);
				m_wallPropertiesWidget->setSelectedWallSegmentItem(nullptr);
			}
		}
	}
}

void FloorplanGraphicsScene::deselectAllWalls()
{
	for (WallSegmentItem * wsi : m_selectedWalls)
	{
		wsi->setSelected(false, false);
	}
	m_selectedWalls.clear();
	m_wallPropertiesWidget->setSelectedWallSegmentItem(nullptr);
	m_wallPropertiesWidget->setVisible(false);
}

std::vector<WallSegmentItem*> &FloorplanGraphicsScene::selectedWalls()
{
	std::vector<WallSegmentItem*> copyVec(this->m_selectedWalls);
	return copyVec;
}

int FloorplanGraphicsScene::selectedWallCount() const
{
	return this->m_selectedWalls.size();
}

void FloorplanGraphicsScene::deleteSelectedWalls()
{
	if (m_selectedWalls.size() == 0)
	{
		return;
	}

	m_wallPropertiesWidget->setSelectedWallSegmentItem(nullptr);
	m_wallPropertiesWidget->setVisible(false);

	std::vector<WallData *> wallsToRemove;
	for (WallSegmentItem * wallItem : m_selectedWalls)
	{
		wallsToRemove.push_back(wallItem->data());
	}

	FloorDesignDocument * document = wallsToRemove[0]->ownerDocument();
	for (WallData * wallData : wallsToRemove)
	{
		document->removeWall(wallData);
	}

	for (WallSegmentItem * wallItem : m_selectedWalls)
	{
		delete wallItem;
	}

	this->m_selectedWalls.clear();

	emit wallsRemoved(&wallsToRemove);
}

void FloorplanGraphicsScene::centerDesign()
{
	FloorDesignDocument * doc = document();
	if (doc != nullptr)
	{
		doc->centerControlls();

		this->resetScene(true);
		this->createSceneGraphicsItemsFromDocumentData(doc);

		emit designedCentered();
	}
}

void FloorplanGraphicsScene::affineTransformPlan()
{
	if (m_selectedWalls.size() > 0)
	{
		WallSegmentItem * wallItem = m_selectedWalls[0];
		WallData * wd = wallItem->data();

		QPointF wd_sp = wd->startPosition();
		QPointF wd_ep = wd->endPosition();
		double wallLen = wd->length();
		QPointF wd_uvec = NormalizeQPointF(wd_ep - wd_sp);

		QPointF &psp = planRegisterLineStartPoint;
		QPointF &pep = planRegisterLineEndPoint;
		QPointF pvec = pep - psp;
		double prll = QPointFLength(pvec);
		QPointF puvec = NormalizeQPointF(pvec);

		double scale = wallLen / prll;

		DEBUG_OUT("!!!!!!!!!! PLACE PLAN: wall item = %s / (%f, %f) ; (%f, %f)", wd->uuid().c_str(), wd_sp.x(), wd_sp.y(), wd_ep.x(), wd_ep.y());
		DEBUG_OUT("(%f, %f) ; (%f, %f)", psp.x(), psp.y(), pep.x(), pep.y());

		qreal angle = qRadiansToDegrees(AngleInRadianBetweenTwoUnitVectors(puvec, wd_uvec));

		QTransform transform;
		transform.translate(wd_sp.x(), wd_sp.y());
		transform.rotate(angle);
		transform.scale(scale, scale);
		transform.translate(-psp.x(), -psp.y());
		m_backgroundPixmapItem->setTransform(transform);

		DEBUG_OUT("******** angle = %f", angle);

		this->removeItem(this->planRegisterLineToAdd);
		delete this->planRegisterLineToAdd;
		this->planRegisterLineToAdd = nullptr;
	}
}

void FloorplanGraphicsScene::resetTransformPlan()
{
	QTransform transform;
	m_backgroundPixmapItem->setTransform(transform);
}

bool FloorplanGraphicsScene::doesPlanRegisterLineExists()
{
	return this->planRegisterLineToAdd != nullptr;
}

bool FloorplanGraphicsScene::doesBackgroundImageExists()
{
	return this->m_backgroundPixmapItem != nullptr;
}

void FloorplanGraphicsScene::setWallPropertiesWidget(QWallPropertiesWidget * wallPropertiesWidget)
{
	m_wallPropertiesWidget = wallPropertiesWidget;
}

FloorDesignDocument * FloorplanGraphicsScene::document()
{
	FloorDesignDocument * doc = nullptr;

	QList<QGraphicsItem *> itemList = items();
	for (auto item : itemList)
	{
		WallSegmentItem * wsi = dynamic_cast<WallSegmentItem*>(item);
		if (wsi != nullptr)
		{
			WallData * wallData = wsi->data();
			if (wallData != nullptr)
			{
				doc = wallData->ownerDocument();
				break;
			}
		}
	}
	return doc;
}

std::vector<WallSegmentItem*> FloorplanGraphicsScene::wallSegmentItems()
{
	std::vector<WallSegmentItem*> wallItems;
	QList<QGraphicsItem *> itemList = items();
	for (auto item : itemList)
	{
		WallSegmentItem * wsi = dynamic_cast<WallSegmentItem*>(item);
		if (wsi != nullptr)
		{
			wallItems.push_back(wsi);
		}
	}
	return wallItems;
}
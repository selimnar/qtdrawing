//#include <qmath.h>
#include <algorithm>

#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QPointF>
#include <QAction>

#include <qmath.h>

#include "ScannedFloorplanGraphicsScene.h"
#include "../CustomWidgets/ScannedFloorplanView.h"

#include "utilities/utilities.h"

const QBrush ScannedFloorplanGraphicsScene::GhostyBrush("#54AAAAAA");

ScannedFloorplanGraphicsScene::ScannedFloorplanGraphicsScene(QObject *parent)
	: QGraphicsScene(parent)
{
	isDrawingSelection = false;

	this->selectionRect = new QGraphicsRectItem(nullptr);

	this->selectionRect->setBrush(GhostyBrush);
	this->selectionRect->setZValue(1000);
	this->addItem(selectionRect);
	this->selectionRect->setVisible(isDrawingSelection);

	m_backgroundPixmapItem = new QGraphicsPixmapItem();
	m_backgroundPixmapItem->setTransformationMode(Qt::SmoothTransformation);
	//QGraphicsPixmapItem * item = new QGraphicsPixmapItem(scaled);
	QMatrix matrix;
	//matrix.scale(0.3, 0.3);
	m_backgroundPixmapItem->setMatrix(matrix);
	//m_backgroundPixmapItem->setOpacity(0.15);
	addItem(m_backgroundPixmapItem);

	QGraphicsEllipseItem * center =
		new QGraphicsEllipseItem(0 - 3, 0 - 3, (3 * 2.0f) - 1.0f, (3 * 2.0f) - 1.0f, nullptr);
	addItem(center);
}

ScannedFloorplanGraphicsScene::~ScannedFloorplanGraphicsScene()
{
}

void ScannedFloorplanGraphicsScene::loadImage(QImage * qImage)
{
	QPixmap bgPixmap = QPixmap::fromImage(*qImage);
	m_backgroundPixmapItem->setPixmap(bgPixmap);
}

bool ScannedFloorplanGraphicsScene::retrieveSelection(QPixmap &subPixmap)
{
	QRect imageRect = m_backgroundPixmapItem->boundingRect().toRect();
	if (imageRect.width() == 0 || imageRect.height() == 0)
	{
		return false;
	}

	QRectF selectRectF = this->selectionRect->rect();
	QPointF selectRectPos = this->selectionRect->pos();
	selectRectF.translate(selectRectPos);
	QRect selectRect = selectRectF.toRect();
	//DEBUG_OUT("rect(%f, %f, %f, %f)", selectRectF.x(), selectRectF.y(), selectRectF.width(), selectRectF.height());
	if (selectRect.width() == 0 || selectRect.height() == 0)
	{
		return false;
	}
	if (!imageRect.contains(selectRect, true))
	{
		return false;
	}
	subPixmap = this->m_backgroundPixmapItem->pixmap().copy(selectRect);
	//m_backgroundPixmapItem->setPixmap(subPixmap);
	return true;
}

void ScannedFloorplanGraphicsScene::resetScene()
{
	QPixmap emptyPixmap;
	m_backgroundPixmapItem->setPixmap(emptyPixmap);
}

void ScannedFloorplanGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if (isDrawingSelection)
	{
		QPointF mousePos = mouseEvent->scenePos();
		if (qApp->keyboardModifiers().testFlag(Qt::ControlModifier))
		{
			float top = qMin(selectionAnchorPos.y(), mousePos.y());
			float left = qMin(selectionAnchorPos.x(), mousePos.x());
			selectionRect->setPos(left, top);
			selectionRect->setRect(0, 0, qAbs(mousePos.x() - selectionAnchorPos.x()),
				qAbs(mousePos.y() - selectionAnchorPos.y()));

			//DEBUG_OUT("---mouseMoveEvent: mousePos(%f, %f)", mousePos.x(), mousePos.y());
		}
	}

	return QGraphicsScene::mouseMoveEvent(mouseEvent);
}

void ScannedFloorplanGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	return QGraphicsScene::mousePressEvent(mouseEvent);
}

void ScannedFloorplanGraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	//QPointF mousePos = mouseEvent->scenePos();
	////DEBUG_OUT("---mousePressEvent: mousePos(%f, %f)", mousePos.x(), mousePos.y());

	QPointF mousePos = mouseEvent->scenePos();
	if (qApp->keyboardModifiers().testFlag(Qt::ControlModifier))
	{
		isDrawingSelection = !isDrawingSelection;
		//isDrawingSelection = true;
		if (isDrawingSelection)
		{
			selectionAnchorPos = mousePos;
			this->selectionRect->setVisible(false);
			//this->selectionRect->setPos(mousePos);

			QRectF rect(mousePos.x(), mousePos.y(), 0, 0);
			this->selectionRect->setRect(rect);
			this->selectionRect->setVisible(true);
		}

		DEBUG_OUT("---mouseMoveEvent: mousePos(%f, %f) ; isDrawingSelection = %d", mousePos.x(), mousePos.y(), isDrawingSelection);
	}

	return QGraphicsScene::mouseReleaseEvent(mouseEvent);
}
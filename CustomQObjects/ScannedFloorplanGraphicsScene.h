#ifndef FLOOR_PLAN_GRAPHICS_SCENE_H
#define FLOOR_PLAN_GRAPHICS_SCENE_H

#include <QApplication>
#include <QGraphicsScene>
#include <QtGui>
#include <QPen>
#include <vector>

class QGraphicsRectItem;

class ScannedFloorplanGraphicsScene : public QGraphicsScene
{
	Q_OBJECT

Q_SIGNALS:
	//void wallAdded(WallData *, bool);

public:
	ScannedFloorplanGraphicsScene(QObject *parent = nullptr);
	~ScannedFloorplanGraphicsScene();

	void resetScene();

	void loadImage(QImage * qImage);

	bool retrieveSelection(QPixmap &subPixmap);

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
	void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

private:
	static const QBrush GhostyBrush;

	QGraphicsRectItem * selectionRect;

	bool isDrawingSelection;
	QPointF selectionAnchorPos;
	QGraphicsPixmapItem * m_backgroundPixmapItem;
};

#endif // FLOOR_PLAN_GRAPHICS_SCENE_H


#ifndef FLOOR_PLAN_GRAPHICS_SCENE_H
#define FLOOR_PLAN_GRAPHICS_SCENE_H

#include <QApplication>
#include <QGraphicsScene>
#include <QtGui>
#include <QPen>
#include <vector>

class WallSegmentItem;
class ControlPointGraphicsItem;
class WallData;
class ControllerData;
class FloorDesignDocument;
class AbstractFloorDesignItem;
class QImageDownloader;
class QWallPropertiesWidget;

enum InteractionMode
{
	View = -1,
	WallSegmentAdd = 0,
	RoomAdd = 1,
	WallSegmentBend = 2,
	WallWidthModify = 3,
	WallBeingDragged = 4,
	SideControlBeingDragged = 5,
	PlanRegisterLineSegmentAdd = 6
};

class FloorplanGraphicsScene : public QGraphicsScene
{
	Q_OBJECT

Q_SIGNALS:
	void wallAdded(WallData *, bool);
	void wallBended(WallData *);
	void wallWidthModified(WallData *);
	void wallSplitted(WallData * firstWD, WallData * secondWD);
	void mergeControlPointToControlPoint(ControllerData *, ControllerData *);
	void attachedToWall(ControllerData *, WallData *);
	void wallSideControlDragEnded(ControllerData *);
	void wallSegmentItemDragEnded(WallData *);
	void wallsRemoved(std::vector<WallData*> *);
	void designedCentered();

public:
	FloorplanGraphicsScene(QObject *parent = nullptr);
	~FloorplanGraphicsScene();

	void resetScene(bool isBackGroundImageToBeLoaded);

	void setInteractionMode(InteractionMode im);
	InteractionMode interactionMode();
	WallData * wallToBend();
	void startWallBending(WallData *);
	void startWallWidthModifying(WallData *);

	void onWallAdded(WallData * wallData, bool isToAddedToCommandHistory);
	void onWallBended(WallData *);
	void onWallWidthModified(WallData *);
	void onWallSplitted(WallData * firstWD, WallData * secondWD, ControllerData * splitBornControllerData);
	void onMergeControlPointToControlPoint(ControllerData * food, ControllerData * swallower);
	void onAttachedToWall(ControllerData * attachedController, WallData * wallPriorToAttachment);

	bool doesWallsHaveWidth();
	void setDoesWallsHaveWidth(bool);

	void createSceneGraphicsItemsFromDocumentData(FloorDesignDocument * );

	AbstractFloorDesignItem * retrieveAbstractFloorItemAt(QPointF position);

	void setControlPointItemThatIsBeingDragged(ControlPointGraphicsItem *);
	void notifyControlPointItemDraggedEnded(ControlPointGraphicsItem * cpgi, bool isToAddedToCommandHistory);

	void setWallSegmentItemThatIsBeingDragged(WallSegmentItem *);
	void notifyWallSegmentItemDraggedEnded(WallSegmentItem *);

	void clearControlPointGraphicItems();
	void clearWallSegmentItems();
	void resizeControlPointGraphicItems(qreal scale);

	void setPenOfDopplerWallsItems(QPen pen, QBrush brush);
	void createDopplerWallItemListDueToWallDrag(WallData * wallData);
	void createDopplerWallItemListDueToControllerDrag(ControllerData * controllerData);
	void clearDopplerWallList();

	void loadImage(const QPixmap &bgPixmap);

	void selectWall(WallSegmentItem * wsi, bool isSelected, bool isCtrlKey, bool isToRemoveWall);
	void deselectAllWalls();
	std::vector<WallSegmentItem*> &selectedWalls();
	int selectedWallCount() const;
	void deleteSelectedWalls();

	void centerDesign();
	void affineTransformPlan();
	void resetTransformPlan();
	bool doesPlanRegisterLineExists();
	bool doesBackgroundImageExists();

	void setWallPropertiesWidget(QWallPropertiesWidget * wallPropertiesWidget);

	FloorDesignDocument * document();
	std::vector<WallSegmentItem*> wallSegmentItems();

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
	void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;
	void keyReleaseEvent(QKeyEvent *event) override;

private:
	static const QBrush GhostyBrush;
	static const qreal CenterPointSize;

	bool isDrawingSelection;
	QPointF selectionAnchorPos;
	QGraphicsRectItem * selectionRect;

	QWallPropertiesWidget * m_wallPropertiesWidget;

	bool m_doesWallsHaveWidth;

	InteractionMode m_interactionMode;

	WallSegmentItem * wallSegmentToAdd;
	WallData * m_wallToModify;
	QGraphicsPathItem * planRegisterLineToAdd;
	QPointF planRegisterLineStartPoint;
	QPointF planRegisterLineEndPoint;

	WallSegmentItem * m_wallSegmentThatIsBeingDragged;
	ControlPointGraphicsItem * m_controlPointGraphicsItemThatIsBeingDraged;

	std::vector<WallSegmentItem *> dopplerWallList;

	QGraphicsPixmapItem * m_backgroundPixmapItem;

	std::vector<WallSegmentItem*> m_selectedWalls;

	void setAllFloorItemsInteractable(bool);
	void createDopplerWallList(std::vector<WallData*> &wallDataList);
	void createDopplerWallDataListDueToControllerDrag(ControllerData* controllerData, std::vector<WallData*> &wallDataList);

	bool retrieveSelection(std::vector<WallData*> &selectedWalls);
};

#endif // FLOOR_PLAN_GRAPHICS_SCENE_H


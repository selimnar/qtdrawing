#ifndef FLOOR_PLAN_DESIGNER_MAINWINDOW_H
#define FLOOR_PLAN_DESIGNER_MAINWINDOW_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QGraphicsItem>

class WallSegmentItem;
class WallData;
class ControllerData;
class FloorplanGraphicsScene;
class FloorplanGraphicsView;
class FloorDesignDocument;
class CommandManager;
class CreateLoadProjectForm;
class Project;
class ScannedFloorDesignSelectorForm;

namespace Ui {
	class FloorPlanDesigner;
}

class FloorPlanDesigner : public QDialog
{
	Q_OBJECT
public:
	explicit FloorPlanDesigner(Project * iProject, std::string inFileToOpen, QWidget *parent = nullptr);
	explicit FloorPlanDesigner(QWidget* parent = nullptr);
	~FloorPlanDesigner();

	void connectWallRemoveSocket(WallData * wallData);
	void disconnectWallRemoveSocket(WallData * wallData);

	FloorplanGraphicsScene * floorplanGraphicsScene();
	FloorDesignDocument * document();
	FloorplanGraphicsView * view();

	void undo();
	void redo();

protected:
	void keyPressEvent(QKeyEvent * event) override;
	void closeEvent(QCloseEvent *event) override;

private:
	Ui::FloorPlanDesigner *ui;
	FloorplanGraphicsScene *scene;
	QGraphicsEllipseItem *ellipse;
	QGraphicsRectItem *rectangle;
	QGraphicsPathItem *pathItem;

	WallSegmentItem *wallSegmentItem;

	FloorDesignDocument *m_document;

	CommandManager * commandManager;

	Project * m_project;

	ScannedFloorDesignSelectorForm * scannedFloorDesignSelectorForm;

	std::string fileToOpen;

	void openFloorDesignDocument(QString fileName);

	void Initialize();
	void InitializeSlots();

private slots:
	void on_rotationSlider_valueChanged(int);
	void on_saveButton_pressed();
	void on_createWallButton_pressed();
	void on_addDoorPushButton_pressed();
	void on_loadButton_pressed();
	void on_undoButton_pressed();
	void on_redoButton_pressed();
	void on_selectFromScannedDesignButton_pressed();
	void on_exitPushButton_pressed();
	void on_lightAngleHorizontalSlider_valueChanged(int value);

	void on_wallAdded(WallData * wallData, bool isToAddedToCommandHistory);
	void on_wallRemoved(WallData *);
	void on_wallsRemoved(std::vector<WallData*> * wallDataList);
	void on_designedCentered();
	void on_WallBended(WallData *);
	void on_WallWidthModified(WallData *);
	void on_WallSplitted(WallData * firstWD, WallData * secondWD);
	void on_MergeControlPointToControlPoint(ControllerData * food, ControllerData * swallower);
	void on_AttachedToWall(ControllerData * attachedController, WallData * wallPriorToAttachment);
	void on_WallSideControlDragEnded(ControllerData *);
	void on_WallSegmentItemDragEnded(WallData *);
	void on_wallSegmentItemPropertiesModifiedViaUIPanel(WallData *);
	void on_scenePropertiesModifiedViaUIPanel();
};

#endif // FLOOR_PLAN_DESIGNER_MAINWINDOW_H
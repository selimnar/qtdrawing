#include <qmath.h>

#include <QGraphicsSceneMouseEvent>
#include <QGraphicsItem>
#include <QGraphicsTextItem>
#include <QPointF>
#include <QPixmap>
#include <QAction>
#include <QMenu>

#include "FloorPlanDesigner.h"

#include "CustomQObjects/FloorplanGraphicsScene.h"
#include "CustomWidgets/FloorplanGraphicsView.h"

#include "ControlPointGraphicsItem.h"

#include "WallSegmentItem.h"

#include "utilities/utilities.h"
#include "utilities/PathUtilities.h"

#include "clipper/clipper_utilities.h"

#include "Data/FloorDesignDocument.h"
#include "data/WallData.h"
#include "data/ControllerData.h"
#include "data/ActionData.h"

#include "Command/FileBasedStateCommand.h"

using namespace std;

class WallSegmentItem_static_constructor {
	friend class constructor;

	struct constructor {
		constructor() { /* do some constructing here � */ }
	};

	static constructor cons;
};

// C++ needs to define static members externally.
WallSegmentItem_static_constructor::constructor WallSegmentItem_static_constructor::cons;

const qreal WallSegmentItem::ControlpointRatio = 0.2;
const qreal WallSegmentItem::QPathToClipperLibPathScaler = 10000;
const qreal WallSegmentItem::ClipperLibPathToQPathScaler = 1 / WallSegmentItem::QPathToClipperLibPathScaler;
const int WallSegmentItem::BezierStepCount = 20;
const qreal WallSegmentItem::DefaultFloorWallWidth = 15;
const qreal WallSegmentItem::DefaultWallWidth = 25;
const qreal WallSegmentItem::DefaultWallHeight = 260;
const int WallSegmentItem::MeasurementFontSize = 20;

const qreal WallSegmentItem::ReferencePathWidth = 2;

const QBrush WallSegmentItem::BlueBrush("#04A9F5");
const QBrush WallSegmentItem::BlackBrush("#000000");//(Qt::black);
const QBrush WallSegmentItem::TransparentBrush("#00000000");
const QBrush WallSegmentItem::MergeStateBrush("#9B9B3276");
const QBrush WallSegmentItem::GhostyBrush("#77777777");

const QPen WallSegmentItem::BluePen(QColor::fromRgba(qRgba(0x04, 0xA9, 0xF5, 255)), ReferencePathWidth);
const QPen WallSegmentItem::BlackPen(Qt::black, ReferencePathWidth);	// QColor::fromRgba(qRgba(0, 0, 0, 255))
const QPen WallSegmentItem::LightBlackPen(QColor::fromRgba(qRgba(0, 0, 0, 70)), ReferencePathWidth);	// QColor::fromRgba(qRgba(0, 0, 0, 255))
const QPen WallSegmentItem::MergeStatePen(QColor::fromRgba(qRgba(0x9B, 0x9B, 0x32, 0x76)), ReferencePathWidth);	// QColor::fromRgba(qRgba(0, 0, 0, 255))
const QPen WallSegmentItem::TransparentPen(QColor::fromRgba(qRgba(0x00, 0x00, 0x00, 0x00)), ReferencePathWidth);
const QPen WallSegmentItem::GhostyPen(QColor::fromRgba(qRgba(0x77, 0x77, 0x77, 0x77)), ReferencePathWidth);
const QPen WallSegmentItem::MergibilityIndicationPenWithWallWidthCase(
	QColor::fromRgba(qRgba(0xAA, 0xAA, 0x22, 0xFF)), ReferencePathWidth);
const QPen WallSegmentItem::SelectedPen(QColor::fromRgba(qRgba(0x00, 0xAA, 0x00, 0xFF)), 5);

const qreal WallSegmentItem::WallLengthLimit = 1.0;

WallSegmentItem::WallSegmentItem(FloorplanGraphicsScene * scene, bool isInteractable, WallData * wallData)
	: QGraphicsPathItem()
{
	this->Initialize(scene, isInteractable, wallData);
}

WallSegmentItem::WallSegmentItem(FloorplanGraphicsScene * scene)
	: QGraphicsPathItem()
{
	this->Initialize(scene, false);
}

void WallSegmentItem::Initialize(FloorplanGraphicsScene * scene, bool isInteractable, WallData * wallData)
{
	m_wallGenericPolygonGenerationData.endControlGenerationCase = CurvedWallEndGenerationCase::CNoGenCase;
	m_wallGenericPolygonGenerationData.startControlGenerationCase = CurvedWallEndGenerationCase::CNoGenCase;
	m_wallGenericPolygonGenerationData.wallData = wallData;
	m_doesCreatingMergedPolygon = true;

	//QString imagePath = "C:/Users/selimnar/Documents/QTDrawing/images/wall.jpg";
	//QPixmap pixmap(imagePath);
	//texturedBrush = new QBrush(Qt::white, pixmap);

	referencePathItem = new QGraphicsPathItem(this);
	referencePathItem->setPen(QPen(Qt::blue));

	measurementPathItem = new QGraphicsPathItem(nullptr);
	measurementPathItem->setPen(QPen(Qt::blue));
	measurementPathItem->setBrush(Qt::NoBrush);
	//measurementPathItem->setFlags(isInteractable ? (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges) : 0);

	measurementTextItem = new QGraphicsTextItem(nullptr);
	QFont font;
	font.setPixelSize(MeasurementFontSize);
	font.setBold(true);
	font.setFamily("Calibri");
	measurementTextItem->setDefaultTextColor(Qt::blue);
	measurementTextItem->setFont(font);

	isDragged = false;
	isDraggedCancelled = false;
	m_isSelected = false;

	if (wallData != nullptr)
	{
		m_data = wallData;
		m_data->setRelatedGraphicsItem(this);
	}
	else
	{
		m_data = new WallData();
		m_data->setRelatedGraphicsItem(this);

		m_data->setType(WallType::Line);
		m_data->setWidth(DefaultWallWidth);

		ControllerData * startControllerData = new ControllerData();
		m_data->bindSelfToControlledItem(WallSegmentControlledPart::Start, startControllerData);

		ControllerData * endControllerData = new ControllerData();
		m_data->bindSelfToControlledItem(WallSegmentControlledPart::End, endControllerData);
	}
	scene->addItem(this);

	this->m_relativeDragPosition = QPointF(0, 0);

	setAcceptHoverEvents(true);

	setInteractable(isInteractable);
	//this->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges);
}

WallSegmentItem::~WallSegmentItem()
{
	delete measurementPathItem;
	delete measurementTextItem;
}

void WallSegmentItem::setInteractable(bool isInteractable)
{
	//this->setFlags(isInteractable ? (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges) : 0);
	this->setFlags(isInteractable ? (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemSendsGeometryChanges) : 0);
}

QList<QAction*> WallSegmentItem::getContextMenuActions()
{
	QList<QAction*> actions;
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	if (scene->interactionMode() != InteractionMode::View)
	{
		return actions;
	}

	ActionData * actionData = new ActionData();
	actionData->setItem(this);
	QVariant variantActionData = qVariantFromValue((void *)actionData);


	QAction * removeAction = new QAction("Sil");
	removeAction->setData(variantActionData);
	m_data->connect(removeAction, &QAction::triggered, m_data, &WallData::removeFloorDesignItem);

	QAction * bendAction = new QAction("Egrilt");
	bendAction->setData(variantActionData);
	m_data->connect(bendAction, &QAction::triggered, m_data, &WallData::startBending);

	//QAction * modifyWidthAction = new QAction("Modify width");
	//modifyWidthAction->setData(variantActionData);
	//m_data->connect(modifyWidthAction, &QAction::triggered, m_data, &WallData::startWidthModifying);

	QAction * splitAction = new QAction("Split");
	splitAction->setData(variantActionData);
	m_data->connect(splitAction, &QAction::triggered, m_data, &WallData::split);

	actions.append(removeAction);
	actions.append(bendAction);
	//if (scene->doesWallsHaveWidth())
	//{
	//	actions.append(modifyWidthAction);
	//}
	if (m_data->type() == WallType::Line)
	{
		actions.append(splitAction);
	}
	//actions.append(addWallAction);

	return actions;
}

void WallSegmentItem::CreatePolygon()
{
	//CreatePolygon(false);
	CreatePolygon(true);
	this->setPen(((m_isSelected) ? SelectedPen : Qt::NoPen));
}

void WallSegmentItem::CreatePolygon(bool drawReferenceAndOutlines)
{
	QPainterPath bezierCurvePath;
	QPainterPath referencePath;

	qreal distanceBetweenStartEnd = QPointFLength(m_data->startPosition() - m_data->endPosition());
	//DEBUG_OUT("------- distanceBetweenStartEnd = %f", distanceBetweenStartEnd);
	// if distance between start and end point are too close do not create a polygon
	if (distanceBetweenStartEnd < WallLengthLimit)
	{
		this->setPath(bezierCurvePath);
		return;
	}

	CreateBasePolygon(referencePath, bezierCurvePath);

	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	if (drawReferenceAndOutlines || !scene->doesWallsHaveWidth())
	{
		referencePathItem->setPath(referencePath);
	}
	else
	{
		QPainterPath emptyPath;
		referencePathItem->setPath(emptyPath);
	}

	this->setPen(Qt::NoPen);
	this->setBrush(scene->doesWallsHaveWidth() ? BlackBrush : TransparentBrush);
	referencePathItem->setPen(BlackPen);
	//this->setBrush(*texturedBrush);
}

void WallSegmentItem::CreateBasePolygon(QPainterPath &referencePath, QPainterPath &bezierCurvePath)
{
	bool isUnconnectedWall = m_data->startControllerData()->controledItemCount() == 1 &&
		m_data->endControllerData()->controledItemCount() == 1;

	if (isUnconnectedWall || !m_doesCreatingMergedPolygon)
	{
		if (m_data->type() == WallType::Curved)
		{
			CreateBezierPolygon(this->m_data, bezierCurvePath, referencePath, m_bezierCurveClipperPath);
		}
		else
		{
			CreateLinePolygon(this->m_data, bezierCurvePath, referencePath, m_bezierCurveClipperPath);
		}
	}
	else
	{
		MergeWallPolygons::CreateFinalBezierPolygon(m_data, bezierCurvePath, referencePath, this->m_relativeDragPosition, this->scene());
	}

	this->setPath(bezierCurvePath);
}

void WallSegmentItem::showMeasurementLine(bool visibility)
{
	if (visibility)
	{
		CreateMeasurementPath();
	}
	else
	{
		//this->measurementPathItem->setPath(QPainterPath());
		this->scene()->removeItem(this->measurementPathItem);
		this->scene()->removeItem(this->measurementTextItem);
		//this->measurementPathItem->setVisible(false);
		//this->measurementTextItem->setVisible(false);
	}
}

void WallSegmentItem::CreateMeasurementPath()
{
	QPointF wallVector = m_data->startPosition() - m_data->endPosition();
	qreal distanceBetweenStartEnd = QPointFLength(wallVector);
	//DEBUG_OUT("------- distanceBetweenStartEnd = %f", distanceBetweenStartEnd);
	// if distance between start and end point are too close do not create a polygon
	if (distanceBetweenStartEnd < WallLengthLimit)
	{
		//this->setPath(bezierCurvePath);
		return;
	}

	m_data->updateNormal();

	qreal measurementLineShifter = 10;

	QPointF normal = this->m_data->normal();
	qreal halfWidth = m_data->width() * 0.5;
	qreal measurementLineShift = halfWidth + measurementLineShifter;
	QPointF sideShiftVec  = normal * halfWidth;
	QPointF shiftVec = normal * measurementLineShift;
	QPointF startPos = m_data->startPosition();
	QPointF endPos = m_data->endPosition();

	QPointF sideStartPos = startPos + sideShiftVec;
	QPointF sideEndPos = endPos + sideShiftVec;

	QPointF shiftedStartPos = startPos + shiftVec;
	QPointF shiftedEndPos = endPos + shiftVec;

	QPointF wallCenter = (m_data->startPosition() + m_data->endPosition()) * 0.5;

	QPointF wallDirection = NormalizeQPointF(wallVector);
	qreal angle = qRadiansToDegrees(qAtan2(wallDirection.y(), wallDirection.x()));
	qreal labelAngle = (angle < -90 || angle > 90) ? (angle + 180) : angle;
	//QPointF labelVector(qSin(labelAngle), qSin(labelAngle));

	if (!scene()->items().contains(measurementTextItem))
	{
		scene()->addItem(measurementTextItem);
	}
	//this->measurementTextItem->setVisible(true);
	float numberInMeter = convertToMeter(distanceBetweenStartEnd);
	//measurementTextItem->setPlainText(QString::number(numberInMeter) + " // " + QString::number(angle));
	measurementTextItem->setPlainText(QString::number(numberInMeter) + " m");

	QRectF br = measurementTextItem->boundingRect();
	QPointF offset = br.center();

	//labelPos +  br.width
	QPointF labelPos = wallCenter + shiftVec;

	QTransform transform;
	transform.translate(labelPos.x(), labelPos.y());
	transform.rotate(labelAngle);
	transform.translate(-offset.x(), -offset.y());
	measurementTextItem->setTransform(transform);

	QPainterPath measurementpath;
	measurementpath.moveTo(sideStartPos);
	measurementpath.lineTo(shiftedStartPos);
	QPointF labelS1 = shiftedStartPos - (wallDirection * ((distanceBetweenStartEnd - br.width()) * 0.5));
	measurementpath.lineTo(labelS1);
	QPointF labelS2 = labelS1 - (wallDirection * br.width());
	measurementpath.moveTo(labelS2);
	measurementpath.lineTo(shiftedEndPos);
	measurementpath.lineTo(sideEndPos);
	this->measurementPathItem->setPath(measurementpath);
	if (!scene()->items().contains(measurementPathItem))
	{
		scene()->addItem(measurementPathItem);
	}
	//scene()->addItem(this->measurementPathItem);
	//this->measurementPathItem->setVisible(true);

}

WallData * WallSegmentItem::data()
{
	return m_data;
}

void WallSegmentItem::setData(WallData *val)
{
	m_data = val;
	m_wallGenericPolygonGenerationData.wallData = this->m_data;
	val->setRelatedGraphicsItem(this);
	CreatePolygon();
}

WallType WallSegmentItem::getWallType() { return m_data->type(); }

void WallSegmentItem::setWallType(WallType wallType_)
{
	m_data->setType(wallType_);
	CreatePolygon();
}

qreal WallSegmentItem::width() { return m_data->width(); }

void WallSegmentItem::setWidth(qreal val)
{
	m_data->setWidth(val);
	CreatePolygon();
}

qreal WallSegmentItem::height() { return m_data->height(); }

void WallSegmentItem::setHeight(qreal val) { m_data->setHeight(val); }

QPointF WallSegmentItem::getStartPosition() { return m_data->startPosition(); }

void WallSegmentItem::setStartPosition(QPointF val)
{
	m_data->setStartPosition(val);
	CreatePolygon();
}

QPointF WallSegmentItem::getEndPosition() { return m_data->endPosition(); }

void WallSegmentItem::setEndPosition(QPointF val)
{
	m_data->setEndPosition(val);
	m_data->UpdateMergedPolygon();
}

QPointF WallSegmentItem::getControlPosition() { return m_data->controlPosition(); }

void WallSegmentItem::setControlPosition(QPointF val)
{
	m_data->setControlPosition(val);
	CreatePolygon();
}

void WallSegmentItem::setWallData(QPointF startPoint, QPointF endPoint,
	QPointF mainControlPoint, qreal wallWidth, qreal wallHeight, WallType wallType)
{
	m_data->setData(startPoint, endPoint, mainControlPoint, wallWidth, wallHeight, wallType);
	CreatePolygon();
}

ClipperLib::Path WallSegmentItem::bezierCurveClipperPath()
{
	return m_bezierCurveClipperPath;
}

QPointF WallSegmentItem::relativeDragPosition()
{
	return this->m_relativeDragPosition;
}

void WallSegmentItem::setReferenceItemPen(QPen pen)
{
	this->referencePathItem->setPen(pen);
}

bool WallSegmentItem::isValid()
{
	int elementCount = this->path().elementCount();
	//DEBUG_OUT("------- elementCount = %d", elementCount);
	return elementCount > 0;
}

void WallSegmentItem::cancelDrag()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	scene->clearDopplerWallList();

	this->setPos(this->m_wallDragInitializeData.dragInitPos);
	this->setStartPosition(this->m_wallDragInitializeData.startPoint);
	this->setEndPosition(this->m_wallDragInitializeData.endPoint);
	this->setControlPosition(this->m_wallDragInitializeData.controlPoint);
	isDraggedCancelled = true;

	DEBUG_OUT("warning: WallSegmentItem::cancelDrag");
}

WallGenericPolygonGenerationData& WallSegmentItem::wallGenericPolygonGenerationData()
{
	return m_wallGenericPolygonGenerationData;
}

bool WallSegmentItem::doesCreatingMergedPolygon()
{
	return m_doesCreatingMergedPolygon;
}

void WallSegmentItem::setDoesCreatingMergedPolygon(bool isMerged)
{
	m_doesCreatingMergedPolygon = isMerged;
}

void WallSegmentItem::setDoesCreatingMergedPolygonForAllRelatedWall(bool isMerged)
{
	setDoesCreatingMergedPolygonForAllSideControllerRelatedWall(this->m_data->startControllerData(), isMerged);
	setDoesCreatingMergedPolygonForAllSideControllerRelatedWall(this->m_data->endControllerData(), isMerged);
}

void SetPenOfWallsRelatedToModifiedController(ControllerData* controllerData, QPen pen)
{
	std::vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData* cmd : controlledList)
	{
		cmd->controlledWall->relatedGraphicsItem()->setReferenceItemPen(pen);
	}
}

void WallSegmentItem::SetPenOfModifiedWalls(WallData * wallData, QPen pen)
{
	SetPenOfWallsRelatedToModifiedController(wallData->startControllerData(), pen);
	SetPenOfWallsRelatedToModifiedController(wallData->endControllerData(), pen);
}

bool WallSegmentItem::createWallJunctionsFromIntersection()
{
	FloorDesignDocument * document = m_data->ownerDocument();
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	return document->createWallJunctionsFromIntersection(scene, this->m_data);
}

void WallSegmentItem::removeSideControllerGraphicItems()
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());

	if (this->m_data->startControllerData()->relatedGraphicsItem() != nullptr)
	{
		scene->removeItem(this->m_data->startControllerData()->relatedGraphicsItem());
		this->m_data->startControllerData()->setRelatedGraphicsItem(nullptr);
	}

	if (this->m_data->endControllerData()->relatedGraphicsItem() != nullptr)
	{
		scene->removeItem(this->m_data->endControllerData()->relatedGraphicsItem());
		this->m_data->endControllerData()->setRelatedGraphicsItem(nullptr);
	}
}

void WallSegmentItem::DetachFromOneOrTwoLinkedWalls(WallData * parralelWall1, WallData * parralelWall2, ControllerData * centralController)
{
	//ControllerData * parallelWall1OtherController = parralelWall1->getOtherController(centralController);
	ControllerData * parallelWall2OtherController = parralelWall2->getOtherController(centralController);

	parralelWall2->detachOneControlledItemSide(parallelWall2OtherController);
	parralelWall2->removeSelf(false, true);

	if (parralelWall1 != nullptr)
	{
		parralelWall1->detachOneSideFromOneControlAndBindToControlledItem(centralController, parallelWall2OtherController);
	}
}

bool WallSegmentItem::checkAndDoReattachAfterMoveDrag(vector<ControllerAndOneWallPair *> &parralelWallList, ControllerData * sideController)
{
	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	FloorDesignDocument * document = this->m_data->ownerDocument();
	qreal dotProduct = 0;

	ControllerData * headControllerData = parralelWallList[0]->controllerData;
	ControllerData * lastControllerData = parralelWallList.back()->controllerData;
	parralelWallList[0]->wallData = nullptr;

	QPointF currentSideControllerPos = sideController->position();
	QPointF listHeadControllerPosition = headControllerData->position();
	QPointF listTailControllerPosition = lastControllerData->position();

	// if dot porduct is positive then this line is moved beyond tail controller position
	dotProduct = DotProduct(currentSideControllerPos - listHeadControllerPosition,
		currentSideControllerPos - listTailControllerPosition);
	if (dotProduct > 0)
	{
		//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - 5, 0 - 5, (5 * 2.0f) - 1.0f, (5 * 2.0f) - 1.0f, nullptr);
		//intersect->setBrush(QBrush(Qt::red));
		//scene->addItem(intersect);
		//intersect->setPos(lastControllerData->position());

		WallData * dragBornWallData = new WallData();
		floorplanDesigner->connectWallRemoveSocket(dragBornWallData);
		document->addWall(dragBornWallData);

		dragBornWallData->setType(WallType::Line);
		dragBornWallData->setWidth(DefaultWallWidth);
		// existing walls old end controller will be split born wall new end controller
		// so add split born walldata to its ControllerMappingData list
		dragBornWallData->bindSelfToControlledItem(WallSegmentControlledPart::Start, lastControllerData);
		dragBornWallData->bindSelfToControlledItem(WallSegmentControlledPart::End, sideController);
		new WallSegmentItem(scene, true, dragBornWallData);
		dragBornWallData->relatedGraphicsItem()->CreatePolygon();
		return true;
	}
	else // if side is moved on other walls than apply attach to wall for that side controller
	{
		for (ControllerAndOneWallPair * caowp : parralelWallList)
		{
			//ControllerData * controllerData = caowp->controllerData;
			//QPointF controllerPos = controllerData->position();

			//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - 5, 0 - 5, (5 * 2.0f) - 1.0f, (5 * 2.0f) - 1.0f, nullptr);
			//intersect->setBrush(QBrush(Qt::red));
			//scene->addItem(intersect);
			//intersect->setPos(controllerPos);


			WallData * wallData = caowp->wallData;
			if (wallData == nullptr)
			{
				continue;
			}

			QPointF currentToWallStart = currentSideControllerPos - wallData->startPosition();
			QPointF currentToWallend = currentSideControllerPos - wallData->endPosition();
			dotProduct = DotProduct(currentToWallStart, currentToWallend);

			if (dotProduct < 0)
			{
				wallData->attachController(sideController, false);
				break;
			}
		}
		return true;
	}
	return false;
}

void WallSegmentItem::checkAndDoReattachAfterMoveDragForBothLinkedWallList(vector<ControllerAndOneWallPair *> &parralelWallList1,
	vector<ControllerAndOneWallPair *> &parralelWallList2, WallData * parralelWall1, WallData * parralelWall2,
	ControllerData * sideController, QPointF initialSideControllerPosition)
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());

	if (parralelWallList1.size() == 0 && parralelWallList2.size() == 0)
	{
		return;
	}
	QPointF currentSideControllerPos = sideController->position();

	ControllerData * controllerData1 = (parralelWallList1.size() == 0) ? nullptr : parralelWallList1[0]->controllerData;
	ControllerData * controllerData2 = (parralelWallList2.size() == 0) ? nullptr : parralelWallList2[0]->controllerData;

	QPointF controller1CurrentPos = (controllerData1 != nullptr) ? controllerData1->position() : currentSideControllerPos;
	QPointF controller2CurrentPos = (controllerData2 != nullptr) ? controllerData2->position() : currentSideControllerPos;

	//ControllerData * controllerData1 = parralelWallList1[0]->controllerData;
	//ControllerData * controllerData2 = parralelWallList2[0]->controllerData;

	//QGraphicsTextItem * textItem = new QGraphicsTextItem("c1"); // +QString::number(index));
	//QFont font;
	//font.setPixelSize(18);
	//font.setBold(true);
	//font.setFamily("Calibri");
	//textItem->setDefaultTextColor(Qt::red);
	//textItem->setFont(font);
	//scene->addItem(textItem);
	//textItem->setPos(controller1CurrentPos);


	//textItem = new QGraphicsTextItem("c2"); // +QString::number(index));
	//font.setPixelSize(18);
	//font.setBold(true);
	//font.setFamily("Calibri");
	//textItem->setDefaultTextColor(Qt::red);
	//textItem->setFont(font);
	//scene->addItem(textItem);
	//textItem->setPos(controller2CurrentPos);

	//QGraphicsEllipseItem * intersect = new QGraphicsEllipseItem(0 - 5, 0 - 5, (5 * 2.0f) - 1.0f, (5 * 2.0f) - 1.0f, nullptr);
	//intersect->setBrush(QBrush(Qt::red));
	//scene->addItem(intersect);
	//intersect->setPos(controllerData2->position());

	QPointF currentToListHead1 = currentSideControllerPos - controller1CurrentPos;
	QPointF currentToListHead2 = currentSideControllerPos - controller2CurrentPos;

	// if wall side controller is attached between two control points
	if (controllerData1 != nullptr && controllerData2 != nullptr)
	{
		qreal dotProduct = DotProduct(currentToListHead1, currentToListHead2);
		DEBUG_OUT("************** LINKED TWO WAYS CASE:  dotProduct = %f", dotProduct);
		// if dotProduct is positive then it is moved outside of current linked controllers
		if (dotProduct > 0)
		{
			DetachFromOneOrTwoLinkedWalls(parralelWall1, parralelWall2, sideController);

			QPointF listHead1ToInitial = controller1CurrentPos - initialSideControllerPosition;
			qreal dotProduct = DotProduct(listHead1ToInitial, currentToListHead1);
			// if dot product is positive than wall is moved to parrallelWall1 and parralelWallList1 direction
			checkAndDoReattachAfterMoveDrag(((dotProduct > 0) ? parralelWallList1 : parralelWallList2), sideController);
		}
	}
	else
	{
		size_t oneSidelinkedControllerCount = (controllerData1 != nullptr) ? parralelWallList1.size() : parralelWallList2.size();
		ControllerData * linkedController = (controllerData1 != nullptr) ? controllerData1 : controllerData2;
		WallData * linkedWall = (controllerData1 != nullptr) ? parralelWall1 : parralelWall2;
		QPointF currentToLinkedController = currentSideControllerPos - linkedController->position();
		QPointF initialToLinkedController = initialSideControllerPosition - linkedController->position();
		qreal dotProduct = DotProduct(currentToLinkedController, initialToLinkedController);
		DEBUG_OUT("************** LINKED ONE WAY CASE:  dotProduct = %f", dotProduct);
		// if controller switched to the other side of the linked controller and
		// there is more than two other controller in the list than it must be detached and attached between that controllers
		if ((oneSidelinkedControllerCount > 1) && dotProduct < 0)
		{
			//implement a new method to detach for one way case just like "SpecialCase3DetachOnMouseButtonRelease"
			//linkedController
			DetachFromOneOrTwoLinkedWalls(nullptr, linkedWall, sideController);
			checkAndDoReattachAfterMoveDrag(((controllerData1 != nullptr) ? parralelWallList1 : parralelWallList2), sideController);
		}
	}
}

void WallSegmentItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	// store start and end postions of the wall to use during drag
	m_wallDragInitializeData.dragInitPos = pos();
	m_wallDragInitializeData.startPoint = m_data->startPosition();
	m_wallDragInitializeData.endPoint = m_data->endPosition();
	m_wallDragInitializeData.controlPoint = m_data->controlPosition();
	m_wallDragInitializeData.isAtOriginalState = true;

	m_wallDragInitializeData.isCutFromSelfStartParalledWall = false;
	m_wallDragInitializeData.isCutFromSelfEndParalledWall = false;

	m_wallDragInitializeData.startParralelWall1 = nullptr;
	m_wallDragInitializeData.startParralelWall2 = nullptr;
	m_wallDragInitializeData.selfStartParralelWall = nullptr;
	m_wallDragInitializeData.endParralelWall1 = nullptr;
	m_wallDragInitializeData.endParralelWall2 = nullptr;
	m_wallDragInitializeData.selfEndParralelWall = nullptr;

	m_wallDragInitializeData.startParralelWallList1.clear();
	m_wallDragInitializeData.startParralelWallList2.clear();
	m_wallDragInitializeData.endParralelWallList1.clear();
	m_wallDragInitializeData.endParralelWallList2.clear();

	bool canMove = m_data->isEligibleForMove(m_wallDragInitializeData.startParralelWall1, m_wallDragInitializeData.startParralelWall2,
		m_wallDragInitializeData.endParralelWall1, m_wallDragInitializeData.endParralelWall2,
		m_wallDragInitializeData.selfStartParralelWall, m_wallDragInitializeData.selfEndParralelWall,
		m_wallDragInitializeData.startSpecialWallMoveDragCase, m_wallDragInitializeData.endSpecialWallMoveDragCase);

	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());


	if (m_wallDragInitializeData.startParralelWall1 != nullptr)
	{
		m_wallDragInitializeData.startParralelWallList1 = WallData::RetrieveStraightWallListAlongOneSide(
			m_data->startControllerData(), m_wallDragInitializeData.startParralelWall1);
		//DEBUG_OUT("m_wallDragInitializeData.startParralelWallList1.size = %d", m_wallDragInitializeData.startParralelWallList1.size());
	}

	if (m_wallDragInitializeData.startParralelWall2 != nullptr)
	{
		m_wallDragInitializeData.startParralelWallList2 = WallData::RetrieveStraightWallListAlongOneSide(
			m_data->startControllerData(), m_wallDragInitializeData.startParralelWall2);
	}

	if (m_wallDragInitializeData.endParralelWall1 != nullptr)
	{
		m_wallDragInitializeData.endParralelWallList1 = WallData::RetrieveStraightWallListAlongOneSide(
			m_data->endControllerData(), m_wallDragInitializeData.endParralelWall1);
	}

	if (m_wallDragInitializeData.endParralelWall2 != nullptr)
	{
		m_wallDragInitializeData.endParralelWallList2 = WallData::RetrieveStraightWallListAlongOneSide(
			m_data->endControllerData(), m_wallDragInitializeData.endParralelWall2);
	}

	//DEBUG_OUT("WallSegmentItem::mousePressEvent");
	this->m_data->updateNormal();
	QGraphicsItem::mousePressEvent(event);
}

void WallSegmentItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//DEBUG_OUT("WallSegmentItem::mouseReleaseEvent");
	if (isDragged && !isDraggedCancelled)
	{
		FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());
		FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
		FloorDesignDocument * document = this->m_data->ownerDocument();
		ControllerData * sideController = nullptr;
		QPointF currentSideControllerPos = QPointF(0, 0);
		qreal dotProduct = 0;

		checkAndDoReattachAfterMoveDragForBothLinkedWallList(m_wallDragInitializeData.startParralelWallList1,
			m_wallDragInitializeData.startParralelWallList2, m_wallDragInitializeData.startParralelWall1,
			m_wallDragInitializeData.startParralelWall2, this->m_data->startControllerData(),
			m_wallDragInitializeData.startPoint);

		checkAndDoReattachAfterMoveDragForBothLinkedWallList(m_wallDragInitializeData.endParralelWallList1,
			m_wallDragInitializeData.endParralelWallList2, m_wallDragInitializeData.endParralelWall1,
			m_wallDragInitializeData.endParralelWall2, this->m_data->endControllerData(),
			m_wallDragInitializeData.endPoint);

		setDoesCreatingMergedPolygonForAllRelatedWall(true);
		bool doesIntersect = createWallJunctionsFromIntersection();
		if (doesIntersect)
		{
			QTimer::singleShot(20, [=] {
				scene->clearControlPointGraphicItems();
				//this->CreatePolygon();
			});
		}
		else
		{
			//this->CreatePolygon();
			this->m_data->UpdateMergedPolygon();
		}

		//this->m_data->UpdateMergedPolygon();
		scene->notifyWallSegmentItemDraggedEnded(this);
		DEBUG_OUT("WallSegmentItem::drag ended");
	}
	else
	{
		if (event->button() == Qt::MouseButton::LeftButton)
		{
			mouseClickEvent(event);
		}
	}
	isDragged = false;
	isDraggedCancelled = false;
	SetPenOfModifiedWalls(this->m_data, BlackPen);
	this->setReferenceItemPen(BluePen);
	QGraphicsItem::mouseReleaseEvent(event);
}

void WallSegmentItem::mouseClickEvent(QGraphicsSceneMouseEvent *event)
{
	bool isCtrlKeyDown = event->modifiers().testFlag(Qt::ControlModifier);
	setSelected((isCtrlKeyDown) ? !m_isSelected : true, isCtrlKeyDown);
}

void WallSegmentItem::setSelected(bool isSlctd, bool isCtrlKeyDown, bool isToRemoveWall)
{
	m_isSelected = isSlctd;

	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());

	scene->selectWall(this, m_isSelected, isCtrlKeyDown, isToRemoveWall);
	this->setPen(((m_isSelected) ? SelectedPen : Qt::NoPen));
	//DEBUG_OUT("********* WallSegmentItem::mouseClickEvent: %s", ((isSelected) ? "TRUE" : "FALSE"));
}

bool WallSegmentItem::selected()
{
	return m_isSelected;
}

QVariant WallSegmentItem::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	FloorplanGraphicsScene * scene = (FloorplanGraphicsScene*)(this->scene());
	//DEBUG_OUT("Calling ControlPointGraphicsItem::itemChange");

	if (change == QGraphicsItem::ItemPositionChange)
	{
		if (isDraggedCancelled)
		{
			//CreateMeasurementPath();
			return pos();
		}
		//QGraphicsItem* item = value.value<QGraphicsItem*>();

		this->removeSideControllerGraphicItems();

		QPointF newPos = value.toPointF();

		// move along wall normal
		bool canMove = m_data->isEligibleForMove();
		if (!canMove)
		{
			return pos();
		}

		if (!isDragged)
		{
			scene->setWallSegmentItemThatIsBeingDragged(this);
			scene->createDopplerWallItemListDueToWallDrag(this->m_data);

			if (m_wallDragInitializeData.startSpecialWallMoveDragCase == SpecialWallMoveDragCase::CaseWith4Links)
			{
				if (m_wallDragInitializeData.selfStartParralelWall != nullptr)
				{
					ControllerData * selfStartCD = this->m_data->startControllerData();
					//ControllerData * newControllerData = 
				}
			}
		}
		isDragged = true;

		QPointF vec = newPos - pos();
		QPointF vecProjectedOnNormal = ProjectBOnA(this->m_data->normal(), vec);
		m_data->move(vecProjectedOnNormal);
		this->m_relativeDragPosition = pos() + vecProjectedOnNormal;

		// did we bypass the margin to translate the wall
		QPointF vecFromDragInit = m_wallDragInitializeData.dragInitPos - pos();
		QPointF vecProjectedFromDragInitOnNormal = ProjectBOnA(this->m_data->normal(), vecFromDragInit);
		qreal dragDisplacement = QPointFLength(vecProjectedFromDragInitOnNormal);
		//DEBUG_OUT("dragDisplacement = %f", dragDisplacement);
		if (dragDisplacement < ControlPointGraphicsItem::Radius)
		{
			SetPenOfModifiedWalls(this->m_data, TransparentPen);
			scene->setPenOfDopplerWallsItems(BlackPen, BlackBrush);
		}
		else
		{
			scene->setPenOfDopplerWallsItems(GhostyPen, GhostyBrush);
			SetPenOfModifiedWalls(this->m_data, BlackPen);
			this->setReferenceItemPen(BluePen);
		}
		CreateMeasurementPath();
		setDoesCreatingMergedPolygonForAllRelatedWall(false);

		return this->m_relativeDragPosition;
	}
	return QGraphicsPathItem::itemChange(change, value);
}

void WallSegmentItem::setDoesCreatingMergedPolygonForAllSideControllerRelatedWall(ControllerData * controllerData, bool isMerged)
{
	vector<ControllerMappingData*> controlledList = controllerData->controlledList();
	for (ControllerMappingData * cmd : controlledList)
	{
		WallData * wallData = cmd->controlledWall;
		wallData->relatedGraphicsItem()->setDoesCreatingMergedPolygon(isMerged);
	}
}

void WallSegmentItem::hoverEnterEvent(QGraphicsSceneHoverEvent*)
{
	FloorplanGraphicsScene *scene = (FloorplanGraphicsScene *)this->scene();
	if (scene->interactionMode() != InteractionMode::View)
	{
		return;
	}

	new ControlPointGraphicsItem(this->m_data->startControllerData(), scene, true);
	new ControlPointGraphicsItem(this->m_data->endControllerData(), scene, false);

	FloorPlanDesigner * floorplanDesigner = qobject_cast<FloorPlanDesigner *>(qApp->activeWindow());
	if (floorplanDesigner == nullptr)
	{
		return;
	}

	FloorplanGraphicsView * view = floorplanDesigner->view();
	if (this->scene() != nullptr)
	{
		qreal sideControlScale = view->calculatedScaleForControlPoints();
		FloorplanGraphicsScene * scene = dynamic_cast<FloorplanGraphicsScene*>(this->scene());
		scene->resizeControlPointGraphicItems(sideControlScale);
	}

	//DEBUG_OUT("information WallSegmentItem::hoverEnterEvent");
	this->setBrush(scene->doesWallsHaveWidth() ? BlueBrush : TransparentBrush);
	this->referencePathItem->setPen(BluePen);
	update(boundingRect());
	showMeasurementLine(true);
}

void WallSegmentItem::hoverLeaveEvent(QGraphicsSceneHoverEvent*)
{
	FloorplanGraphicsScene *scene = (FloorplanGraphicsScene *)this->scene();
	if (scene->interactionMode() != InteractionMode::View)
	{
		return;
	}

	this->removeSideControllerGraphicItems();

	//DEBUG_OUT("WallSegmentItem::hoverLeaveEvent");
	this->setBrush(scene->doesWallsHaveWidth() ? BlackBrush : TransparentBrush);
	this->referencePathItem->setPen(BlackPen);
	update(boundingRect());
	showMeasurementLine(false);
}
#ifndef WALL_ITEM_H
#define WALL_ITEM_H

#include <QtGui>
#include <QGraphicsItem>

#include "AbstractFloorDesignItem.h"

#include "clipper/clipper.hpp"

#include "data/WallData.h"
#include "data/ControllerData.h"

#include "utilities/MergeWallPolygons.h"

class QPointF;

class BezierCurveBasicControlItem;
class ControlPointGraphicsItem;
class FloorplanGraphicsScene;

class WallSegmentItem : public QGraphicsPathItem, public AbstractFloorDesignItem
{
public:
	const static qreal QPathToClipperLibPathScaler;
	const static qreal ClipperLibPathToQPathScaler;
	const static int BezierStepCount;
	const static qreal DefaultFloorWallWidth;
	const static qreal DefaultWallWidth;
	const static qreal DefaultWallHeight;
	const static int MeasurementFontSize;

	const static QBrush BlueBrush;
	const static QBrush BlackBrush;
	const static QBrush TransparentBrush;
	const static QBrush MergeStateBrush;
	const static QBrush GhostyBrush;
	const static QPen BluePen;
	const static QPen BlackPen;
	const static QPen LightBlackPen;
	const static QPen MergeStatePen;
	const static QPen TransparentPen;
	const static QPen GhostyPen;
	const static QPen MergibilityIndicationPenWithWallWidthCase;
	const static QPen SelectedPen;

	WallSegmentItem(FloorplanGraphicsScene * scene, bool isInteractable, WallData * wallData = nullptr);
	WallSegmentItem(FloorplanGraphicsScene * scene);
	~WallSegmentItem();

	void Initialize(FloorplanGraphicsScene * scene, bool isInteractable, WallData * wallData = nullptr);

	void CreatePolygon();
	void showMeasurementLine(bool visibility);

	QList<QAction*> getContextMenuActions() override;
	void setInteractable(bool) override;

	WallData * data();
	void setData(WallData*);

	WallType getWallType();
	void setWallType(WallType);
	qreal width();
	void setWidth(qreal);
	qreal height();
	void setHeight(qreal val);
	QPointF getStartPosition();
	void setStartPosition(QPointF);
	QPointF getEndPosition();
	void setEndPosition(QPointF);
	QPointF getControlPosition();
	void setControlPosition(QPointF);
	void setWallData(QPointF startPoint, QPointF endPoint, QPointF mainControlPoint,
		qreal wallWidth, qreal wallHeight, WallType wallType);

	ClipperLib::Path bezierCurveClipperPath();

	QPointF relativeDragPosition();

	void setReferenceItemPen(QPen pen);

	bool isValid();

	void cancelDrag();

	WallGenericPolygonGenerationData& wallGenericPolygonGenerationData();

	bool doesCreatingMergedPolygon();
	void setDoesCreatingMergedPolygon(bool isMerged);
	void setDoesCreatingMergedPolygonForAllRelatedWall(bool isMerged);

	void setSelected(bool isSelected, bool isCtrlKeyDown, bool isToRemoveWall = false);
	bool selected();

protected:
	QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &data) override;
	void hoverEnterEvent(QGraphicsSceneHoverEvent* event) override;
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* event) override;
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

	virtual void mouseClickEvent(QGraphicsSceneMouseEvent *event);

private:
	const static qreal ControlpointRatio;

	const static qreal ReferencePathWidth;

	bool m_doesCreatingMergedPolygon;

	bool isDragged;
	bool isDraggedCancelled;
	bool m_isSelected;

	WallGenericPolygonGenerationData m_wallGenericPolygonGenerationData;

	//QBrush *texturedBrush;

	const static qreal WallLengthLimit;

	WallData * m_data;

	QPointF m_relativeDragPosition;

	ClipperLib::Path m_bezierCurveClipperPath;

	QGraphicsPathItem * referencePathItem;
	QGraphicsPathItem * measurementPathItem;
	QGraphicsTextItem * measurementTextItem;
	

	WallDragInitializeData m_wallDragInitializeData;

	void CreatePolygon(bool drawReferenceAndOutlines);
	void CreateBasePolygon(QPainterPath &referencePath, QPainterPath &bezierCurvePath);
	void CreateMeasurementPath();

	bool createWallJunctionsFromIntersection();

	void removeSideControllerGraphicItems();

	void setDoesCreatingMergedPolygonForAllSideControllerRelatedWall(ControllerData * controllerData, bool isMerged);

	void DetachFromOneOrTwoLinkedWalls(
		WallData * parralelWall1, WallData * parralelWall2, ControllerData * centralController);

	bool checkAndDoReattachAfterMoveDrag(std::vector<ControllerAndOneWallPair *> &parralelWallList,
		ControllerData * sideController);

	void checkAndDoReattachAfterMoveDragForBothLinkedWallList(std::vector<ControllerAndOneWallPair *> &parralelWallList1,
		std::vector<ControllerAndOneWallPair *> &parralelWallList2, WallData * parralelWall1,
		WallData * parralelWall2, ControllerData * sideController, QPointF initialSideControllerPosition);

	// STATIC PRIVATE METHODS
private:
	static void SetPenOfModifiedWalls(WallData * wallData, QPen pen);
};

#endif // WALL_ITEM_H
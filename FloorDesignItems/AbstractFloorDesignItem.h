#ifndef ABSTRACT_FLOOR_DESIGN_ITEM_H
#define ABSTRACT_FLOOR_DESIGN_ITEM_H

#include <QList>
//class ActionData;

class AbstractFloorDesignItem
{
public:
	AbstractFloorDesignItem();
	~AbstractFloorDesignItem();

	virtual void setInteractable(bool) = 0;
	virtual QList<QAction*> getContextMenuActions() = 0;
};

#endif // ABSTRACT_FLOOR_DESIGN_ITEM_H

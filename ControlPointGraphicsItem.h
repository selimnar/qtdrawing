#ifndef CONTROL_POINT_GRAPHICS_ITEM_H
#define CONTROL_POINT_GRAPHICS_ITEM_H

#include <QtGui>
#include <QGraphicsItem>
#include <QPointF>

//#include "QubicBezierItem.h"
#include "FloorDesignItems/WallSegmentItem.h"

class ControllerData;

class ControlPointGraphicsItem : public QGraphicsEllipseItem
{
public:
	const static QBrush MergeStateBrush;
	const static QBrush StartNormalStateBrush;
	const static QBrush EndNormalStateBrush;
	const static qreal Radius;

	ControlPointGraphicsItem(ControllerData * controllerData, FloorplanGraphicsScene *scene, bool inIsIndicatedAsStart);
	~ControlPointGraphicsItem();

	void setPosition(QPointF newPos, bool isItemChangeToBeCalled);

	ControllerData * controllerData();
	void setControllerData(ControllerData *);

	void cancelDrag();

protected:
	QVariant itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &data) override;
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;

private:
	QPointF position;
	QPointF previousPosition;
	QPointF relativePositionToControlledPath;

	ControllerData * m_controllerData;

	ControllerData * mergableControlPoint;
	WallData * attachableWall;

	bool isDragged;
	bool isDraggedCancelled;

	QPointF m_positionBeforeDragStarted;

	bool isIndicatedAsStart;

	void indicateMergeability();

	bool createWallJunctionsFromIntersection();
	void updateControlledWallNormals();
	QBrush NormalStateBrush();
};

#endif // CONTROL_POINT_GRAPHICS_ITEM_H


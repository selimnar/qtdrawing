#include "MeshPart.h"

#include <Qt3DRender/QMesh>
#include <QBuffer>
#include <QAttribute>
#include "qattribute.h"
#include "Qt3DRender/private/qaxisalignedboundingbox_p.h"


//#include <Qt3DRenderer/private/renderlogging_p.h>
#include <QFile>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QTextStream>
#include <QVector>

#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>

#include <QIODevice>

#include "../data/WallData.h"

#include "../utilities/utilities.h"

#include "../FloorDesignItems/WallSegmentItem.h"

#include "delaunator.hpp"
#include "../MeshGeneration/polygon.h"

typedef Qt3DRender::QBuffer QRenderBuffer;

QT_BEGIN_NAMESPACE

using namespace Qt3DRender;

const int MeshPart::QPathToClipperLibPathScaler = 100;

const int MeshPart::PositionAttributeSize = 3;
const int MeshPart::TextureCoordinateAttributeSize = 2;
const int MeshPart::NormalAttributeSize = 3;
const int MeshPart::TangentAttributeSize = 4;

inline uint qHash(const FaceIndices &faceIndices)
{
	return faceIndices.positionIndex
		+ 10 * faceIndices.texCoordIndex
		+ 100 * faceIndices.normalIndex;
}

static void addFaceVertex(const FaceIndices &faceIndices,
	QVector<FaceIndices> &faceIndexVector,
	QHash<FaceIndices, unsigned int> &faceIndexMap)
{
	if (faceIndices.positionIndex != std::numeric_limits<unsigned int>::max())
	{
		faceIndexVector.append(faceIndices);
		if (!faceIndexMap.contains(faceIndices))
		{
			faceIndexMap.insert(faceIndices, faceIndexMap.size());
		}
	}
	else
	{
		//qCWarning(Render::Io) << "Missing position index";
		DEBUG_OUT("Missing position index %d", 0);
	}
}

void SetAttribute(QGeometry *geometry, QRenderBuffer *buf, bool hasAttribute, QString attributeName,
	quint32 componentDataSize, quint32 &offset, int count, const quint32 stride)
{
	if (hasAttribute)
	{
		QAttribute *normalAttribute = new QAttribute(buf, attributeName, QAttribute::Float, componentDataSize, count, offset, stride);
		geometry->addAttribute(normalAttribute);
		offset += sizeof(float) * componentDataSize;
	}
}

Qt3DRender::QGeometry * MeshPart::geometry() const
{
	QByteArray bufferBytes;
	const int count = m_points.size();
	const quint32 elementSize = 3 + (hasTextureCoordinates() ? 2 : 0)
		+ (hasNormals() ? 3 : 0)
		+ (hasTangents() ? 4 : 0);
	const quint32 stride = elementSize * sizeof(float);
	bufferBytes.resize(stride * count);
	float* fptr = reinterpret_cast<float*>(bufferBytes.data());

	for (int index = 0; index < count; ++index) {
		*fptr++ = m_points.at(index).x();
		*fptr++ = m_points.at(index).y();
		*fptr++ = m_points.at(index).z();

		if (hasTextureCoordinates()) {
			*fptr++ = m_texCoords.at(index).x();
			*fptr++ = m_texCoords.at(index).y();
		}

		if (hasNormals()) {
			*fptr++ = m_normals.at(index).x();
			*fptr++ = m_normals.at(index).y();
			*fptr++ = m_normals.at(index).z();
		}

		if (hasTangents()) {
			*fptr++ = m_tangents.at(index).x();
			*fptr++ = m_tangents.at(index).y();
			*fptr++ = m_tangents.at(index).z();
			*fptr++ = m_tangents.at(index).w();
		}
	} // of buffer filling loop

	QRenderBuffer *buf(new QRenderBuffer(QRenderBuffer::VertexBuffer));
	buf->setData(bufferBytes);

	quint32 componentDataSize = PositionAttributeSize;
	quint32 offset = 0;

	QGeometry *geometry = new QGeometry();
	SetAttribute(geometry, buf, true, QAttribute::defaultPositionAttributeName(), PositionAttributeSize, offset, count, stride);
	SetAttribute(geometry, buf, hasTextureCoordinates(), QAttribute::defaultTextureCoordinateAttributeName(), TextureCoordinateAttributeSize, offset, count, stride);
	SetAttribute(geometry, buf, hasNormals(), QAttribute::defaultNormalAttributeName(), NormalAttributeSize, offset, count, stride);
	SetAttribute(geometry, buf, hasTangents(), QAttribute::defaultTangentAttributeName(), TangentAttributeSize, offset, count, stride);

	QByteArray indexBytes;
	Qt3DRender::QAttribute::VertexBaseType ty;
	if (m_indices.size() < 65536) {
		// we can use USHORT
		ty = QAttribute::UnsignedShort;
		indexBytes.resize(m_indices.size() * sizeof(quint16));
		quint16* usptr = reinterpret_cast<quint16*>(indexBytes.data());
		for (int i = 0; i<m_indices.size(); ++i)
			*usptr++ = static_cast<quint16>(m_indices.at(i));
	}
	else {
		// use UINT - no conversion needed, but let's ensure int is 32-bit!
		ty = QAttribute::UnsignedInt;
		Q_ASSERT(sizeof(int) == sizeof(quint32));
		indexBytes.resize(m_indices.size() * sizeof(quint32));
		memcpy(indexBytes.data(), reinterpret_cast<const char*>(m_indices.data()), indexBytes.size());
	}

	QRenderBuffer *indexBuffer(new QRenderBuffer(QRenderBuffer::IndexBuffer));
	indexBuffer->setData(indexBytes);
	QAttribute *indexAttribute = new QAttribute(indexBuffer, ty, 1, m_indices.size());
	indexAttribute->setAttributeType(QAttribute::IndexAttribute);
	geometry->addAttribute(indexAttribute);

	return geometry;
}


void MeshPart::generateAveragedNormals(const QVector<QVector3D>& points,
	QVector<QVector3D>& normals,
	const QVector<unsigned int>& faces) const
{
	for (int i = 0; i < points.size(); ++i)
		normals.append(QVector3D());

	for (int i = 0; i < faces.size(); i += 3)
	{
		const QVector3D& p1 = points[faces[i]];
		const QVector3D& p2 = points[faces[i + 1]];
		const QVector3D& p3 = points[faces[i + 2]];

		QVector3D a = p2 - p1;
		QVector3D b = p3 - p1;
		QVector3D n = QVector3D::crossProduct(a, b).normalized();

		normals[faces[i]] += n;
		normals[faces[i + 1]] += n;
		normals[faces[i + 2]] += n;
	}

	for (int i = 0; i < normals.size(); ++i)
		normals[i].normalize();
}

void MeshPart::generateTangents(const QVector<QVector3D>& points,
	const QVector<QVector3D>& normals,
	const QVector<unsigned  int>& faces,
	const QVector<QVector2D>& texCoords,
	QVector<QVector4D>& tangents) const
{
	tangents.clear();
	QVector<QVector3D> tan1Accum;
	QVector<QVector3D> tan2Accum;

	for (int i = 0; i < points.size(); i++)
	{
		tan1Accum.append(QVector3D());
		tan2Accum.append(QVector3D());
		tangents.append(QVector4D());
	}

	// Compute the tangent vector
	for (int i = 0; i < faces.size(); i += 3)
	{
		const QVector3D& p1 = points[faces[i]];
		const QVector3D& p2 = points[faces[i + 1]];
		const QVector3D& p3 = points[faces[i + 2]];

		const QVector2D& tc1 = texCoords[faces[i]];
		const QVector2D& tc2 = texCoords[faces[i + 1]];
		const QVector2D& tc3 = texCoords[faces[i + 2]];

		QVector3D q1 = p2 - p1;
		QVector3D q2 = p3 - p1;
		float s1 = tc2.x() - tc1.x(), s2 = tc3.x() - tc1.x();
		float t1 = tc2.y() - tc1.y(), t2 = tc3.y() - tc1.y();
		float r = 1.0f / (s1 * t2 - s2 * t1);
		QVector3D tan1((t2 * q1.x() - t1 * q2.x()) * r,
			(t2 * q1.y() - t1 * q2.y()) * r,
			(t2 * q1.z() - t1 * q2.z()) * r);
		QVector3D tan2((s1 * q2.x() - s2 * q1.x()) * r,
			(s1 * q2.y() - s2 * q1.y()) * r,
			(s1 * q2.z() - s2 * q1.z()) * r);
		tan1Accum[faces[i]] += tan1;
		tan1Accum[faces[i + 1]] += tan1;
		tan1Accum[faces[i + 2]] += tan1;
		tan2Accum[faces[i]] += tan2;
		tan2Accum[faces[i + 1]] += tan2;
		tan2Accum[faces[i + 2]] += tan2;
	}

	for (int i = 0; i < points.size(); ++i)
	{
		const QVector3D& n = normals[i];
		QVector3D& t1 = tan1Accum[i];
		QVector3D& t2 = tan2Accum[i];

		// Gram-Schmidt orthogonalize
		tangents[i] = QVector4D(QVector3D(t1 - QVector3D::dotProduct(n, t1) * n).normalized(), 0.0f);

		// Store handedness in w
		tangents[i].setW((QVector3D::dotProduct(QVector3D::crossProduct(n, t1), t2) < 0.0f) ? -1.0f : 1.0f);
	}
}

void MeshPart::center(QVector<QVector3D>& points, QVector3D &center)
{
	if (points.isEmpty())
		return;

	Qt3DRender::QAxisAlignedBoundingBox bb(points);
	center = bb.center();

	// Translate center of the AABB to the origin
	for (int i = 0; i < points.size(); ++i)
	{
		QVector3D& point = points[i];
		point = point - center;
	}
}

QT_END_NAMESPACE
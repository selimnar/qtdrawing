#ifndef WALL_MESH_PART_H
#define WALL_MESH_PART_H

#include <QImage>

#include <Qt3DRender/QGeometry>

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <limits>

#include "MeshPart.h"

//QT_BEGIN_NAMESPACE

class WallData;

class SideSurfaceMeshPart : public MeshPart
{
public:
	virtual void prepareMesh(std::vector<QPointF> &sideVectors, const QList <QVariant> &arguments);

};

class TopSurfaceMeshPart : public MeshPart
{
public:
	virtual void prepareMesh(std::vector<QPointF> &topVectors, const QList <QVariant> &arguments);

};


//QT_END_NAMESPACE

#endif // WALL_MESH_PART_H
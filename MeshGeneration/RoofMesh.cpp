#include "RoofMesh.h"

#include <Qt3DRender/QMesh>
#include <QBuffer>
#include <QAttribute>
#include "qattribute.h"
#include "Qt3DRender/private/qaxisalignedboundingbox_p.h"


//#include <Qt3DRenderer/private/renderlogging_p.h>
#include <QFile>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QTextStream>
#include <QVector>

#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>

#include <QIODevice>


#include "../utilities/utilities.h"
#include "utilities\TrianglePP.h"

typedef Qt3DRender::QBuffer QRenderBuffer;

QT_BEGIN_NAMESPACE

using namespace Qt3DRender;

void RoofMesh::prepareMesh(std::vector<QPointF> &roofPoints, const QList <QVariant> &arguments)
{
	float wallHeight = arguments[0].toFloat();
	float heightOffset = arguments[1].toFloat();
	bool isUp = arguments[2].toBool();

	int faceCount = 0;

	// Parse faces taking into account each vertex in a face can index different indices
	// for the positions, normals and texture coords;
	// Generate unique vertices (in OpenGL parlance) and output to m_points, m_texCoords,
	// m_normals and calculate mapping from faces to unique indices
	QVector<QVector3D> &vertices = m_points;
	QVector<QVector3D> &normals = m_normals;
	//QVector<QVector2D> &texCoords = m_texCoords;
	QVector<unsigned int> &triangles = m_indices;
	//vertices.clear();
	//normals.clear();
	////texCoords.clear();
	//triangles.clear();

	for (const QPointF &p : roofPoints)
	{
		QVector3D v(p.x(), wallHeight + heightOffset, p.y());
		vertices.append(v);
	}

	std::vector<Triangle> delanuayTriangles = TriangulatePolygon(roofPoints);

	if (isUp)
	{
		for (Triangle triangle : delanuayTriangles)
		{
			triangles.append(triangle.i3());
			triangles.append(triangle.i2());
			triangles.append(triangle.i1());
			//DEBUG_OUT("----------- %d %d %d", triangle.i1(), triangle.i2(), triangle.i3());
		}
	}
	else
	{
		for (Triangle triangle : delanuayTriangles)
		{
			triangles.append(triangle.i1());
			triangles.append(triangle.i2());
			triangles.append(triangle.i3());
			//DEBUG_OUT("----------- %d %d %d", triangle.i1(), triangle.i2(), triangle.i3());
		}
	}

	if (m_normals.isEmpty())
	{
		generateAveragedNormals(vertices, normals, triangles);
	}
}

void BlockSideWallsMesh::prepareMesh(std::vector<QPointF> &roofPoints, const QList <QVariant> &arguments)
{
	float wallHeight = arguments[0].toFloat();
	float heightOffset = arguments[1].toFloat();

	int faceCount = 0;

	// Parse faces taking into account each vertex in a face can index different indices
	// for the positions, normals and texture coords;
	// Generate unique vertices (in OpenGL parlance) and output to m_points, m_texCoords,
	// m_normals and calculate mapping from faces to unique indices
	QVector<QVector3D> &vertices = m_points;
	QVector<QVector3D> &normals = m_normals;
	//QVector<QVector2D> &texCoords = m_texCoords;
	QVector<unsigned int> &triangles = m_indices;
	//vertices.clear();
	//normals.clear();
	////texCoords.clear();
	//triangles.clear();

	roofPoints.push_back(roofPoints[0]);

	size_t pointCount = roofPoints.size();
	for (int i = 1; i < pointCount; i++)
	{
		const QPointF &p1 = roofPoints[i - 1];
		const QPointF &p2 = roofPoints[i];

		QVector3D upperLeft(p1.x(), wallHeight + heightOffset, p1.y());
		QVector3D upperRight(p2.x(), wallHeight + heightOffset, p2.y());
		QVector3D lowerLeft(p1.x(), heightOffset, p1.y());
		QVector3D lowerRight(p2.x(), heightOffset, p2.y());

		vertices.append(upperLeft);
		vertices.append(upperRight);
		vertices.append(lowerLeft);
		vertices.append(lowerRight);

		// also create triangle indices
		int index = (i - 1) * 4;
		triangles.append(index);
		triangles.append(index + 1);
		triangles.append(index + 2);

		triangles.append(index + 1);
		triangles.append(index + 3);
		triangles.append(index + 2);

		///////

		triangles.append(index + 2);
		triangles.append(index + 1);
		triangles.append(index);

		triangles.append(index + 2);
		triangles.append(index + 3);
		triangles.append(index + 1);
	}

	if (m_normals.isEmpty())
	{
		generateAveragedNormals(vertices, normals, triangles);
	}
}

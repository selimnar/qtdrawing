#ifndef WALL_MESH_GENERATOR_H
#define WALL_MESH_GENERATOR_H

#include <QImage>

#include <Qt3DRender/QGeometry>

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <limits>

#include "WallMeshPart.h"

//QT_BEGIN_NAMESPACE

class WallData;

class WallMeshGenerator
{
public:
	WallMeshGenerator();
	~WallMeshGenerator();

	void generateWallData(WallData * wallData);

	MeshPart * firstWallSideMeshPart() { return m_firstWallSideMeshPart; }
	MeshPart * secondWallSideMeshPart() { return m_secondWallSideMeshPart; }
	MeshPart * topWallMeshPart() { return m_topWallMeshPart; }

	void onFail();

private:

	MeshPart * m_firstWallSideMeshPart;
	MeshPart * m_secondWallSideMeshPart;
	MeshPart * m_topWallMeshPart;
};

//QT_END_NAMESPACE

#endif // WALL_MESH_GENERATOR_H
#ifndef ROOF_MESH_H
#define ROOF_MESH_H

#include <QImage>

#include <Qt3DRender/QGeometry>

#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include <limits>

#include "MeshPart.h"

class RoofMesh : public MeshPart
{
public:
	virtual void prepareMesh(std::vector<QPointF> &sideVectors, const QList <QVariant> &arguments);
};

class BlockSideWallsMesh : public MeshPart
{
public:
	virtual void prepareMesh(std::vector<QPointF> &sideVectors, const QList <QVariant> &arguments);
};

#endif // ROOF_MESH_H
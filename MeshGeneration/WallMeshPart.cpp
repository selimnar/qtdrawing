#include "WallMeshPart.h"

#include <Qt3DRender/QMesh>
#include <QBuffer>
#include <QAttribute>
#include "qattribute.h"
#include "Qt3DRender/private/qaxisalignedboundingbox_p.h"


//#include <Qt3DRenderer/private/renderlogging_p.h>
#include <QFile>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QTextStream>
#include <QVector>

#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>

#include <QIODevice>

#include "../data/WallData.h"

#include "../utilities/utilities.h"

#include "../FloorDesignItems/WallSegmentItem.h"

#include "delaunator.hpp"
#include "../MeshGeneration/polygon.h"

typedef Qt3DRender::QBuffer QRenderBuffer;

QT_BEGIN_NAMESPACE

using namespace Qt3DRender;

void SideSurfaceMeshPart::prepareMesh(std::vector<QPointF> &sideVectors, const QList <QVariant> &arguments)
{
	float wallHeight = arguments[0].toFloat();

	int faceVertices = 0;

	// Parse faces taking into account each vertex in a face can index different indices
	// for the positions, normals and texture coords;
	// Generate unique vertices (in OpenGL parlance) and output to m_points, m_texCoords,
	// m_normals and calculate mapping from faces to unique indices
	QVector<QVector3D> &vertices = m_points;
	QVector<QVector3D> &normals = m_normals;
	//QVector<QVector2D> &texCoords = m_texCoords;
	QVector<unsigned int> &triangles = m_indices;
	//vertices.clear();
	//normals.clear();
	////texCoords.clear();
	//triangles.clear();

	QVector3D verticalVec(0, wallHeight, 0);

	QPointF *fp = &(sideVectors[0]);
	QPointF *sp = &(sideVectors[1]);

	QPointF normal = PerpendicularVector(NormalizeQPointF(*sp - *fp));
	QVector3D normal3D(normal.x(), 0.0f, normal.y());
	normals.append(normal3D);
	normals.append(normal3D);
	normals.append(normal3D);
	normals.append(normal3D);

	vertices.append(QVector3D(fp->x(), 0, fp->y()));
	vertices.append(QVector3D(fp->x(), wallHeight, fp->y()));
	vertices.append(QVector3D(sp->x(), 0, sp->y()));
	vertices.append(QVector3D(sp->x(), wallHeight, sp->y()));
	

	triangles.append(faceVertices);
	triangles.append(faceVertices + 1);
	triangles.append(faceVertices + 3);

	triangles.append(faceVertices);
	triangles.append(faceVertices + 3);
	triangles.append(faceVertices + 2);

	faceVertices += 4;



	// if this is a curved wall
	if (sideVectors.size() > 4)
	{
		fp = sp;
		QVector<QPointF> sideNormals;
		auto vIter = sideVectors.begin();
		while (++vIter != sideVectors.end() - 1)
		{
			sp = &(*vIter);

			QVector3D bv(sp->x(), 0, sp->y());
			QVector3D tv = bv + verticalVec;

			QPointF sideNormal = PerpendicularVector(NormalizeQPointF(*sp - *fp));
			sideNormals.append(sideNormal);

			//DEBUG_OUT("***************** (%f, %f)", p.x(), p.y());

			vertices.append(bv);
			vertices.append(tv);

			triangles.append(faceVertices - 2);
			triangles.append(faceVertices - 1);
			triangles.append(faceVertices + 1);

			triangles.append(faceVertices - 2);
			triangles.append(faceVertices + 1);
			triangles.append(faceVertices);

			faceVertices += 2;

			fp = sp;
		}

		// calculate side normals
		auto nIter = sideNormals.begin();
		QPointF fsNormal = *nIter;
		QVector3D fsNormal3D(fsNormal.x(), 0.0f, fsNormal.y());
		normals.append(fsNormal3D * -1);
		normals.append(fsNormal3D * -1);

		while (++nIter != sideNormals.end())
		{
			QPointF ssNormal = *nIter;
			QPointF aNormal = (fsNormal + ssNormal) * 0.5;
			QVector3D aNormal3D(aNormal.x(), 0.0f, aNormal.y());
			normals.append(aNormal3D);
			normals.append(aNormal3D);

			fsNormal = ssNormal;
		}

		//normals.append(fsNormal3D);
		//normals.append(fsNormal3D);
	}
	else
	{
		fp = &(sideVectors[1]);
		sp = &(sideVectors[2]);

		vertices.append(QVector3D(fp->x(), 0, fp->y()));
		vertices.append(QVector3D(fp->x(), wallHeight, fp->y()));
		vertices.append(QVector3D(sp->x(), 0, sp->y()));
		vertices.append(QVector3D(sp->x(), wallHeight, sp->y()));

		triangles.append(faceVertices);
		triangles.append(faceVertices + 1);
		triangles.append(faceVertices + 3);

		triangles.append(faceVertices);
		triangles.append(faceVertices + 3);
		triangles.append(faceVertices + 2);

		faceVertices += 4;

		normal = PerpendicularVector(NormalizeQPointF(*sp - *fp));
		normal3D = QVector3D(normal.x(), 0.0f, normal.y());
		normals.append(normal3D);
		normals.append(normal3D);
		normals.append(normal3D);
		normals.append(normal3D);
	}





	fp = sp;
	sp = &(*(sideVectors.end() - 1));
	//sp = &(sideVectors[sideVectors.size()]);
	
	vertices.append(QVector3D(fp->x(), 0, fp->y()));
	vertices.append(QVector3D(fp->x(), wallHeight, fp->y()));
	vertices.append(QVector3D(sp->x(), 0, sp->y()));
	vertices.append(QVector3D(sp->x(), wallHeight, sp->y()));

	triangles.append(faceVertices);
	triangles.append(faceVertices + 1);
	triangles.append(faceVertices + 3);

	triangles.append(faceVertices);
	triangles.append(faceVertices + 3);
	triangles.append(faceVertices + 2);

	faceVertices += 4;

	// last width side normal
	normal = PerpendicularVector(NormalizeQPointF(*sp - *fp));
	normal3D = QVector3D(normal.x(), 0.0f, normal.y());
	normals.append(normal3D);
	normals.append(normal3D);
	normals.append(normal3D);
	normals.append(normal3D);

	//if (m_normals.isEmpty())
	//{
	//	generateAveragedNormals(vertices, normals, triangles);
	//}
}

void TopSurfaceMeshPart::prepareMesh(std::vector<QPointF> &topVectors, const QList <QVariant> &arguments)
{
	float wallHeight = arguments[0].toFloat();

	int faceCount = 0;

	std::vector<double> coords;
	std::vector<Point2d> polygonICVector;

	// Parse faces taking into account each vertex in a face can index different indices
	// for the positions, normals and texture coords;
	// Generate unique vertices (in OpenGL parlance) and output to m_points, m_texCoords,
	// m_normals and calculate mapping from faces to unique indices
	QVector<QVector3D> &vertices = m_points;
	QVector<QVector3D> &normals = m_normals;
	//QVector<QVector2D> &texCoords = m_texCoords;
	QVector<unsigned int> &triangles = m_indices;
	//vertices.clear();
	//normals.clear();
	////texCoords.clear();
	//triangles.clear();

	auto vIter = topVectors.begin();
	for (QPointF &p : topVectors)
	{
		QVector3D v(p.x(), wallHeight, p.y());
		coords.push_back(p.x());
		coords.push_back(p.y());
		Point2d p2d((int)(p.x() * QPathToClipperLibPathScaler), (int)(p.y() * QPathToClipperLibPathScaler));
		polygonICVector.push_back(p2d);

		//DEBUG_OUT("***************** (%f, %f)", p.x(), p.y());

		vertices.append(v);
	}

	//triangulation happens here
	delaunator::Delaunator d(coords);

	PolygonIC polygonIC(polygonICVector);
	float triAverager = 1.0f / 3.0f;
	for (std::size_t i = 0; i < d.triangles.size(); i += 3)
	{
		size_t uti0 = d.triangles[i];
		size_t uti1 = d.triangles[i + 1];
		size_t uti2 = d.triangles[i + 2];

		QPointF tp1(d.coords[2 * uti0], d.coords[2 * uti0 + 1]);
		QPointF tp2(d.coords[2 * uti1], d.coords[2 * uti1 + 1]);
		QPointF tp3(d.coords[2 * uti2], d.coords[2 * uti2 + 1]);

		QPointF triCenter = (tp1 + tp2 + tp3) * triAverager;

		Point2d tc((int)(triCenter.x() * QPathToClipperLibPathScaler), (int)(triCenter.y() * QPathToClipperLibPathScaler));

		if (!polygonIC.inPolygon(tc))
		{
			continue;
		}

		triangles.append(uti0);
		triangles.append(uti1);
		triangles.append(uti2);
	}

	if (m_normals.isEmpty())
	{
		generateAveragedNormals(vertices, normals, triangles);
	}
}

QT_END_NAMESPACE
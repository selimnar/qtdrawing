#include "WallMeshGenerator.h"

#include <Qt3DRender/QMesh>
#include <QBuffer>
#include <QAttribute>
#include "qattribute.h"
#include "Qt3DRender/private/qaxisalignedboundingbox_p.h"


//#include <Qt3DRenderer/private/renderlogging_p.h>
#include <QFile>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QTextStream>
#include <QVector>

#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QAttribute>
#include <Qt3DRender/QBuffer>

#include <QIODevice>

#include "../data/WallData.h"

#include "../utilities/utilities.h"

#include "../FloorDesignItems/WallSegmentItem.h"

#include "delaunator.hpp"
#include "../MeshGeneration/polygon.h"

typedef Qt3DRender::QBuffer QRenderBuffer;

QT_BEGIN_NAMESPACE

using namespace Qt3DRender;

WallMeshGenerator::WallMeshGenerator()
{
	this->m_firstWallSideMeshPart = new SideSurfaceMeshPart();
	this->m_secondWallSideMeshPart = new SideSurfaceMeshPart();
	this->m_topWallMeshPart = new TopSurfaceMeshPart();
}

WallMeshGenerator::~WallMeshGenerator()
{
	delete this->m_firstWallSideMeshPart;
	delete this->m_secondWallSideMeshPart;
	delete this->m_topWallMeshPart;
}

void WallMeshGenerator::generateWallData(WallData * wallData)
{
	qreal wallHeight = wallData->height();

	WallSegmentItem * wsi = wallData->relatedGraphicsItem();
	WallGenericPolygonGenerationData &wgpgd = wsi->wallGenericPolygonGenerationData();
	std::vector<QPointF> &polygonVectors = wgpgd.polygonVectors;

	QList<QVariant> arguments;
	QVariant varWallHeight(wallHeight);
	arguments.append(varWallHeight);

	m_firstWallSideMeshPart->prepareMesh(wgpgd.firstSideVectors, arguments);
	m_secondWallSideMeshPart->prepareMesh(wgpgd.secondSideVectors, arguments);
	m_topWallMeshPart->prepareMesh(wgpgd.polygonVectors, arguments);
}

void WallMeshGenerator::onFail()
{

}

QT_END_NAMESPACE
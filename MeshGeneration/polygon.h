/**
 * Header only class for handling point in polygon problem.
 * The line-segments intersection algorithm is implemented based on Introduction to Algorithms 3rd (CLRS).
 * Point in polygon is implemented based on https://stackoverflow.com/questions/217578/how-can-i-determine-whether-a-2d-point-is-within-a-polygon.
 *
 * Created by Zebin Xu on 9/27/2017
 */
#ifndef _POLYGON_H_
#define _POLYGON_H_

#include <vector>
#include <limits>
#include <algorithm>

struct Point2d {
    int x;
    int y;

	Point2d(int x, int y);

	Point2d(const Point2d& p);

	Point2d& operator = (const Point2d& p);
};

int direction(Point2d pi, Point2d pj, Point2d pk);

bool onSegment(Point2d pi, Point2d pj, Point2d pk);

bool segmentIntersect(Point2d p1, Point2d p2, Point2d p3, Point2d p4);

struct BoundingBox {
    int xmin;
    int xmax;
    int ymin;
    int ymax;
    
	BoundingBox(int xmin, int xmax, int ymin, int ymax);

	BoundingBox();
};

struct PolygonIC {
    std::vector<Point2d> points;
    BoundingBox bbox;

	PolygonIC(std::vector<Point2d>& points);

	bool inBoundingBox(Point2d point);

	bool inPolygon(Point2d point);
    
private:

	void calcBoundingBox(std::vector<Point2d> points);
};


#endif /* _POLYGON_H_ */
